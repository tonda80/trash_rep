import java.io.File;

import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collection;
import java.util.Iterator;
import java.util.Arrays;
import java.util.Vector;
import java.util.Comparator;
import java.lang.Integer;
import java.util.Timer;
import java.util.TimerTask;



public class Main
{
	private static void f () {U.p("Main.f");}
	
	public static void main (String[] args) throws Exception
	{
		Timer tmr = new Timer();
		tmr.schedule(new TimerTask() {
			@Override
			public void run() {
					U.p("it's timer");
					f();
					destroy();
				}
			private void f () {U.p("TimerTask.f");}
			}, 0, 3000);
		
		U.p("press enter to exit");
		U.input();
	}
		
}

class C
{
	public C() {}
	
	public void method2() {	U.p("C.method2");}
}

class D extends C
{
	public void method() {
		U.p("D.method");
		method2();
	}
}


class U
{
	public static <T> void p(T obj) {
		System.out.println(obj);
	}
	
	private static BufferedReader inBufReader;
	
	public static String input() {
		if (inBufReader == null) {
			inBufReader = new BufferedReader(new InputStreamReader(System.in));
		}
		try {
			return inBufReader.readLine();
		}
		catch (IOException e) {
			return null;
		}
	}
	
	public static <T> void pAll(Collection<T> coll) {pAll(coll, ", ");}
	public static <T> void pAll(Collection<T> coll, String dl) {
		Iterator<T> i = coll.iterator();
		while ( i.hasNext()) {
			System.out.print(i.next());		
			if ( i.hasNext()) {
				System.out.print(dl);				
			}
		}
		System.out.println();
	}

	public static void sleep(int delay) {
		try {
			Thread.sleep(delay);
		}
		catch (InterruptedException e)
		{}
	}
	
	public static void deleteDirectory(File dir) {
		if (dir != null) {
			if (!dir.isDirectory()) {
				U.p("deleteDirectory error: " + dir.getAbsolutePath() + " is not a directory");
				return;
			}

			try {
				for (File f : dir.listFiles()) {
					if (f.isDirectory()) {
						deleteDirectory(f);
					}
					else {
						f.delete();
					}
				}
				dir.delete();
			}
			catch (Exception e) {
				p("Cannot delete a directory " + dir.getName() + ": " + e.getMessage());
			}
		}
	}
	
	public static void deleteFile(File f) {
		if (f != null) {
			try {
				f.delete();
			}
			catch (Exception e) {
				p("Cannot delete a file " + f.getName() + ": " + e.getMessage());
			}
		}
	}
}
