#!/bin/bash

F='/home/ant/temp/1/1.txt'
V=1
GV=`git config --local --get user.programmerAndroid`
echo "input data: [$F $V $GV]"
if [ -f $F -a $V == "1" -a -n $GV ] ; then
    echo 'if'
else
    echo 'else'
fi

if [ -z $GV ] ; then
    echo 'if2'
else
    echo 'else2'
fi
