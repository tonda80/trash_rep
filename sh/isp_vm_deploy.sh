#!/usr/bin/env bash
set -e
#set -x

function copy {
        local DCP='docker cp'
        local DI=dci_builder.clang7.1:/usr/src/project6/back/build
        local DO=vm_box:/opt/ispsystem/vm

        local src=$1
        local dst=$2

        $DCP $DI/$src .
        $DCP `basename $src` $DO/$dst
        echo "copied: $src > $dst"
}

copy bin/vmctl  bin/
copy bin/vm  bin/
copy dist/opt/ispsystem/vm/scripts/kvm/host_snapshot.py  scripts/kvm/

copy etc/permissions.conf.d/admin.json  etc/permissions.conf.d/
copy etc/permissions.conf.d/user.json  etc/permissions.conf.d/

copy etc/vm_api.json  etc/
copy etc/schemas/host_{host_id}_snapshot.json  etc/schemas/
copy etc/schemas/host_{host_id}_snapshot_apply_{snapshot_id}.json  etc/schemas/

docker exec vm_box supervisorctl restart vm_reader vm_writer
