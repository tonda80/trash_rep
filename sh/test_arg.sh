#!/bin/bash

# 1-й аргумент - путь к подготовленным ресурсам (абсолютный или относительный)
if [ "$1" = "" ]
then
	exit 1
fi

if [[ "$1" = /* ]]
then
	WORK_DIR=$1
else
	WORK_DIR=$PWD/$1
fi

echo "WORK_DIR=$WORK_DIR"
