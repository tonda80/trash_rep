#!/usr/bin/env bash
set -e
#set -x

function cp {
        local src_path=$1
        local dst_host=$2
        local dst_image=$3
        local dst_path=$4

        local src_base_name=`basename ${src_path}`
        local dst_buf=buf/

        rsync ${src_path} ${dst_host}:${dst_buf}
        ssh ${dst_host} docker cp ${dst_buf}${src_base_name} ${dst_image}:${dst_path}

        echo -e "copied: ${src_base_name} \t\t`date`"
}

#HOST=root@172.31.48.60
#HOST=root@172.31.49.110
HOST=root@10.99.0.70

function cp_equip_handler {
    local rel_path=$1

    local rep_path=/home/ant/isp_reps/equip_service     #/equip_handler
    local dst_path=/opt/ispsystem                       #/equip_handler
    local image=eservice_handler

    # не надо но может пригодится realpath -m /opt/ispsystem/equip_handler/ipmi_common/handlers/redfish_common.py --relative-to=/opt/ispsystem/equip_handler/ipmi_common

    cp ${rep_path}/${rel_path} ${HOST} ${image} ${dst_path}/${rel_path}
}

# git status -s  =>  M cp_equip_handler

cp_equip_handler equip_handler/ipmi_common/handlers/ipmi_common.py

# supervisorctl restart consumer_handler  &&  tail -F /var/log/consumer_handler.log
