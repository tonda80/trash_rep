#!/bin/bash
set -e

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

systemctl stop mnt-navigation mnt-rout

~/ab/mnt2_test/mnt2_disable.py /opt/mnt2/configurator/conf/navigation.json
~/ab/mnt2_test/mnt2_disable.py /opt/mnt2/configurator/conf/ROUT.json

systemctl --no-pager status mnt-navigation mnt-rout
