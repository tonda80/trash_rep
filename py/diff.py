#coding=utf8

# Некий кастомный diff, может тут и выйдет что то похожее на полноценную утилиту


import itertools
import sys
import signal 

from baseapp import BaseConsoleApp


class AppError(RuntimeError):
	pass


class App(BaseConsoleApp):
	def add_arguments(self, parser):
		parser.add_argument('path1')
		parser.add_argument('path2')

	def main(self):
		signal.signal(signal.SIGPIPE, signal.SIG_DFL)	# не ругаться при закрытии less

		comparator = self.comparator1	# TODO? выбор из параметров
		cnt_line = 0
		with open(self.args.path1, 'rb') as file1, open(self.args.path2, 'rb') as file2:
			for line1, line2 in itertools.izip_longest(file1, file2):
				cnt_line += 1
				if (line1 is None) ^ (line2 is None):
					self.out('{}: file{} is finished'.format(cnt_line, 1 if line1 is None else 2))
					break
				if not comparator(line1, line2):
					self.out('{}:\n>{}>{}'.format(cnt_line, line1, line2))
					
	def out(self, s):
		sys.stdout.write(s)
		sys.stdout.write('\n')
		
	def comparator0(self, l1, l2):
		return l1 == l2

	# обрезаем // комментарий
	def comparator1(self, l1, l2):
		return l1.partition('//')[0] == l2.partition('//')[0]

if __name__ == '__main__':
	try:
		App().main()
	except KeyboardInterrupt:
		pass
