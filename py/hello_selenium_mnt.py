from selenium import webdriver
from selenium.webdriver.common.by import By
#from selenium.webdriver.common.keys import Keys
#from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

import time


class MntUiTest:
	def __init__(self, url, step_delay = 0):
		self.step_delay = step_delay

		#options =  webdriver.FirefoxOptions()
		#options.add_argument('-headless')
		#self.driver = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub', desired_capabilities=DesiredCapabilities.FIREFOX, options=options)

		self.driver = webdriver.Firefox()
		print('created driver')
		self.driver.get(url)
		print('got url')

	def get_element(self, value, by = By.XPATH, timeout = None):
		expire_time = time.time() + timeout if timeout else None
		while 1:
			elems = self.driver.find_elements(by, value)
			len_ = len(elems)
			if len_ == 1:
				return elems[0]
			elif len_ > 1:
				raise RuntimeError(f'[get_element] many elements ({len_}) for "{by}:{xp}"')
			elif expire_time and expire_time < time.time():
				raise RuntimeError(f'[get_element] timeout {timeout} for "{by}:{xp}"')
			time.sleep(0.5)

	def click_on(self, value, by = By.XPATH, timeout = 30):
		self.get_element(value, by=by, timeout=timeout).click()

	def close(self):
		self.driver.close()

	def after_step(self, label):
		print('done', label)
		time.sleep(self.step_delay)

	def test_1(self, delay = 2):
		self.click_on("//div[text()='Устройства аудио']/..//div[text()='Подробнее']")
			# "//div[@class='equipment__item' and text()='Устройства аудио']/../div[@class='equipment__item equipment__item--button']/div[text()='Подробнее']"
		self.after_step('click по Устройства аудио')
		self.click_on("//div[@class='button equipment-details__button ' and text()='Закрыть']")
		self.after_step('click по Закрыть')


mnt_ui_test = MntUiTest("http://192.168.136.114:8080/#primary", step_delay=2)
mnt_ui_test.test_1()
mnt_ui_test.close()
