#!/usr/bin/env python3

import json
import datetime
import sys


def main():
	in_file_name = sys.argv[1]
	with open(in_file_name) as in_file:
		for i, line in enumerate(in_file):
			try:
				obj = json.loads(line)
			except:
				print('[warn] line {} has no json'.format(i+1))
				continue
			check_1(i+1, obj)


def check_1(i, obj):
	try:
		date = obj['date']
		ts = obj['Pack'][2]['TimeStamp']
	except:
		#print('[warn] no date or ts in {}'.format(i+1))
		return
	#print('__deb', date, ts)
	dt1 = datetime.datetime.strptime(date, '%Y/%m/%d %H:%M:%S')
	dt2 = datetime.datetime.fromtimestamp(ts)
	if dt1 - dt2 > datetime.timedelta(seconds=600):
		print('{i} {dt1} {dt2}'.format(i+1))


if __name__ == '__main__':
	main()
