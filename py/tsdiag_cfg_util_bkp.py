	# ./tsdiag_cfg_util.py -c ~/job/builds/tsdiag/par_dict1.json --dec_dir ~/rep-s/mnt2/go/cmd/canDeCoder/nefaz_5299  dbread  --db /home/anton/temp/tsdiag/76/tsdiag.db
	def db_read_main(self):
		def open_desc_constant(s):
			if '$' not in s:
				return s
			for k, v in self.tsdiag_cfg['vars'].items():	# TODO соптимизировать
				k = '$'+k
				if k in s:
					s = s.replace(k, v)
			return s

		curs, db_version = self.db_cursor(self.args.db)
		err_ids = set()
		select_str = 'SELECT time, id, map FROM diag ORDER BY time DESC'		# WHERE time > "2020-02-19 13:00:00"
		unique_ids = set()
		for row in curs.execute(select_str):
			time_, id_, map_ = row
			if id_ not in self.tsdiag_params:
				if not self.args.ignore_err:
					raise RuntimeError(f'{id_} not in params')
				err_ids.add(id_)
				continue
			if id_ in unique_ids:
				pass#continue
			unique_ids.add(id_)

			par = self.tsdiag_params[id_]
			decoder_items = self.decoder_items.get(id_ if 'dm1Name' not in par else par['dm1Name'])
			if decoder_items is None:
				if id_ in self.no_decoder_items:
					print(f'{id_}\t{time_}\t{map_obj["value"]}\t\t{desc}')
					continue
				else:
					raise RuntimeError(f'{id_} not in no_decoder_items')

			decoder_item = decoder_items[0]
			map_obj = json.loads(map_)
			desc = '; '.join((par.get('desc'), par.get('err_code', ''))).format(**map_obj)
			desc = open_desc_constant(desc)

			print(f'{id_}\t{time_}\t{decoder_item[1]}\t{map_obj["value"] if "value" in map_obj else None}\t\t{desc}')

		if err_ids:
			print(f'\nunknown id: {", ".join(err_ids)}', file=sys.stderr)




# ./tsdiag_cfg_util.py -c ~/job/builds/tsdiag/par_dict1.json --dec_dir ~/rep-s/mnt2/go/cmd/canDeCoder/nefaz_5299  invest  --inv_path /home/anton/temp/tsdiag/76
	def invest_main(self):
		def check_id(id_, unique_ids, where):
			was_in = id_ in unique_ids
			if was_in:
				return False
			unique_ids.add(id_)
			if id_  not in self.tsdiag_params:
				print(f'{where} has unknown id {id_}', file=sys.stderr)
				return False
			return not was_in

		def write_id(id_, file_):
			decoder_item = self.decoder_items[id_][0]
			file_.write(f'{id_}\t\t{decoder_item[1]}\t\t{decoder_item[6]}\n')

		base = os.path.basename(self.args.inv_path)
		with open(os.path.join(self.args.inv_path, f'unique_items_{base}.txt'), 'w') as report_f:
			report_f.write('Unique database can items\n-------------------------\n')
			curs, _ = self.db_cursor(os.path.join(self.args.inv_path, 'tsdiag.db'))
			unique_ids = set()
			for row in curs.execute('SELECT id, value FROM diag'):
				id_ = row[0]
				if not check_id(id_, unique_ids, 'db'):
					continue
				write_id(id_, report_f)

			report_f.write('\nUnique ZMQ can items\n-------------------------\n')
			with open(os.path.join(self.args.inv_path, 'tsdiag_syslog')) as syslog_f:
				tmpl = re.compile(r'\[CanItemHandler::process_config_parameter\] will process (\w+) (\w+)')
				unique_ids = set()
				for l in syslog_f:
					mo = tmpl.search(l)
					if mo:
						id_, flags = mo.groups()
						if not check_id(id_, unique_ids, 'syslog'):
							continue
						if 'z' in flags or 'Z' in flags:
							write_id(id_, report_f)


