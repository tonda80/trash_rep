#coding=utf8

# чистим выходной файл dextra, чтоб сравнить diff-ом


import os

from baseapp import BaseConsoleApp


class AppError(RuntimeError):
	pass


class App(BaseConsoleApp):
	def add_arguments(self, parser):
		parser.add_argument('ipathes', nargs='+')
		parser.add_argument('--not_remove', help='Do not remove the old output file')

	def main(self):
		for ipath in self.args.ipathes:
			p, ext = os.path.splitext(ipath)
			opath = p+'_crop'+ext
			if os.path.exists(opath):
				if self.args.not_remove:
					raise AppError('output exists: '+opath)
				raw_input('old output will be removed [Y/ctrl-c]')
				os.remove(opath)
			self.log.info(opath+' will be created')
			with open(ipath, 'rb') as ifile, open(opath, 'wb') as ofile:
				for line in ifile:
					s = line.find('//')
					if s != -1:
						line = line[:s] + '\n'				# обрезаем // комментарий
					s = line.find('/*'); e = line.find('*/')
					if s != -1 and s != -1 and s < e:
						line = line[:s] + line[e+2:] 		# обрезаем /**/ комментарий
					 
					ofile.write(line)
			

if __name__ == '__main__':
	try:
		App().main()
	except KeyboardInterrupt:
		pass
