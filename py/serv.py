import socket

HOST = ''
PORT = 9010

s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.bind((HOST,PORT))
s.listen(1)

f_exit = False
while 1:
	if f_exit:
		break

	conn, addr=s.accept()
	print ('Connect from %s [%s]'%(addr,conn))

	while 1:
		data = conn.recv(1024)
		if not data:
			print( 'Connection was closed')
			break
		if data == 'exit':
			print ('Exiting..')
			f_exit = True
			break
		print (data)
		conn.send(b'answer '+data)

conn.close()
