#!/usr/bin/env python3

import json
import sys
import shutil

cfg_path = sys.argv[1]

shutil.copy(cfg_path, cfg_path+'.bkp')

with open(cfg_path) as f:
	cfg = json.load(f)

cfg['ModuleEnabled'] = False

with open(cfg_path, 'w') as f:
	json.dump(cfg, f, indent=4, sort_keys=True)
