#!/usr/bin/env python3

# gui представление мнт логов


import gzip
import math
import time
import datetime
import json

from baseapp import BaseConsoleApp
import uTkinter as utk
import mix_utils


class G:
	log = None


class Config:
	def __init__(self, path):
		with open(path) as f:
			root = json.load(f)
		self.filters = root['filters']
		self._normalize()

	def _normalize(self):
		report = []
		temp_window = utk.Tk()
		for i,  filter_ in enumerate(self.filters):
			filter_ = self.filters[i]
			for req_str in ('name', 'contains'):	# обязательные строковые поля
				value = filter_.get(req_str, None)
				if value is None:
					report.append(f'{i}-th filter have no field "{req_str}"' )
				elif not isinstance(value, str):
					report.append(f'field "{req_str}" of {i}-th filter is not string')

			filter_['contains'] = filter_['contains'].encode('utf8')

			filter_.setdefault('color', 'gray')
			try:
				utk.Label(temp_window, bg=filter_.get('color'))
			except utk.TclError:
				report.append(f'{i}-th filter has wrong color' )

		temp_window.destroy()
		if report:
			report_str = '\n'.join(report)
			G.log.error(f'There are the following config errors:\n{report_str}\n')
			raise RuntimeError('config error')

	def check_line(self, line):
		for i,  filter_ in enumerate(self.filters):
			if filter_.get('contains') in line:
				return i

	def get_color(self, i):
		return self.filters[i]['color']

	def all_filters(self):
		for i in range(len(self.filters)):
			yield i



class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('deb_log_path')
		self.parser.add_argument('deb_cfg_path')


	def main(self):
		G.log = self.log

		self.config = Config(self.args.deb_cfg_path)

		self.log_reader = LogFileReader(self.args.deb_log_path, self.config.check_line)		# TODO

		root = self.create_gui()

		utk.maximizeWindow(root)
		root.after(250, self.after_opening)

		root.mainloop()

	def delete_callback(self):
		self.log_reader.close()

	def create_gui(self):
		timeline_height = 150	# подстраивать под размер?

		root = utk.uTk(title='Log reader')
		root.set_exit_by_escape()
		root.set_delete_callback(self.delete_callback)

		self.status_bar = root.statusBar

		frame_log_view = utk.uFrame(root, gmArgs = {'side':utk.TOP, 'expand':utk.YES, 'fill':utk.BOTH})
		frame_control = utk.uFrame(root, gmArgs = {'side':utk.BOTTOM, 'expand':utk.NO, 'fill':utk.X})
		frame_timeline = utk.uFrame(root, height=timeline_height, gmArgs = {'side':utk.BOTTOM, 'expand':utk.NO, 'fill':utk.X})

		self.log_view = utk.uListbox(frame_log_view, 'xy', font=('Helvetica', 11), gmArgs = {'side':utk.TOP, 'expand':utk.YES, 'fill':utk.BOTH})

		self.timeline = Timeline(self, frame_timeline, height=timeline_height, gmArgs = {'side':utk.TOP, 'expand':utk.NO, 'fill':utk.X})

		btn_gmArgs = {'side':utk.LEFT}
		utk.uButton(frame_control, self.log_view_up, 'Up', gmArgs = btn_gmArgs)
		utk.uButton(frame_control, self.log_view_down, 'Down', gmArgs = btn_gmArgs)
		utk.uButton(frame_control, self.log_view_first, 'First', gmArgs = btn_gmArgs)
		utk.uButton(frame_control, self.log_view_last, 'Last', gmArgs = btn_gmArgs)
		btn_gmArgs = {'side':utk.RIGHT}
		utk.uButton(frame_control, self.timeline.more_points, '+', gmArgs = btn_gmArgs)
		utk.uButton(frame_control, self.timeline.less_points, '-', gmArgs = btn_gmArgs)
		utk.uButton(frame_control, self.timeline.right, '>', gmArgs = btn_gmArgs)
		utk.uButton(frame_control, self.timeline.left, '<', gmArgs = btn_gmArgs)
		utk.uButton(frame_control, self.timeline.last, '>>', gmArgs = btn_gmArgs)
		utk.uButton(frame_control, self.timeline.first, '<<', gmArgs = btn_gmArgs)

		return root

	def after_opening(self):
		self.listbox_size = self._get_listbox_size()
		assert self.listbox_size > 1

		self.represent_lines(0)

		self.timeline.represent_points(0)

	def _get_listbox_size(self):
		# костылище, но умней не нашлось и не придумалось
		self.log_view.append(''); self.log_view.append('')
		for i in range(50):
			if self.log_view.nearest(i) == 1:
				break
		self.log_view.clear()
		return math.ceil(self.log_view.winfo_height()/(i-1))
		#print('_deb', self.listbox_size, self.log_view.winfo_height())
		#for i in range(self.listbox_size): self.log_view.append(f'{i}')

	def represent_lines(self, pos, from_top=True):
		line_tuples = []
		add_func = line_tuples.append if from_top else lambda o: line_tuples.insert(0, o)
		while 1:
			line_tuple = self.log_reader.get_line(pos)
			if line_tuple is None:
				if not self.log_reader.valid_line_number(pos):
					break
			else:
				add_func((pos, ) + line_tuple)
				if len(line_tuples) >= self.listbox_size:
					break
			pos = pos + 1 if from_top else pos - 1

		if line_tuples:
			self.log_view.clear()
			for pos, line, kind in line_tuples:
				out_s = f"{pos+1}: {line.decode('utf8')}"
				self.log_view.add(utk.END, out_s, bg=self.config.get_color(kind))


	def _log_view_line_pos(self, index):
		line = self.log_view.get(index)
		if not line:
			return -1
		n = line.find(':')
		if n == -1:
			return -1
		return int(line[:n]) - 1

	def log_view_up(self):
		self.represent_lines(self._log_view_line_pos(0)-1, False)

	def log_view_down(self):
		self.represent_lines(self._log_view_line_pos(utk.END)+1)

	def log_view_first(self):
		self.represent_lines(0)

	def log_view_last(self):
		self.listbox_pos = self.log_reader.size() - self.listbox_size
		self.represent_lines(self.log_reader.size()-1, False)

	def timeline_point_click_handler(self, pos):
		self.represent_lines(pos)


class LogFileReader:
	def __init__(self, path, filter_):
		G.log.info(f'opening of {path}')
		t0 = time.time()

		self.file_obj = self._get_file_obj(path)
		self.lines_index = {}
		i = 0
		while 1:
			pos = self.file_obj.tell()
			line = self.file_obj.readline()
			if not line:
				break
			kind = filter_(line)
			if kind is not None:
				self.lines_index[i] = (pos, kind)
			i += 1

		self._file_lines = i
		G.log.info(f'added {self._file_lines} lines in {time.time() - t0:.2f} seconds')

	def close(self):
		G.log.info(f'closing of {self.file_obj.name}')
		self.file_obj.close()

	def _get_file_obj(self, path):
		if mix_utils.check_ext(path, '.gz'):
			fobj = gzip.open(path, 'rb')
			fobj.peek(1)	# test
		else:
			fobj = open(path, 'rb')
		return fobj
		#raise NotImplementedError

	def size(self):		# кол-во строк в исходном файле
		return self._file_lines

	def get_line(self, i):
		try:
			pos, kind  = self.lines_index[i]
		except KeyError:
			return
		self.file_obj.seek(pos, 0)
		return (self.file_obj.readline(), kind)

	def valid_line_number(self, i):
		return 0 <= i < self._file_lines

	def get_line_timestamp(self, i):
		try:
			pos, kind = self.lines_index[i]
		except KeyError:
			return
		self.file_obj.seek(pos, 0)
		return (Timestamp.from_line(self.file_obj.read(Timestamp.str_len)), kind)


class Timestamp:
	str_len = 15
	_format = '%b %d %H:%M:%S'

	@staticmethod
	def from_line(line):
		return datetime.datetime.strptime(line.decode('utf8'), Timestamp._format).timestamp()

	@staticmethod
	def from_line2(line):
		return datetime.datetime.strptime(line[:Timestamp.str_len].decode('utf8'), Timestamp._format).timestamp()

	@staticmethod
	def to_line(ts):
		return datetime.datetime.fromtimestamp(ts).strftime(Timestamp._format)



class Timeline(utk.uCanvas):
	def __init__(self, owner, *args, **kwargs):
		utk.uCanvas.__init__(self, *args, **kwargs)

		self.owner = owner
		self.log_reader = owner.log_reader

		self.interval_options = (60, 600, 3600, 3*3600, 12*3600, 24*3600, 48*3600)
		self.interval_index = 0

		self.points = {}

		self.hint_tag = 'hint'
		self.point_tag = 'point'
		self.line_tag = 'line'
		self.label_tag = 'label'

		self.create_text(0, 0, tags = self.hint_tag, anchor=utk.SW, font='Courier 10 bold')

		self.point_diam = 20

		self.bind('<ButtonPress-1>', self._button1_press_handler)
		self.bind('<Motion>', self._motion_handler)

	def represent_points(self, pos, from_left=True):
		ts_tuples = []
		add_func = ts_tuples.append if from_left else lambda o: ts_tuples.insert(0, o)

		interval = self.interval_options[self.interval_index]
		t_value = interval//60; t_unit = 'min'
		if t_value >= 60:
			t_value = interval//3600; t_unit = 'hour'
		self.owner.status_bar.set_r(f'{t_value} {t_unit}')

		point0 = None
		while 1:
			ts_tuple = self.log_reader.get_line_timestamp(pos)
			if ts_tuple is None:
				if not self.log_reader.valid_line_number(pos):
					break
			else:
				if point0 is None:
					point0 = ts_tuple[0]
				dt = ts_tuple[0] - point0
				if not from_left:
					dt *= -1
				if dt >= interval:
					break
				add_func((pos, ) + ts_tuple)
			pos = pos + 1 if from_left else pos - 1

		if ts_tuples:
			self.delete(self.point_tag)
			self.points.clear()

			coef = self.winfo_width()/interval
			shift = 5

			ts0 = ts_tuples[0][1]
			for pos, ts, kind in ts_tuples:
				x = shift + (ts - ts0)*coef
				self._add_point(x, pos, ts, kind)

			x2t = lambda x: (x - shift)/coef + ts0
			self.draw_extra(x2t)

	def draw_extra(self, x2t):
		self.delete(self.line_tag)
		for i in self.owner.config.all_filters():
			y = self._y_of(i)[1]
			self.create_line((0, y, self.winfo_width(), y), tags = self.line_tag, width = 1)

		self.delete(self.label_tag)
		y = self._y_of(-1)[0]
		q = 5
		for i in range(q):
			x = i*self.winfo_width()/q
			self.create_text(x + 5, y + 5, text = Timestamp.to_line(x2t(x)), tags = self.label_tag, anchor=utk.NW)
			self.create_line((x, 0, x, self.winfo_height()), tags = self.line_tag, width = 1)

	def _y_of(self, i):
		h = self.point_diam + 5
		y = (i + 1)*h
		return y, y+h//2

	def _add_point(self, x, pos, timestamp, kind):
		y = self._y_of(kind)[0]
		color = self.owner.config.get_color(kind)

		item = self.create_oval(x, y, x+self.point_diam, y+self.point_diam, fill=color, outline='white', tags=self.point_tag)
		self.points[item] = (pos, timestamp)
		self.last_added = pos


	def _button1_press_handler(self, event):
		item = self._find_point_item(event)
		if item is not None:
			self.itemconfig(self.point_tag, outline='white')
			self.itemconfig(item, outline='black')
			self.owner.timeline_point_click_handler(self.points[item][0])


	def	_motion_handler(self, event):
		item = self._find_point_item(event)
		if item:
			x, y, _, _ = self.coords(item)
			self.coords(self.hint_tag, x, y)
			self.itemconfig(self.hint_tag, text = Timestamp.to_line(self.points[item][1]), anchor=utk.SW if x < self.winfo_width()/2 else utk.SE)
			self.tag_raise(self.hint_tag)
		else:
			self.itemconfig(self.hint_tag, text = '')

	def _find_point_item(self, event):
		x = self.canvasx(event.x)
		y = self.canvasy(event.y)
		d = 2
		for it in reversed(self.find_overlapping(x, y, x+1, y+1)):
			if self.point_tag in self.gettags(it):
				return it

	def more_points(self):
		if self.interval_index < len(self.interval_options) - 1:
			self.interval_index += 1
			pos = min(pos for pos, timestamp in self.points.values())
			self.represent_points(pos)

	def less_points(self):
		if self.interval_index > 0:
			self.interval_index -= 1
			pos = min(pos for pos, timestamp in self.points.values())
			self.represent_points(pos)

	def right(self):
		pos = max(pos for pos, timestamp in self.points.values()) + 1
		if pos <= self._max_pos():
			self.represent_points(pos)

	def left(self):
		pos = min(pos for pos, timestamp in self.points.values()) - 1
		if pos >= 0:
			self.represent_points(pos, False)

	def last(self):
		self.represent_points(self._max_pos(), False)

	def first(self):
		self.represent_points(0)

	def _max_pos(self):
		return self.owner.log_reader.size() - 1




if __name__ == '__main__':
	App().main()

