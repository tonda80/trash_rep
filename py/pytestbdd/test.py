from pytest_bdd import scenario, given, when, then
import pytest


# https://github.com/pytest-dev/pytest-bdd
# python3 -m pytest test.py  to run


_logf = open('debug.txt', 'a')
def log(s):
	_logf.write(s)
	_logf.write('\n')
	_logf.flush()
	#print(s)		# это пропадает в никуда (


class C:
	def __init__(self):
		self.i = 0
	def do(self):
		self.i += 1
		return self.i

@pytest.fixture
def fixt1():
	return C()


@scenario('test.feature', 'scen1')
def test_scen1():				# должно начинаться с test_ !!
	log('test_scen1\n\n')


@given("giv00")
def giv00():
	log('giv00')


@given("giv1")
def giv1(fixt1):
	log(f'giv1 {fixt1.do()}')


@when("when1")
def when1(fixt1):
	log(f'when1 {fixt1.do()}')

@then("then1")
def then1(fixt1):
	log(f'then1 {fixt1.do()}')
