# coding=utf8

import subprocess
import lsh


# страшная версия
def get_one_process_ugly(name, excluded_pids = ()):
	ret = None
	for p in lsh.find_process(name):
		if p.pid not in excluded_pids:
			if ret is not None:
				return
			ret = p
	return ret

def get_one_process_from_2lines(name, excluded_pids = ()):
	pp = list(p for p in lsh.find_process(name) if p.pid not in excluded_pids)
	return pp[0] if len(pp) == 1 else None


old_bash_pids = set(p.pid for p in lsh.find_process('bash'))

cmd = 'gnome-terminal -- bash'.split()
#subprocess.Popen(cmd).poll()	Popen() оставляет defunct процесс, а call почему то нет, наверное из-за отложенного удаления объекта
subprocess.call(cmd)

new_bash_processes = list(p for p in lsh.find_process('bash') if p.pid not in old_bash_pids)
run_proc = new_bash_processes[0] if len(new_bash_processes) == 1 and new_bash_processes[0].parent().name() == 'gnome-terminal-server' else None
# на всякий случай проверяем родителя и не забыть поругаться если не нашли

print run_proc.pid

raw_input('waiting')

run_proc.terminate()
