#coding=utf8

# https://docs.python.org/2/library/ttk.html#treeview

from Tkinter import *
import ttk

root = Tk()

tree = ttk.Treeview(root)

tree["columns"]=("one","two")
tree.column("one", width=100 )
tree.column("two", width=100)
tree.heading("one", text="coulmn A")
tree.heading("two", text="column B")

#tree.configure(selectmode = 'none')	# none extended browse(default)

tree.tag_configure('special_tag', foreground='red')

tree.insert("" , 0,    text="Line 1", values=("1A","1b"))

id2 = tree.insert("", 1, "dir2", text="Dir 2")
tree.insert(id2, "end", "dir 2", text="sub dir 2", values=("2A","2B"))

##alternatively:
tree.insert("", 3, "dir3", text="Dir 3")
tree.insert("dir3", 3, text=" sub dir 3",values=("3A"," 3B"))

i = 0
id_= 'item%d'%i; tree.insert('', 'end', id_, text=id_); i+=1
id_= 'item%d'%i; tree.insert('', 'end', id_, text=id_); i+=1
id_= 'item%d'%i; tree.insert('', 'end', id_, text=id_, tags=('special_tag')); i+=1
id_= 'item%d'%i; tree.insert('', 'end', id_, text=id_); i+=1
id_= 'item%d'%i; tree.insert('', 'end', id_, text=id_); i+=1
id_= 'item%d'%i; tree.insert('item4', 'end', id_, text=id_, tags=('special_tag'));

def bind_args(event):
	def handler(*aa, **kw):
		print event
		for a in aa:
			print type(a), a #, dir(a)
		for k,v in kw.iteritems():
			print k, type(v), v
	return event, handler

def btn3_handler(ev):
	print '<Button-3>'
	item = tree.identify_row(ev.y)
	tree.selection_add(item)	#selection_set

def tag_handler(ev):
	print 'tag_handler!!!', ev

#tree.bind(*bind_args("<<TreeviewSelect>>"))
tree.bind(*bind_args("<<TreeviewOpen>>"))
tree.bind(*bind_args("<<TreeviewClose>>"))
tree.bind("<Button-3>", btn3_handler)

tree.tag_bind('special_tag', '<<TreeviewSelect>>', tag_handler)

tree.pack()

root.mainloop()
