import json

from uTkinter import *
from mix_utils import Struct



class RouteMap(uCanvas):
	def __init__(self):
		w = uTk()
		self.window = w

		uCanvas.__init__(self, w)

		self.kx = self.route_points = None

		f1 = uFrame(w, relief=FLAT, gmArgs={'side': BOTTOM})
		self.edt_lat_lon = uEntry(f1, '59.985212,30.426162', callback = self.update_vehicle_position, gmArgs={'side': LEFT})
		#self.edt_lon = uEntry(f1, '30.426182', callback = self.update_vehicle_position, gmArgs={'side': LEFT})
		self.update_vehicle_position()

		self.bind('<Configure>', self.redraw)

	def mainloop(self):
		self.window.mainloop()

	def set_route(self, points):	# x0, y0, x1, y1, ....
		assert not len(points)%2
		self.route_points = points
		self.redraw()

	def update_vehicle_position(self, *a):
		try:
			lat, lon = map(float, self.edt_lat_lon.getVar().split(','))
		except:
			return
		self.vehicle_position = lon, lat
		self.redraw()#__draw_vehicle()


	def redraw(self, ev=None):
		self.__draw_route()
		self.__draw_vehicle()

	def set_draw_parameters(self, kx, ky, minx, miny, d):
		self.kx = kx
		self.ky = ky
		self.minx = minx
		self.miny = miny
		self.d = d

	def __draw_x(self, x):
		return (x - self.minx)*self.kx + self.d

	def __draw_y(self, y):
		return self.winfo_height() - (y - self.miny)*self.ky + self.d

	def __draw_route(self):
		self.delete('route')

		if not self.route_points:
			return

		maxx = maxy = float('-inf')
		minx = miny = float('inf')
		for i, v in enumerate(self.route_points):
			if i%2 == 0:
				if v < minx: minx = v
				if v > maxx: maxx = v
			else:
				if v < miny: miny = v
				if v > maxy: maxy = v
		d = 4
		kx = (self.winfo_width() - 2*d)/(maxx - minx)
		ky = (self.winfo_height() - 2*d)/(maxy - miny)
		kx = ky = min(kx, ky)
		#print('__deb1', minx, maxx, miny, maxy, kx, ky)
		self.set_draw_parameters(kx, ky, minx, miny, d)

		pp = self.route_points
		for i in range(0, len(pp)-2, 2):
			x1 = self.__draw_x(pp[i])
			y1 = self.__draw_y(pp[i+1])
			x2 = self.__draw_x(pp[i+2])
			y2 = self.__draw_y(pp[i+3])
			self.create_line(x1, y1, x2, y2, width=2, fill='blue', tag='route')
			self.__draw_segment_begin(x1, y1, int(i/2))
			#if 6 <= i/2 < 9:
			#	self.__draw_segment_closest(Struct(x=pp[i], y=pp[i+1]), Struct(x=pp[i+2], y=pp[i+3]))

	def __draw_segment_begin(self, x, y, num):
		size = 10
		self.draw_oval(x, y, size, outline='blue', fill='blue', tag='route')

		self.create_text(x, y - 20, text=num, tag='route', font=("Symbol", 7))

	def __draw_segment_closest(self, s0, s1):
		closest = segment_closest_point(s0, s1, Struct(x=self.vehicle_position[0], y=self.vehicle_position[1]))
		size = 7; color = 'black'
		x = self.__draw_x(closest.x)
		y = self.__draw_y(closest.y)
		print(closest.x, closest.y, s0.x, s0.y, s1.x, s1.y)
		self.draw_oval(x, y, size, outline=color, fill=color, tag='route')

	def __draw_vehicle(self):
		self.delete('vehicle')
		if not self.vehicle_position or not self.kx:
			return
		size = 15
		x = self.__draw_x(self.vehicle_position[0])
		y = self.__draw_y(self.vehicle_position[1])
		self.draw_oval(x, y, size, outline='red', fill='red', tag='vehicle')


def get_points_from_route(path):
	geometry = json.load(open(path))['geometry']
	assert not len(geometry)%2
	#for i in range(0, len(geometry), 2): geometry[i], geometry[i+1] = geometry[i+1], geometry[i]
	return geometry


def segment_closest_point(s0, s1, p):
	sd2 = (s0.x - s1.x)**2 + (s0.y - s1.y)**2
	assert sd2 > 0
	u = ((p.x - s0.x)*(s1.x - s0.x) + (p.y - s0.y)*(s1.y - s0.y))/sd2
	if u < 0: u = 0
	elif u > 1: u = 1

	x = s0.x + u*(s1.x - s0.x)
	y = s0.y + u*(s1.y - s0.y)

	return Struct(x=x, y=y)

# -----------------

rt_map = RouteMap()

pp = get_points_from_route('/home/anton/job/rout/400t/routes/0400t_forward.json')[:400]
#pp = [0, 0, 100, 100, 200, 0, 300, 400]
rt_map.set_route(pp)

rt_map.mainloop()

