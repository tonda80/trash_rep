#!/usr/bin/env python3

# собираем в кучу кан конфиги из папки

import sys
import os
import json

from baseapp import BaseConsoleApp


class App(BaseConsoleApp):
	def add_arguments(self, parser):
		parser.add_argument('-i', required=True, help='Входная директория')
		parser.add_argument('-f', action='store_true', help='Добавить имя файла в вывод')
		#parser.add_argument('-o', help='Выходная директория')

	def main(self):
		lines = []
		inp_dir = self.args.i
		for f in os.listdir(inp_dir):
			with open(os.path.join(inp_dir, f)) as fo:
				if self.args.f:
					lines.extend((f'{f}:{n+1}\t{l}' for n,l in enumerate(fo)))
				else:
					lines.extend(fo)
		self.log.debug(f'readed {len(lines)}')

		# prettiness
		lines = list(filter(lambda l: l.strip(), lines))
		lines.sort()

		self.log.info(f'total {len(lines)}')

		for l in lines:
			sys.stdout.write(l)


# это для выполнения в ipython
# g = {'__name__': 'ipython_test'}; exec(open('/home/anton/_my/trash_rep/py/can_config.py').read(), g)	# g['s2json'](....)
def s2json(start, end,  dm1_name):
	out_arr = []
	cnt = 0
	spns = set(); fmis = set()
	with open('/home/anton/job/job_notes/tsdiag/spns.txt') as _file:
		for i, line in enumerate(_file):
			line = line.strip()
			if i+1 < start or not line or line.startswith('.'):
				continue
			if i+1 > end:
				break
			# -
			words = line.split('\t')

			#if words[1] != '0x18FECACC': continue
			if int(words[2]) in spns: continue
			spns.add(int(words[2]))
			#fmis.add(words[4])

			print(i+1, line)
			out_arr.append({})
			out_arr[-1]['id'] = f'{dm1_name}|{cnt}'					#f'{dm1_name}|{words[0]}'
			spn = int(words[2]); out_arr[-1]['spn'] = spn; assert len(f'spn:b') < 20
			out_arr[-1]['desc'] = f'{words[3]} ({{spn}}): $fmi_desc{{fmi}} ({{fmi}})'							# + ', блок {canid}'
			#out_arr[-1]['fmi'] = int(words[4])
			#out_arr[-1]['fmi_desc'] = words[5]
			out_arr[-1]['advice'] = '$advice1'	#words[6]
			out_arr[-1]['groupId'] = 999
			out_arr[-1]['flags'] = ''
			out_arr[-1]['dm1Name'] = dm1_name
			cnt += 1

	l = list(fmis); l.sort()
	print(f'total {cnt}')
	with open('/home/anton/job/job_notes/tsdiag/out.txt', 'w') as _file:
		json.dump(out_arr, _file, ensure_ascii=False, indent='\t')

# и специальный кейс для 24 шин!
def s2j_tires(start, end):
	dm1_name = 'TPDM'
	out_arr = []
	cnt = 0
	spns = set(); fmis = set()
	with open('/home/anton/job/job_notes/tsdiag/spns.txt') as _file:
		for i, line in enumerate(_file):
			line = line.strip()
			if i+1 < start or not line or line.startswith('.'):
				continue
			if i+1 > end:
				break
			words = line.split('\t')
			print(i+1, line)

			spn = int(words[0])
			assert len(f'spn:b') < 20

			for i in range(24):
				e = {}
				out_arr.append(e)
				e['id'] = f'{dm1_name}|{1000 + cnt*100 + i+1}'		# смещение, так как есть и другие параметры
				e['spn'] = spn + i
				e['fmi'] = int(words[2])
				e['groupId'] = 5
				e['flags'] = ''
				e['dm1Name'] = dm1_name
				e['desc'] = f'{words[1]}'.format(N=i+1) + ': ({spn}): $fmi_desc{fmi} ({fmi})'

			cnt += 1

	print(f'total {cnt}')
	with open('/home/anton/job/job_notes/tsdiag/out.txt', 'w') as _file:
		json.dump(out_arr, _file, ensure_ascii=False, indent='\t')

# и ещё один специальный кейс для шин!
def s2j_tires2(start, end):
	dm1_name = 'TPDM'
	out_arr = []
	cnt = 0
	spns = set(); fmis = set()
	with open('/home/anton/job/job_notes/tsdiag/spns.txt') as _file:
		for i, line in enumerate(_file):
			line = line.strip()
			if i+1 < start or not line or line.startswith('.'):
				continue
			if i+1 > end:
				break
			words = line.split('\t')
			print(i+1, line)

			spn = int(words[0])
			assert len(f'spn:b') < 20

			e = {}
			out_arr.append(e)
			e['id'] = f'{dm1_name}|{100 + cnt}'		# смещение, так как есть и другие параметры
			e['dm1Name'] = dm1_name
			e['spn'] = spn
			e['fmi'] = int(words[2])
			e['desc'] = words[1] + ' ({spn}): $fmi_desc{fmi} ({fmi})'
			e['groupId'] = 5
			e['flags'] = 't'

			cnt += 1

	print(f'total {cnt}')
	with open('/home/anton/job/job_notes/tsdiag/out.txt', 'w') as _file:
		json.dump(out_arr, _file, ensure_ascii=False, indent='\t')


if __name__ == '__main__':
	sys.exit(App().main())
