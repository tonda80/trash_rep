#!/usr/bin/env python3

# Тренируемся делать http сервис-дискавери для прометея https://prometheus.io/docs/prometheus/latest/http_sd/

import flask						# pip3 install Flask
from flask import request, make_response
from random import randint


app = flask.Flask(__name__)


def metric(type_, name, labels_value_str_list, help_=''):
	'simplest mock'
	assert  type_ in ('counter', 'gauge', 'summary')

	help_ = help_ or 'useful help'
	ret = f'# HELP {name} {help_}\n# TYPE {name} {type_}\n'

	if isinstance(labels_value_str_list, str):
		labels_value_str_list = [labels_value_str_list]
	for e in labels_value_str_list:
		ret += f'{name}{e} \n'

	return ret


# {__name__=~"my.*"}	, job="my_"

def metric_set0():
	ri = [randint(0, 10) for i in range(4)]
	return \
		metric('gauge', 'my_hot_metric', f' {ri[3]}') + \
		metric('gauge', 'my_cool_metric_77', (f'{{color="red"}} {ri[0]}', f'{{color="green"}} {ri[1]}', f'{{color="blue"}} {ri[2]}'))


def metric_set1():
	ri = [randint(0, 3) for i in range(4)]
	return \
		metric('gauge', 'my_mtr_1', f' {ri[3]}') + \
		metric('gauge', 'my_mtr_2', (f'{{color="red"}} {ri[0]}', f'{{color="green"}} {ri[1]}', f'{{color="blue"}} {ri[2]}'))

def metric_set2():
	ri = [randint(4, 8) for i in range(4)]
	return \
		metric('gauge', 'my_delicate_tender_beast', f' {ri[3]}') + \
		metric('gauge', 'my_heart_stopped', (f'{{color="red"}} {ri[0]}', f'{{color="green"}} {ri[1]}', f'{{color="blue"}} {ri[2]}'))

def metric_responce(set_func, label):
	print(label)
	response = make_response(set_func(), 200)
	response.mimetype = "text/plain"
	return response


@app.route('/metrics')
def metrics(*a, **kw):
	return metric_responce(metric_set0, 'metrics request')


@app.route('/sd')
def discovery(*a, **kw):
	print('discovery request')
	# ещё есть вариант __metrics_path__ настраивать через relabel_configs

	return [
		{
			"targets": ["localhost:8010"],
			"labels": {"__metrics_path__": "/serv1"}
		},
		{
			"targets": ["localhost:8010"],
			"labels": {"__metrics_path__": "/serv2"}
		}
]


@app.route('/serv1')
def serv1(*a, **kw):
	return metric_responce(metric_set1, 'serv1 request')


@app.route('/serv2')
def serv2(*a, **kw):
	return metric_responce(metric_set2, 'serv2 request')


if __name__ == '__main__':
	app.run(host='0.0.0.0', port=8010, debug=True)
	# debug позволяет не перезапускать сервер для подхватывания изменений!
	# 0.0.0.0 для слушания запросов извне
