import asyncio
import concurrent.futures
import time
import functools

def blocking_io(ret, delay=1):
    # File operations (such as logging) can block the
    # event loop: run them in a thread pool.
    #with open('/dev/urandom', 'rb') as f: return f.read(100)
    time.sleep(delay)
    return ret

def cpu_bound():
    # CPU-bound operations will block the event loop:
    # in general it is preferable to run them in a
    # process pool.
    return sum(i * i for i in range(10 ** 7))

async def main():
    loop = asyncio.get_running_loop()

    ## Options:

    # 1. Run in the default loop's executor:
    res1 = await loop.run_in_executor(None, functools.partial(blocking_io, 1))
    print('default thread pool1', res1)

    res2 = await loop.run_in_executor(None, functools.partial(blocking_io, 2))
    print('default thread pool2', res2)

    res = await asyncio.gather(
		loop.run_in_executor(None, functools.partial(blocking_io, 3)),
		loop.run_in_executor(None, functools.partial(blocking_io, 4)),
		loop.run_in_executor(None, functools.partial(blocking_io, 5)),
	)
    print(res)

'''
    # 2. Run in a custom thread pool:
    with concurrent.futures.ThreadPoolExecutor() as pool:
        result = await loop.run_in_executor(
            pool, blocking_io)
        print('custom thread pool', result)

    # 3. Run in a custom process pool:
    with concurrent.futures.ProcessPoolExecutor() as pool:
        result = await loop.run_in_executor(
            pool, cpu_bound)
        print('custom process pool', result)
'''

asyncio.run(main())
