#!/usr/bin/env python3

# простой тестовый скрипт для сборки b2b артефактов на виндовой машине
# W_HOST= W_USER=   ~/trash_rep/py/art_build.py


import os

import ussh
from baseapp import BaseConsoleApp



class App(BaseConsoleApp):
	def add_arguments(self):
		self.parser.add_argument('-s', '--sent_files', nargs = '*', help='Files needed to send before building')
		self.parser.add_argument('-t', '--build_target', default = '5_art', help='Build target: 4_art, 5_art or both')


	def app_init(self):

		class conn_params:
			host =  os.environ['W_HOST']
			port = 22
			user =  os.environ['W_USER']
			pwd = None

		self.ssh = ussh.SshClient(conn_params, timeout=40, log=self.log)


	def main(self):
		#self.ssh.exec_cmd('cd art_builder && git checkout develop', exc_err=0)
		#print(self.ssh.exec_cmd('cd art_builder && git pull && git submodule update --init --recursive && git status', out=1))
		print(self.ssh.exec_cmd('cd art_builder && git branch && git status', out=1))
		# TODO тут че то все плохо пока руками делаем
		input('continue?')

		if self.args.sent_files:
			for f in self.args.sent_files:
				self.send(f)


		target = '4_art 5_art'
		#docker_build_cmd = fr'docker container run --name ab_abab --rm -v "$(pwd)\art_builder:C:\art_builder"  buildtools:latest  ' \
		#  fr'"powershell.exe -C cmake -S .\art_builder\ -B .\art_builder\build ; cmake --build  .\art_builder\build\ --config Release  --target {target} -- -maxCpuCount "'
		#print(docker_build_cmd)
		# строка выше не работает, cmake попадает в аргументы докера, не знаю как с этим бороться, но похоже она не нужна
		# TODO "cmake -S .\art_builder\ -B .\art_builder\build" если нет build папки
		docker_build_cmd = r'docker container run --name ab_abab --rm -v "${pwd}\art_builder:C:\art_builder"  buildtools:latest  ' \
		  fr' cmake --build  .\art_builder\build\ --config Release  --target {self.args.build_target} -- -maxCpuCount'

		out = self.ssh.exec_cmd(docker_build_cmd, out=1)
		if 'error' in out:
			raise RuntimeError(f'build error:\n{out}')


		if '4_art' in self.args.build_target:
			self.ssh.get(r'art_builder\build\4_art\Release\4_art.exe', '4_art.exe')
		if '5_art' in self.args.build_target:
			self.ssh.get(r'art_builder\build\5_art\Release\5_art.exe', '5_art.exe')


	def send(self, file_):
		# все на скорую руку
		src_root = '/home/ant/b2b_reps/art_builder'
		dst_root = 'art_builder/'

		if os.path.isabs(file_):
			rel_path =  os.path.relpath(file_, src_root)
			src_path = file_
		else:
			rel_path =  file_
			src_path = os.path.join(src_root, file_)

		assert os.path.isfile(src_path)

		dst_path = dst_root + rel_path

		self.ssh.put(src_path, dst_path)


if __name__ == '__main__':
	App().start()
