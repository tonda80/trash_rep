import pickle


def get_debug_dummy_list(self, fname):
        # дебажная заглушка, вместо долгого запроса к асана
        if self.args.log_only and os.path.isfile(fname):
            return pickle.load(open(fname, 'rb'))

    
generator = self.get_debug_dummy_list('asana_users.pickle') or \
            self.asana_client.users.find_by_workspace(C.asana_workspace, fields=('email', 'name'))
    
generator = self.get_debug_dummy_list('asana_projects.pickle') or \
            self.asana_client.projects.find_by_workspace(C.asana_workspace, fields=('name',))
