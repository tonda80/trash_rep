# для запуска в ipython через
# %run ~/_my/job/b2b/anvil_debug.py

import os
import threading
import time


os.chdir('/home/ant/b2b_reps/anvil_art/scripts/anvil_tests')

import common
import amqp
import config
import anvil.accounts.account_service_pb2


log = common.log

def new_rpc_client(n):
	ret = []
	for i in range(n):
		ret.append(amqp.RpcClient(config.amqp.connection, config.amqp.exchange, config.amqp.req_queue, log))
	return ret



def get_margin_level(login):
	if login is None:
		body = b''
	else:
		account = anvil.accounts.account_service_pb2.Account()
		account.login = login
		body = account.SerializeToString()

	reply = rpc_client.sync_request('AccountService.GetMarginLevel', body)
	return reply.pb_obj(anvil.accounts.account_service_pb2.MarginLevel)
