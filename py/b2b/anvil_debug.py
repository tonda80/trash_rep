# для запуска в ipython через
# %run ~/_my/job/b2b/anvil_debug.py

import os
import threading
import time


os.chdir('/home/ant/b2b_reps/anvil_art/scripts/anvil_tests')

import common
import amqp
import config
import anvil.prices.chart_service_pb2


log = common.log

def new_rpc_client(n):
	ret = []
	for i in range(n):
		ret.append(amqp.RpcClient(config.amqp.connection, config.amqp.exchange, config.amqp.req_queue, log))
	return ret


def exec_ChartServiceReadLast(rc, symbol = 'EURUSD!'):
	rep = rc.sync_request('ChartService.ReadLast', common.pb_str(anvil.prices.chart_service_pb2.Bar, symbol=symbol))
	if rep.is_success():
		print('OK')
	else:
		print('ERROR')

def exec_ChartServiceReadLast2(rcs, symbol = 'EURUSD!'):
	for rc in rcs:
		threading.Thread(target=exec_ChartServiceReadLast, args=(rc, symbol)).start()


rc = new_rpc_client(7)
rc.insert(0, common.rpc_client)

print('-- go --')
exec_ChartServiceReadLast(rc[0])
time.sleep(1.5)
exec_ChartServiceReadLast(rc[0])
time.sleep(1.5)
exec_ChartServiceReadLast2(rc)
time.sleep(1.5)
print('-- fatality! --')
exec_ChartServiceReadLast(rc[0])
