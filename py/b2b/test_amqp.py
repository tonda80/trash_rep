# AMQP RPC client


import uuid

import pika		# pip3 install pika

import os
import itertools
import threading
import time
import queue
import sys


sys.path.append('/home/ant/b2b_reps/anvil_art/scripts/anvil_tests')

import common
import amqp
import config
import anvil.accounts.account_service_pb2

log = common.log




class RpcClient2:
	def __init__(self, conn_par_dict, exchange, req_queue, logger):
		self.exchange = exchange
		self.req_queue = req_queue
		self.log = logger
		self.correlation_id = self.reply = None

		self.connection = pika.BlockingConnection(pika.ConnectionParameters(**conn_par_dict))
		self.channel = self.connection.channel()

		self.channel.queue_declare(queue=req_queue)

		res = self.channel.queue_declare(queue='', exclusive=True)
		self.rep_queue = res.method.queue

		self.channel.basic_consume(queue=self.rep_queue,
					on_message_callback=self.on_reply,
					auto_ack=False
		)

	# не по науке, но не так критично и тут удобно
	def __del__(self):
		self.connection.close()


	def on_reply(self, ch, method, props, body):
		#print('[RpcClient.on_reply]', ch, method, props, body)
		if props.correlation_id == self.correlation_id:
			self.reply = amqp.RpcReply(props, body)
			#ch.basic_ack(delivery_tag = method.delivery_tag)
		else:
			self.log.warning(f'unexpected correlation id {props.correlation_id}, expected id {self.correlation_id}')


	def sync_request(self, endpoint, body, timeout = 1):
		self.correlation_id = str(uuid.uuid4())

		headers = {'endpoint': endpoint}		# request_id
		self.channel.basic_publish(exchange=self.exchange, routing_key=self.req_queue,
				properties=pika.BasicProperties(reply_to=self.rep_queue, correlation_id=self.correlation_id, headers=headers),
				body=body)
				# TODO? except pika.exceptions.StreamLostError, channel state

		# TODO? use consuming thread
		self.reply = None
		dt = 0.2
		while self.reply is None:
			self.connection.process_data_events(dt)		# 0 - ASAP, None - till events

			if self.reply is None:
				timeout -= dt		# c None сюда не попадем
				if timeout <= 0:
					self.log.warning(f'[RpcClient.sync_request] timeout {self.reply}')
					break

		return self.reply


def create_clients(n):
	ret = []
	for i in range(n):
		ret.append(RpcClient2(config.amqp.connection, config.amqp.exchange, config.amqp.req_queue, log))
	return ret


def get_margin_level(rpc_client, login):
	if isinstance(login, bytes):
		body = login
	else:
		account = anvil.accounts.account_service_pb2.Account()
		account.login = login
		body = account.SerializeToString()

	reply = rpc_client.sync_request('AccountService.GetMarginLevel', body)
	if reply is None:
		print('NONE\n')
	elif reply.is_success():
		print(reply.pb_obj(anvil.accounts.account_service_pb2.MarginLevel))
	else:
		print('NOT SUCCESS\n')



class MarginReqThread(threading.Thread):
	queue = queue.Queue()

	def __init__(self, *args):
		threading.Thread.__init__(self)
		self.args = args

	def run(self):
		try:
			get_margin_level(*self.args)
		except Exception:
			exc = sys.exc_info()
			print(exc)
			queue.put(exc)

	@classmethod
	def check_queue(cls):
		try:
			return cls.queue.get(block=False)
		except queue.Empty:
			return None



logins = (667000999, 21000, b'None', b'', b'zzzzzzz', 0, 1, 3)

def margin_level_pack(clients, logins = logins):
	next_login = itertools.cycle(logins).__next__
	thrs = []
	for cl in clients:
		thrs.append(MarginReqThread(cl, next_login()))
		thrs[-1].start()
	for t in thrs:
		t.join()


rc = create_clients(2)
rc0 = rc[0]

def margin_stress():
	while 1:
		rc = create_clients(10)
		while 1:
			margin_level_pack(rc)

			restart = False
			while MarginReqThread.check_queue():
				print('EXCEPTION')
				restart = True
			if restart:
				break

			#time.sleep(0.1)


def margin_work():
	while 1:
		get_margin_level(rc0, 21000)
		time.sleep(0.5)


if __name__ == '__main__':
	margin_work()
