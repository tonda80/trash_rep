from selenium import webdriver
from selenium.webdriver.common.keys import Keys

import time


# нужно установить драйвер, например geckodriver (на линукс просто качаем бинарь и кладем в директорию из PATH, или можно указать firefox_binary в конструкторе драйвера)


driver = webdriver.Firefox()
print('driver is created')

driver.get("http://www.python.org")
print('page is loaded')

assert "Python" in driver.title
elem = driver.find_element_by_name("q")
elem.send_keys("pycon")
elem.send_keys(Keys.RETURN)
assert "No results found." not in driver.page_source

print('done.. waiting 15 sec')		#   \n{driver.page_source}
time.sleep(15)

driver.close()
