#!/usr/bin/env python3
# coding=utf8


import time
import os
import sys

sys.path.append(os.path.expanduser('~/ab/py_env'))
from zmq_client_mnt2 import *


def rec_handler(msgs):
	print(msgs)


G.log = logging.getLogger()
logging.basicConfig(level=logging.DEBUG)
G.stop = threading.Event()

zmqClient = ZmqMntClient(b'zmqTester', rec_handler, ('tcp://127.0.0.1:10125', 'tcp://127.0.0.1:10126'), sniffer_mode=2)

zmqClient.join()
