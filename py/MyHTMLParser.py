class MyHTMLParser(HTMLParser.HTMLParser):
    def handle_starttag(self, tag, attrs):
        print "Encountered a start tag:", tag, attrs

    def handle_endtag(self, tag):
        print "Encountered an end tag :", tag
	#if tag == 'p':
	#    raise StopIteration

    def handle_data(self, data):
        print "Encountered some data  :", data
	
    def feed(self, html):
	try:
	    HTMLParser.HTMLParser.feed(self, html)
	except StopIteration:
	    pass

MyHTMLParser().feed("<p>original: <a href=\"/display/development/Test+page\">Test page</a></p><p>\u0412\u0440\u0435\u043c\u0435\u043d\u043d\u0430\u044f \u0442\u0435\u0441\u0442\u043e\u0432\u0430\u044f \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0430 \u0434\u043b\u044f <a class=\"external-link\" href=\"https://app.asana.com/0/171813664233617/854756476071356/f\" rel=\"nofollow\">https://app.asana.com/0/171813664233617/854756476071356/f</a></p><p>\u0422\u0443\u0442 \u043a\u0430\u043a\u043e\u0439-\u0442\u043e \u043e\u0447\u0435\u043d\u044c \u043f\u043e\u043b\u0435\u0437\u043d\u044b\u0439 \u0442\u0435\u043a\u0441\u0442</p><p><br/></p><p><br/></p>")
#MyHTMLParser().feed("<p><a href=\"https://knowledge.playrix.com/pages/viewpage.action?spaceKey=development&amp;title=Test+page\" rel=\"nofollow\">_original_page</a></p><p>\u0412\u0440\u0435\u043c\u0435\u043d\u043d\u0430\u044f \u0442\u0435\u0441\u0442\u043e\u0432\u0430\u044f \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0430 \u0434\u043b\u044f <a class=\"external-link\" href=\"https://app.asana.com/0/171813664233617/854756476071356/f\" rel=\"nofollow\">https://app.asana.com/0/171813664233617/854756476071356/f</a></p><p>\u0422\u0443\u0442 \u043a\u0430\u043a\u043e\u0439-\u0442\u043e \u043e\u0447\u0435\u043d\u044c \u043f\u043e\u043b\u0435\u0437\u043d\u044b\u0439 \u0442\u0435\u043a\u0441\u0442</p><p><br/></p><p><br/></p>")
