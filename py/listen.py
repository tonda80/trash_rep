import socket

def listen(port):
	with socket.socket(socket.AF_INET,socket.SOCK_STREAM) as sock:
		sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		sock.bind(('127.0.0.1', port))
		conns = []
		while 1:
			sock.listen()
			conn, addr = sock.accept()
			print(f'connected: {conn.recv(65536)}')
			conns.append(conn)


listen(80)

#input('Connected.. Enter to stop')
