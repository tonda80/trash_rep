#!/usr/bin/env python3


# 1 вариант анвил теста для b2b
# переделал на многофайловый


import unittest
import logging
import argparse
import uuid
import os
import sys

import pika

# да, не по науке, но другие варианты имхо ещё страшнее
def import_proto(dir_):
	# dir_ - директория указанная в python_out команды
	# (export ANV_PROTO=/absPathTo/anvil_art/mt_proto ORC_PROTO=/absPathTo/anvil_art/orchestra/proto  &&
	#		protoc -I$ANV_PROTO -I$ORC_PROTO --python_out=dir_  `find $ANV_PROTO $ORC_PROTO -name *.proto`)
	global anvil

	if not os.path.isdir(dir_):
		raise RuntimeError('Incorrect protobuf files directory')

	sys.path.append(dir_)

	import anvil.groups.group_service_pb2




class G:
	log = args = rpc_client = None

	@staticmethod
	def init_logger(level = logging.INFO, root_level = logging.WARNING, name = 'App'):
		logging.basicConfig(format='[%(asctime)s.%(msecs)d][%(levelname)s] %(message)s',
				datefmt='%d %b %H:%M:%S')

		log = logging.getLogger(name)

		log.setLevel(level)
		logging.getLogger().setLevel(root_level)

		return log

	@staticmethod
	def init_args(arg_src = None):
		parser = argparse.ArgumentParser()
		parser.add_argument('--pb', dest='protobuf_dir', required=True, help='Directory with compiled proto files')

		# хотим чтобы unittest не только получил аргументы но даже показал свой хелп
		# поэтому немного костылей
		try:
			args, rest = parser.parse_known_args(arg_src)
		except SystemExit as e:
			if e.code != 0:
				raise		# не хватает аргументов
			print('\n\n+++++++ UNITTEST HELP +++++++\n\n')
			return None, arg_src
		rest.insert(0, sys.argv[0])		# unittest needs it

		return args, rest

	@classmethod
	def start(cls):
		cls.args, unittest_argv = cls.init_args()		# TODO? config

		cls.log = cls.init_logger()

		cls.rpc_client = None
		if cls.args is not None:	# not help run
			import_proto(cls.args.protobuf_dir)
			cls.rpc_client = RpcClient({'host': 'localhost'}, '', 'master-queue')		# TODO args, config

		try:
			unittest.main(argv=unittest_argv)
		finally:
			cls.cleanup()

	@classmethod
	def cleanup(cls):
		if cls.rpc_client:
			cls.rpc_client.cleanup()


class RpcClient:
	def __init__(self, conn_par_dict, exchange, req_queue):
		self.exchange = exchange
		self.req_queue = req_queue
		self.correlation_id = self.reply_body = None

		#conn_par_dict.setdefault('heartbit', 0)
		self.connection = pika.BlockingConnection(pika.ConnectionParameters(**conn_par_dict))
		self.channel = self.connection.channel()

		self.channel.queue_declare(queue=req_queue)

		res = self.channel.queue_declare(queue='', exclusive=True)
		self.rep_queue = res.method.queue

		self.channel.basic_consume(queue=self.rep_queue,
					on_message_callback=self.on_reply,
					auto_ack=True
		)

		# TODO? start_consuming thread

	def cleanup(self):
		self.connection.close()


	def on_reply(self, ch, method, props, body):
		#print('[RpcClient.on_reply]', ch, method, props, body)
		if props.correlation_id == self.correlation_id:
			self.reply_body = body
		else:
			G.log.warning(f'unexpected correlation id {props.correlation_id}, expected id {self.correlation_id}')


	def sync_request(self, endpoint, body, timeout = 3):
		self.correlation_id = str(uuid.uuid4())

		headers = {'endpoint': endpoint}		# request_id
		self.channel.basic_publish(exchange=self.exchange, routing_key=self.req_queue,
				properties=pika.BasicProperties(reply_to=self.rep_queue, correlation_id=self.correlation_id, headers=headers),
				body=body)

		# TODO? use consuming thread
		self.reply_body = None
		dt = 1
		while self.reply_body is None:
			self.connection.process_data_events(dt)		# 0 - ASAP, None - till events

			if isinstance(timeout, (int, float)):
				timeout -= dt
				if timeout <= 0:
					G.log.warning('[RpcClient.sync_request] timeout')
					break

		return self.reply_body


def pb_str(pb_Class, **kw):
	obj = pb_Class()
	for k, v in kw.items():
		setattr(obj, k, v)

	return obj.SerializeToString()


def pb_obj(pb_Class, s):
	obj = pb_Class()
	obj.ParseFromString(s)
	return obj

# ------------- собственно тесты -------------


class GroupServiceTest(unittest.TestCase):

	def read(self, name):
		body = pb_str(anvil.groups.group_service_pb2.Group, name = name)

		reply = G.rpc_client.sync_request('GroupService.Read', body)

		return pb_obj(anvil.groups.group_service_pb2.Group, reply)


	def test_read(self):
		group_name = 'ANTUSDTEST-A'	# TODO config

		group = self.read(group_name)

		#print('__deb recv group: ', group)

		self.assertEqual(group.name, group_name)	# if false - no such group








if __name__ == '__main__':
	G.start()
