import vlc
import time

# какие вообще бывают состояния
# list(filter(lambda s:s[0].isupper(), dir(vlc.State)))

def state_demo(path, what, end_state=vlc.State.Ended):
	print('', what, '-'*len(what), sep='\n')

	mediaPlayer = vlc.MediaPlayer()

	media = vlc.Media(path)
	mediaPlayer.set_media(media)

	# https://stackoverflow.com/questions/11450981/verify-mediafiles-with-libvlc-and-python
	media.parse() #get media info
	if media.get_duration():
		print('OK')
	else:
		print('NO MEDIA')

	prev_state = None
	t0 = time.time()
	cnt = 0
	mediaPlayer.play()
	while prev_state != end_state:
		state = mediaPlayer.get_state()
		if state != prev_state:
			print(cnt, time.time() - t0, state)
		prev_state = state
		cnt += 1
	mediaPlayer.release()


state_demo('/home/anton/temp/mp3s/bubble-lg-e455.mp3', 'ok')
state_demo('/home/anton/temp/mp3s/cuckoo.mp3', 'ok long', vlc.State.Playing)
state_demo('/home/anton/temp/temp.pcap', 'no media')
state_demo('/home/anton/temp/zzzzzzz3', 'nonexistent')
