#!/usr/bin/env python3

# правим данные для isp юнит тестов
#
# 'hdd_Patriot Blaze_A45E0768105000019578': '120',
# 'model_hdd_Patriot Blaze_A45E0768105000019578': 'Patriot Blaze',
# 'serial_hdd_Patriot Blaze_A45E0768105000019578': 'A45E0768105000019578',
# 'size_hdd_Patriot Blaze_A45E0768105000019578': '120',
# 'vendor_hdd_Patriot Blaze_A45E0768105000019578': '',
# 'rr_Patriot Blaze_A45E0768105000019578': 'Solid State Device',
# 'smart_Patriot Blaze_A45E0768105000019578'


from baseapp import BaseWalkerApp
import json


PATH = '/home/ant/isp_reps/back/test/data/hardware_info'


class App(BaseWalkerApp):

	def __init__(self, *a, **kw):
		super().__init__(*a, **kw)
		self.warn_cnt = 0
		self.changed_files_cnt = 0

	def log_format(self):
		return {'format': '[%(levelname)s] %(message)s'}

	def add_arguments(self):
		super().add_arguments()
		self.parser.add_argument('-asf', '--add_storage_fields', action='store_true', help='Adds storage fields')
		self.parser.add_argument('-fs', '--find_storage', help='Finds storage')

	def extension_filter(self, ext):
		return ext == '.json'

	def file_callback(self, path):
		self.log.debug(path)

		with open(path) as jf:
			jo = json.load(jf)
		assert len(jo) == 2 and all(k in jo for k in ('configuration', 'dmidecode'))

		if self.args.find_storage:
			self.find_storage(path, jo['dmidecode'], self.args.find_storage)
		elif self.args.add_storage_fields:
			if self.add_storage_fields(path, jo['dmidecode']):
				with open(path, 'w') as jf:
					json.dump(jo, jf, separators=(',', ':'))
				self.changed_files_cnt += 1
		else:
			self.log.error('Nothing to do')
			exit(0)


	def storage_keys(self, name):
		name_wo_pref = name[4:]
		return f'model_{name}', f'serial_{name}', f'size_{name}', f'vendor_{name}', f'rr_{name_wo_pref}', f'smart_{name_wo_pref}'


	def add_storage_fields(self, path, dmidecode_obj):
		hdd_names = [k for k in dmidecode_obj.keys() if k.startswith('hdd_')]

		for name in hdd_names:
			model_key, serial_key, size_key, vendor_key, rr_key, smart_key = self.storage_keys(name)

			assert all(k not in dmidecode_obj for k in (model_key, serial_key, size_key, vendor_key))

			if not all(k in dmidecode_obj for k in (rr_key, smart_key)):
				self.log.debug(f'WARNING. No rr or smart fields: {path}: {json.dumps(dmidecode_obj, sort_keys=1, indent=4)}')
				self.warn_cnt += 1

			name_parts = name.split('_')
			len_parts = len(name_parts)
			if len_parts < 3 or len_parts > 5:
				raise RuntimeErrorint(f'Bad name: {name}')

			if len_parts == 3:
				model = name_parts[1]
				vendor = name_parts[1].split()[0]
			elif len_parts == 4:
				model = name_parts[2]
				vendor = name_parts[1]
			elif len_parts == 5:
				model = '_'.join(name_parts[1:3])
				vendor = name_parts[1]
			else:
				raise NotImplementedError()

			size = dmidecode_obj[name]
			assert size

			dmidecode_obj[model_key] = model
			dmidecode_obj[serial_key] = name_parts[-1]
			dmidecode_obj[size_key] = size
			dmidecode_obj[vendor_key] = vendor

			#print(name, '=>', model, name_parts[-1], size, vendor)

		return bool(hdd_names)

	def find_storage(self, path, dmidecode_obj, need_name):
		hdd_names = [k for k in dmidecode_obj.keys() if k.startswith('hdd_')]
		for name in hdd_names:
			if name == need_name:
				model_key, serial_key, size_key, vendor_key, rr_key, smart_key = self.storage_keys(name)
				_keys = locals()
				def out_line(what):
					key = _keys[f'{what}_key']
					return f"{what} = {dmidecode_obj.get(key)}"
				out = f"name value = {dmidecode_obj[name]}\n"
				out += '\n'.join(out_line(k) for k in ('model', 'serial', 'size', 'vendor', 'rr', 'smart', ))
				self.log.info(f"Found in file {path}:\n\n{out}\n")
				self.log.debug(json.dumps(dmidecode_obj, sort_keys=1, indent=4))

				exit(0)


	def main(self):
		super().main()
		if self.changed_files_cnt:
			self.log.info(f'Changed files: {self.changed_files_cnt}')
		self.log.info(f'Total warnings: {self.warn_cnt}')





if __name__ == '__main__':
	App(PATH).main()
