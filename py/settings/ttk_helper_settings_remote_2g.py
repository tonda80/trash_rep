# coding=utf8

import itertools


# remote host
class Host:
	def __init__(self, host, user, pwd, name, *, in_opt=True, port=22):
		self.host = host
		self.user = user
		self.pwd = pwd
		self.port = port
		self.name = name if name is not None else host
		#self.mnt_root = mnt_root
		if in_opt:
			self.mnt_root = '/opt/mnt2'
		else:
			self.mnt_root = f'/home/{user}/mnt2'

	def __str__(self):
		return f'Host({self.__dict__})'

host77 = Host('192.168.136.77', None, None, 'me')
#
host20 = Host('192.168.136.20', 'mnt', 'q12345', '20')
host92 = Host('192.168.136.92', 'mnt', 'q12345', '92')
host114 = Host('192.168.136.114', 'mnt', 'q12345', '114')
host32 = Host('192.168.136.32', 'mnt', 'q12345', '32')
#host13 = Host('192.168.109.13', 'mnt', 'cPPZOP!', '13')
host130 = Host('192.168.109.130', 'mnt', 'cPPZOP!', '130')
host131 = Host('192.168.109.131', 'mnt', 'cPPZOP!', '131')
host132 = Host('192.168.109.132', 'mic', 'cPPZOP!', '132')
host134 = Host('192.168.109.134', 'nuvo', '1q2w3e4r', '134', in_opt=False)
host135 = Host('192.168.109.135', 'mnt', 'cPPZOP!', '135')
host139 = Host('192.168.109.139', 'mnt', 'cPPZOP!', '139')
host144 = Host('192.168.109.144', 'mnt', 'cPPZOP!', '144')
host145 = Host('192.168.109.145', 'mnt', 'cPPZOP!', '145')
host146 = Host('192.168.109.146', 'mnt', 'cPPZOP!', '146')
host147 = Host('192.168.109.147', 'mnt', 'cPPZOP!', '147')
#
#host_smart_stop = Host('172.27.5.6', 'mic', 'cPPZOP!', 'smart_stop')
host_maxima = Host('10.144.61.240', 'mic', 'micqwerty', 'maxima', in_opt=False, port=44422)
host_rupd2 = Host('192.168.207.5', 'mnt', 'cPPZOP!', 'rupd2')
host_arm64 = Host('192.168.136.91', 'mnt', 'q12345', 'arm64')
#
#host_ext = Host('192.168.160.37', 'mnt', 'cPPZOP!', None)
#host_ext2 = Host('192.168.160.37', 'mnt', 'cPPZOP!', 'ext2')
#host_term = Host('172.24.0.81', 'root', 'xmJfADNgqE6g', name='term', port=29849)
#host_tmp = Host('172.27.4.176', 'mnt', 'cPPZOP!', 'tmp')
#

all_hosts = tuple(obj for obj in globals().values() if isinstance(obj, Host))
all_hosts_dict = dict((h.name, h) for h in all_hosts)

obligatory_update_hosts = (
	host145,		# msk mic2
	host114,		# sar rco
	host20,			# sar smth
	#host135,
)


class SshWindowsSettings:
	all_hosts_dict = all_hosts_dict

	btn_hosts = (
		host77,
		host114,
		host20,
		host92,
		host32,
		#host135,
		#host_term,
	)


builds_dir = '/home/ant/job/builds/'
deploy_dir = '/home/ant/job/deploy/'
qt_rep_dir = '/home/ant/ttm_rep/qt/'
cpp_rep_dir = '/home/ant/ttm_rep/cpp/'
common_rep_dir = '/home/ant/common_rep/'
qt_dir = '/opt/qt/5.12.10/gcc_64/'


class DeploySettings:
	all_hosts_dict = all_hosts_dict
	obligatory_update_hosts = obligatory_update_hosts
	pull_init_dir = '/home/ant/temp'
	push_init_dir = '/home/ant/temp'

	class pyenvFiles:
		pb_path = '/home/ant/job/mnt_pb_py/'
		files = {
			common_rep_dir+'py3/modules/baseapp.py' : '~/ab/py_env/',
			common_rep_dir+'py3/job/ttk/ttk_utils.py' : '~/ab/py_env/',
			common_rep_dir+'py3/job/ttk/zmq_client_mnt2.py' : '~/ab/py_env/',
			pb_path+'usbdiag_pb2.py' : '~/ab/py_env/',
			pb_path+'configurator_pb2.py' : '~/ab/py_env/',
			pb_path+'mntproto_pb2.py' : '~/ab/py_env/',
			pb_path+'canparser_pb2.py' : '~/ab/py_env/',
			pb_path+'board_pb2.py' : '~/ab/py_env/',
			pb_path+'boarddevice_pb2.py' : '~/ab/py_env/',
			pb_path+'routes_pb2.py' : '~/ab/py_env/',
			pb_path+'navigation_pb2.py' : '~/ab/py_env/',
			pb_path+'alert_pb2.py' : '~/ab/py_env/',
			pb_path+'dido_pb2.py' : '~/ab/py_env/',
			pb_path+'uiactions_pb2.py' : '~/ab/py_env/',
			pb_path+'getversion_pb2.py' : '~/ab/py_env/',
			pb_path+'redis_pb2.py' : '~/ab/py_env/',
			pb_path+'wssm_pb2.py' : '~/ab/py_env/',
			pb_path+'gtfs_pb2.py' : '~/ab/py_env/',
			pb_path+'dispatcher_pb2.py' : '~/ab/py_env/',
			pb_path+'duter_pb2.py' : '~/ab/py_env/',
			pb_path+'apc_pb2.py' : '~/ab/py_env/',
		}
	class usbdiagFiles:
		files = {
			builds_dir+'usbdiag/usbdiag' : '!/usbdiag/',
			deploy_dir+'usbdiag/usbdiag_deploy_config.json' : '!/configurator/conf/usbdiag.json'
		}
	class validatorFiles_:
		files = {
			builds_dir+'validator/validator' : '!/validator/validator',
		}
	class updaterFiles_:
		files = {
			common_rep_dir+'py3/job/ttk/updater/updater.py' : '~/ab/updater/updater.py',
			#common_rep_dir+'py3/job/ttk/updater/updater_debug_config.json' : '~/ab/updater/updater_debug_config.json',
		}
	class mnt2testFiles:
		files = {
			common_rep_dir+'py3/job/ttk/mnt2_test.py' : '~/ab/mnt2_test/',
			common_rep_dir+'py3/job/ttk/mnt_zmq_test.py' : '~/ab/mnt2_test/',
			#common_rep_dir+'py3/job/ttk/nmea_player.py' : '~/ab/mnt2_test/',
		}
	class addModuleFiles:
		files = {
			common_rep_dir+'py3/job/ttk/add_module.py' : '~/ab/add_module/',
			common_rep_dir+'py3/job/ttk/local_deploy.py' : '~/ab/add_module/',
		}
	class tsdiagBinConfigFiles:
		files = {
			builds_dir+'tsdiag/tsdiag' : '!/tsdiag/',
			deploy_dir+'tsdiag/tsdiag_deploy_config.json' : '!/configurator/conf/tsdiag.json'
		}
	class tsdiagDictsFiles:	# словари надо переписывать целиком TODO?? придумать что-то умнее чем отдельное правило
		no_merge_json = True
		files = {
			cpp_rep_dir+'deb/tsdiag/opt/mnt2/tsdiag/par_dict*.json' : '!/tsdiag/',
		}
	class wipssenderFiles:
		files = {
			builds_dir+'wipssender/wipssender' : '!/wipssender/',
			deploy_dir+'wipssender/wipssender_deploy_config.json' : '!/configurator/conf/wipssender.json',
		}
	class pcmBridgeFiles:
		files = {
			builds_dir+'pcm_bridge/pcm_bridge' : '~/ab/pcm_bridge/',
		}
	class tempFiles:
		files = {
			'/home/anton/rep-s/mnt2/cpp/apc/apc' : '!/apc/',
		}

	class wssmFiles__:
		files = {
			builds_dir+'wssm/wssm' : '~/ab/wssm/',
			#
			'/home/anton/job/projects/wssm_smart_stop/out/wssm/lib/*' : '~/ab/wssm/lib/',
			#
			builds_dir+'wssm_debug_config.json' : '~/ab/',
			builds_dir+'global.json' : '~/ab/',
		}
	class wssmFiles:
		files = {
			builds_dir+'wssm/wssm' : '!/wssm/',
			#'/home/anton/job/projects/wssm_smart_stop/out/wssm/lib/*' : '!/wssm/lib/',
			builds_dir+'wssm_debug_config.json' : '!/configurator/conf/wssm.json',
		}

	class routFiles:
		files = {
			qt_rep_dir+'cpp/build-routesModule-Desktop_Qt_5_12_10_GCC_64bit-Release/routesModule' : '!/routesModule/',
		}

	class rUpdaterFiles:
		files = {
			qt_rep_dir+'cpp/build-routesModuleUpdater-Desktop_Qt_5_12_10_GCC_64bit-Release/routesModuleUpdater' : '!/routesModule/',
			#qt_rep_dir+'cpp/build-routesModuleUpdater-Desktop_Qt_5_12_10_GCC_64bit-Release/routesModuleUpdater' : '!/routesModuleUpdater/',
		}

	class audioModuleFiles:
		files = {
			qt_rep_dir+'cpp/build-audioModule-Desktop_Qt_5_12_10_GCC_64bit-Release/audioModule' : '!/sound/',
		}

	class routeupd2Files:
		files = {
			builds_dir+'routeupd2/routeupd2' : '~/ab/routeupd2/',
			builds_dir+'routeupd2_debug_config.json' : '~/ab/',
			cpp_rep_dir+'deb/routeupd2/opt/mnt2/routeupd2/incfg.json' : '~/ab/routeupd2/',
			#'/home/anton/job/builds/global.json' : '~/ab/',
			#'/home/anton/job/projects/wssm_smart_stop/out/wssm/lib/*' : '~/ab/routeupd2/lib/',
		}

	class QtLibsFiles_:
		files = {
			qt_dir+'lib/libQt5Network.so.5' : '~/ab/qtlib/',
			qt_dir+'lib/libQt5Core.so.5' : '~/ab/qtlib/',
			qt_dir+'lib/libicui18n.so.56' : '~/ab/qtlib/',
			qt_dir+'lib/libicuuc.so.56' : '~/ab/qtlib/',
			qt_dir+'lib/libicudata.so.56' : '~/ab/qtlib/',
			#
			qt_dir+'lib/libQt5Sql.so.5' : '~/ab/qtlib/',
			#
			qt_dir+'plugins/bearer/libqconnmanbearer.so' : '~/ab/qtlib/bearer/',
			qt_dir+'plugins/bearer/libqgenericbearer.so' : '~/ab/qtlib/bearer/',
			qt_dir+'plugins/bearer/libqnmbearer.so' : '~/ab/qtlib/bearer/',
		}



icon_path = '/usr/share/icons/HighContrast/48x48/emblems/emblem-system.png'
