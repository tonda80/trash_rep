# бекап EntryPopupMenu с сортировкой по частоте использования, был выпилен, как сырой и оказавшийся не особо удобным, может таки когда-нибудь захочется его восстановить


class uEntry(Entry, GeometryManager, VariableWrapper):
	# тут кусочек класса с методами имеющими отношение к делу, которые тоже переделаны
	def set_popup(self, popup):
		self.popup = popup
		self.getVar = self._getVarPopupOverriden

	# TODO! подумать таки правильно ли что каждое чтение entry добавляет метку (увеличивает счетчик использования)
	# может есть случаи когда это неправильно
	def _getVarPopupOverriden(self):
		res = VariableWrapper.getVar(self)
		self.popup.add_label(res)
		return res

# popup хранящий варианты выбора для Entry
class EntryPopupMenu(PopupMenu):
	# label_dict - {'текст элемента попапа': счетчик выбора} для сортировки элементов
	# тут предлагается передавать словарь из commonconfig, сохраняем ссылку что позволяет "автоматически" сохранять значения
	# сохранение элементов в label_dict происходит в момент чтения связанной entry, см uEntry._getVarPopupOverriden
	# TODO добавить создание в Entry
	def __init__(self, master_entry, label_dict, limit = 50, font = 'System 9'):
		PopupMenu.__init__(self, master_entry)
		trace_control_l(master_entry)		# фокус должен быть в entry

		self.label_font = font
		self.limit = limit
		self.label_dict = label_dict			# не копируем словарь!
		print('__deb', len(label_dict), label_dict)

		master_entry.set_popup(self)
		if self.label_dict:
			master_entry.setVar(self.__most_freq_label())
		self.master_entry = master_entry

		self.__refresh_all_labels()

	def labels(self):
		return [i[0] for i in sorted(self.label_dict.items(), reverse = True, key = lambda e: e[1]) if i[0]]

	def __most_freq_label(self):
		return sorted(self.label_dict.items(), key = lambda e: e[1])[-1][0]

	def __set_first(self, label):
		M = 1 << 31
		most_freq_label = self.__most_freq_label()
		if label != most_freq_label:
			self.label_dict[most_freq_label] &= ~M
			self.label_dict[label] |= M

	# клик по label
	def __on_select(self, label):
		if self.master_entry.is_control_l_pressed:
			del self.label_dict[label]
			self.__refresh_all_labels()
		else:
			self.master_entry.setVar(label)
			self.__on_select2(label)

	# реакция на выбор label
	def __on_select2(self, label):
		self.label_dict[label] += 1
		self.__set_first(label)
		self.__refresh_all_labels()

	def __refresh_all_labels(self):
		# все удаляем
		self.delete(0, self.index('end'))		# https://stackoverflow.com/a/62717092  не особо что ещё принимает index() (но не сама метка)
		# и добавляем заново. в py/utils/tkgrep.py другой подход, см если тут вдруг будет тормозить
		self.add_callback(self.labels(), self.__on_select)
		#print('__deb', self.labels(), self.label_dict)
		self.add_separator()
		self.add_callback('Clear all', self.clear)

	def add_label(self, label):
		# удалим сверх лимита
		sorted_labels = sorted(self.label_dict.items(), key = lambda e: e[1], reverse = True)
		for label, _ in sorted_labels[self.limit:]:
			del self.label_dict[label]

		self.label_dict.setdefault(label, 0)
		self.__on_select2(label)

	def clear(self, *_):
		self.label_dict.clear()
		self.__refresh_all_labels()
