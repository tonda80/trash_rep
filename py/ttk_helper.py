#!/usr/bin/env python
# coding=utf8

# изначальная версия для 2-го питона (хелпер по рутинным действиям в транстелематике)


import platform
import subprocess
import importlib
import psutil

import pyperclip	# pip install pyperclip

from baseapp import BaseTkApp
import uTkinter as utk
from commonconfig import CommonConfig
import lsh


class G:
	app = None


def set_clipboard(s):
	pyperclip.copy(s)
	pyperclip.paste()


class App(BaseTkApp):
	def load_config(self):
		app_name = 'ttkk helper'
		default_config = {

		}
		self.config = CommonConfig(app_name, **default_config)

		self.settings = importlib.import_module('ttk_helper_settings')

	def save_config(self):
		#self.config['{some_key}'] = '{some_type_value}'
		self.config.save()

	def create_gui(self):
		root = utk.uTk(u'ttk helper', 640, 480, createStatus=True)

		f_gmArgs = {'side':utk.TOP, 'expand':utk.NO, 'fill':utk.X, 'padx':3, 'pady':2}
		SshWindowActions(utk.uLabelFrame(root, 'ssh', gmArgs=f_gmArgs), self.settings.SshWindowsSettings)

		root.set_exit_by_escape()
		root.set_delete_callback(self.save_config)

		return root


class SshWindowActions:
	def __init__(self, root, settings):
		self.windows = []

		btn_props = {'width': 12, 'font': ("System", 8),'gmArgs': {'side':utk.LEFT, 'padx':3}}
		for host, user, pwd in settings.sessions:
			utk.uButton(root, lambda host=host, user=user, pwd=pwd: self.create_window(user, host, pwd), host, **btn_props)
		utk.uButton(root, self.close_all, 'Close all', gmArgs = {'side': utk.RIGHT, 'padx':20})

	def create_window(self, user, host, pwd):
		self.windows.append(SshWindow(user, host, pwd))

	def close_all(self):
		while self.windows:
			self.windows.pop().close()


class SshWindow:
	def __init__(self, user, host, pwd, pos = None):
		if platform.linux_distribution()[0] == 'Ubuntu':
			self.process = self.run_gnome_terminal(user, host, pwd, pos)
			#print '__deb', self.process
		else:
			raise NotImplementedError

	def close(self):
		if self.process:
			try:
				self.process.terminate()
			except psutil.Error as e:
				msg = '[SshWindow.close] closing error {}'.format(e)
				G.app.log.warn(msg)
				G.app.set_status_bar('[WARN] {}'.format(msg))

	def run_gnome_terminal(self, user, host, pwd, pos):
		# gnome-terminal --geometry=10x10+200+5 -- sshpass -p {pwd} ssh {user}@{host}
		cmd = ['gnome-terminal', '--geometry=', '--', 'sshpass',  '-p', pwd, 'ssh', '{user}@{host}'.format(user=user, host=host)]
		if pos:
			cmd[1] += pos
		else:
			cmd.pop(1)

		# в итоге sshpass стартует gnome-terminal-server и получить pid через subprocess не получается
		# получаем его через такой workaround
		proc_name = 'sshpass'
		old_pids = set(p.pid for p in lsh.find_process(proc_name))
		subprocess.call(cmd)			# Popen() оставляет defunct gnome-terminal процесс, а call почему то нет, наверное из-за отложенного удаления объекта
		new_processes = list(p for p in lsh.find_process(proc_name) if p.pid not in old_pids)
		if len(new_processes) == 1 and new_processes[0].parent().name() == 'gnome-terminal-server':		# + на всякий случай проверяем родителя
			return new_processes[0]
		else:
			msg = '[SshWindow.run_gnome_terminal] cannot find started process'
			G.app.log.warn('{} {}'.format(msg, new_processes))
			G.app.set_status_bar('[WARN] {}'.format(msg))


if __name__ == '__main__':
	G.app = App()
	G.app.mainloop()


# xdotool windowraise 88104394
# xdotool search --name <заголовок окна>
# или прямо так, xdotool search --name 'anton@' windowraise
# ssh mnt@192.168.136.114 -t "cd ab; bash"
# перенести настройки из репы
