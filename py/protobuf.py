# вот так работаем с Any messages (SubscribeData сообщение в модуле mntproto_pb2)

if mntCommand.header.name == 'TestRequest1':
	print mntCommand.data.Is(mntproto_pb2.SubscribeData.DESCRIPTOR)		# SubscribeData - имя прото сообщения
	print mntCommand.data.Is(mntproto_pb2.UnsubscribeData.DESCRIPTOR)
	m1 = mntproto_pb2.SubscribeData()
	m2 = mntproto_pb2.UnsubscribeData()
	mntCommand.data.Unpack(m1)
	print m1
	mntCommand.data.Unpack(m2)		# в отличие от ParseFromString тут нет никакой ошибки, просто m2 остается пустым
	print m2


# c repeated string полем messageNames в m1 работаем так
print m1, len(m1.messageNames), m1.messageNames[0]

# protoc --help
# protoc --cpp_out=../../cpp/pb --python_out=../../python/pb usbdiag.proto		лучше запускать из директории с прото файлом, иначе он повторит в выходном файле путь


