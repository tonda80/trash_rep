import uTkinter as u


w = u.uTk()
w.bind('<space>', lambda ev: print(f'w "{e1.getVar()}"'))

f = u.uFrame(w)
f.focus_set()		# только так оно получит евент
f.bind('<space>', lambda ev: print('f'))

e1 = u.uEntry(f)
e1.bind('<space>', lambda ev: print('111'))		# этот бинд не будет работать
e1.bind('<space>', lambda ev: print(f'1 "{e1.getVar()}"'))

e2 = u.uEntry(f)
e2.bind('<space>', lambda ev: print('2') or 'break')	# это не попадет к w и даже не напечатается пробел в ентри


# непонятно, как именно нажатие пробела попадает в ентри, но видно что сперва выполняется наш бинд на ентри
# а потом какой то другой, до выполнение нашего бинда на окно

w.mainloop()
