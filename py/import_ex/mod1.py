print('[mod1] started')


#import mod2		# circular import error

try:
	print(f'[mod1] a = {a}')
except NameError:
	print('[mod1] no a')
	a = 0

#import mod2		# тут уже ОК

a += 1

print(f'[mod1] a = {a}')


print('[mod1] finished')
