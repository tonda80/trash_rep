
# учимся писать программы!

import random


min_number = 1
max_number = 10000


puzzled_number = random.randint(min_number, max_number)
print('Привет, я загадал число 1-10000, угадай!')

number = None
while puzzled_number != number:
	s = input()
	number = int(s)

	if puzzled_number > number:
		print('Мое число больше')
	elif puzzled_number < number:
		print('Мое число меньше')
	else:
		print('Угадал!')

print('Пока!')
