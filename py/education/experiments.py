


def pima(x):
	print('Привет, я pima! Меня вызвали с аргументом равным ', x)
	result = x*2
	print('И мой результат равен ', result)
	return result


z = pima(8)
print('----------- z =', z)
z = pima(1)
print('----------- z =', z)
z = pima(10)
print('----------- z =', z)
z = pima(100)
print('----------- z =', z)
