# based on
# https://stackoverflow.com/questions/21433660/how-to-interact-with-ssh-using-subprocess-module


import time
import pty
import os


def read_and_print(d):
	out = os.read(d, 4096)
	print(f'out [{len(out)}]: \n{out.decode()}')



child_pid, child_fd = pty.fork()		# https://docs.python.org/3/library/pty.html#pty.fork

if not child_pid:
	# Child process
	# Replace child process with our SSH process
	cmd = ['/usr/bin/ssh', '192.168.109.135']
	os.execv(cmd[0], cmd)


# parent process
time.sleep(1)
read_and_print(child_fd)

os.write(child_fd, b'yes\n')

time.sleep(1)
read_and_print(child_fd)

os.waitpid(child_pid, 0)		# https://docs.python.org/3/library/os.html#os.waitpid
