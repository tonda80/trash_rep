-- https://www.lua.org/cgi-bin/demo

function p_boll_state(var, name)
  if var then print(name..' is true') else print(name..' is false') end
end


local a = '' -- nil -- 'hello'
local b = nil
a = a or 'world'

print(a)
p_boll_state(a, 'a')
p_boll_state(b, 'b')
