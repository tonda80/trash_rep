#include "zmq_loop_thread.h"
#include <QCoreApplication>
#include <mnt2app.h>

void ZmqLoopThread::run() {
	Mnt2App::instance().zmq_poll_loop();

	QObject* quit_sender = new QObject();
	QObject::connect(quit_sender, &QObject::destroyed, QCoreApplication::quit);
	quit_sender->deleteLater();
}
