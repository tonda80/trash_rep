    QDir tDir("/home/mnt/ab/rootDir");
    tDir.mkpath(".");

    QProcess pr;
    pr.start("touch", {tDir.absoluteFilePath("tmp.txt")});
    pr.waitForFinished();
    logerror << "1:" << pr.readAllStandardOutput().toStdString();
    logerror << "2:" << pr.readAllStandardError().toStdString();

    QProcess pr2;
    pr2.start("ffmpeg", {"-f", "ffmpeg", "-f", "s16le", "-ar", "8000", "-ac", "1", "-i", "/home/mnt/ab/rec_c.pcm", "-f", "s16le",
                         "-ar", "8000", "-ac", "1", "-i", "/home/mnt/ab/rec_p.pcm",  tDir.absoluteFilePath("tmp.aac")});
    pr2.waitForFinished();
    logerror << "1:" << pr2.readAllStandardOutput().toStdString();
    logerror << "2:" << pr2.readAllStandardError().toStdString();

    QRegExp re2("\\$func\\(aa;(@\\w+);\\)");
    if (re2.exactMatch("$func(aa;@qqww;)")) qDebug() << "match" << re2.cap(1);
    else qDebug() << "no match";
