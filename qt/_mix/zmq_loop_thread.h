#ifndef ZMQ_LOOP_THREAD_H
#define ZMQ_LOOP_THREAD_H

#include <QThread>


class ZmqLoopThread : public QThread {
    Q_OBJECT
    void run() override;
};

#endif // ZMQ_LOOP_THREAD_H
