#ifndef DUMMY_H
#define DUMMY_H

#include <QObject>

class Dummy : public QObject {
    Q_OBJECT
public:
    Dummy(QObject* parent);
};

#endif // DUMMY_H
