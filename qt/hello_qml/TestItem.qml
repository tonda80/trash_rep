import QtQuick 2.0

import QtQuick.Controls 2.12

Rectangle {
        color: "green"

        property int cnt : 0
        property int cnt2 : 0

        signal someSignal

        Label {
            id : cntLabel
            anchors.centerIn: parent
            text : `${cnt} ${cnt2}`
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                someSignal()
            }
        }

        onSomeSignal: {
            cnt += 1
        }

}
