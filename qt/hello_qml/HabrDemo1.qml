import QtQuick 2.0

// https://habr.com/ru/post/181712/

Rectangle {
    width: 360
    height: 360

    ListModel {
        id: dataModel

        ListElement {
            color: "orange"
            text: "first"
        }
        ListElement {
            color: "skyblue"
            text: "second"
        }
    }

    ListView {
        id: view

        anchors.margins: 10
        anchors.fill: parent
        spacing: 10
        model: dataModel

        delegate: Rectangle {
            width: view.width
            height: 100
            color: model.color

            Text {
                anchors.centerIn: parent
                renderType: Text.NativeRendering
                text: model.text
            }
        }
    }
}
