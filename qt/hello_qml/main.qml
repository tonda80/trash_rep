import QtQuick 2.12
import QtQuick.Window 2.12
//import QtQuick.Layouts 1.12

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello QML")

    Column {
        width: parent.width
        height : parent.height

        Rectangle {
            id : redRect

            //anchors.horizontalCenter: parent.horizontalCenter
            //anchors.top : parent.top
            width: parent.width
            height : 100

            color: "red"

            Text {
                anchors.centerIn: parent
                text: "Hello, qml!"
            }
        }

        TestItem {
            id : greenRect

            //anchors.horizontalCenter: parent.horizontalCenter
            //anchors.top : redRect.bottom
            //anchors.bottom : parent.bottom
            width: parent.width
            height: 100

            onSomeSignal: {
                cnt2 += 1
            }
        }

        HabrDemo1 {
            anchors.horizontalCenter: parent.horizontalCenter
        }

    }
}
