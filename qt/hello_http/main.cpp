#include <QCoreApplication>

#include <csignal>
//#include <iostream>

#include "downloader.h"
#include "qtstdout.h"


static std::function<void(int)> signal_handler_impl;
void signal_handler(int signal) {
    signal_handler_impl(signal);
}


int main(int argc, char *argv[]) {
    qStdOut() << "hi!" << endl;
    //std::cout << "hello!\n";

    QCoreApplication a(argc, argv);

    signal_handler_impl = [&a](int){qStdOut() << "bye!" << endl; a.quit();};
    std::signal(SIGINT, signal_handler);

    Downloader ldr(&a);
    ldr.get("http://www.cbr.ru/scripts/XML_daily.asp");
    ldr.get("https://api.weather.yandex.ru/v2/forecast?lat=51.541&lon=46.009", "X-Yandex-API-Key", "6c082a9a-404f-4717-8473-74c97cad59f0");
    //ldr.get("http://www.google.com");

    return a.exec();
}
