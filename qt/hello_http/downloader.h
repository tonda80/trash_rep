#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <QNetworkAccessManager>
#include <QNetworkReply>

#include "qtstdout.h"


class Downloader : public QObject {
    Q_OBJECT

    QNetworkAccessManager* mgr;

public:
    virtual ~Downloader() = default;

    Downloader(QObject* parent) {
        mgr = new QNetworkAccessManager(parent);

        connect(mgr, &QNetworkAccessManager::finished, [this](QNetworkReply *reply){replyFinished(reply);});
    }

    void get(QString url) {
        mgr->get(QNetworkRequest{QUrl(url)});
    }

    void get(QString url, const QByteArray &headerName, const QByteArray &headerValue) {
        QNetworkRequest req{QUrl(url)};
        req.setRawHeader(headerName, headerValue);
        mgr->get(req);
    }

public slots:
    void replyFinished(QNetworkReply *reply) {
        if (reply->error() == QNetworkReply::NoError) {
            qStdOut() << "download ok:"
                      << reply->readAll()
                      << endl;
        }
        else {
            qStdOut() << "download error: " << reply->errorString() << endl
                         << "\t" << reply->url().toString() << endl
                         << endl;
        }

        reply->deleteLater();
    }
};



#endif // DOWNLOADER_H
