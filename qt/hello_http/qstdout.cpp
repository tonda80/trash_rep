#include "qtstdout.h"

QTextStream& qStdOut() {
    static QTextStream ts(stdout, QIODevice::WriteOnly);
    return ts;
}
