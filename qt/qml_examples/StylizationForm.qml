import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.10

import com.Project 1.0


Dialog {
    standardButtons: Dialog.Ok | Dialog.Cancel
    footer: DialogButtonBox {
        alignment: Qt.AlignHCenter
        spacing: 30
    }
    modal: true
    closePolicy: Popup.CloseOnEscape
    x: 30
    y: 0
    width: parent.width - x*2
    height: parent.height - y*2

    palette { button: "#33adff"; buttonText: "white" }

    function _form2edit() {
        var e = DictStylizationsModel.editStylization
        e.name = name.text
        e.comment = comment.text
        e.boardIndex = boardModel.currentIndex
    }

    function _edit2form() {
        var e = DictStylizationsModel.editStylization
        name.text = e.name
        comment.text = e.comment
        boardModel.currentIndex = e.boardIndex
    }

    function init_open(title_) {
        title = title_
        _edit2form()
        error.text = ""
        _redraw_board_view()
        open()
    }

    function _redraw_board_view() {
        var board = DictBoardsModel.get(boardModel.currentIndex)
        if (!board) board = DictStylizationsModel.editStylization.getBoard()
        if (!board) {
            heightLabel.value = -1
            widthLabel.value = -1
            return
        }

        heightLabel.value = board.height
        widthLabel.value = board.width
    }

    onAccepted: {
        _form2edit()
        if (!DictStylizationsModel.saveEdit()) {
            error.text = DictStylizationsModel.validationError
            open()
        }
    }

    //onRejected: {}

    ColumnLayout {
        anchors.fill: parent

        spacing: 15

        Label {
            id: error
            color: "red"
            font.pointSize: 10
            font.italic: true
        }
        //Label {text: `__deb ${teeeeeeeest.height}`}

        RowLayout {
            spacing: 5

            Label {
                text: qsTr("Название") + " *"
                font.bold: true
            }
            TextField {
                id: name
                selectByMouse: true
                Layout.preferredWidth: parent.width * 0.3
            }

            Item {width : 30}

            Label {
                text: qsTr("Комментарий")
                font.bold: true
            }
            TextField {
                id: comment
                selectByMouse: true
                Layout.fillWidth: true
            }
        }

        RowLayout {
            Layout.preferredHeight: 300
            spacing: 5

            GridLayout {
                Layout.preferredWidth: parent.width/3
                Layout.preferredHeight: parent.height
                columns: 2
                rows: 2

                Label {
                    text: qsTr("Модель табло") + " *"
                    font.bold: true
                }
                ComboBox {
                    id: boardModel
                    Layout.preferredWidth: 350
                    model : DictBoardsModel
                    textRole : "boardViewRole"
                    currentIndex: -1
                    onCurrentIndexChanged: {
                        if (currentIndex >= 0) {
                            _redraw_board_view()
                        }
                    }
                }
                Label {
                    text: qsTr("Тип табло")
                }
                ComboBox {
                    id: boardType
                    Layout.preferredWidth: 350
                    currentIndex: -1
                    model : DictStylizationsModel.boardTypeViews()
                }
            }

            Label {
                id: heightLabel
                property int value : -1
                text: qsTr(`Высота ${value >= 0 ? value : ""}`)
                Layout.alignment: Qt.AlignHCenter
                Layout.preferredWidth: 15
                rotation: -90
            }
            ColumnLayout {
                Rectangle {
                    color: "#eeeeee"
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                }
                Label {
                    id: widthLabel
                    property int value : -1
                    text: qsTr(`Ширина ${value >= 0 ? value : ""}`)
                    Layout.alignment: Qt.AlignHCenter
                    Layout.preferredHeight: 15
                }
            }
        }

        ScrollView {
            Layout.fillWidth: true
            Layout.fillHeight: true

            clip: true
            ScrollBar.vertical.policy: ScrollBar.AlwaysOn

            Column {
                width: parent.width

                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw8"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw28"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw8"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"qiwquiwquwtqutw1"}
                Label {text:"-----------------"}
            }
        }
    }


}
