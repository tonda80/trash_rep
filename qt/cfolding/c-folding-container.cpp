#include "c-folding-container.h"

#include <QtCore/QTimerEvent>
#include <QVBoxLayout>
#include <QStackedWidget>
#include <QtGui/QPainter>

CFoldingContainer::CFoldingContainer(QWidget *parent)
    :QWidget(parent)
{
    // Нет виджета контента
    m_pwgtContent = 0;
    m_eComponentState = CS_NO_CONTENT;

    // Анимация разрешена
    m_bIsAnimationEnabled = true;
    // Длительность анимации в миллисекундах
    m_nAnimationDurationMs = 300;

    // Таймер анимации не запущен
    m_nTimerId = -1;
    // Таймер с меньшим разрешением не имеет смысла - большинство ОС имеют jiffies порядка 10 мс.
    m_nTimerPeriodMs = 20;
}

void CFoldingContainer::setAnimationEnabled(bool enable)
{
    m_bIsAnimationEnabled = enable;
}

void CFoldingContainer::setAnimationDuration(int period)
{
    // Нет смысла задавать общую длительность анимации меньше величины двух периодов анимации
    int base = 2*m_nTimerPeriodMs;
    if (period < base) period = base;
    m_nAnimationDurationMs = period;
}

void CFoldingContainer::setContent(QWidget *content_ptr)
{
    if (m_pwgtContent) return;

    m_pwgtContent = content_ptr;
    m_pwgtContent->setParent(this);

    QVBoxLayout *pvb = new QVBoxLayout(this);
    pvb->setMargin(0);
    pvb->addWidget(m_pwgtContent, Qt::AlignCenter);

    m_eComponentState = CS_EXPANDED;
}

void CFoldingContainer::expand()
{
    // Операция возможна только для свернутого состояния
    if (m_eComponentState != CS_COLLAPSED) return;

    if (m_bIsAnimationEnabled) {
        m_eComponentState = CS_EXPANDING;

        m_nAnimationStepPx = m_nExpandedHeight*m_nTimerPeriodMs/m_nAnimationDurationMs;

        m_nTimerId = startTimer(m_nTimerPeriodMs);
    } else {
        m_eComponentState = CS_EXPANDED;
        m_pwgtContent->setVisible(true);
        emit signalExpandOk();
    }
}

void CFoldingContainer::collapse()
{
    // Операция возможна только для развернутого состояния
    if (m_eComponentState != CS_EXPANDED) return;

    {   // Необходимо выполнить в любом случае!!! Нужно быть готовым к изменению разрешения анимации!!!
        m_grabbedPixmap = QPixmap::grabWidget(this);
        m_nExpandedHeight = height();
    }

    if (m_bIsAnimationEnabled) {

        setFixedHeight(m_nExpandedHeight);
        m_nCurrentAnimationHeight = m_nExpandedHeight;

        m_eComponentState = CS_COLLAPSING;
        m_pwgtContent->setVisible(false);

        m_nAnimationStepPx = m_nExpandedHeight*m_nTimerPeriodMs/m_nAnimationDurationMs;
        m_nTimerId = startTimer(m_nTimerPeriodMs);
    } else {
        m_eComponentState = CS_COLLAPSED;
        m_nCurrentAnimationHeight = 0;
        m_pwgtContent->setVisible(false);
        emit signalCollapseOk();
    }
}

void CFoldingContainer::setExpanded(bool enable)
{
    if (enable) expand();
    else collapse();
}

/** Особое исполнение прорисовки виджета выполняется только в режимах анимации.
  * В этом случае, "лицом" виджета становится вычисленная от текущего шага
  * анимации часть объекта класса QPixmap полученного перехватом преданимационного
  * развернутого состояния виджета.
  *
  */
void CFoldingContainer::paintEvent(QPaintEvent *ev)
{
    switch (m_eComponentState) {
    case CS_NO_CONTENT:
    case CS_EXPANDED:
    case CS_COLLAPSED:
        QWidget::paintEvent(ev);
        break;
    case CS_EXPANDING:
    case CS_COLLAPSING:
    {
        QPainter p(this);
        int w = m_grabbedPixmap.width();
        int h = m_grabbedPixmap.height();
        p.drawPixmap(0,m_nCurrentAnimationHeight - h, w, h, m_grabbedPixmap);
    }
        break;
    }
}

void CFoldingContainer::timerEvent(QTimerEvent *event)
{
    int id = event->timerId();
    if (m_nTimerId != id) return;

    killTimer(m_nTimerId);
    m_nTimerId = -1;

    if (m_pwgtContent == 0) return;

    bool stop = true;
    switch (m_eComponentState) {
    case CS_NO_CONTENT:
    case CS_EXPANDED:
    case CS_COLLAPSED:
        break;
    case CS_EXPANDING:
        stop = expandAtom();
        break;
    case CS_COLLAPSING:
        stop = collapseAtom();
        break;
    }

    if ( ! stop) {
        m_nTimerId = startTimer(m_nTimerPeriodMs);
    }
}

bool CFoldingContainer::expandAtom()
{
    int delta;
    bool stop = false;
    if (m_nCurrentAnimationHeight + m_nAnimationStepPx < m_nExpandedHeight) {
        delta = m_nAnimationStepPx;
    } else {
        delta = m_nExpandedHeight - m_nCurrentAnimationHeight;
        stop = true;
        m_eComponentState = CS_EXPANDED;
        m_pwgtContent->setVisible(true);
        emit signalExpandOk();
    }
    m_nCurrentAnimationHeight += delta;

    if (stop) {
        setMinimumHeight(0);
        setMaximumHeight(QWIDGETSIZE_MAX);
    } else {
        setFixedHeight(m_nCurrentAnimationHeight);
    }

    return stop;
}

bool CFoldingContainer::collapseAtom()
{
    int delta;
    bool stop = false;
    if (m_nCurrentAnimationHeight > m_nAnimationStepPx) {
        delta = m_nAnimationStepPx;
    } else {
        delta = m_nCurrentAnimationHeight;
        stop = true;
        m_eComponentState = CS_COLLAPSED;
        emit signalCollapseOk();
    }
    m_nCurrentAnimationHeight -= delta;

    if (stop) {
        setMinimumHeight(0);
        setMaximumHeight(QWIDGETSIZE_MAX);
    } else {
        setFixedHeight(m_nCurrentAnimationHeight);
    }

    return stop;
}
