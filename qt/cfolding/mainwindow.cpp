#include "mainwindow.h"

#include "c-panel.h"
#include "c-folding-container.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QScrollArea>
#include <QLabel>
#include <QPixmap>
#include <QLineEdit>
#include <QTextEdit>
#include <QTableWidget>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setWindowTitle(tr("Qt. Task 1. Animation"));
    createFormEnvironment();
}

MainWindow::~MainWindow()
{
    
}

void MainWindow::createFormEnvironment()
{
    m_pwgtMain = new QWidget(this);
    setCentralWidget(m_pwgtMain);

    QHBoxLayout *hbox = new QHBoxLayout(m_pwgtMain);
    hbox->setSpacing(3);
    hbox->setMargin(3);

    m_pwgtLeft = new QWidget(m_pwgtMain);
    hbox->addWidget(m_pwgtLeft);
    hbox->setStretch(0, 1);

    m_pwgtCenter = new QWidget(m_pwgtMain);
    hbox->addWidget(m_pwgtCenter);
    hbox->setStretch(1, 1);

    m_pwgtRight = new QWidget(m_pwgtMain);
    hbox->addWidget(m_pwgtRight);
    hbox->setStretch(2, 1);

    createLeftEnvironment();
    createCenterEnvironment();
    createRightEnvironment();
}

void MainWindow::createLeftEnvironment()
{
    QVBoxLayout *pvblMain = new QVBoxLayout(m_pwgtLeft);
    pvblMain->setSpacing(0);
    pvblMain->setMargin(3);

    QScrollArea *sa = new QScrollArea(m_pwgtLeft);
    {
        QWidget *wgt = new QWidget(sa);

        QVBoxLayout *vbox = new QVBoxLayout(wgt);

        {
            CPanel *pnl = new CPanel(wgt);
            QLabel *lb = new QLabel(pnl);
            QPixmap pixmap(":/images/images/cat_200px.jpg");
            lb->setPixmap(pixmap);
            //lb->setText("It's a tiger!");
            lb->setAlignment(Qt::AlignCenter);
            pnl->foldingContainer()->setContent(lb);
            vbox->addWidget(pnl);
        }
        {
            CPanel *pnl = new CPanel(wgt);
            QLineEdit *edit = new QLineEdit(pnl);
            edit->setText("Hello world");
            pnl->foldingContainer()->setContent(edit);
            vbox->addWidget(pnl);
        }
        {
            CPanel *pnl = new CPanel(wgt);
            QTextEdit *edit = new QTextEdit(pnl);
            fillEdit(edit, 10);
            pnl->foldingContainer()->setContent(edit);
            vbox->addWidget(pnl);
        }
        {
            CPanel *pnl = new CPanel(wgt);
            CPanel *in_pnl = new CPanel(wgt);
            QTextEdit *edit = new QTextEdit(pnl);
            fillEdit(edit, 12);
            in_pnl->foldingContainer()->setContent(edit);
            pnl->foldingContainer()->setContent(in_pnl);
            vbox->addWidget(pnl);
        }
        sa->setWidget(wgt);

        vbox->addStretch(1);
    }
    pvblMain->addWidget(sa);
}

void MainWindow::createCenterEnvironment()
{
    QVBoxLayout *vbox = new QVBoxLayout(m_pwgtCenter);

    {
        CPanel *pnl = new CPanel(m_pwgtCenter);
        vbox->addWidget(pnl);
        QTextEdit *edit = new QTextEdit(pnl);
        fillEdit(edit, 14);
        pnl->foldingContainer()->setContent(edit);
    }

    {
        CPanel *pnl = new CPanel(m_pwgtCenter);
        vbox->addWidget(pnl);
        QTableWidget *table = new QTableWidget(pnl);
        table->setColumnCount(3);
        table->setRowCount(8);
        pnl->foldingContainer()->setContent(table);
    }

    vbox->addStretch(1);
}

void MainWindow::createRightEnvironment()
{
    QVBoxLayout *vbox = new QVBoxLayout(m_pwgtRight);

    CPanel *pnl = new CPanel(m_pwgtRight);
    vbox->addWidget(pnl);
    QTextEdit *edit = new QTextEdit(pnl);
    fillEdit(edit, 16);
    pnl->foldingContainer()->setContent(edit);

    vbox->addStretch(1);
}

void MainWindow::fillEdit(QTextEdit *edit, int rows)
{
    for(int i=0; i<rows; ++i) {
        QString s = QString("%1: row number %2").arg(edit->metaObject()->className()).arg(i);
        edit->append(s);
    }
}
