#ifndef CPANEL_H
#define CPANEL_H

#include <QWidget>

class QLabel;

class CFoldingContainer;

/** @class CPanel
  * @ingroup Task_1
  * @brief Виджет панели управления объектом класса CFoldingContainer
  *
  * Расширением поставленной задачи является реализация контекстного меню на области заголовка
  * позволяющая управлять разрешением и длительностью анимации.
  */
class CPanel : public QWidget
{
    Q_OBJECT
public:
    explicit CPanel(QWidget *parent = 0);
    
    //! \brief Получить объект класс CFoldingContainer
    CFoldingContainer* foldingContainer() { return m_pFoldingContainer; }    

private slots:
    //! \brief Обработчик события завершения развертывания контента объекта класса CFoldingContainer
    void slotExpandOk();
    //! \brief Обработчик события завершения сворачивания контента объекта класса CFoldingContainer
    void slotCollapseOk();
    //! \brief Обработчик контекстного меню
    void slotContextMenuRequested(QPoint);

private:
    //! \brief Объект управляемого контейнера
    CFoldingContainer *m_pFoldingContainer;
    //! \brief Объект панели заголовка
    QWidget *m_pwgtTitle;

    //! \brief CSS стиль главного виджета заголовка при развернутом контенте
    QString m_sStyleOnExpanded;
    //! \brief CSS стиль главного виджета заголовка при свернутом контенте
    QString m_sStyleOnCollapsed;
    //! \brief CSS стиль главного виджета заголовка при развернутом контенте (режим hover)
    QString m_sStyleOnExpanded_hover;
    //! \brief CSS стиль главного виджета заголовка при свернутом контенте (режим hover)
    QString m_sStyleOnCollapsed_hover;
    //! \brief CSS стиль объекта с текстом (подчеркивание)
    QString m_sTitleUnderlineStyle;
    //! \brief CSS стиль объекта с текстом (без подчеркивания)
    QString m_sTitleNormalStyle;
    //! \brief CSS стиль объекта со значком "плюс"
    QString m_sPlusStyle;
    //! \brief CSS стиль объекта со значком "минус"
    QString m_sMinusStyle;

    //! \brief Объект с текстом "Expanded"/"Collapsed"
    QLabel *m_plbTitle;
    //! \brief Объект с изображением значков "плюс"/"минус"
    QLabel *m_plbPlusMinus;
    //! \brief Объект с изображением значка "закрыть"
    QLabel *m_plbClose;
    //! \brief Флаг режима hover используемый для правильной установки стилей @sa slotExpandOk(), slotCollapseOk()
    bool m_bTitleOnHoover;

    //! \brief Создание и размещение виджетов на объекте панели CPanel
    void createFormEnvironment();
    //! \brief Выбор и задание стилей виджетов заголовка по текущему состоянию объекта
    void titleStatusChanged();

    //! \brief Фильтр для перехвата событий заголовка
    bool eventFilter(QObject *obj, QEvent *event);
};

#endif // CPANEL_H
