#include "c-panel.h"

#include <QVBoxLayout>
#include <QLabel>
#include <QtCore/QEvent>
#include <QtGui/QMouseEvent>
#include <QMenu>
#include <QInputDialog>

#include "c-folding-container.h"

CPanel::CPanel(QWidget *parent) :
    QWidget(parent)
{
    m_bTitleOnHoover = false;

    m_sStyleOnExpanded = "* "
            " { "
            " border-width: 1px; "
            " border-style: solid; "
            " border-top-left-radius: 10px; "
            " border-top-right-radius: 10px; "
            " border-color: white; "
            " padding: 5px; "
            " background: qlineargradient(x1:0.5, y1:0, x2:0.5, y2:1, "
            " stop:0 #33CCCC, stop: 0.4 #33FFFF, stop:1 #339999) "
            " }";

    m_sStyleOnCollapsed = "* "
            " { "
            " border-width: 1px; "
            " border-style: solid; "
            " border-top-left-radius: 10px; "
            " border-top-right-radius: 10px; "
            " border-color: white; "
            " padding: 5px; "
            " background: qlineargradient(x1:0.5, y1:0, x2:0.5, y2:1, "
            " stop:0 #33CCCC, stop: 0.4 #33FFCC, stop:1 #009966) "
            " }";

    m_sStyleOnExpanded_hover = "* "
            " { "
            " border-width: 1px; "
            " border-style: solid; "
            " border-top-left-radius: 10px; "
            " border-top-right-radius: 10px; "
            " border-color: white; "
            " padding: 5px; "
            " background: qlineargradient(x1:0.5, y1:0, x2:0.5, y2:1, "
            " stop:0 #55EEEE, stop: 0.4 #33FFFF, stop:1 #228888) "
            " }";

    m_sStyleOnCollapsed_hover = "* "
            " { "
            " border-width: 1px; "
            " border-style: solid; "
            " border-top-left-radius: 10px; "
            " border-top-right-radius: 10px; "
            " border-color: white; "
            " padding: 5px; "
            " background: qlineargradient(x1:0.5, y1:0, x2:0.5, y2:1, "
            " stop:0 #55EEEE, stop: 0.4 #33FFCC, stop:1 #008855) "
            " }";

    m_sTitleUnderlineStyle = "* { border: none; background-color: rgba(0,0,0,0); text-decoration: underline; }";
    m_sTitleNormalStyle    = "* { border: none; background-color: rgba(0,0,0,0); text-decoration: none; }";

    m_sPlusStyle  = "* { background-image: url(:/images/images/plus.png); }";
    m_sMinusStyle = "* { background-image: url(:/images/images/minus.png); }";

    createFormEnvironment();
}

void CPanel::createFormEnvironment()
{
    QVBoxLayout *vbox = new QVBoxLayout(this);
    vbox->setSpacing(0);
    vbox->setMargin(0);

    {
        m_pwgtTitle = new QWidget(this);
        m_pwgtTitle->setStyleSheet(m_sStyleOnExpanded);
        m_pwgtTitle->setMouseTracking(true);
        m_pwgtTitle->setAttribute(Qt::WA_Hover);
        m_pwgtTitle->installEventFilter(this);
        {
            QHBoxLayout *hbox = new QHBoxLayout(m_pwgtTitle);
            {
                m_plbPlusMinus = new QLabel(m_pwgtTitle);
                m_plbPlusMinus->setFixedSize(18,18);
                m_plbPlusMinus->setStyleSheet(m_sMinusStyle);
                hbox->addWidget(m_plbPlusMinus);
            }

            {
                m_plbTitle = new QLabel(tr("Expanded"), m_pwgtTitle);
                m_plbTitle->setStyleSheet(m_sTitleNormalStyle);
                hbox->addWidget(m_plbTitle);
            }

            hbox->addStretch(1);

            {
                m_plbClose = new QLabel(m_pwgtTitle);
                m_plbClose->setFixedSize(18,18);
                m_plbClose->setStyleSheet("* { background-image: url(:/images/images/close.png);}");
                hbox->addWidget(m_plbClose);
            }

        }
        m_pwgtTitle->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
        vbox->addWidget(m_pwgtTitle);
    }

    m_pFoldingContainer = new CFoldingContainer(this);
    connect(m_pFoldingContainer, SIGNAL(signalExpandOk()), this, SLOT(slotExpandOk()));
    connect(m_pFoldingContainer, SIGNAL(signalCollapseOk()), this, SLOT(slotCollapseOk()));
    vbox->addWidget(m_pFoldingContainer, Qt::AlignTop);
}

void CPanel::slotExpandOk()
{
    m_plbTitle->setText(tr("Expanded"));
    m_plbPlusMinus->setStyleSheet(m_sMinusStyle);
    titleStatusChanged();
}

void CPanel::slotCollapseOk()
{
    m_plbTitle->setText(tr("Collapsed"));
    m_plbPlusMinus->setStyleSheet(m_sPlusStyle);
    titleStatusChanged();
}

void CPanel::slotContextMenuRequested(QPoint pos)
{
    QPoint globalPos = m_pwgtTitle->mapToGlobal(pos);

    QMenu myMenu;
    QAction *pactAnimation = myMenu.addAction(tr("Animation"));
    pactAnimation->setCheckable(true);
    pactAnimation->setChecked(m_pFoldingContainer->getAnimationEnabled());

    myMenu.addSeparator();

    QAction *pactDuration = myMenu.addAction(tr("Duration ..."));


    QAction* selectedItem = myMenu.exec(globalPos);
    if (selectedItem == pactAnimation) {
        m_pFoldingContainer->setAnimationEnabled(pactAnimation->isChecked());
    } else if (selectedItem == pactDuration) {
        bool ok;
        int i = QInputDialog::getInt(this, tr("Input Duration"),
                                     tr("Duration [ms]:"),
                                     m_pFoldingContainer->getAnimationDuration(),
                                     100, 50000, 50, &ok);
        if (ok) {
            m_pFoldingContainer->setAnimationDuration(i);
        }
    }
}

bool CPanel::eventFilter(QObject *obj, QEvent *event)
{
    QWidget *wgt = qobject_cast<QWidget*>(obj);
    if (wgt == m_pwgtTitle) {
        QEvent::Type type = event->type();
        if  (type == QEvent::HoverLeave) {

            m_pwgtTitle->setCursor(Qt::ArrowCursor);
            m_plbTitle->setStyleSheet(m_sTitleNormalStyle);
            m_bTitleOnHoover = false;
            titleStatusChanged();

        } else if (type == QEvent::HoverEnter) {

            m_pwgtTitle->setCursor(Qt::PointingHandCursor);
            m_plbTitle->setStyleSheet(m_sTitleUnderlineStyle);
            m_bTitleOnHoover = true;
            titleStatusChanged();

        } else if (type == QEvent::MouseButtonPress) {

            QMouseEvent *mev = dynamic_cast<QMouseEvent*>(event);
            if (mev) {
                if (mev->button() == Qt::LeftButton) {
                    int x = mev->x() - m_plbClose->x();
                    int y = mev->y() - m_plbClose->y();
                    if (m_plbClose->rect().contains(x,y)) {
                        deleteLater();
                    } else {
                        if (m_pFoldingContainer->isExpanded()) {
                            m_pFoldingContainer->collapse();
                        } else {
                            if (m_pFoldingContainer->isCollapsed()) {
                                m_pFoldingContainer->expand();
                            }
                        }
                    }
                } else {
                    QPoint pos = mev->pos();
                    slotContextMenuRequested(pos);
                }
            }

        }
    }
    return QWidget::eventFilter(obj, event);
}

void CPanel::titleStatusChanged()
{
    if (m_bTitleOnHoover) {
        if (m_pFoldingContainer->isExpanded()) {
            m_pwgtTitle->setStyleSheet(m_sStyleOnExpanded_hover);
        } else {
            if (m_pFoldingContainer->isCollapsed()) {
                m_pwgtTitle->setStyleSheet(m_sStyleOnCollapsed_hover);
            }
        }
    } else {
        if (m_pFoldingContainer->isExpanded()) {
            m_pwgtTitle->setStyleSheet(m_sStyleOnExpanded);
        } else {
            if (m_pFoldingContainer->isCollapsed()) {
                m_pwgtTitle->setStyleSheet(m_sStyleOnCollapsed);
            }
        }
    }
}
