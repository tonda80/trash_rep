#ifndef CFOLDINGCONTAINER_H
#define CFOLDINGCONTAINER_H

#include <QWidget>
#include <QtGui/QPixmap>

class QVBoxLayout;

/** @defgroup Task_1 Главные классы задачи
  *
  * \brief Группу образуют классы составляющие суть решения поставленной задачи.
  */


/** @class CFoldingContainer
  * @ingroup Task_1
  * @brief Объект виджет позволяющий управлять анимационным сокрытием и развертыванием своего содержимого
  *
  */
class CFoldingContainer: public QWidget
{
    Q_OBJECT
public:
    CFoldingContainer(QWidget *parent = 0);

    //! Set animation mode.
    void setAnimationEnabled(
            bool enable //!< Use animated operations if true, do operations immediately if false
            );

    //! \brief Получить флаг разрешения анимации
    bool getAnimationEnabled() { return m_bIsAnimationEnabled; }

    //! Set animation duration.
    void setAnimationDuration(
            int period=300 //!< Animation duration in milliseconds
            );

    //! \brief Получить длительность анимации
    int getAnimationDuration() { return m_nAnimationDurationMs; }

    //! Set content widget.
    /**
    This component will own the content widget, i.e. there is no need to destroy it elsewhere
    */
    void setContent(
            QWidget* content_ptr //!< Widget that will be used as content for this component
            );

    //! \brief Проверить состояние "Развернуто"
    bool isExpanded() { return m_eComponentState == CS_EXPANDED; }

    //! \brief Проверить состояние "Свернуто"
    bool isCollapsed() { return m_eComponentState == CS_COLLAPSED; }

public slots:
    //! \brief Развернуть контент
    void expand();

    //! \brief Свернуть контент
    void collapse();

    //! Expand or collapse content depending on the “enable” argument.
    void setExpanded(
            bool enable //!< Expand if true, collapse if false
            );
private:
    //! \brief Управляемый виджет переданного контента
    QWidget *m_pwgtContent;
    //! \brief Объект сохраняющий "фотографию" развернутого контента для анимации
    QPixmap m_grabbedPixmap;
    //! \brief Флаг разрешения анимации
    bool m_bIsAnimationEnabled;
    //! \brief Переменная общей длительности анимации
    int m_nAnimationDurationMs;
    //! \brief Идентификатор таймера анимации
    int m_nTimerId;
    //! \brief Переменная с вычисленным периодом текущей анимации
    int m_nTimerPeriodMs;
    //! \brief Переменная с пиксельным шагом текущей анимации
    int m_nAnimationStepPx;
    //! \brief Переменная для сохранения полной высоты виджета развернутого контента
    int m_nExpandedHeight;
    //! \brief Переменная для сохранения текущей высоты виджета контента в анимации
    int m_nCurrentAnimationHeight;

    //! \brief Тип состояния объекта управления контентом
    typedef enum {
        CS_NO_CONTENT, //!< Состояние "Нет контента"
        CS_EXPANDED,   //!< Состояние "Развернуто"
        CS_EXPANDING,  //!< Состояние "Анимация разворачивания"
        CS_COLLAPSED,  //!< Состояние "Свернуто"
        CS_COLLAPSING  //!< Состояние "Анимация сворачивания"
    } ComponentState_t;

    //! \brief Переменная состояния объекта управления контентом
    ComponentState_t m_eComponentState;

    //! \brief Итерация развертывания, исполняемая по событию от таймера анимации
    bool expandAtom();
    //! \brief Итерация свертывания, исполняемая по событию от таймера анимации
    bool collapseAtom();

    //! \brief Обработчик события от таймера анимации
    void timerEvent(QTimerEvent * event);

    //! \brief Обработчик события отрисовки поля виджета
    void paintEvent(QPaintEvent *);
signals:
    //! \brief Сигнал информирующий об успешном завершении разворачивания контента
    void signalExpandOk();

    //! \brief Сигнал информирующий об успешном завершении сворачивания контента
    void signalCollapseOk();
};

#endif // CFOLDINGCONTAINER_H
