#-------------------------------------------------
#
# Project created by QtCreator 2012-06-19T14:12:35
#
#-------------------------------------------------

QT       += core widgets

TARGET = folder-test
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    c-folding-container.cpp \
    c-panel.cpp

HEADERS  += mainwindow.h \
    c-folding-container.h \
    c-panel.h

RESOURCES += \
    folder-test-res.qrc
