// Сделаем тут минимальный пример работы с базами в qt
//   и заодно проверим проблему с работой в разных потоках

// cmake --build
// apt install libqt5sql5-mysql     этого достаточно для загрузки
// docker run --name my_mysql --rm -p3306:3306 -e MYSQL_ROOT_PASSWORD=pwd -e MYSQL_DATABASE=db1 mysql:8.0
// apt install mysql-client         для теста
// mysql -hlocalhost -ppwd -P3306 -uroot --protocol=tcp db1


#include <QCoreApplication>
#include <QDebug>
#include <QMap>

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>

#include <thread>


struct DbSetup {
    const char* type = nullptr;
    const char* db_name = nullptr;
    const char* host = nullptr;
    const char* user = nullptr;
    int port = 0;
    const char* pwd = nullptr;
};

constexpr DbSetup sqlite_setup {
    .type = "QSQLITE",
    .db_name = "./qt.sqlite",
};

constexpr DbSetup mysql_setup {
    .type = "QMYSQL",
    .db_name = "db1",
    .host = "127.0.0.1",        // localhost не работает!)
    .user = "root",
    .port = 3306,
    .pwd = "pwd",
};

const DbSetup& db_type_select() {
    auto args = QCoreApplication::arguments();
    QString db_arg;
    if (args.size() > 1) db_arg = args.at(1);

    QMap<QString, const DbSetup*> setups = {
        {"sqlite", &sqlite_setup},
        {"mysql", &mysql_setup},
    };

    const auto it = setups.find(db_arg);
    if (it != setups.end()) {
        return *it.value();
    }

    qDebug() << "Possible arguments:" << setups.keys();
    throw std::runtime_error("Choose db type, e.g. \"./db_ex sqlite\"");
}


QSqlDatabase get_db(const DbSetup &db_setup, const QString& conn_name) {
    auto db = QSqlDatabase::addDatabase(db_setup.type, conn_name);

    db.setDatabaseName(db_setup.db_name);
    if (db_setup.host) db.setHostName(db_setup.host);
    if (db_setup.port) db.setPort(db_setup.port);
    if (db_setup.user) db.setUserName(db_setup.user);
    if (db_setup.pwd) db.setPassword(db_setup.pwd);

    if (!db.open()) {
        qDebug() << db.lastError().text();
        throw std::runtime_error("db open error");
    }

    qDebug() << "db is open" << db_setup.type << db_setup.db_name;
    return db;
}

void exec(QSqlDatabase& db, const QString& req, bool strict = true) {
    QSqlQuery query(db);
    qDebug() << "exec" << req;

    if (!query.exec(req)) {
        qDebug() << query.lastError().text();
        if (!strict) return;
        throw std::runtime_error("query exec error");
    }

    while (query.next()) {
        auto rec = query.record();
        auto out = qDebug().nospace();
        for (int i = 0; i < rec.count(); ++i) {
            out << "  " << rec.fieldName(i) << "=" << rec.value(i).toString();
        }
    }
}


void database_action(const QString& conn_name, bool read_only) {
    auto db = get_db(db_type_select(), conn_name);

    if (!read_only) {
        exec(db, "DROP TABLE tb1", false);
        exec(db, "CREATE TABLE tb1 (id INT, name VARCHAR(256))");
        exec(db, "INSERT INTO tb1 (id, name) VALUES (1, 'Vasya')");
        exec(db, "INSERT INTO tb1 (id, name) VALUES (2, 'Petya')");
        exec(db, "INSERT INTO tb1 (id, name) VALUES (3, 'Kolya')");
        exec(db, "UPDATE tb1 SET name = 'Masha' WHERE id = 3");
    }
    exec(db, "SELECT * FROM tb1");

    db.close();
}

void thread_func() {
    qDebug() << "thread " << 1;
    database_action("thread_conn", true);
}

void main_func() {
    qDebug() << "main " << 1;
    database_action("main_conn", true);
}


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    qDebug() << "Accessible db drivers:" << QSqlDatabase::drivers();

    database_action("init_conn", false);

    std::thread thr(thread_func);
    main_func();
    thr.join();

    return 0;
}
