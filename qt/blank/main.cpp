#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QProcess>
#include <QThread>
#include <QDir>
#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDateTime>
//#include <QtSerialPort/QSerialPort>

#include "../../../common_rep/cpp/common.h"

struct C {
    C(int i_ = 0) : i(i_) {pp("C", i);}
    ~C() {pp("~C", i);}
    C(const C& o) : i(o.i) {pp("C&", i);}
    C(C&& o) : i(o.i) {pp("C&&", i);}
    C& operator=(const C& o) {
        i = o.i;
        pp("C=", i);
        return *this;
    }
    C& operator=(C&& o) {
        i = o.i;
        pp("C=&&", i);
        return *this;
    }

    void do_smth() const {pp("C::do_smth", i);}

    int i;
};

void f1() {
    QList<int> l1, l2;
    l1 << 1 << 2 << 3;

    qDebug() << "1" << l1 << l2;

    l2 = l1;
    l1 << 4;
    l2 << 5;

    qDebug() << "2" << l1 << l2;

    l1 = l2;

    qDebug() << "3" << l1 << l2;
}

void plistc(const char* what, const QList<C>& ll) {
    pp("list", what);
    for (auto& c: ll) {
        pp("C(", c.i, ")");
    }
}

void f2() {
    QList<C> l1, l2;

    C c1(1), c2(2);

    p("f2 work");

    l1 << c1 << c2 << 3;

    p("1 --------");
    plistc("l1", l1);
    plistc("l2", l2);

    l2 = l1;

    p("2 --------");
    plistc("l1", l1);
    plistc("l2", l2);

    c1.i = 999;
    p("changing");
    l1[0].i = 888;
    l2[1].i = 777;

    p("3 --------");
    plistc("l1", l1);
    plistc("l2", l2);

    p("4 --------");
}

bool is_working_process(const QByteArray& cmd) {
    QProcess psProc;
    psProc.start("ps", {"-e", "-o", "command"});
    psProc.waitForFinished();
    auto out = psProc.readAllStandardOutput();
    //qDebug() << QString("out='{%1}'").arg(QString(out).split("\n").join("|"));
    for (const auto& line : out.split('\n')) {
        if (line.contains(cmd)) {
            return true;
        }
    }
    return false;
}

bool is_working_process2(const QString& cmd) {
    QProcess psProc;
    psProc.start("ps", {"-e", "-o", "command"});      // смотрим все процессы
    psProc.waitForFinished();
    QString out = psProc.readAllStandardOutput();
    for (const auto& line : out.split('\n')) {
        if (line == cmd || line.startsWith(cmd + " ")) {
            return true;
        }
    }
    return false;
}

QString fileInfoMark(const QFileInfo& fi) {
    if (fi.isDir()) {
        return "D";
    }
    else if (fi.isFile()) {
        return "F";
    }
    return "?";
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QFile f("/home/anton/temp/qttf.txt");
    qDebug() << f.open(QIODevice::ReadOnly) << 1 << 2 << "qwe" << "rty";

//    auto dd = f.readAll();
//    dd += QDateTime::currentDateTime().toString() + "\n";
//    f.resize(0);
//    f.write(dd);
//    f.close();



    return 0;   //a.exec();
}
