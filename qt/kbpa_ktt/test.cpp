#include "test.h"

Test::Test()
{

    QHBoxLayout* playout1 = new QHBoxLayout(this);

            MyAGR * agr = new MyAGR;
            textedit1 =new QTextEdit;
            textedit1->setFixedHeight(25);
            textedit2 =new QTextEdit;
            textedit2->setFixedHeight(25);
            QLabel * label1= new QLabel("Крен");
            QLabel * label2= new QLabel("Тангаж");
            label1->setBuddy(textedit1);
            label2->setBuddy(textedit2);
            connect(textedit1, SIGNAL(textChanged()), this, SLOT (slotRoll()));
            connect(textedit2, SIGNAL(textChanged()), this, SLOT (slotPitch()));
            connect(this, SIGNAL(signalPitch(float)), agr, SLOT (slotPitchchange(float)));
            connect(this, SIGNAL(signalRoll(float)), agr, SLOT (slotRollchange(float)));

            QVBoxLayout* playout = new QVBoxLayout;
            // а вот так можно => playout->setContentsMargins(1,1,1,1);
            playout->setSpacing(1);
            playout->addWidget(label1);
            playout->addWidget(textedit1);
            playout->addWidget(label2);
            playout->addWidget(textedit2);
            playout->addStretch();

            // так было => playout1->setMargin(1);
            playout1->setSpacing(1);
            playout1->addLayout(playout);
            playout1->addWidget(agr);

            QPalette pal;
                QBrush qbr(Qt::darkGray);
                pal.setBrush(this->backgroundRole(),qbr);
                this->setPalette(pal);




            this->setLayout(playout1);
}

void Test::slotRoll()
{
    float f=(textedit1->toPlainText()).toFloat();
     f= (f>60)? 60 : (f<-60)? -60 : f;
    emit signalRoll(f);
}

void Test::slotPitch()
{
    float f=(textedit2->toPlainText()).toFloat();
     f= (f>60)? 60 : (f<-60)? -60 : f;
    emit signalPitch(f);
}
