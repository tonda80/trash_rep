#ifndef MYAGRCPP_H
#define MYAGRCPP_H

//#include <QObject>
#include <QWidget>

class MyAGR : public QWidget
{
    Q_OBJECT
private:
    float _roll=0;
    float _pitch=0;
    float cur_roll=0;
    float cur_pitch=0;
    const float scale_pitch=700/90;
    const float scale_roll=3.14/180;

public:
    MyAGR(QWidget *parent=nullptr);
    QRegion createClipRegion(bool top=true);
    void start();

protected:
    void paintEvent(QPaintEvent * event) override;

public slots:
    void slotRollchange(float roll);
    void slotPitchchange(float pitch);
    void slotUpdate();
};

#endif // MYAGRCPP_H
