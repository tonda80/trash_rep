#include "test.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Test wgt;
    wgt.show();

    return a.exec();
}
