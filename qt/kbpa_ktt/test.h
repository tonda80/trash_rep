#ifndef TEST_H
#define TEST_H
#include <QWidget>
#include <QtWidgets>
#include "myagrcpp.h"

class Test: public QWidget
{
     Q_OBJECT
public:
    QTextEdit *textedit1;
    QTextEdit *textedit2;

    Test();

signals:
    void signalRoll(float);
    void signalPitch(float);

    public slots:
    void slotRoll();
    void slotPitch();
};

#endif // TEST_H
