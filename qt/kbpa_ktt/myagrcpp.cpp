#include "myagrcpp.h"
#include <QtWidgets>
#include <QPaintEvent>
#include <QtMath>


class QPaintEvent;


MyAGR::MyAGR(QWidget *parent) : QWidget(parent)
{
    this->setFixedSize(QSize(700,700));
    start();

}


QRegion MyAGR::createClipRegion(bool top)
{
    int x1=this->rect().topLeft().x();
    int y1=this->rect().topLeft().y()+350;
    int x2=this->rect().topRight().x();
    int y2=this->rect().topRight().y()+350;

    float fy1=y1+cur_pitch*scale_pitch;
    float fy2=y2+cur_pitch*scale_pitch;
    y1=static_cast<int>(fy1-qTan(cur_roll*scale_roll)*350);
    y2=static_cast<int>(fy2+qTan(cur_roll*scale_roll)*350);


    if (top)
    {
       QRegion region(QPolygon() << QPoint(x1, y1) << QPoint(x2, y2)  << this->rect().bottomRight()<< this->rect().bottomLeft());
       return region;
    }
    else
    {
       QRegion region(QPolygon() << QPoint(x1, y1) << QPoint(x2, y2)  << this->rect().topRight()<< this->rect().topLeft());
       return region;
    }
}


void MyAGR::paintEvent(QPaintEvent * event)
{
    QPainter painter;
        painter.begin(this);
        painter.setRenderHint(QPainter::Antialiasing, true);
        painter.setPen(QPen(Qt::black,3));

        painter.setClipRegion(createClipRegion(true));
        painter.setBrush(QBrush(Qt::red));
        painter.drawEllipse(10, 10, 680, 680);

        painter.setClipRegion(createClipRegion(false));
        painter.setBrush(QBrush(Qt::blue));
        painter.drawEllipse(10, 10, 680, 680);


        painter.setClipRegion(QRegion(this->rect()));
        painter.setPen(QPen(Qt::white,5));
        painter.setBrush(QBrush(Qt::transparent));
        painter.drawEllipse(340, 340, 20, 20);
        painter.drawLine(QLine(270, 350,340, 350));
        painter.drawLine(QLine(360, 350,430, 350));



        painter.end();


    QWidget::paintEvent(event);

}

void MyAGR::slotRollchange(float roll)
{
    _roll=roll;
}

void MyAGR::slotPitchchange(float pitch)
{
    _pitch=pitch;
}

 void MyAGR::start()
 {

         QTimer::singleShot(20, this, SLOT(slotUpdate()));
 }

 void MyAGR::slotUpdate()
 {
     cur_roll=(abs(cur_roll-_roll)<40*scale_roll)? cur_roll : (cur_roll>_roll)? cur_roll-20*scale_roll : cur_roll+20*scale_roll;
     cur_pitch=(abs(cur_pitch-_pitch)<0.1*scale_pitch)? cur_pitch : (cur_pitch>_pitch)? cur_pitch-scale_pitch/20 : cur_pitch+scale_pitch/20;

     this->update();
     start();
 }
