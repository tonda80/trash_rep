import readline

def fibo(n):
	assert n > 1
	ll = [0, 1]

	while True:
		s = ll[-1]+ll[-2]
		if s > n:
			break
		ll.append(s)

	return ll

was_failed = False
while 1:
	try:
		n = int(input())
	except KeyboardInterrupt:
		break
	except:
		print('Не то вводишь! Давай заново.')
		was_failed = True
		continue

	if was_failed:
		print('Ну наконец то что надо!')
	was_failed = False

	print(f'{n} -> {fibo(n)}')

print('Пока!')
