
# Программа угадывающая и загадывающая числа

import random


def guess_number(min_n, max_n):
	assert min_n < max_n, 'Less is not less'
	print(f'Загадай число от {min_n} до {max_n}(невключительно).')
	while True:
		#print(f'__deb min_n={min_n} max_n={max_n}')
		if min_n == max_n:
			print('Где-то ты меня обманул! Я обиделся, пока! :-(')
			return
		n = (min_n + max_n)//2
		answer = is_this_number(n)
		if answer == 'б':
			min_n = n + 1
		elif answer == 'м':
			max_n = n
		elif answer == 'р':
			print('Ура!!! Я угадал! Спасибо за игру, пока!')
			return
		else:
			assert False

def is_this_number(n):
	while True:
		answer = input(f'Это {n}? (б/м/р) ')
		if answer == 'б' or answer == 'м' or answer == 'р':
			return answer
		else:
			print('Не понимаю. Ответь: б[ольше], м[еньше], р[авно].')


def propose_number(min_n, max_n):
	assert min_n < max_n, 'Less is not less'
	print(f'Я буду загадывать число от {min_n} до {max_n} (невключительно), а ты постарайся угадать.')
	my_number = random.randrange(min_n, max_n)
	while True:
		str_answer = input('Введи число: ')
		try:
			answer = int(str_answer)
		except ValueError:
			print(f'По твоему "{str_answer}" это число?')
			continue
		if my_number < answer:
			print('Меньше')
		elif my_number > answer:
			print('Больше')
		else:
			print('Молодец, ты угадал!')
			return





while True:
	propose_number(1, 100)
#guess_number(1, 3)
#guess_number(1, 100)

