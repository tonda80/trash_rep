#ifndef MNT2APP_H
#define MNT2APP_H

#include <string>
#include <memory>
#include <atomic>

#include <zmqpb.h>

#include "utils.h"


struct mnt2app_error : public std::runtime_error {
    mnt2app_error(const std::string& msg) : std::runtime_error("mnt2app_error: "+msg) {}
};


class Mnt2App : private utils::Noncopyable
{
public:
    static Mnt2App& instance() {
        static Mnt2App instance;
        return instance;
    }

    void init(const std::string_view module_name_, const std::string_view router_client_name_);

    zmqpb::RouterClient* router_client() {return router_client_ptr.get();}

    const Json::Value& config() {return *config_ptr;}           // TODO? лучше инициализировать конфиг приложения на старте
    const std::unique_ptr<const Json::Value>& main_config();

    std::string get_module_name() {return module_name;}

    void zmq_poll_loop();
    void stop_zmq_poll() {zmq_poll_work.store(false);}

    std::unique_ptr<Json::Value> get_configuration(const std::string& deb_config_path = "", const std::string& config_name="", int timeout = 5);
protected:
    Mnt2App() = default;
    std::string module_name;
    std::string router_client_name;

    std::unique_ptr<zmqpb::RouterClient> router_client_ptr;
    std::unique_ptr<Json::Value> config_ptr;

    std::atomic_bool zmq_poll_work{true};
};

#endif // MNT2APP_H
