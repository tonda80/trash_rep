#ifndef COMMON_UTILS_H
#define COMMON_UTILS_H

#include <memory>
#include <string>
#include <sstream>
#include <fstream>

#include <json/json.h>


namespace utils
{
    struct Noncopyable {
        Noncopyable() = default;
        ~Noncopyable() = default;
        Noncopyable(const Noncopyable&) = delete;
        Noncopyable& operator=(const Noncopyable&) = delete;
    };


    // simple way to make single string (c++17)
    class str final : public std::string {
    public:
        template<typename T> str& operator<<(const T& t) {
            if constexpr(std::is_convertible<T, std::string>()) {
                *this += t;
            }
            else {
                *this += std::to_string(t);
            }
            return *this;
        }
    };


    bool has_option(const std::string& option, int argc, const char** argv);

    std::unique_ptr<Json::Value> read_json(const std::string& path, std::string& err);

    Json::StreamWriter* json_fast_writer();

    const Json::Value& req_child(const Json::Value& jvalue, const std::string& name);

    template <typename T>
    std::string hex(T num) {
        std::stringstream stream;
        stream << std::hex << num;
        return stream.str();
    }

    long timestamp_sec();
    long timestamp_ms();

    std::string remove_spaces(const std::string& src);

}

#endif // COMMON_UTILS_H
