#include "utils.h"

#include <algorithm>
#include <fstream>
#include <chrono>

#include <fmt_wrapper.h>

namespace utils
{

bool has_option(const std::string& option, int argc, const char** argv)
{
    auto last = argv + argc;
    return std::find(argv, last, option) != last;
}

std::unique_ptr<Json::Value> read_json(const std::string& path, std::string& err)
{
    std::ifstream ifs(path, std::fstream::in);
    if (!ifs.good()) {
        err = "cannot read file " + path;
        return nullptr;
    }
    Json::Reader reader;
    auto json_value = std::make_unique<Json::Value>();
    if (!reader.parse(ifs, *json_value)) {
        err = "cannot parse file " + path;// + ": " + reader.getFormattedErrorMessages();   // TODO падает в getFormattedErrorMessages на некорректном json вида {1:2}
        return nullptr;
    }
    return json_value;
}

Json::StreamWriter* json_fast_writer() {
    thread_local static Json::StreamWriter* json_fast_writer = nullptr;
    if (!json_fast_writer) {
        Json::StreamWriterBuilder builder;
        builder["commentStyle"] = "None";
        builder["indentation"] = "";
        json_fast_writer = builder.newStreamWriter();
    }
    return json_fast_writer;
}

const Json::Value& req_child(const Json::Value& jvalue, const std::string& name) {
    const Json::Value& ret = jvalue[name];
    if (ret.empty()) {
        throw std::runtime_error(Fmt::format("Config error, no required parameter '{}'", name));
    }
    return ret;
}

long timestamp_sec() {
    return std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

long timestamp_ms() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

std::string remove_spaces(const std::string& src) {
    std::string ret = src;
    ret.erase(std::remove_if(ret.begin(), ret.end(), [](char c){return std::isspace(c);}), ret.end());
    return ret;
}

}   // namespace utils
