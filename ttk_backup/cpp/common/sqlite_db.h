#ifndef SQLITE_DB_H
#define SQLITE_DB_H


#include <sqlite3.h>
#include <string>
#include <memory>
#include <vector>

#include "utils.h"



struct sqlitedb_error : public std::runtime_error {
    sqlitedb_error(const std::string& msg) : std::runtime_error("sqlitedb_error: "+msg) {}
};


class SqliteDbBase : private utils::Noncopyable
{
public:
    virtual ~SqliteDbBase() {close();}
    bool close();

protected:
    SqliteDbBase() = default;
    void open(const char* dbname);

    sqlite3* get_db() {return db;}

    using exec_callback_t = int(*)(void*,int,char**,char**);
    using err_callback_t = void(*)(const char*);

    bool exec_request(const std::string&, exec_callback_t callback = nullptr, void* callback_arg = nullptr) const;

    class Statement final : private utils::Noncopyable {
        //friend class SqliteDbBase;
    public:
        Statement(std::string_view str, SqliteDbBase* o) : statement_str(str) {
            init(o);
        }

        ~Statement() {finalize();}
        bool finalize();

        void reset();

        int step();

        template<typename T>
        void bind(int pos, const T& value)
        {
            if (!statement) {
                throw sqlitedb_error("null statement bind");
            }

            int ret;
            if constexpr(std::is_integral<T>()) {
                if constexpr(sizeof(T) > sizeof(int)) {
                    ret =  sqlite3_bind_int64(statement, pos, value);
                }
                else {
                    ret =  sqlite3_bind_int(statement, pos, value);
                }
            }
            else if constexpr (std::is_same<T, std::string>()) {
                ret = sqlite3_bind_text(statement, pos, value.c_str(), value.size(), SQLITE_TRANSIENT);	// TODO? customize last arg
            }
            else if constexpr (std::is_same<T, std::string_view>()) {
                ret = sqlite3_bind_text(statement, pos, value.data(), value.size(), SQLITE_TRANSIENT);	// TODO? customize last arg
            }
            // TODO double etc
            else {
                value.some_non_existent_method();   // compile time assert
            }
            if (ret != SQLITE_OK) {
                throw sqlitedb_error("bind statement error \"" + statement_str + "\", pos " + std::to_string(pos) +
                                                 ", returned " + std::to_string(ret));  //", value " + std::to_string(value) +
            }
        }

        template<typename T>
        T column(int pos) const
        {
            if (!statement) {
                throw sqlitedb_error("null statement column");
            }
            if (sqlite3_data_count(statement)-1 < pos) {
                throw sqlitedb_error("no column " + std::to_string(pos)+ ": " + statement_str);
            }
            int column_type = sqlite3_column_type(statement, pos);

            // TODO? нужна ли проверка типа?
            if constexpr(std::is_integral<T>()) {
                if (column_type != SQLITE_INTEGER) {
                    throw sqlitedb_error("no int in column " + std::to_string(pos) + ": " + statement_str);
                }
                return sqlite3_column_int64(statement, pos);
            }
            else if constexpr(std::is_same<T, std::string>()) {
                if (column_type != SQLITE_TEXT) {
                    throw sqlitedb_error("no text in column " + std::to_string(pos) + ": " + statement_str);
                }
                return std::string(reinterpret_cast<const char*>(sqlite3_column_text(statement, pos)),
                                   static_cast<size_t>(sqlite3_column_bytes(statement, pos)));
            }
            // TODO double etc
            else {
                T t; t.some_non_existent_method();   // compile time assert
            }
        }

        template<typename... Args>
        int exec(Args... args) {
            reset();
            return bind_and_step(1, args...);
        }

        template<typename... Args>
        const Statement& exec_r(Args... args) {         // for one-line reading
            if (exec(args...) != SQLITE_ROW) {
                throw sqlitedb_error("no data for: "+statement_str);
            }
            return *this;
        }

        template<typename... Args>
        int exec_no_throw(err_callback_t err_clb, Args... args) {
            try {
                return exec(args...);
            }
            catch (sqlitedb_error& e) {
                err_clb(e.what());
            }
            return SQLITE_ERROR;
        }

    private:
        //SqliteDbBase* owner = nullptr;
        std::string statement_str;
        sqlite3_stmt* statement = nullptr;

        void init(SqliteDbBase*);

                                 // no args
        int bind_and_step(int/* pos*/) {
            return step();
        }
        template<typename T>    // final
        int bind_and_step(int pos, T arg) {
            bind(pos, arg);
            return step();
        }
        template<typename T, typename... Args>
        int bind_and_step(int pos, T arg, Args... rest) {
            bind(pos, arg);
            return bind_and_step(++pos, rest...);
        }
    };

    Statement statement(std::string_view str) {return Statement(str, this);}
private:
    sqlite3* db;

};

#endif // SQLITE_DB_H
