#include "fmt_wrapper.h"
#include <fmt/chrono.h>

#include <iostream>
#include <sstream>
#include <map>
#include <ctime>


// g++ -std=c++17 -Wall -I../../thirdparty/fmt/include fmt_wrapper_demo.cpp ../../thirdparty/fmt/src/format.cc
// OR
// g++ -std=c++17 -Wall -Ithirdparty/include -Lthirdparty/lib common/fmt_wrapper/fmt_wrapper_demo.cpp -lfmt

#define P2(x) std::cout << __LINE__ << ") " << #x << " = \"" << x << "\"" << std::endl

struct MyStream : public std::ostringstream {
};

void init_array(char* buf, size_t n, char c) {
    size_t i = 0;
    for (; i < n-1; ++i) {
        buf[i] = c;
    }
    buf[i] = 0;
}


struct MyStruct { int i=7; std::string s = "hello";};

// https://fmt.dev/latest/api.html#formatting-user-defined-types
template <>
struct fmt::formatter<MyStruct> {
    char dbg;
    constexpr auto parse(format_parse_context& ctx) {
        auto it = ctx.begin();
        dbg = *it;
        if (*it != '}') throw format_error("my format error");
        return it;
    }

    template <typename FormatContext>
    auto format(const MyStruct& s, FormatContext& ctx) {
        return format_to(ctx.out(), "i={}, s={}, dbg={}", s.i, s.s, dbg);
    }
};

std::function<void(const std::string&)> Fmt::error_handler = [](const std::string& msg) {std::cerr << "<!>\t" << msg << std::endl;};

int main(int, char**)
{
    std::string s = Fmt::format("The answer is {:03}", 42);
    P2(s);

    std::string bad_s = Fmt::format("ошибка! }", 42);
    P2(bad_s);

    Fmt::print(std::cerr, "float = {:.2}\n", 10.0/3);

    char buf[32];
    init_array(buf ,sizeof (buf), 'Z');
    P2(buf);
    fmt::format_to_n(buf, sizeof(buf), "Writing to buffer, {} ", "BUT!");
    P2(buf);
    // c враппером норм
    init_array(buf ,sizeof (buf), 'Z');
    P2(buf);
    Fmt::format_to_n(buf, sizeof(buf), "Writing to buffer, {} ", "OK!");
    P2(buf);
    //
    Fmt::format_to_n(buf, 0, "ещё ошибка {} {} {} {}", "");


    MyStream ss;
    Fmt::print(ss, "Can I use {}? {}!", "ostringstream", "Yes");
    P2(ss.str());

    Fmt::print("{0} + {0} = {1}\n", 2, 4);
    Fmt::print("и вот такая ошибка {0} + {0} = {1}", 2);

    // именованные аргументы
    s = Fmt::format("{a} != {b}", fmt::arg("a",17), fmt::arg("b", 15));
    P2(s);
    using namespace fmt::literals;
    s = Fmt::format("Hello, {name}! The answer is {number}. Goodbye, {name}.", "name"_a="World", "number"_a=42);
    P2(s);
    bad_s = Fmt::format("{a} != {b}", fmt::arg("a",17));
    // и из map
    std::map<std::string, std::string> mm = {{"k1", "v1"}, {"k2", "v2"}};
    auto m_arg = [&mm](const char* key){return fmt::arg(key, mm[key]);};
    s = Fmt::format("k1={k1}; k2={k2}", m_arg("k1"), m_arg("k2"));
    P2(s);

    // пользовательские типы
    MyStruct data;
    s = Fmt::format("data = {}!", data);
    P2(s);
    bad_s = Fmt::format("{:z}", data);

    // есть FMT_STRING - compile time проверка, но для неё надо использовать родные функции! Ну или допилить враппер.
    //fmt::format(FMT_STRING("{:d}"), "foo");     // тут ошибка компиляции
    fmt::format(FMT_STRING("{:d}"), 33);          // так ок

    // https://fmt.dev/latest/api.html#date-and-time-formatting
    std::time_t t = std::time(nullptr) - 10*3600*24;
    Fmt::print("current time: {:%Y-%m-%d}\n", *std::gmtime(&t));

    return 0;
}

