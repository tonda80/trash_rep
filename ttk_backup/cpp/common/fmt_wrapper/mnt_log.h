#ifndef MNT_LOG_H
#define MNT_LOG_H

#include <lib/logger/logger.h>
#include "fmt_wrapper.h"

namespace mnt {

template <typename S, typename... Args, typename Char = fmt::enable_if_t<fmt::internal::is_string<S>::value, fmt::char_t<S>>>
void log(SyslogOut&& logger, const S& format_str, Args&&... args) {
    if (logger.get_priority() <= SyslogOut::get_max_priority()) {
        Fmt::print(logger, format_str, std::forward<Args>(args)...);
    }
}

} // mnt

#endif // MNT2_LOG_H
