#ifndef FMT_WRAPPER_H
#define FMT_WRAPPER_H

#include <fmt/format.h>
#include <fmt/ostream.h>

#include <functional>


namespace Fmt {

extern std::function<void(const std::string&)> error_handler;

template <typename S>
void report_error(const std::runtime_error& e, const S& format_str) {
    error_handler(std::string(e.what()) + ", format string: \'" + format_str + "'");
}

template <typename S, typename... Args, typename Char = fmt::char_t<S>>
inline std::basic_string<Char> format(const S& format_str, Args&&... args) {
    try {
        return fmt::format(format_str, std::forward<Args>(args)...);
    }
    catch (std::runtime_error& e) {
        report_error(e, format_str);
    }
    return std::basic_string<Char>();
}

template <typename S, typename... Args, typename Char = fmt::enable_if_t<fmt::internal::is_string<S>::value, fmt::char_t<S>>>
void print(std::basic_ostream<Char>& os, const S& format_str, Args&&... args) {
    try {
        fmt::print(os, format_str, std::forward<Args>(args)...);
    }
    catch (std::runtime_error& e) {
        report_error(e, format_str);
    }
}

// лучше использовать mnt::log
//template <typename S, typename... Args, typename Char = fmt::enable_if_t<fmt::internal::is_string<S>::value, fmt::char_t<S>>>
//void print(std::basic_ostream<Char>&& os, const S& format_str, Args&&... args) {
//    Fmt::print(os, format_str, std::forward<Args>(args)...);
//}


template <typename S, typename... Args, fmt::FMT_ENABLE_IF(fmt::internal::is_string<S>::value)>
inline void print(const S& format_str, Args&&... args) {
    try {
        fmt::print(format_str, std::forward<Args>(args)...);
    }
    catch (std::runtime_error& e) {
        report_error(e, format_str);
    }
}

template <typename OutputIt, typename S, typename... Args,
          fmt::FMT_ENABLE_IF(fmt::internal::is_string<S>::value&&fmt::internal::is_output_iterator<OutputIt>::value)>
inline fmt::format_to_n_result<OutputIt> format_to_n(OutputIt out, std::size_t n, const S& format_str, const Args&... args) {
    try {
        auto res = fmt::format_to_n(out, n-1, format_str, args...);
        *(res.out) = 0;
        return {++res.out, res.size+1};
    }
    catch (std::runtime_error& e) {
        report_error(e, format_str);
    }
    return {out, 0};
}

} // namespace Fmt


#endif // FMT_WRAPPER_H
