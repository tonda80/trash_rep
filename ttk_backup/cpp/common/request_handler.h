#ifndef REQUEST_HANDLER_H
#define REQUEST_HANDLER_H

#include <memory>
#include <map>
#include <functional>

#include <mntproto.pb.h>

#include <mnt_log.h>


class IRequestHandler
{
public:
    using Ptr = std::unique_ptr<IRequestHandler>;

    static Ptr create(const pb::MntCommand& mnt_request) {
        auto it = creators.find(mnt_request.header().name());
        if (it == creators.end()) {
            return nullptr;
        }
        return it->second(mnt_request);
    }

    virtual void process() = 0;
    virtual ~IRequestHandler() = default;

private:
    using T_handler_map = std::map<std::string, std::function<Ptr(const pb::MntCommand&)>>;
    static T_handler_map creators;
};


template <typename TDer>
class RequestNoDataHandler : public IRequestHandler
{
public:
    static IRequestHandler::Ptr creator(const pb::MntCommand& req) {
        return create_handler(req);
    }

protected:
    RequestNoDataHandler() = default;

    static std::unique_ptr<TDer> create_handler(const pb::MntCommand& req) {
        auto ret = std::make_unique<TDer>();
        if (!ret->is_needed()) {
            return nullptr;
        }
        ret->request = &req;
        return ret;
    }

    virtual bool is_needed() {return true;}     // subclasses can filter requests here by request

    const pb::MntCommand* request;              // no copying for this moment!!!
};


template <typename TAny, typename TDer>
class RequestHandler : public RequestNoDataHandler<TDer>
{
public:
    static IRequestHandler::Ptr creator(const pb::MntCommand& req) {
        auto ret = RequestNoDataHandler<TDer>::create_handler(req);
        if (!ret) {
            return nullptr;
        }
        const auto& h = req.header();
        if (!req.data().Is<TAny>()) {
            mnt::log(logerror, "Request '{}' has expected type {}, but it contains {}", h.name(), TAny::descriptor()->name(), req.data().type_url());
            return nullptr;
        }
        if (!req.data().UnpackTo(&ret->data)) {
            logerror  << "cannot unpack request " << h.name() << ", it will be ignored";
            return nullptr;
        }

        return ret;
    }

protected:
    TAny data;
};


#endif // REQUEST_HANDLER_H
