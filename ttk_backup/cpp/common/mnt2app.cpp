#include "mnt2app.h"

#include <fstream>
#include <thread>

#include <json/json.h>

#include "request_handler.h"

#include <mnt_log.h>

#include <zmqpb.h>

#include <configurator.pb.h>

void Mnt2App::init(const std::string_view module_name_, const std::string_view router_client_name_)
{
    module_name = module_name_;
    router_client_name = router_client_name_;

    router_client_ptr = std::make_unique<zmqpb::RouterClient>(router_client_name, 1);

    config_ptr = get_configuration(module_name+"_debug_config.json");
    if (!config_ptr) {
        throw mnt2app_error("cannot get configuration");
    }
    loginfo << "loglevel = " << config_ptr->get("loglevel", "");
    SyslogOut::configure(*config_ptr);
}

std::unique_ptr<Json::Value> Mnt2App::get_configuration(const std::string& deb_config_path, const std::string& config_name, int timeout)
{
    auto ret = std::make_unique<Json::Value>();
    Json::Reader reader;

    // local debug config (if any)
    std::ifstream ifs(deb_config_path, std::fstream::in);
    if (ifs.good()) {
        logwarning  << "discovered local debug config file " << deb_config_path << ", using it";
        if (!reader.parse(ifs, *ret)) {
            logerror  << "cannot parse json from local debug config file " << deb_config_path << ": " << reader.getFormattedErrorMessages();
            return nullptr;
        }
        return ret;
    }

    // work config from configurator
    pb::GetConfig pb_request;
    pb_request.set_module(config_name.empty() ? router_client_ptr->client_name() : config_name);
    pb::Config pb_reply;
    zmqpb::MntProtocolRc rc = send_request_get_reply({"CONFIGURATOR"}, "GetConfig", pb_request, pb_reply, router_client(), timeout);
    if(rc == zmqpb::MNT_RPOTOCOL_OK) {
        if (reader.parse(pb_reply.config(), *ret)) {
            mnt::log(loginfo, "got config {} from configurator", pb_request.module());
        }
        else {
            logerror  << "cannot parse json from configurator reply: " << reader.getFormattedErrorMessages();
            return nullptr;
        }
    }
    else {
        logerror  << "GetConfig error. send_request_get_reply returned " << rc;
        return nullptr;
    }
    return ret;
}

const std::unique_ptr<const Json::Value>& Mnt2App::main_config() {
    static std::unique_ptr<const Json::Value> main_cfg;
    if (!main_cfg) {
        main_cfg = Mnt2App::instance().get_configuration("main_debug_config.json", "MAIN");
    }
    return main_cfg;
}

void Mnt2App::zmq_poll_loop() {
    while (zmq_poll_work) {
        pb::MntCommand mnt_request;
        if (!router_client()->wait_request(mnt_request, std::chrono::seconds(1))) {
            continue;
        }

        if (auto req_handler = IRequestHandler::create(mnt_request)) {
            req_handler->process();
        }
        else {
            const auto& h = mnt_request.header();
            logdebug  << "unknown request " << h.name() << " from " << h.source() << ", ignoring";
        }
    }
    loginfo  << "[Mnt2App::zmq_poll_loop] exiting";
}
