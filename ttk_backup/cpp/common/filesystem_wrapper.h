#ifndef FILESYSTEM_WRAPPER_H
#define FILESYSTEM_WRAPPER_H

#if __GNUC__ == 7

#include <experimental/filesystem>
#define _filesystem std::experimental::filesystem

// #define __GNUC__ 7       in __project.config for qtcreator

#else

#error "put your code here"

#endif

#endif // FILESYSTEM_WRAPPER_H
