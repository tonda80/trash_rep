// g++ pcm_bridge.cpp -lpthread -lasound -o pcm_bridge
// apt install libasound2-dev


#include <iostream>
#include <thread>
#include <atomic>
#include <chrono>
#include <termios.h>
#include <csignal>
#include <poll.h>
#include <cstring>
#include <cmath>

#include <alsa/asoundlib.h>


static const char* device = "default";
static const auto pcm_format = SND_PCM_FORMAT_S16;
static const int pcm_format_size = 2;
static const unsigned int channels = 1;
static const unsigned int rate = 8000;


static volatile std::sig_atomic_t work_flag;
static std::atomic_bool dev_ready{false};

static std::atomic_bool mute_mic{false};
static double mute_level = 0;
static int unmute_delay = 0;

snd_pcm_t* pcm_capture_handle()
{
    snd_pcm_t* handle;
    int err;

    if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_CAPTURE, 0)) < 0) {
        std::cerr << "[pcm_capture_handler] snd_pcm_open error: " << snd_strerror(err) << " " << err << std::endl;
        return nullptr;
    }
    if ((err = snd_pcm_set_params(handle,
                                  pcm_format,
                                  SND_PCM_ACCESS_RW_INTERLEAVED,
                                  channels,
                                  rate,
                                  1,            // allow resampling
                                  500000)) < 0) {
        std::cerr << "[pcm_capture_handler] snd_pcm_set_params error: " << snd_strerror(err) << " " << err << std::endl;
        return nullptr;
    }

    std::cout << "[pcm_capture_handler] OK" << std::endl;
    return handle;
}

snd_pcm_t* pcm_playback_handle(void)
{
    snd_pcm_t* handle;
    int err;

    if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
        std::cerr << "[pcm_playback_handler] snd_pcm_open error: " << snd_strerror(err) << " " << err << std::endl;
        return nullptr;
    }
    if ((err = snd_pcm_set_params(handle,
                                  pcm_format,
                                  SND_PCM_ACCESS_RW_INTERLEAVED,
                                  channels,
                                  rate,
                                  1,        // allow resampling
                                  500000)) < 0) {
        std::cerr << "[pcm_playback_handler] snd_pcm_set_params error: " << snd_strerror(err) << " " << err << std::endl;
        return nullptr;
    }

    std::cout << "[pcm_playback_handler] OK" << std::endl;
    return handle;
}


int pOpenVoicePort(const char * port)
{
    struct termios lOptions;

    int lFd = open(port, O_RDWR | O_NDELAY);
    if (lFd != -1) {
        /* read operations are set to blocking according to the VTIME value */
        fcntl(lFd, F_SETFL, FNDELAY);

        tcgetattr(lFd, &lOptions);

        /* Set to 115200 */
        cfsetispeed(&lOptions, B115200);
        cfsetospeed(&lOptions, B115200);

        /* set to 8N1 */
        lOptions.c_cflag &= ~PARENB;
        lOptions.c_cflag &= ~CSTOPB;
        lOptions.c_cflag &= ~CSIZE;
        lOptions.c_cflag |= CS8;
        lOptions.c_iflag &= ~(INPCK | BRKINT |PARMRK | ISTRIP | INLCR | IGNCR | ICRNL);
        lOptions.c_iflag |= IGNBRK;
        lOptions.c_iflag &= ~(IXON|IXOFF|IXANY);
        /* set to RAW mode for input */
        //lOptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

        /* set to RAW mode for output */
        //lOptions.c_oflag &= ~OPOST;
        lOptions.c_oflag=0;
        lOptions.c_lflag=0;
        lOptions.c_cc[VMIN] = 0;
        lOptions.c_cc[VTIME] = 20;

        tcsetattr(lFd, TCSANOW, &lOptions);

        return(lFd);
    }
    return -1;
}

void signal_handler(int/*signal*/) {
    work_flag = 0;
}

void print_timestamp(const std::string& pref)
{
    auto check_point = std::chrono::steady_clock::now();
    auto tstamp_ms = std::chrono::duration_cast<std::chrono::milliseconds>(check_point.time_since_epoch()).count();
    static auto old_tstamp_ms = tstamp_ms;
    std::cout << pref << " " << tstamp_ms - old_tstamp_ms << std::endl;
    old_tstamp_ms = tstamp_ms;
}


double average_s16(const int16_t* buf, size_t size) {
    double sum = 0;
    for (size_t i=0; i<size; ++i) {
        sum += std::abs(buf[i]);
        //std::cout << std::hex << buf[i] << " ";
    }
    return sum/size;
}


void capture_thread(int dev_fd, snd_pcm_t* handle)
{
    const int buf_size = 1600;
    char buf[buf_size];

    snd_pcm_start(handle);
    while (work_flag) {
        if (!dev_ready.load()) {
            //std::cout << "__deb [capture_thread] not ready" << std::endl;
            mute_mic.store(false);
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
            continue;
        }

        //print_timestamp("[capture_thread] loop ");

        auto rd = snd_pcm_readi(handle, buf, buf_size/pcm_format_size);
        if (rd < 0) {
            auto rec_res = snd_pcm_recover(handle, rd, 1);
            if (rec_res < 0) {
                std::cerr << "[capture_thread] snd_pcm_recover error: " << std::strerror(rec_res) << " " << rec_res << " " << rd << std::endl;
                break;
            }
            snd_pcm_start(handle);
            continue;
        }
        else if (rd == 0) {
            continue;
        }
        if (rd != buf_size/pcm_format_size) {
            std::cout << "[capture_thread] unexpectedly readed " << rd << std::endl;
        }

        if (mute_mic.load()) {
            continue;
        }

        //print_timestamp("[capture_thread] before write ");
        auto need_wr = rd*pcm_format_size;
        auto wr = write(dev_fd, buf, need_wr);
        fsync(dev_fd);
        //std::cout << "[capture_thread] tried to write " << need_wr << std::endl;

        if (wr != need_wr) {
            auto res = snd_pcm_reset(handle);
            std::cout << "[capture_thread] unexpectedly written: " << wr << "!=" << need_wr << ", " << res << std::endl;
        }
    }   //while (work_flag)

    std::cout << "[capture_thread] finished" << std::endl;
}


void playback_thread(int dev_fd, snd_pcm_t* handle)
{
    const int buf_size = 640;
    char buf[buf_size];

    int unmute_counter = 0;

    struct pollfd fds;
    fds.fd = dev_fd;
    fds.events = POLLIN;
    while (work_flag) {
        int res = poll(&fds, 1, 250);
        if (res == -1) {
            std::cerr << "[playback_thread] poll error: " << std::strerror(res) << " " << res << std::endl;
            dev_ready.store(false);
            break;
        }
        else if (res == 0) {  // timeout
            dev_ready.store(false);
        }
        else if ( res > 0) {
            if (fds.revents & POLL_IN ) {
                fds.revents = 0;

                dev_ready.store(true);

                auto rd = read(dev_fd, buf, buf_size);
                //std::cout << "[playback_thread] readed bytes " << rd << std::endl;
                if (rd > 0) {
                    double aver = average_s16(reinterpret_cast<int16_t*>(buf), rd/pcm_format_size);
                    bool need_muting = mute_level > 0 && aver > mute_level;
                    //print_timestamp("[playback_thread] after read: " + std::to_string(aver) + " " + std::to_string(need_muting));
                    if (need_muting) {
                        mute_mic.store(true);
                        unmute_counter = unmute_delay;    // 15 => ~300мс
                    }
                    else if (unmute_counter > 0) {
                        --unmute_counter;
                    }
                    else {
                        mute_mic.store(false);
                    }

                    auto wr = snd_pcm_writei(handle, buf, rd/pcm_format_size);
                    if (wr < 0) {
                        auto rec_res = snd_pcm_recover(handle, wr, 1);
                        if (rec_res < 0) {
                            std::cerr << "[playback_thread] snd_pcm_recover error: " << snd_strerror(rec_res) << " " << rec_res << " " << wr << std::endl;
                            break;
                        }
                    }
                }
            }
        }
    }
    std::cout << "[playback_thread] finished" << std::endl;
}



int main(int argc, const char** argv) {
    if (argc != 3) {
        std::cerr << "Usage: ./pcm_bridge NMEADevice MuteThreshold:UnmuteDelay; e.g. ./pcm_bridge /dev/ttyUSB1 100:15\n";
        return -1;
    }

    const char* serial_device_name = argv[1];

    std::string mute_parameters = argv[2];
    size_t pos = 0;
    try {
        mute_level = std::stod(mute_parameters, &pos);
        if (mute_parameters[pos] != ':') throw std::runtime_error("bad delimiter");
        unmute_delay = std::stoi(mute_parameters.substr(++pos));
        if (mute_level<0 || unmute_delay<0) throw std::runtime_error("negative values");
    }
    catch (std::runtime_error& e) {
        std::cerr << "wrong mute parameters '" + mute_parameters + "': " + e.what() + "; exiting\n";
        return -1;
    }
    std::cout << "mute parameters: " << mute_level << ":" << unmute_delay << std::endl;

    int dev_fd = pOpenVoicePort(serial_device_name);
    if (dev_fd < 0) {
        std::cerr << "cannot open " << serial_device_name << std::endl;
        return -1;
    }
    std::cout << "opened " << serial_device_name << std::endl;

    snd_pcm_t* capture_pcm = pcm_capture_handle();
    if (!capture_pcm) {
        std::cerr << "cannot get capture handler\n";
        return -1;
    }
    snd_pcm_t* playback_pcm = pcm_playback_handle();
    if (!playback_pcm) {
        std::cerr << "cannot get playback handler\n";
        return -1;
    }

    work_flag = 1;
    std::thread cpt_thread = std::thread(capture_thread, dev_fd, capture_pcm);
    std::thread plb_thread = std::thread(playback_thread, dev_fd, playback_pcm);

    std::signal(SIGINT, signal_handler);

    cpt_thread.join();
    plb_thread.join();

    close(dev_fd);
    std::cout << "device closed\n";
    snd_pcm_close(capture_pcm);
    snd_pcm_close(playback_pcm);
    std::cout << "pcm handles closed\n";

    std::cout << "\nexiting" << std::endl;

    return 0;
}
