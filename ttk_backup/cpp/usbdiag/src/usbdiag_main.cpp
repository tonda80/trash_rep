#include <fstream>
#include <sstream>
#include <iomanip>
//#include <chrono>
//#include <thread>

#include "lib/logger/logger.h"
//#include "lib/process_monitor/pmon_observer.h"
#include "zmqpb.h"
#include "lib/global_conf.h"

#include "configurator.pb.h"
#include "usbdiag.pb.h"
#include "canparser.pb.h"
#include "board.pb.h"


const std::string ModuleName = "usbdiag";
const std::string RouterClientName = "USBDIAG";

const std::string DefaultCanItemName = "USBCHRG";
const std::string DefaultCanAddress = "18FF0A00";


const std::string version()
{
    return "2.0.2";
    // 0.2      поддержка usbcharger firmware 1.5 (дополнительное сообщение)
    // 0.1      pre-release version
}


namespace G {
    static zmqpb::RouterClient* router_client = nullptr;
    static std::unique_ptr<Json::Value> cfg;
    static std::string can_item_name;
    static std::string can_address;
    static std::unique_ptr<Json::StreamWriter> json_fast_writer;
}


template<typename T>
bool unpack_data_message(const pb::MntCommand& mnt_request, T& data)
{
	if (!mnt_request.data().Is<T>()) {
        logerror  << "Request " << mnt_request.header().name() <<
			"has expected type " << T::descriptor()->name() <<
			", but it is not";
		return false;
	}
	if (!mnt_request.data().UnpackTo(&data)) {
        logerror  << "cannot unpack request " << mnt_request.header().name() << ", it will be ignored";
		return false;
	}
    logdebug  << mnt_request.header().name() << " will be handled";
	return true;
}


void usbdiagControl_handler(const pb::MntCommand& mnt_request)
{
	pb::usbdiagControl data;
	if (!unpack_data_message(mnt_request, data)) {
		return;
	}

    auto cmd_address = data.address();
    uint8_t addr = cmd_address & 0xf;
    bool check_addr = false;

    uint8_t command = 0;
    if (data.command() == pb::usbdiagControl::SET_ADDRESS) {
        command = 0xA;
        check_addr = true;
    }
    else if (data.command() == pb::usbdiagControl::RESET_ADDRESS) {
        command = 0xE;
        check_addr = true;
    }
    else if (data.command() == pb::usbdiagControl::RESET_ALL) {
        command = 0xE;
        addr = 0;
    }
    else if (data.command() == pb::usbdiagControl::REQUEST_INFO) {
        command = 0xD;
        addr = 0;
    }
    else {
        logerror  << "[usbdiagControl] unknown command " << data.command() << ", ignoring";
        return;
    }

    if (check_addr && (cmd_address<1 || cmd_address>8)) {
        logerror  << "[usbdiagControl] wrong address " << data.address() << ", ignoring";
        return;
    }

    std::stringstream ss;
    auto def_flags = ss.flags();

    Json::Value json_can;      // пример json в can.proto
    Json::Value& can_message = json_can["CANMessages"][0u];
    can_message["Address"] = G::can_address;
    can_message["CanInd"] = G::cfg->get("canindex", 0).asInt();
    can_message["Wait"] = 0;
    //
    int value = addr << 4 | command;
    ss << std::hex << std::uppercase<< std::setw(2) << std::setfill('0') << value;
    can_message["Data"] = ss.str();

    ss.str(std::string());  // reset stringstream
    ss.flags(def_flags);

    G::json_fast_writer->write(json_can, &ss);
    loginfo  << "usbdiagControl " << ss.str();

    pb::CANData can_out;
    can_out.set_data(ss.str());

    zmqpb::send_mnt_msg({"CAN"}, "CanMessage", can_out, G::router_client);
    // шлем в кан, а не в енкодер потому что энкодер пока не умеет слать 1 байт данных, а железка хочет один
}

void send_usbdiagState(uint64_t raw_state)
{
    auto log = logdebug;
    log  << "usbcharger raw_state: " << std::hex << raw_state << std::dec;

    // 0 byte - старший
    bool message_type = raw_state & 0x80'0000'0000;             // 0 byte 7 bit
    if (message_type) {  // основное сообщение 5 байт
        pb::usbdiagState pb_state;

        int addr = raw_state >> 32 & 0xf;                                           // 0 byte 3:0 bits
        bool fail = raw_state & 0x10'0000'0000;                                     // 0 byte 4 bit
        uint d_power = (raw_state >> 29 & 0x100) | (raw_state & 0xff);              // 0 byte 5 bit "and" 4 byte
        float power = d_power*0.1f;
        log << "; state addr=" << addr << " f=" << fail << " P=" << power;
        for (int i = 1; i < 4; ++i) {
            uint d_voltage = raw_state >> 8*(4-i) & 0xff;                                // i byte
            float voltage = d_voltage*0.05f;
            log << " U" << i << "=" << voltage;// << " " << d_voltage;
            pb_state.add_voltage(voltage);
        }

        pb_state.set_address(addr);
        pb_state.set_fail(fail);
        pb_state.set_power(power);
        zmqpb::send_mnt_msg({"UI"}, "usbdiagState", pb_state, G::router_client);
    }
    else {      // дополнительное сообщение 2 байта
        pb::usbdiagExtraState pb_extra_state;

        int addr = raw_state >> 8 & 0xf;                                    // 0 byte 3:0 bits
        bool fail = raw_state & 0x10'00;                                    // 0 byte 4 bit
        uint major_ver = raw_state >> 4 & 0xf;                              // 1 byte 7:4 bits
        uint minor_ver = raw_state & 0xf;                                   // 1 byte 3:0 bits

        std::string ver = std::to_string(major_ver)+"."+std::to_string(minor_ver);
        log << "; state addr=" << addr << " f=" << fail << " ver=" << ver;
        pb_extra_state.set_address(addr);
        pb_extra_state.set_fail(fail);
        pb_extra_state.set_version(ver);
        zmqpb::send_mnt_msg({"UI"}, "usbdiagExtraState", pb_extra_state, G::router_client);
    }
}

void CanMessage_handler(const pb::MntCommand& mnt_request)
{
    pb::CanPacket data;
	if (!unpack_data_message(mnt_request, data)) {
		return;
    }

    for (const auto& item : data.items()) {
        if (item.name() == G::can_item_name) {
            send_usbdiagState(item.parsedvalue());
        }
    }
}

void send_version(const pb::MntHeader& req_header)
{
    /*
    std::stringstream ss;
    Json::Value root;
    root["version"] = version();
    G::json_fast_writer->write(root, &ss);
    */
    std::string json_version = "{\"version\":\"" + version() +"\"}";
    logdebug  << "send_version " << json_version;

    zmqpb::send_mnt_reply(req_header, json_version, pb::Success, G::router_client);

}

void zmq_poll_loop() {
    while (1) {
         pb::MntCommand mnt_request = G::router_client->wait_request();
         const std::string& req_header_name = mnt_request.header().name();

         if (req_header_name == "usbdiagControl") {
            usbdiagControl_handler(mnt_request);
		 }
         else if (req_header_name == "CanMessage") {
            CanMessage_handler(mnt_request);
		 }
         else if (req_header_name == "getversion") {
            send_version(mnt_request.header());
         }
         else {
             logerror  << "unknown request " << req_header_name << " from " << mnt_request.header().source() << ", ignoring";
		 }
	}
}

std::unique_ptr<Json::Value> get_configuration(zmqpb::RouterClient* pRt)
{
    auto ret = std::make_unique<Json::Value>();
	Json::Reader reader;

    // local debug config
    const std::string deb_conf_name = ModuleName+"_debug_config.json";
    std::ifstream ifs(deb_conf_name, std::fstream::in);
    if (ifs.good()) {
        logwarning  << "discovered local debug config file " << deb_conf_name << ", using it";
        if (!reader.parse(ifs, *ret)) {
            logerror  << "cannot parse json from local debug config file " << deb_conf_name << ": " << reader.getFormattedErrorMessages();
            ret = nullptr;
        }
        return ret;
    }

    // work config from configurator
	pb::GetConfig pb_request;
    pb_request.set_module(RouterClientName);
	pb::Config pb_reply;
    zmqpb::MntProtocolRc rc = send_request_get_reply({"CONFIGURATOR"}, "GetConfig", pb_request, pb_reply, pRt, 10);
    if(rc == zmqpb::MNT_RPOTOCOL_OK) {
        if (reader.parse(pb_reply.config(), *ret)) {
            loginfo  << "got config from configurator";
        }
        else {
            logerror  << "cannot parse json from configurator reply: " << reader.getFormattedErrorMessages();
            ret = nullptr;
        }
    }
    else {
        logerror  << "GetConfig error. send_request_get_reply returned " << rc;
        ret = nullptr;
    }
	return ret;
}

bool has_option(const std::string& option, int argc, const char** argv)
{
    auto last = argv + argc;
    return std::find(argv, last, option) != last;
}

int main(int argc, const char** argv)
{
    if (has_option("-v", argc, argv)) {
        std::cout << "usbdiag version " << version() << std::endl;
        return 0;
    }

    loginfo << version() << " started";

	//ProcessMonitor::Observer::instance().wait_started();

    zmqpb::RouterClient router_client(RouterClientName, 1);

    G::cfg = get_configuration(&router_client);
    if (!G::cfg) {
        logcritical << "cannot get configuration, exiting";
        exit(1);
    }
    SyslogOut::configure(*G::cfg);

    Json::StreamWriterBuilder builder;
    builder["commentStyle"] = "None";
    builder["indentation"] = "";
    G::json_fast_writer = std::unique_ptr<Json::StreamWriter>(builder.newStreamWriter());

    G::router_client = &router_client;
    G::can_item_name = G::cfg->get("canitemname",  DefaultCanItemName).asString();
    G::can_address = G::cfg->get("canaddress",  DefaultCanAddress).asString();


    logdebug << "can_item_name=" << G::can_item_name << ", can_address=" << G::can_address <<
                "; starting zmq_poll_loop";
    zmq_poll_loop();

    loginfo  << "exiting";
	return 0;
}
