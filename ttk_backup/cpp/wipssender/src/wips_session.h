#ifndef WIPSSESSION_H
#define WIPSSESSION_H

#include <boost/asio.hpp>

#include <atomic>
#include <list>


struct WIpsServerConfig;
class WipsClient;

namespace wips_protocol {
    struct Answer;
    struct DataPacket;
}

template <typename T> struct BufHolder;
using StringHolder = BufHolder<std::string>;


class WipsSession
{
public:
    WipsSession(boost::asio::io_context& ctx, WipsClient& owner_, const WIpsServerConfig& config_);
    WipsSession(WipsSession&&) = delete;
    WipsSession(WipsSession&) = delete;

    inline bool is_logged_in() const;

    void send_ping();
    void send_data(int64_t id, std::string&&);

    // все приватные методы вызываются только в потоке контекста
private:
    using ErrCode = const boost::system::error_code&;

    WipsClient& owner;

    boost::asio::io_context& context;
    boost::asio::ip::tcp::endpoint ep;
    std::string str_label;
    std::unique_ptr<boost::asio::ip::tcp::socket> tcp_socket;
    std::string rd_buf;

    const WIpsServerConfig& config;

    bool on_error(std::string_view, ErrCode);

    void connect();
    void disconnect();

    void send(std::shared_ptr<StringHolder>);
    void receive();
    void on_receive(const wips_protocol::Answer&& packet);

    std::unique_ptr<boost::asio::deadline_timer> create_deadline_timer(uint32_t seconds, std::string_view where);

    void login();
    void on_login(const wips_protocol::Answer& packet);
    std::atomic<bool> login_state = false;
    std::unique_ptr<boost::asio::deadline_timer> login_timer;

    void on_data_report(const wips_protocol::Answer& packet);

    void send_black_box();
    void on_black_box_report(const wips_protocol::Answer& packet);
    std::unique_ptr<boost::asio::deadline_timer> bbox_timer;

    void request_send_data(std::shared_ptr<StringHolder>);
    std::list<std::shared_ptr<StringHolder>> send_data_queue;
};

#endif // WIPSSESSION_H
