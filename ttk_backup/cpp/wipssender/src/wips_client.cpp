#include "wips_client.h"
#include "wips_protocol.h"
#include "wips_datamanager.h"

#include <mnt_log.h>



WipsBlackBox WipsClient::black_box_storage;


using ErrCode = const boost::system::error_code&;


class WipsClient::Timer {
public:
    Timer(boost::asio::io_context& io_context, WipsClient* client_, const WIpsServerConfig& cfg, std::string_view type_)
            : timer(io_context), client(client_), serv_name(cfg.name),
              is_ping(type_ == "ping"), is_move(type_ == "move"),
              timer_period(is_ping ? cfg.ping_period : (is_move ? cfg.send_period_move : cfg.send_period_idle))
    {
        if (timer_period == 0) {
            mnt::log(lognotice, "Disabled {} timer for {}", type(), serv_name);
            return;
        }
        start_timer();
    }

    const char* type() {return is_move ? "move" : "idle";}

private:
    void start_timer() {
        timer.expires_from_now(boost::posix_time::seconds(timer_period));
        timer.async_wait([this] (ErrCode err) {
            if (!err) {
                start_timer();
                client->on_timer(this);
            }
        });
    }

    boost::asio::deadline_timer timer;
    WipsClient* client;

public:
    const std::string& serv_name;

    const bool is_ping;     // timer type
    const bool is_move;
private:
    const uint32_t& timer_period;
};


struct SessionData {
    std::unique_ptr<WipsSession> session;
    const WIpsServerConfig& serv_cfg;

    double dist_counter = 0;
    double sent_angle = 0;

    BlackBoxData sent_bbox;

    SessionData(boost::asio::io_context& ctx, WipsClient* client, const WIpsServerConfig& serv_cfg_)
        : serv_cfg(serv_cfg_)
    {
        session = std::make_unique<WipsSession>(ctx, *client, serv_cfg);
    }
};



WipsClient::WipsClient(const AppConfig& config) : io_context(), io_work(io_context.get_executor()) {
    create_sessions(config);
}

WipsClient::~WipsClient() {
    mnt::log(loginfo, "[WipsClient] exiting");
    io_work.reset();
    io_context.stop();
    work_thread.join();
}

void WipsClient::create_sessions(const AppConfig& config) {
    for (const auto& cfg : config.wips_servers) {
        sessions.emplace(cfg.name, SessionData(io_context, this, cfg));

        timers.emplace_back(io_context, this, cfg, "ping");
        timers.emplace_back(io_context, this, cfg, "move");
        timers.emplace_back(io_context, this, cfg, "idle");
    }

    work_thread = std::thread([this]() {
        io_context.run();
    });
    // TODO? N work_threads (io_contexts or strands etc)
}

void WipsClient::on_timer(Timer* tmr) {
    //mnt::log(logdebug, "on_timer {} {}", tmr->serv_cfg.name, tmr->type());

    if (tmr->is_ping) {
        sessions.at(tmr->serv_name).session->send_ping();
    }
    else if (!(tmr->is_move ^ WipsDataManager::instance().is_movement())) {
        trigger_send(Fmt::format("{} timer", tmr->type()), sessions.at(tmr->serv_name));
    }
}

void WipsClient::on_new_position(double dist, double curr_angle) {
    for (auto& s : sessions) {
        SessionData& session_data = s.second;

        if (dist < 0) {     // инициализация
            session_data.sent_angle = curr_angle;
            session_data.dist_counter = 0;     // на всякий случай
            continue;
        }

        session_data.dist_counter += dist;
        bool r1 = session_data.dist_counter - session_data.serv_cfg.send_distance >= 0;

        double turn = curr_angle - session_data.sent_angle;
        bool r2 = (fabs(turn) - session_data.serv_cfg.send_angle) >= 0;

        if (r1 || r2) {
            auto msg = r1 ? Fmt::format("distance {}", session_data.dist_counter) : Fmt::format("angle {}", turn);
            trigger_send(msg, session_data);

            session_data.sent_angle = curr_angle;
            session_data.dist_counter = 0;
        }
    }
}

void WipsClient::get_session_data(wips_protocol::DataPacket& /*pck*/, SessionData& /*session_data*/) {
    // TODO add local session data
}

void WipsClient::trigger_send(std::string_view where, SessionData& session_data) {
    mnt::log(loginfo, "[WipsClient::trigger_send] for {} by '{}'", session_data.serv_cfg.name, where);

    wips_protocol::DataPacket pck;
    WipsDataManager::instance().get_data(pck);
    get_session_data(pck, session_data);

    send(pck, session_data);
}

void WipsClient::send_all(const wips_protocol::DataPacket& const_pck) {
    if (sessions.size() == 0) {
        return;
    }

    black_box_storage.begin_transaction();
    for (auto& s : sessions) {
        wips_protocol::DataPacket pck(const_pck);
        get_session_data(pck, s.second);

        send(pck, s.second);
    }
    black_box_storage.commit();
}

void WipsClient::send(const wips_protocol::DataPacket& pck, SessionData& session_data) {
    std::string pck_str = wips_protocol::encode_data(pck);
    auto [type, data] = wips_protocol::stored_parts(pck_str);

    int64_t pck_id = black_box_storage.store(session_data.serv_cfg.name, type, data);

    mnt::log(logdebug, "[WipsClient::send] send packet {} to {}", pck_id, session_data.serv_cfg.name);
    session_data.session->send_data(pck_id, std::move(pck_str));
}

void WipsClient::on_data_delivered(int64_t pck_id) {
    mnt::log(logdebug, "[WipsClient::on_data_delivered] delivered packet {}", pck_id);
    if (pck_id != BboxItem::no_id()) {
        black_box_storage.mark_as_delivered(pck_id);
    }
}

std::string WipsClient::get_black_box_packet(const std::string& name) {
    auto& bbox = sessions.at(name).sent_bbox;
    bbox.clear();
    black_box_storage.get_black_box(bbox, name, wips_protocol::black_box_max_size());
    size_t sz = bbox.size();
    mnt::log(loginfo, "[WipsClient::get_black_box] {}, black box size {}", name, sz);
    if (sz == 0) {
        return "";
    }
    return wips_protocol::encode_black_box(bbox);
}

void WipsClient::on_black_box_delivered(const std::string& name, const wips_protocol::Answer& answer) {
    int recv_sz = wips_protocol::recv_black_box_packets(answer);
    auto& bbox = sessions.at(name).sent_bbox;
    int sz = static_cast<int>(bbox.size());
    if (recv_sz != sz) {
        mnt::log(logerror, "[WipsClient::on_black_box_delivered] unexpected answer from {}, reported packets number {}, expected {}",
                   name, recv_sz, sz);
        return;
    }

    mnt::log(loginfo, "[WipsClient::on_black_box_delivered] {}", name);
    black_box_storage.begin_transaction();
    for (auto& e : bbox) {
        black_box_storage.mark_as_delivered(e.id);
    }
    black_box_storage.commit();

    bbox.resize(0);
}
