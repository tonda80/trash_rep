#ifndef WIPSCLIENT_H
#define WIPSCLIENT_H

#include <list>
#include <map>
#include <thread>

#include "wips_session.h"
#include "wipssender_config.h"
#include "wips_blackbox.h"


struct WIpsServerConfig;
struct SessionData;



class WipsClient
{
public:
    WipsClient(const AppConfig&);
    ~WipsClient();

    void send_all(const wips_protocol::DataPacket& pck);
    void on_data_delivered(int64_t pck_id);

    void on_new_position(double dist, double curr_angle);

    std::string get_black_box_packet(const std::string& name);
    void on_black_box_delivered(const std::string& name, const wips_protocol::Answer& answer);

private:
    boost::asio::io_context io_context;
    boost::asio::executor_work_guard<boost::asio::io_context::executor_type> io_work;
    std::thread work_thread;

    std::map<std::string, SessionData> sessions;
    void create_sessions(const AppConfig& config);

    class Timer;
    std::list<Timer> timers;
    void on_timer(Timer*);

    void trigger_send(std::string_view where, SessionData&);
    void send(const wips_protocol::DataPacket&, SessionData&);

    static WipsBlackBox black_box_storage;      // static из-за static statements внутри методов

    void get_session_data(wips_protocol::DataPacket&, SessionData&);
};

#endif // WIPSCLIENT_H
