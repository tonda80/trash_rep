#include "wipssender_config.h"

#include <stdexcept>
#include <algorithm>

#include <mnt2app.h>
#include <fmt_wrapper.h>


static const char* PREF = "Config error:";

AppConfig::AppConfig()
{
    auto json_conf = Mnt2App::instance().config();

    std::vector<std::string> names;

    for (const auto& s : utils::req_child(json_conf, "servers")) {
        if (utils::req_child(s, "enabled").asBool()) {
            std::string address = utils::req_child(s, "address").asString();
            size_t d = address.find(':');
            if (d == std::string::npos) {
                throw std::runtime_error(Fmt::format("{} wrong server address {}", PREF, address));
            }
            std::string ip = address.substr(0, d);
            int port = std::stoi(address.substr(d+1));
            if (port < 1 || port >= (1 << 16)) {
                throw std::runtime_error(Fmt::format("{} wrong server port {}", PREF, port));
            }
            wips_servers.emplace_back(WIpsServerConfig{
                                       utils::req_child(s, "name").asString(),
                                       ip,
                                       static_cast<uint16_t>(port),
                                       utils::req_child(s, "reconnectTimeout").asUInt(),
                                       utils::req_child(s, "login_id").asString(),
                                       utils::req_child(s, "password").asString(),

                                       s.get("loginTimeout", 10).asUInt(),
                                       s.get("blackBoxTimeout", 10).asUInt(),
                                       s.get("sendDataTimeout", 10).asUInt(),
                                       s.get("sendQueueMax", 30).asUInt(),

                                       utils::req_child(s, "pingPeriod").asUInt(),
                                       utils::req_child(s, "sendPeriodIdle").asUInt(),
                                       utils::req_child(s, "sendPeriodMove").asUInt(),
                                       utils::req_child(s, "sendDistance").asDouble(),
                                       utils::req_child(s, "sendAngle").asDouble(),
            });

            // проверим что все ок
            const auto& curr_cfg = wips_servers.back();

            if (std::find(names.begin(), names.end(), curr_cfg.name) != names.end()) {
                throw std::runtime_error(Fmt::format("{} repeated server name '{}'", PREF, curr_cfg.name));
            }
            names.push_back(curr_cfg.name);

            // пока все нулевые параметры имеют смысл
        }
    }


}
