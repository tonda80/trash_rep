#include "wips_datamanager.h"

#include <cmath>

#include <mnt_log.h>

#include "wipssender_config.h"
#include "wips_client.h"
#include "wips_protocol.h"



double dist_by_coords(double lat1, double lon1, double lat2, double lon2);


void WipsDataManager::init(/*const AppConfig& app_config_, */WipsClient& wips_client_) {
    //app_config = &app_config_;
    wips_client = &wips_client_;
}

WipsDataManager::~WipsDataManager() {
    mnt::log(loginfo, "[WipsDataManager] exiting");
}

bool WipsDataManager::is_movement() const {
    return navigation_data.has_value() && navigation_data->speed > 7.0;    // константа из сенддаты
}

void WipsDataManager::trigger_send(std::string_view where) {
    mnt::log(loginfo, "[WipsDataManager::trigger_send] by '{}'", where);

    wips_protocol::DataPacket pck;
    get_data(pck);
    wips_client->send_all(pck);
}

void WipsDataManager::get_data(wips_protocol::DataPacket& pck) {
    std::lock_guard lck(data_mtx);

    wips_protocol::form_package_navigation(pck, navigation_data);
    // TODO alert_state, fire_state
    wips_protocol::form_package_dido(pck, di_state, do_state);
    wips_protocol::form_package_ai(pck, a1_state, a2_state);
}


void WipsDataManager::process_inn(bool new_fire_state) {
    bool trigg = !fire_state.has_value() || (*fire_state ^ new_fire_state);
    fire_state = new_fire_state;
    if (trigg) {
        trigger_send(fire_state ? "fire on" : "fire off");
    }
}

void WipsDataManager::process_inn(const pb::NavigationData &data) {
    if (!data.valid()) {
        loginfo << "[NavigationData] ignoring invalid data";
        return;
    }
    logdebug << "[NavigationData] processing";

    uint16_t q_satelites = 0;
    for (const auto& s : data.satellites()) {
        q_satelites += s.satellites();
    }
    NavigationData new_data{
        data.lat(),
        data.lon(),
        data.speed()*1.852,     // узлы => км/час (?)
        data.heading(),
        data.hdop(),
        data.altitude(),
        q_satelites,
        data.unixtime()
    };
    //  repeated string GNSS = 1; double Time = 2; double DeltaTime = 3; double PDOP = 9;

    double dist = -1;
    if (navigation_data.has_value()) {
        dist = dist_by_coords(new_data.lat, new_data.lon, navigation_data->lat, navigation_data->lon);
    }

    navigation_data = new_data;

    wips_client->on_new_position(dist, new_data.head);  // после переписывания navigation_data
}

void WipsDataManager::process_inn(const pb::Alert& data) {
    logdebug << "[Alert] processing";
    bool trigg = !alert_state.has_value() || (*alert_state ^ data.id());
    alert_state =  data.id();
    if (trigg) {
        trigger_send(*alert_state ? "alert on" : "alert off");
    }
}

void WipsDataManager::process_inn(const pb::DiDoType1& data) {
    logdebug << "[DiDoType1] processing";
    if (data.dinexists()) {
        di_state = data.din();
    }
    if (data.doutexists()) {
        do_state = data.dout();
    }
    if (data.ain1exists()) {
        a1_state = data.ain1();
    }
    if (data.ain2exists()) {
        a2_state = data.ain2();
    }
}


// возвращает расстояние в километрах по координатам в градусах
double dist_by_coords(double lat1, double lon1, double lat2, double lon2) {
    static const double earth_radius = 6371.0088;		// км по WGS 84
    static auto hav = [](double x) {return pow(sin(x/2), 2);};		// гаверсинус
    static auto g2r = [](double x) {return x*M_PI/180;};

    lat1 = g2r(lat1); lon1 = g2r(lon1); lat2 = g2r(lat2); lon2 = g2r(lon2);

    double d_lat = fabs(lat1 - lat2);
    double d_lon = fabs(lon1 - lon2);

    double angle = 2*asin(sqrt( hav(d_lat) + cos(lat1)*cos(lat2)*hav(d_lon) ));

    return angle*earth_radius;
}
