#include "wips_session.h"

#include <mnt_log.h>
#include <utils.h>

#include "wipssender_config.h"
#include "wips_protocol.h"
#include "wips_client.h"



template <typename T>
struct BufHolder : private utils::Noncopyable {
    BufHolder(T&& data_, int64_t id_= BboxItem::no_id()) : data(std::move(data_)), id(id_) {}
    //~BufHolder() {mnt::log(logdebug, "BufHolder with {} released", data);}
    T data;
    //
    int64_t id;
    std::unique_ptr<boost::asio::deadline_timer> timer;
};



WipsSession::WipsSession(boost::asio::io_context& ctx, WipsClient& owner_, const WIpsServerConfig& config_) :
            owner(owner_), context(ctx), config(config_) {
    ep = boost::asio::ip::tcp::endpoint(boost::asio::ip::make_address(config.ip), config.port);
    str_label = Fmt::format("'{} {}:{}'", config.name, ep.address().to_string(), ep.port());

    boost::asio::dispatch(context, [this](){ connect(); });
}

void WipsSession::connect() {
    mnt::log(loginfo, "try connecting to {}", str_label);
    tcp_socket = std::make_unique<boost::asio::ip::tcp::socket>(context);
    tcp_socket->async_connect(ep, [this](ErrCode err) {
        if (!on_error("connect", err)) {
            mnt::log(lognotice, "[WipsSession] connected to {}", str_label);
            login();
            receive();
        }
    });
}

void WipsSession::disconnect() {
    tcp_socket.reset();
    rd_buf.clear();
    send_data_queue.clear();
    login_state = false;
    login_timer.reset();
    bbox_timer.reset();

    // connect вызовется в on_error из постоянно вызывающегося receive
}

bool WipsSession::on_error(std::string_view where, ErrCode err) {
    if (!err) {
        return false;
    }
    mnt::log(logerror, "[WipsSession::on_error] {} {}. {} ({}). Reconnecting in {} seconds",
                        where, str_label, err.message(), err.value(), config.reconnect_timeout);
    disconnect();
    auto t = std::make_shared<boost::asio::deadline_timer>(context, boost::posix_time::seconds(config.reconnect_timeout));
    t->async_wait([this, t](ErrCode err) {
        if (!err) connect();
    });
    return true;
}


void WipsSession::send_ping() {
    if (!is_logged_in()) {
        mnt::log(logdebug, "[WipsSession::send_ping] not logged in to {}", str_label);
        return;
    }
    mnt::log(logdebug, "[WipsSession::send_ping]");
    boost::asio::dispatch(context, [this]() {
        send(std::make_shared<StringHolder>(wips_protocol::encode_ping()));
    });
}


// это может быть вызвано из любого потока
void WipsSession::send_data(int64_t id, std::string&& data) {
    if (!is_logged_in()) {
        mnt::log(logdebug, "[WipsSession::send_data] not logged in to {}", str_label);
        return;
    }
    auto holder = std::make_shared<StringHolder>(std::move(data), id);
    boost::asio::dispatch(context, [this, holder](){request_send_data(holder);});
}

// это вызывается из потока контекста
void WipsSession::request_send_data(std::shared_ptr<StringHolder> holder) {
    if (!is_logged_in()) {
        mnt::log(logdebug, "[WipsSession::request_send_data] not logged in to {}", str_label);
        return;
    }

    send_data_queue.push_back(holder);

    auto sz = send_data_queue.size();
    if (sz > 1) {                           // есть недоставленное
        if (config.send_queue_max && sz > config.send_queue_max) {       // и даже слишком много
            mnt::log(logerror, "[WipsSession::request_send_data] send queue full (max={}) for {}, disconnecting", config.send_queue_max, str_label);
            disconnect();
        }
        return;
    }

    holder->timer = create_deadline_timer(config.send_data_timeout, "data");

    send(holder);
}

void WipsSession::on_data_report(const wips_protocol::Answer& packet) {
    mnt::log(logdebug, "[WipsSession::on_data_report] {}", str_label);

    if (send_data_queue.empty()) {
        mnt::log(logerror, "[WipsSession::on_data_report] unexpected packet {} {}", packet.type, packet.code);
        return;
    }

    owner.on_data_delivered(send_data_queue.front()->id);
    send_data_queue.pop_front();    // timer.cancel тут

    if (!send_data_queue.empty()) {
        send(send_data_queue.front());
    }
}

void WipsSession::send(std::shared_ptr<StringHolder> holder) {
    if (!tcp_socket) {
        mnt::log(logdebug, "[WipsSession::send] disconnected {}", str_label);
        return;
    }

    mnt::log(logdebug, "[WipsSession::send] send packet {} to {}", holder->id, str_label);
    async_write(*tcp_socket, boost::asio::buffer(holder->data), [this, holder](ErrCode err, std::size_t) {
        if (!on_error("send", err)) {
            //mnt::log(logdebug, "[WipsSession] send OK");
        }
    });
}

void WipsSession::receive() {
    if (!tcp_socket) {
        mnt::log(logdebug, "[WipsSession::receive] disconnected {}", str_label);
        return;
    }
    async_read_until(*tcp_socket, boost::asio::dynamic_buffer(rd_buf), "\r\n", [this](ErrCode err, std::size_t q) {
        if (!on_error("receive", err)) {
            std::string_view packet{rd_buf.c_str(), q};
            //mnt::log(logdebug, "[WipsSession::receive] received '{}' from {}", packet, str_ep);
            try {
                on_receive(wips_protocol::decode_answer(packet));
            }
            catch (wips_protocol::error& e) {
                mnt::log(logerror, "[WipsSession::receive] cannot decode '{}' from {}: {}", packet, str_label, e.what());
            }

            rd_buf.erase(0, q);
            receive();
        }
    });
}

void WipsSession::on_receive(const wips_protocol::Answer&& packet) {
    if (packet.type == "AL") {
        on_login(packet);
    }
    else if (packet.type == "AD") {
        on_data_report(packet);
    }
    else if (packet.type == "AB") {
        on_black_box_report(packet);
    }
    else if (packet.type == "AP") {
        mnt::log(logdebug, "[WipsSession::on_receive] ping report from {}", str_label);
    }
    else {
        mnt::log(logerror, "[WipsSession::on_receive] unknown packet type '{}' from {}", packet.type, str_label);
    }
}

void WipsSession::login() {
    send(std::make_shared<StringHolder>(wips_protocol::encode_login(config.login_id, config.password)));
    login_timer = create_deadline_timer(config.login_timeout, "login");
}

void WipsSession::on_login(const wips_protocol::Answer& packet) {
    mnt::log(logdebug, "[WipsSession::on_login] {}", str_label);

    if (!login_timer && config.login_timeout) {
        mnt::log(logwarning, "[WipsSession::on_login] unexpected login report for {}", str_label);
    }
    login_timer.reset();

    if (packet.ok) {
        mnt::log(lognotice, "Logged in to {}", str_label);
        send_black_box();   // до изменения login_state
        login_state = true;
    }
    else {
        mnt::log(logerror, "Login error to {}; return code {}", str_label, packet.code);
        disconnect();
    }
}

bool WipsSession::is_logged_in() const {
    return login_state;
}


void WipsSession::send_black_box() {
    std::string pck = owner.get_black_box_packet(config.name);
    if (pck.empty()) {
        return;
    }
    send(std::make_shared<StringHolder>(std::move(pck)));
    bbox_timer = create_deadline_timer(config.black_box_timeout, "black_box");
}

void WipsSession::on_black_box_report(const wips_protocol::Answer& packet) {
    mnt::log(logdebug, "[WipsSession::on_black_box_report] {}", str_label);

    if (!bbox_timer && config.black_box_timeout) {
        mnt::log(logwarning, "[WipsSession::on_black_box_report] unexpected black_box report for {}", str_label);
    }
    bbox_timer.reset();

    if (packet.ok) {
        //mnt::log(loginfo, "Delivered black box to {}", str_ep);
        owner.on_black_box_delivered(config.name, packet);
    }
    else {
        mnt::log(logerror, "Black box sending error to {}; return code '{}'", str_label, packet.code);
        disconnect();
    }
}

std::unique_ptr<boost::asio::deadline_timer> WipsSession::create_deadline_timer(uint32_t seconds, std::string_view where) {
    if (seconds == 0) {
        return nullptr;
    }
    auto timer = std::make_unique<boost::asio::deadline_timer>(context, boost::posix_time::seconds(seconds));
    timer->async_wait([this, where, seconds](ErrCode err) {
        if (!err) {
            // теоретически это может вызваться сразу после успешного репорта, но пренебрежем этим
            mnt::log(logwarning, "{} timer expired {}, {} disconnecting", where, seconds, str_label);
            disconnect();
        }
    });

    return timer;
}
