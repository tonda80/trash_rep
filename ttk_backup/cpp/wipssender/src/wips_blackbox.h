#ifndef WIPSBLACKBOX_H
#define WIPSBLACKBOX_H


#include <string_view>
#include <memory>
#include <mutex>

#include "sqlite_db.h"

#include "wips_blackbox_data.h"



// WipsBlackBox должен быть статическим из за статических стейтментов
class WipsBlackBox : public SqliteDbBase
{
public:
    WipsBlackBox();

    void begin_transaction();
    void commit();

    int64_t store(std::string_view serv_name, std::string_view type, std::string_view data);
    void mark_as_delivered(int64_t id);

    void get_black_box(BlackBoxData& bbox, std::string_view serv_name, size_t limit);

private:
    void init_tables();
    void clear_tables();

    std::mutex mtx;
};


#endif // WIPSBLACKBOX_H
