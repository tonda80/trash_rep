#include "wips_protocol.h"

#include <boost/algorithm/string.hpp>

#include <ctime>
#include <cmath>
#include <fmt/chrono.h>
#include <mutex>

#include <mnt_log.h>

//#include <iostream> // DEBUG


static std::string crc16(const void* data, size_t data_size);
std::tm _gmtime(const std::time_t*);        // thread safe gmtime
std::string coord2nmea(double, bool);       // градусы в nmea строку


namespace wips_protocol
{

auto get_field(const DataPacket& pck, const std::string& name, bool req = false) {
    static const std::string NA = "NA";
    const std::string* pStr = &NA;

    const auto it = pck.data.find(name);
    if (it != pck.data.end()) {
        pStr = &it->second;
    }
    else if (req) {
        throw error(Fmt::format("packet has no necessary data field {}", name));
    }

    return fmt::arg(name, *pStr);
}


std::string encode_login(std::string_view id, std::string_view passwd) {
    static const char* version = "2.0";
    std::string res = Fmt::format("#L#{};{};{};", version, id, passwd);
    res.append(crc16(res.c_str(), res.size()));
    res.append("\r\n");
    return res;
}


std::string encode_data(const DataPacket& pck) {
    std::string res = Fmt::format(
        "#D#{Date};{Time};{Lat1};{Lat2};{Lon1};{Lon2};{Speed};{Course};{Alt};{Sats};{HDOP};{Inputs};{Outputs};{ADC};{Ibutton};{Params};",
                get_field(pck, "Date"), get_field(pck, "Time"), get_field(pck, "Lat1"), get_field(pck, "Lat2"),
                get_field(pck, "Lon1"), get_field(pck, "Lon2"), get_field(pck, "Speed"), get_field(pck, "Course"),
                get_field(pck, "Alt"), get_field(pck, "Sats"), get_field(pck, "HDOP"), get_field(pck, "Inputs"),
                get_field(pck, "Outputs"), get_field(pck, "ADC"), get_field(pck, "Ibutton"), get_field(pck, "Params")
    );
    res.append(crc16(res.c_str(), res.size()));
    res.append("\r\n");
    return res;
}

const char* encode_ping() {
    return "#P#\r\n";
}


std::string encode_black_box(const BlackBoxData& bbox) {
    std::string res = "#B#";
    for (const auto& e : bbox) {
        res.append(e.data);
        res.append("|");
    }
    res.append(crc16(res.c_str(), res.size()));
    res.append("\r\n");
    return res;
}

int recv_black_box_packets(const wips_protocol::Answer& answ) {
    int res = -1;
    try {
        size_t pos = 0;
        res = std::stoi(answ.code, &pos);
        if (answ.code[pos] != 0) {
            throw std::invalid_argument("not whole string is number");
        }
    }
    catch (std::exception& e) {
        mnt::log(logerror, "[recv_black_box_packets] protocol error {}, code {}", e.what(), answ.code);
    }
    return res;
}


Answer decode_answer(std::string_view str) {
    std::vector<std::string> parts;
    boost::algorithm::split(parts, str, boost::is_any_of("#"));
    if (parts.size() < 3 || !parts.front().empty() || !boost::algorithm::ends_with(parts.back(), "\r\n")) {
        throw error(Fmt::format("cannot decode answer '{}'", str));
    }
    Answer pck{parts[1], parts.back().substr(0, parts.back().size()-2), false};
    if (pck.type == "AL" || pck.type == "AD") {
        pck.ok = pck.code == "1";
    }
    else if (pck.type == "AB") {
        pck.ok = !pck.code.empty();
    }
    else if (pck.type == "AP") {
        pck.ok = true;
    }
    else {
        throw error(Fmt::format("[decode_answer] unknown type '{}'", pck.type));
    }
    return pck;
}

std::tuple<std::string_view, std::string_view> stored_parts(std::string_view str) {
    // считаем что входная строка всегда ОК
    auto p1 = str.find_first_of('#', 1);
    auto p2 = str.find_last_of(';');
    //std::cerr << Fmt::format("__deb {} {} {}\n", p1, p2, str);
    return std::make_tuple(str.substr(1, p1-1), str.substr(p1+1, p2-(p1+1)));     // type, data
}

void form_package_navigation(DataPacket& pck, const std::optional<NavigationData>& data) {
    if (!data.has_value()) {
        return;
    }
    auto& dt = *data;
    std::tm tm = _gmtime(static_cast<const std::time_t*>(&dt.time));
    pck.data.emplace("Date", Fmt::format("{:%d%m%Y}", tm));
    pck.data.emplace("Time", Fmt::format("{:%H%M%S}", tm));
    pck.data.emplace("Lat1", coord2nmea(dt.lat, true));
    pck.data.emplace("Lon1", coord2nmea(dt.lon, false));
    pck.data.emplace("Speed", std::to_string(static_cast<int>(dt.speed)));
    pck.data.emplace("Course", std::to_string(static_cast<int>(dt.head)));
    pck.data.emplace("Alt", std::to_string(static_cast<int>(dt.alt)));
    pck.data.emplace("Sats", std::to_string(dt.sats));
    pck.data.emplace("HDOP", Fmt::format("{:.4f}", dt.hdop));      // точность?
    // TODO Lat2; Lon2; ?
}

void form_package_dido(DataPacket& pck, std::optional<int32_t> di, std::optional<int32_t> do_) {
    if (di.has_value()) {
        pck.data.emplace("Inputs", std::to_string(*di));
    }
    if (do_.has_value()) {
        pck.data.emplace("Outputs", std::to_string(*do_));
    }
}

void form_package_ai(DataPacket& pck, std::optional<int32_t> a1, std::optional<int32_t> a2) {
    std::string s;
    if (a1.has_value() || a2.has_value()) {
        s = Fmt::format("{},{}", a1.has_value() ? std::to_string(*a1) : "", a1.has_value() ? std::to_string(*a2) : "");
        // TODO так ли надо формировать строку если 1-го значения нет?
    }
    pck.data.emplace("ADC", s);
}


} // namespace wips_protocol


static const uint16_t crc16_table[256] = {
    0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
    0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
    0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
    0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
    0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
    0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
    0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
    0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
    0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
    0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
    0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
    0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
    0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
    0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
    0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
    0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
    0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
    0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
    0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
    0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
    0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
    0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
    0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
    0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
    0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
    0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
    0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
    0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
    0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
    0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
    0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
    0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040
};

std::string crc16(const void* data, size_t data_size) {
    if (!data || !data_size)
        throw wips_protocol::error("crc16 call error");
    uint16_t crc = 0;
    const uint8_t* data_u8 = static_cast<const uint8_t*>(data);
    while (data_size--) {
        crc = (crc >> 8) ^ crc16_table[static_cast<uint8_t>(crc) ^ *data_u8++];
    }
    return Fmt::format("{:04X}", crc);
}

std::tm _gmtime(const std::time_t* t) {
    static std::mutex mtx;
    std::lock_guard lck(mtx);
    return *gmtime(t);

}

std::string coord2nmea(double coord, bool is_lat) {
    double u_coord = fabs(coord);
    uint16_t grad = static_cast<uint16_t>(u_coord);
    double min = (u_coord - grad)*60;
    uint16_t min_int = static_cast<uint16_t>(min);  // вроде нельзя сделать "01.23" из дробного
    uint16_t min_fract2 = static_cast<uint16_t>(round((min - min_int)*100));

    std::string s = coord >= 0 ? "E" : "W";
    if (is_lat) {
        s = coord >= 0 ? "N" : "S";
        return Fmt::format("{:02d}{:02d}.{:02d}{}", grad, min_int, min_fract2, s);
    }
    return Fmt::format("{:03d}{:02d}.{:02d}{}", grad, min_int, min_fract2, s);
}
