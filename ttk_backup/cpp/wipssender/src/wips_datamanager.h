#ifndef WIPSDATAMANAGER_H
#define WIPSDATAMANAGER_H


#include <mutex>

#include <request_handler.h>

#include "wips_data.h"

#include "pb/navigation.pb.h"
#include "pb/alert.pb.h"
#include "pb/dido.pb.h"



class WipsClient;
class AppConfig;

namespace wips_protocol {
    struct DataPacket;
}

class WipsDataManager
{
public:
    static WipsDataManager& instance() {
        static WipsDataManager instance;
        return instance;
    }

    ~WipsDataManager();

    void init(/*const AppConfig&, */WipsClient&);

    template <typename T>
    void process(const T& data) {
        std::lock_guard lck(data_mtx);
        process_inn(data);
    }

    bool is_movement() const;

    void get_data(wips_protocol::DataPacket&);

private:
    WipsDataManager() = default;
    //const AppConfig* app_config = nullptr;
    WipsClient* wips_client = nullptr;      // TODO? const

    void trigger_send(std::string_view where);

    // data
    std::recursive_mutex data_mtx;
    void process_inn(bool new_fire_state);              // fire on/off
    void process_inn(const pb::NavigationData&);
    void process_inn(const pb::Alert&);
    void process_inn(const pb::DiDoType1&);
    //
    std::optional<bool> fire_state;
    std::optional<NavigationData> navigation_data;
    std::optional<uint32_t> alert_state;
    std::optional<int32_t> di_state, do_state;
    std::optional<int32_t> a1_state, a2_state;

};


template <typename TAny, typename TDer>
class RequestHandlerBridge : public RequestHandler<TAny, TDer>
{
public:
    void process() override final {
        WipsDataManager::instance().process<TAny>(this->data);
    }
};


class FireOnHandler : public RequestNoDataHandler<FireOnHandler> {
    void process() {
        WipsDataManager::instance().process(true);
    }
};

class FireOffHandler : public RequestNoDataHandler<FireOffHandler> {
    void process() {
        WipsDataManager::instance().process(false);
    }
};

class NavigationDataHandler : public RequestHandlerBridge<pb::NavigationData, NavigationDataHandler> {};
class AlarmButtonHandler : public RequestHandlerBridge<pb::Alert, AlarmButtonHandler> {};
class DiDoType1Handler : public RequestHandlerBridge<pb::DiDoType1, DiDoType1Handler> {};


#endif // WIPSDATAMANAGER_H
