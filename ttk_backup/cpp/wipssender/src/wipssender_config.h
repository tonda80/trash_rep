#ifndef CONFIG_H
#define CONFIG_H

#include <string>
#include <vector>


struct WIpsServerConfig {
    std::string name;

    std::string ip;
    uint16_t port;
    uint32_t reconnect_timeout;

    std::string login_id;
    std::string password;

    uint32_t login_timeout;             // ожидание ответа на логин
    uint32_t black_box_timeout;         // ожидание ответа на черный ящик
    uint32_t send_data_timeout;         // ожидание ответа на пакет с данными (от async_write)
    uint32_t send_queue_max;            // максимальный размер очереди сообщений на отправку

    uint32_t ping_period;               // период отправки пинг сообщений
    uint32_t send_period_idle;          // период отправки сообщений во время остановки
    uint32_t send_period_move;          // период отправки сообщений во время движения
    double send_distance;               // отправка сообщения по пройденному расстоянию (км)
    double send_angle;                  // отправка сообщения по изменению угла (град)
};


struct AppConfig
{
    AppConfig();

    std::vector<WIpsServerConfig> wips_servers;
};

#endif // CONFIG_H
