#include "wips_blackbox.h"

#include <chrono>

#include <mnt_log.h>

#include "wips_protocol.h"




void log_err_clb(const char* what) {
    logerror << "[sqlite error] " << what;
}


WipsBlackBox::WipsBlackBox() {
    const char* db_path = "wipssender.db";
    open(db_path);

    init_tables();
    clear_tables();

    mnt::log(lognotice, "opened {} {}", db_path, sqlite3_threadsafe()) ;
}

void WipsBlackBox::init_tables() {
    // -- black box
    // id       уникальный id записи
    // time     время записи
    // serv     имя сервера
    // type     тип записи (D, может будет SD и что-то ещё) на всякий случай
    // data     собственно данные для хранения (строки без типа и crc)
    // deliv    флаг доставлено
    if (!exec_request(R"(
            CREATE TABLE IF NOT EXISTS bbox
            (
                id          INTEGER PRIMARY KEY,
                time        TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
                serv        TEXT,
                type        TEXT,
                data        TEXT,
                deliv       BOOLEAN

            );
        )")) {
        throw sqlitedb_error("cannot create db table");
    }

    if (!exec_request("PRAGMA journal_mode=WAL;")) {
        logerror << "cannot open db in WAL mode";
    }
}

void WipsBlackBox::clear_tables() {
    // удаляем доставленное
    Statement stm_del_deliv("DELETE FROM bbox WHERE deliv = 1;", this);
    stm_del_deliv.exec_no_throw(log_err_clb);

    // удаляем старые которые все равное не попадут  в пакет (black_box_max_size)
    Statement stm_del_old(
        "DELETE FROM bbox WHERE serv = ?1 AND id NOT IN (SELECT id FROM bbox WHERE serv = ?1 ORDER BY time DESC LIMIT ?2);",
        this);
    Statement stm_sel("SELECT DISTINCT serv FROM bbox;", this);
    int res = stm_sel.exec_no_throw(log_err_clb);
    while (res == SQLITE_ROW) {
        std::string serv = stm_sel.column<std::string>(0);
        //mnt::log(logdebug, "[WipsBlackBox::clear_tables] delete records for {}", serv);
        stm_del_old.exec_no_throw(log_err_clb, serv, wips_protocol::black_box_max_size());
        res = stm_sel.step();
    }
}


void WipsBlackBox::begin_transaction() {
    static Statement stm("BEGIN TRANSACTION;", this);

    std::lock_guard lck(mtx);
    stm.exec_no_throw(log_err_clb);
}

void WipsBlackBox::commit() {
    static Statement stm("COMMIT;", this);

    std::lock_guard lck(mtx);
    stm.exec_no_throw(log_err_clb);
}

int64_t WipsBlackBox::store(std::string_view serv_name, std::string_view type, std::string_view data) {
    static Statement stm(
        "INSERT INTO bbox (serv, type, data, deliv) VALUES (?, ?, ?, 0);", this);

    std::lock_guard lck(mtx);
    if (stm.exec_no_throw(log_err_clb, serv_name, type, data) != SQLITE_ERROR) {
        return sqlite3_last_insert_rowid(get_db());
    }
    return BboxItem::no_id();
}

void WipsBlackBox::mark_as_delivered(int64_t id) {
    static Statement stm(
        "UPDATE bbox SET deliv = 1 WHERE id = ?;", this);

    std::lock_guard lck(mtx);
    stm.exec_no_throw(log_err_clb, id);
}

void WipsBlackBox::get_black_box(std::vector<BboxItem>& bbox, std::string_view serv_name, size_t limit) {
    static Statement stm(
        "SELECT id, data FROM bbox WHERE serv = ? AND deliv = 0 ORDER BY time DESC LIMIT ?;", this);

    std::lock_guard lck(mtx);
    int res = stm.exec_no_throw(log_err_clb, serv_name, limit);
    while (res == SQLITE_ROW && bbox.size() < limit) {
        bbox.emplace_back(BboxItem{stm.column<int>(0), stm.column<std::string>(1)});
        res = stm.step();
    }
}
