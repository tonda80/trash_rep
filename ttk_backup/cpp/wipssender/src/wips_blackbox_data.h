#ifndef WIPS_BLACKBOX_DATA_H
#define WIPS_BLACKBOX_DATA_H

#include <vector>
#include <cstdint>
#include <string>


struct BboxItem {
    int64_t id;
    std::string data;
    //std::string type;

    static constexpr int64_t no_id() {return -1;}
};

using BlackBoxData = std::vector<BboxItem>;


#endif // WIPS_BLACKBOX_DATA_H
