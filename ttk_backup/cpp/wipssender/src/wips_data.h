#ifndef WIPS_DATA_H
#define WIPS_DATA_H


#include <cstdint>
#include <optional>


struct NavigationData {
    double lat;
    double lon;
    double speed;
    double head;
    double hdop;
    double alt;
    uint16_t sats;
    int64_t time;
};


#endif // WIPS_DATA_H
