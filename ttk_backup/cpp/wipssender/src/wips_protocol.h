#ifndef WIPSPROTOCOL_H
#define WIPSPROTOCOL_H

#include <string>
#include <map>
#include <vector>

#include "wips_data.h"
#include "wips_blackbox_data.h"


struct BboxItem;


namespace wips_protocol
{
    enum class ExtraParamType : uint8_t {INT = 1, DOUBLE = 2, STRING = 3};

    struct ExtraParam
    {
        std::string name;
        ExtraParamType type;
        std::string value;
    };


    struct DataPacket
    {
        std::map<std::string, std::string> data;    // field_name : field_value
        std::vector<ExtraParam> extra_params;
    };


    struct Answer
    {
        std::string type;
        std::string code;
        bool ok;
    };

    struct error : public std::runtime_error {
        error(const std::string& msg) : std::runtime_error(msg) {}
    };

    std::string encode_login(std::string_view id, std::string_view passwd = "NA");
    std::string encode_data(const DataPacket& pck);

    const char* encode_ping();

    constexpr size_t black_box_max_size() {return 5000;}
    std::string encode_black_box(const BlackBoxData& bbox);
    int recv_black_box_packets(const wips_protocol::Answer& answ);     // количество принятых пакетов

    Answer decode_answer(std::string_view str);

    std::tuple<std::string_view, std::string_view> stored_parts(std::string_view str);

    void form_package_navigation(DataPacket&, const std::optional<NavigationData>&);
    void form_package_dido(DataPacket&, std::optional<int32_t>, std::optional<int32_t>);
    void form_package_ai(DataPacket&, std::optional<int32_t>, std::optional<int32_t>);
}

#endif // WIPSPROTOCOL_H
