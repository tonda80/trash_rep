#include <csignal>

#include <zmqpb.h>

#include <mnt_log.h>
#include <request_handler.h>
#include <mnt2app.h>

#include "wipssender_config.h"
#include "wips_client.h"
#include "wips_datamanager.h"




const std::string version()
{
    return "2.0.1";
    // 0.1      pre-release version
}


class VersionHandler : public RequestNoDataHandler<VersionHandler>
{
    void process() {
        std::string json_version = "{\"version\":\"" + version() +"\"}";
        logdebug  << "send_version " << json_version;
        zmqpb::send_mnt_reply(request->header(), json_version, pb::Success, Mnt2App::instance().router_client());
    }
};


#define PAIR(Name, HandlerClass) {Name, HandlerClass::creator}
IRequestHandler::T_handler_map IRequestHandler::creators = {
    PAIR("getversion", VersionHandler),
    PAIR("FireOn", FireOnHandler),
    PAIR("FireOff", FireOffHandler),
    PAIR("NavigationData", NavigationDataHandler),
    PAIR("AlarmButton", AlarmButtonHandler),
    PAIR("DiDoType1", DiDoType1Handler),
};


void fmt_error_handler(const std::string& msg) {
    logerror << "Format error: " << msg;
}
std::function<void(const std::string&)> Fmt::error_handler = fmt_error_handler;


void signal_handler(int signal) {
    Mnt2App::instance().stop_zmq_poll();
}


int main(int argc, const char** argv)
{
    const char* module_name = "wipssender";
    const char* router_client_name = "WIPSSENDER";

    if (utils::has_option("-v", argc, argv)) {
        Fmt::print("{} {}\n", module_name, version());
        return 0;
    }

    try {
        Mnt2App::instance().init(module_name, router_client_name);
        mnt::log(lognotice, "started {}, version {}", module_name, version());

        AppConfig app_config;

        std::signal(SIGINT, signal_handler);

        WipsClient wips_client(app_config);

        WipsDataManager::instance().init(/*app_config,*/ wips_client);

        lognotice << "starting zmq_poll_loop";
        Mnt2App::instance().zmq_poll_loop();
    }
    catch (std::exception& e) {
        logcritical << "exiting due to an exception: " << e.what();
    }
    catch (...) {
        logcritical << "exiting due to an unknown exception";
    }

    return 0;
}
