#include "canitem_processer.h"

#include <map>
#include <algorithm>

#include <json/json.h>

#include "mnt2app.h"
#include "filesystem_wrapper.h"

#include "tsdiag_sqlite_storage.h"
#include "config_dictionary.h"
#include "tsdiag_processer.h"
#include "temperature_processer.h"


std::tuple<std::string, std::string> get_can_item_value(const ConfigParameter& par, const pb::CanItem& item, extra_data_container& extra_data) {
    uint64_t parsed_value = item.parsedvalue();

    std::string value;
    std::string filter_key;
    if (par.id == "TIRE_PRESS") {   // для давления шин значение получаем сами, и учитываем что в одном кан сообщении приходят значения для разных колес
        auto value_num = parsed_value & 0xff;
        auto wheel_num = parsed_value >> 8 & 0xff;

        std::string wheel_num_str = utils::hex(wheel_num);
        extra_data["wheel_num"] = wheel_num_str;

        value = std::to_string(value_num/25.0);     // переводим в бары x*4000/100000
        filter_key = par.id + wheel_num_str;
    }
    else {
        if (par.is_dtc()) {
            value = "1";
        }
        else {
            value = ConfigDictionary::get_value(par, parsed_value);
        }
        filter_key = par.id;
    }

    extra_data["value"] = value;

    return std::make_tuple(value, filter_key);
}


void process_parameter(const ConfigParameter& par, const pb::CanItem& item, extra_data_container& extra_data) {
    if (!ConfigDictionary::instance().is_needed_canindex(item)) {
        logdebug  << "[CanItemHandler::process_parameter] ignored by inappropriate canindex: " << item.canindex() << " " << std::hex << item.address();
        return;
    }

    if (par.has_flag('m') && !BoardTemperatureHandler::use_can_temperature()) {
        logdebug  << "[CanItemHandler::process_parameter] ignored temperature parameter " << par.id;
        return;
    }

    if (par.has_placeholder("canid")) {
        extra_data["canid"] = utils::hex(item.address());
    }

    auto [value, filter_key] = get_can_item_value(par, item, extra_data);

    process_tsdiag_item(par, extra_data, true, &filter_key, item.rawvalue());
}


// helper. n в диапазоне [1, 8], считаем что байт 1 соответствует старшему байту нашего RawValue
uint _b(uint64_t src, uint8_t n, uint8_t mask=0xff) {
    return src >> 8*(8 - n) & mask;
}

void process_dm_message(const dm1_par_container& par_map, const pb::CanItem& item, extra_data_container& extra_data) {
    const uint64_t& v = item.rawvalue();
    uint spn = (_b(v, 5, 0xe0) << (2*8-5)) | (_b(v, 4) << 1*8) | _b(v, 3);
    uint fmi = _b(v, 5, 0x1f);

    auto par_ptr = ConfigDictionary::instance().dm1_parameter_ptr(par_map, spn, fmi);
    if (!par_ptr) {
        logdebug << "unknown spn " << spn << " fmi " << fmi << " for CanItem " << item.name();
        return;
    }
    const ConfigParameter& par = *par_ptr;

    if (par.has_placeholder("spn")) {
        extra_data.emplace("spn", std::to_string(spn));
    }
    if (par.has_placeholder("fmi")) {
        extra_data.emplace("fmi", std::to_string(fmi));
    }
    if (par.has_flag('t')) {
        extra_data.emplace("N", std::to_string(par.spn - par.base_spn + 1));
    }

    process_parameter(par, item, extra_data);
}


void CanItemHandler::process() {
    for (const auto& item : data.items()) {
        extra_data_container extra_data;

        if (const auto map_ptr = ConfigDictionary::instance().dm1_parameters_ptr(item.name())) {
            process_dm_message(*map_ptr, item, extra_data);
        }
        else if (const auto par_ptr = ConfigDictionary::instance().parameter_ptr(item.name())) {
            process_parameter(*par_ptr, item, extra_data);
        }
        else {
            //logdebug  << "[CanItemHandler::process] ignored: " << item.name() << " " << item.address() << " " << item.comment();
        }
    }

    auto transaction_time = TsdiagStorage::instance().commit_if_need();
    if (transaction_time > 0) {
        logdebug << "[CanItemHandler::process] canitems sqlite processing time " << transaction_time;
    }
}
