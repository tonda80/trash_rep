#include "expression.h"

#include <vector>
#include <map>
#include <optional>
#include <cctype>



struct Token;
class Argument;

using NumT = Expression::NumT;
using StrIter = std::string::const_iterator;

using TokenPtr = std::unique_ptr<Token>;
using TokenContainer = std::vector<TokenPtr>;
using ValueContainer = std::vector<NumT>;
using ArgumContainer = std::vector<Argument*>;
using ArgsT = const ValueContainer&;
using FuncT = std::function<NumT(ArgsT)>;


#if defined(EXPRESSION_TEST)
#include <iostream>
void print_tokens(const std::string& pref, TokenContainer& tt);
#endif


std::function<void(const std::string&)> Expression::report_warn;


enum Priority {
    P_NONE = -1,
    P_PLUS = 10,
    P_MULT = 20,
    P_FUNC = 30,
};


struct Token {
    virtual ~Token() = default;
    virtual std::string what() = 0;

    virtual bool is_operand() {return false;}
    virtual bool is_operation() {return false;}
    virtual bool is_symbol(const char) {return false;}

    virtual Priority priority() {return  P_NONE;}

    virtual NumT get_value() {throw expression_error("unexpected ExprToken::get_value call");}
    virtual void eval(ValueContainer&) {throw expression_error("unexpected ExprToken::eval call");}
};


class Symbol: public Token {
    const char type;
public:
    Symbol(char t) : type(t) {}
    virtual bool is_symbol(const char t) final {return t == type;}
    virtual std::string what() final {return std::string(1, type);}

    static TokenPtr create(StrIter& it) {
        for (auto c : ")(,") {
            if (*it == c) return TokenPtr(new Symbol(*it++));
        }
        return nullptr;
    }
};


class Operand: public Token {
protected:
    std::optional<NumT> value;
public:
    virtual NumT get_value() final {
        if (value) {
            return value.value();
        }
        throw expression_error("[Operand::get_value] uninitialized operand");
    }
    virtual bool is_operand() final {return true;}
};


class Number : public Operand {
public:
    virtual std::string what() final {return std::to_string(value.value());}
    Number(NumT v) {value = v;}

    template<typename T>
    static TokenPtr create(StrIter& it, const StrIter end);
};
// double specialization
template<>
TokenPtr Number::create<double>(StrIter& it, const StrIter end) {
    std::size_t pos;
    try {
        double d = std::stod(std::string(it, end), &pos);
        it += pos;
        return TokenPtr(new Number(d));
    }
    catch (...) {
        return nullptr;
    }
}


class Argument : public Operand {
public:
    const char name;
    Argument(char n) : name(n) {}
    virtual std::string what() final {
        return std::string(1, name) + "=" + (value ? std::to_string(value.value()) : "?");
    }

    void set_value(const NumT& v) {
        value = v;
    }

    static TokenPtr create(StrIter& it, const std::string& options, ArgumContainer& arg_cont) {
        if (options.find(*it) != std::string::npos) {
            arg_cont.push_back(new Argument(*it++));
            return TokenPtr(arg_cont.back());
        }
        return nullptr;
    }
};


class Operation: public Token {
public:
    virtual bool is_operation() final {return true;}
    virtual Priority priority() final {return  priority_;}

    virtual void eval(ValueContainer& eval_stack) final {
        eval_stack.push_back(operation(get_args(eval_stack)));
    }

protected:
    Operation(const FuncT& o, size_t q, Priority p) : operation(o), q_args(q), priority_(p) {}

private:
    FuncT operation;
    size_t q_args;
    Priority priority_;

    ValueContainer get_args(ValueContainer& eval_stack) {
        if (eval_stack.size() < q_args) {
            throw expression_error("[Operation::get_args] not enough arguments for "+what());
        }
        ValueContainer res(eval_stack.end() - q_args, eval_stack.end());
        eval_stack.erase(eval_stack.end() - q_args, eval_stack.end());
        return res;
    }
};


class BinOperation: public Operation {
    const char type;
public:
    BinOperation(char t, const FuncT& o, Priority p) : Operation(o, 2, p), type(t) {}
    virtual std::string what() final {return std::string(1, type);}

    static TokenPtr create(StrIter& it) {
        char c = *it++;
        if (c == '*') {
            return TokenPtr(new BinOperation(c, [](ArgsT a){return a[0]*a[1];}, P_MULT));
        }
        else if (c == '/') {
            return TokenPtr(new BinOperation(c, [](ArgsT a){return a[0]/a[1];}, P_MULT));
        }
        else if (c == '+') {
            return TokenPtr(new BinOperation(c, [](ArgsT a){return a[0]+a[1];}, P_PLUS));
        }
        else if (c == '-') {
            return TokenPtr(new BinOperation(c, [](ArgsT a){return a[0]-a[1];}, P_PLUS));
        }
        --it;
        return nullptr;
    }
};


struct FunctionDescriptor {
    std::string name;
    FuncT operation;
    size_t q_args;
    FunctionDescriptor(const std::string& n, const FuncT& o, size_t q) :
        name(n), operation(o), q_args(q) {}
};

class Function: public Operation {
    const std::string name;
public:
    Function(const FunctionDescriptor& f) : Operation(f.operation, f.q_args, P_FUNC), name(f.name) {}

    virtual std::string what() final {return name;}

    static TokenPtr create(const std::initializer_list<FunctionDescriptor>& funcs, StrIter& it, const StrIter end) {
        for (const auto& f : funcs) {
            auto sz = f.name.size();
            if ((end - it) > sz && f.name == std::string(it, it+sz)) {
                it += sz;
                return TokenPtr(new Function(f));
            }
        }
        return nullptr;
    }
};


namespace  {
    // меняет местами 2 младших байта parsed_value
    NumT swap2(ArgsT args) {
        uint64_t parsed_value = static_cast<uint64_t>(args[0]);
        if (Expression::report_warn && parsed_value >= (1 << 16)) {
            Expression::report_warn("[swap2] value >= 2^16");
        }

        uint64_t swap_value = ((parsed_value>>8)&0xff) | ((parsed_value<<8)&0xff00);
        //std::cout << "__deb Swap2Func " << parsed_value << " " << swap_value << std::endl;
        return swap_value;
    }

    NumT swap4(ArgsT args) {
        uint64_t parsed_value = static_cast<uint64_t>(args[0]);
        if (Expression::report_warn && parsed_value >= (1l << 32)) {
            Expression::report_warn("[swap4] value >= 2^32");
        }

        uint64_t swap_value = ((parsed_value>>24)&0xff) | ((parsed_value>>8)&0xff00) | ((parsed_value<<8)&0xff0000) | ((parsed_value<<24)&0xff000000);
        //std::cout << "__deb Swap2Func " << parsed_value << " " << swap_value << std::endl;
        return swap_value;
    }
}


static const std::initializer_list<FunctionDescriptor> functions = {
#if defined(EXPRESSION_TEST)
    FunctionDescriptor("sqr", [](ArgsT a){return a[0]*a[0];},  1),
    FunctionDescriptor("max3", [](ArgsT a){return std::max(std::max(a[0], a[1]), a[2]);}, 3),
#endif
    FunctionDescriptor("swap2", swap2, 1),
    FunctionDescriptor("swap4", swap4, 1),
};



std::string remove_spaces(const std::string& src) {
    std::string ret = src;
    ret.erase(std::remove_if(ret.begin(), ret.end(), [](char c){return std::isspace(c);}), ret.end());
    return ret;
}


class ExpressionImpl {
    ArgumContainer args;
    TokenContainer rpn_expression;
    std::string src_str;
    std::string arg_options;
public:
    const std::string& get_src_string() {return src_str;}

    ExpressionImpl(const std::string& str, const std::string& str_args) {
        src_str = remove_spaces(str);
        arg_options = remove_spaces(str_args);

        TokenContainer src_expression = split();
        init_rpn(src_expression);
    }

    NumT eval(const std::map<char, NumT> args_map) const {
        set_args(args_map);

        ValueContainer stack;
        for (const auto& token : rpn_expression) {
            if (token->is_operand()) {
                stack.push_back(token->get_value());
            }
            else if (token->is_operation()) {
                token->eval(stack);
            }
            else {
                throw expression_error("[ExpressionImpl::eval] unknown token " + token->what());
            }
        }

        if (stack.size() != 1) {
            throw expression_error(src_str + " malformed expression : no result after evaluation, stack_size=" + std::to_string(stack.size()));
        }
        return stack[0];
    }

private:
    TokenContainer split() {
        std::initializer_list<std::function<TokenPtr(StrIter&)>> creators = {
            Symbol::create,
            BinOperation::create,
            [this] (StrIter& it) {return Argument::create(it, arg_options, args);},
            [this] (StrIter& it) {return Number::create<NumT>(it, src_str.end());},             // строго после BinOperation
            [this] (StrIter& it) {return Function::create(functions, it, src_str.end());},      // цикл внутри
        };

        TokenContainer tokens;
        for (auto it = src_str.cbegin(); it < src_str.cend(); ) {
            if (std::isspace(*it)) {
                ++it;
                continue;
            }

            TokenPtr token;
            for (const auto& creator : creators) {
                token = creator(it);

                // убедимся, что +- не часть числа
                if (token && token->is_operation() && (token->what()=="+" || token->what()=="-") &&
                        (tokens.empty() || !(tokens.back()->is_operand() || tokens.back()->is_symbol(')')))) {
                    token = nullptr;    // таки похоже, что часть
                    --it;
                }

                if (token) break;
            }
            if (token) {
                tokens.push_back(std::move(token));
            }
            else {
                throw expression_error("[split] cannot get token ("+std::to_string(it-src_str.begin())+")");
            }
        }
        return tokens;
    }

    static TokenContainer::value_type _pop(TokenContainer& stack) {  // helper
        auto res = std::move(stack.back());
        stack.pop_back();
        return res;
    }

    void init_rpn(TokenContainer& src_expression) {
        TokenContainer stack;
        for (auto& token : src_expression) {
            if (token->is_operand()) {
                rpn_expression.push_back(std::move(token));
            }
            else if (token->is_operation()) {
                while (!stack.empty() && stack.back()->is_operation() && stack.back()->priority() >= token->priority()) {
                    rpn_expression.push_back(_pop(stack));
                }
                stack.push_back(std::move(token));
            }
            else if (token->is_symbol('(')) {
                stack.push_back(std::move(token));
            }
            else if (token->is_symbol(')')) {
                while (!stack.empty() && !stack.back()->is_symbol('(')) {
                    rpn_expression.push_back(_pop(stack));
                }
                if (stack.back()->is_symbol('(')) {
                    _pop(stack);
                }
                   else {
                    throw expression_error("inconsistent brackets in " + src_str);
                }
            }
            else if (token->is_symbol(',')) {
                // validate?
            }
            else {
                throw expression_error("uknown token " + token->what());
            }
        }

        while (!stack.empty()) {
           if (!stack.back()->is_operation()) {
               throw expression_error("malformed expression: no operation into stack (inconsistent brackets): " + src_str);
           }
           rpn_expression.push_back(_pop(stack));
        }
    }

    void set_args(const std::map<char, NumT> args_map) const {
        for (auto& arg : args) {
            auto it = args_map.find(arg->name);
            if (it == args_map.end()) {
                throw expression_error("[ExpressionImpl::eval] not initialized argument " + arg->what());
            }
            arg->set_value(it->second);
        }
    }
};



Expression::Expression(const std::string& src)
{
    impl = std::make_unique<ExpressionImpl>(src, "x");
}

Expression::~Expression() = default;

std::string Expression::calculate(NumT value,  NumT* result) const
{
    std::map<char, NumT> args_map = {{'x', value}};
    NumT res = impl->eval(args_map);
    if (result) {
        *result = res;
    }

    char buf[64] = {};
    snprintf(buf, sizeof(buf)-1, "%g", res);
    //std::string str_res = std::to_string(res);
    return buf;
}

const std::string& Expression::source_string() const {
    return impl->get_src_string();
}



#if defined(EXPRESSION_TEST)
// c++ -std=c++17 -DEXPRESSION_TEST expression.cpp


void print_tokens(const std::string& pref, TokenContainer& tt) {
    std::cout << "\n__deb: " << pref << " => ";
    for (const auto& t : tt) {
        std::cout << t->what() << " ";
    }
    std::cout << std::endl;
}

void test_expression(const std::string& s, NumT exp_res) {
    try {
        Expression expr(s);
        NumT calc_res;
        auto res = expr.calculate(0, &calc_res);
        auto report = calc_res == exp_res ? "OK\t" : "FAIL\t";
        std::cout << report << expr.source_string() << " = " << res << "\t" << std::endl;
    }
    catch (expression_error& err) {
        std::cout << "THROW\t" << s << " => " << err.what() << std::endl;
    }
}

void test_expression_args(Expression& expr, NumT arg_value, NumT exp_res) {
    auto pref = expr.source_string() + " with (" + std::to_string(arg_value) + ")";
    try {
        NumT calc_res;
        auto res = expr.calculate(arg_value, &calc_res);
        auto report = calc_res == exp_res ? "OK\t" : "FAIL\t";
        std::cout << report << pref << " = " << res << std::endl;
    }
    catch (expression_error& err) {
        std::cout << "THROW\t" << pref << " => " << err.what() << std::endl;
    }
}

int main(int argc, const char** argv)
{
    Expression::report_warn = [](const std::string& msg){std::cout << "warning! " << msg << std::endl;};

    test_expression("-3",    -3);
    test_expression("1 + 2*\t\t 3 ",   7);
    test_expression("1+20/-3+3*4",   19./3);
    test_expression("1+ 2.2 +3.1   +4",     10.3);
    test_expression("1+2*3 - sqr( 2 )",     3);
    //test_expression("1/-0",  -inf);
    test_expression("sqr(4) - 3*4 + max3(-20, -7, -10)",     -3);
    test_expression("max3(2, 7, 100)*sqr(-2) - 3*4",     388);
    test_expression("swap2(17185)",     0x2143);       // 0x4321 == 17185

    // arg
    Expression e1("sqr(4) - x*4.1 + max3(-7, x, 0)");
    test_expression_args(e1, 2.2,       9.18);
    test_expression_args(e1, 10, -15);

    /*
    std::cout << "\nwarnings and errors\n";
    test_expression("swap2(9991764)",     0x5476);       // 0x987654 == 9991764
    test_expression("1+(2*3", 0);
    test_expression("1+++", 0);
    test_expression("1 + qwa", 0);
    test_expression("sqr(4, 12, 34)", 0);
    test_expression("sqr(4) - 3*4 + max3(2, 7)", 0);
    test_expression("max3(2, 7)*sqr(4) - 3*4", 0);
    // */

    return 0;
}

#endif // defined(EXPRESSION_TEST)
