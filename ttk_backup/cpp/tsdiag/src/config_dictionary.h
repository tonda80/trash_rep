#ifndef CONFIGDICTIONARY_H
#define CONFIGDICTIONARY_H

#include <string>
#include <set>
#include <algorithm>

#include <json/json.h>

#include "expression.h"

#include <mnt_log.h>



namespace pb {
    class CanItem;
}


struct ConfigParameter {
    std::string id;
    //std::string desc;
    std::string flags;      // zZhd - флаги обработки.
                            // + специальные признаки (может их надо отдельно иметь?):
                            // t (Tires) - шины обрабатываются по особому, чтобы не загромоэжать словарь одинаковыми параметрами
                            // m (teMperature) - игнорируем параметры температуры в специальных случаях
    int group_id;
    // dtc
    std::string dm1_name;
    int spn;
    int fmi;
    //
    std::set<std::string> placeholders;
    //
    int base_spn;   // костыль для параметров шин

    ConfigParameter(const Json::Value& p);

    bool is_dtc() const {
        return !dm1_name.empty();
    }

    bool has_flag(char f) const {
        return flags.find(f) != std::string::npos;
    }

    bool has_placeholder(std::string_view p) const {
        return std::find(placeholders.begin(), placeholders.end(), p) != placeholders.end();
    }

private:
    void init_placeholders(const Json::Value& par);
};


using dm1_par_container = std::multimap<uint, ConfigParameter>;         // spn : ConfigParameter


class ConfigDictionary {
public:
    static std::string par_dict_path();

    static ConfigDictionary& instance() {
        static ConfigDictionary instance;
        return instance;
    }
    void init();

private:
    std::map<std::string, ConfigParameter> parameters;              // id(canItemName) : ConfigParameter
    std::map<std::string, dm1_par_container> dm1_parameters;        // dm1CanItemName : {spn : ConfigParameter}

    std::map<uint32_t, std::set<uint32_t>> can_index_filter;        // can_addr : [can_indexes]

    std::map<std::string, Expression> expressions;                  // par.id : expression

    void add_dm1_parameter(ConfigParameter&& par);

    void can_index_filter_init(const Json::Value& json_obj);

    void params_init(const Json::Value& json_params);

    void try_add_expression(const ConfigParameter& par, const std::string& str);
    const Expression* get_expression(const ConfigParameter& par);
public:
    const dm1_par_container* dm1_parameters_ptr(const std::string& name);

    const ConfigParameter* dm1_parameter_ptr(const dm1_par_container& par_map, uint spn, uint fmi);

    const ConfigParameter* parameter_ptr(const std::string& name);

    bool is_needed_canindex(const pb::CanItem& item) const;

    template <typename T>
    static std::string get_value(const ConfigParameter& par, const T num_value) {
        std::string value;
        if (auto expr = ConfigDictionary::instance().get_expression(par)) {
            try {
                value = expr->calculate(num_value);
            }
            catch (expression_error& e) {
                mnt::log(logerror, "cannot calculate value for {} with value={} : {}", par.id, num_value, e.what());
                value = std::to_string(num_value);
            }
        }
        else {
            value = std::to_string(num_value);
        }
        return value;
    }
};


#endif // CONFIGDICTIONARY_H
