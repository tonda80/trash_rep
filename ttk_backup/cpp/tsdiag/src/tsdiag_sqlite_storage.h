#ifndef TSDIAG_SQLITE_STORAGE_H
#define TSDIAG_SQLITE_STORAGE_H

#include <string_view>
#include <memory>

#include "sqlite_db.h"

class TsdiagStorage : public SqliteDbBase
{
public:
    static std::string db_path();
    static std::string db_old_path();

    static TsdiagStorage& instance() {
        static TsdiagStorage instance;
        return instance;
    }
    void init();

    void add_event(std::string_view id, std::string value, std::string_view map, int group_id, uint64_t raw_value);

public:
    void begin_transaction_once();      // no thread-safe
    long commit_if_need();

private:
    TsdiagStorage() = default;
    static void db_rotate();
    void init(const std::string& dbpath, int db_limit_day);
    void init_tables();

    long begin_transaction_time = 0;
};

#endif // TSDIAG_SQLITE_STORAGE_H
