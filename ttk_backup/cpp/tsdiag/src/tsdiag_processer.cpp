#include "tsdiag_processer.h"

#include <lib/logger/logger.h>

#include "utils.h"
#include "mnt2app.h"
#include "config_dictionary.h"
#include "tsdiag_sqlite_storage.h"


void ProcFilter::init() {
    pub_timeout = Mnt2App::instance().config().get("pubTimeout", 5).asInt();
}

bool ProcFilter::check_change_and_remember(const std::string& key, const std::string& value) {
    const auto it = last_values.find(key);
    bool res = it == last_values.end() || it->second != value;
    last_values[key] = value;
    //logdebug << "[check_change_and_remember] " << key << " " << value << " " << res;
    return res;
}

bool ProcFilter::check_pub_time(const std::string& key) {
    const auto it = last_pub_times.find(key);
    bool res = it == last_pub_times.end() || it->second < (utils::timestamp_sec() - pub_timeout);
    //logdebug << "[check_pub_time_and_remember_if_true]" << pub_timeout << " " << key << " " << res << " " << last_pub_times[key];
    return res;
}

void ProcFilter::remember_pub_time(const std::string& key) {
    last_pub_times[key] = utils::timestamp_sec();
}


// helper
std::string extra_data_to_string(const extra_data_container& extra_data) {
    std::stringstream ss;
    Json::Value root;
    for (const auto& e : extra_data) {
        root[e.first] = e.second;
    }
    utils::json_fast_writer()->write(root, &ss);
    return ss.str();
}

void publish_message(const ConfigParameter& par, const std::string& value, const std::string& extra_data_str) {
    std::stringstream ss;
    Json::Value root;

    root["id"] = par.id;
    root["map"] = extra_data_str;
    root["groupId"] = par.group_id;
    root["value"] = value;

    utils::json_fast_writer()->write(root, &ss);

    //logdebug  << "send_to_zmq " << ss.str();
    zmqpb::send_mnt_msg({"UI"}, "tsdiagevent", ss.str(), Mnt2App::instance().router_client());
}

void process_tsdiag_item(const ConfigParameter& par, const extra_data_container& extra_data, bool begin_transaction,
                         const std::string* filter_key, uint64_t can_raw_value) {
    logdebug  << "[process_tsdiag_item] " << par.id << " " << par.flags;

    if (!filter_key) {
        filter_key = &par.id;
    }

    const std::string& value = extra_data.at("value");  // must be
    bool is_changed = ProcFilter::instance().check_change_and_remember(*filter_key, value);

    std::string extra_data_str = extra_data_to_string(extra_data);

    bool pub_flag = par.has_flag('Z');
    pub_flag = pub_flag || (par.has_flag('z') && (is_changed || ProcFilter::instance().check_pub_time(*filter_key)));
    if (pub_flag) {
        publish_message(par, value, extra_data_str);
        ProcFilter::instance().remember_pub_time(*filter_key);
    }

    if (par.has_flag('h') || (par.has_flag('d') && is_changed)) {
        if (begin_transaction) {
            TsdiagStorage::instance().begin_transaction_once();
        }
        TsdiagStorage::instance().add_event(par.id, value, extra_data_str, par.group_id, can_raw_value);
    }
}
