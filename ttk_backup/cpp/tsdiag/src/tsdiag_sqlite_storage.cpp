#include "tsdiag_sqlite_storage.h"

#include <chrono>

#include "lib/logger/logger.h"

#include "filesystem_wrapper.h"
#include "mnt2app.h"



_filesystem::path db_dir() {
    static std::string path;
    if (path.empty()) {
        std::string name = Mnt2App::instance().config().get("dbDirPath", ".").asString();
        path = _filesystem::canonical(name);
    }
    return path;
}

// helper
void remove_if_exists(std::string_view p) {
    if (_filesystem::exists(p)) {
        _filesystem::remove(p);
    }
}

void TsdiagStorage::db_rotate() {
    uint64_t rotate_size = Mnt2App::instance().config().get("dbRotateSizeMb", 0).asUInt();
    if (rotate_size == 0) {
        return;
    }
    loginfo << "db rotate size " << rotate_size;

    rotate_size *= 1024*1024;
    uint64_t db_size = 0;
    try {
        db_size = _filesystem::file_size(db_path());
    }
    catch (...) {
        return;
    }

    if (db_size > rotate_size) {
        logwarning << "db will be rotated (" << db_size << ")";

        std::string tmp_path = db_path() + ".bkp";
        remove_if_exists(tmp_path);

        _filesystem::rename(db_path(), tmp_path);
        remove_if_exists(db_old_path());
        _filesystem::rename(tmp_path, db_old_path());

        logwarning << "db was rotated";
    }
}

std::string TsdiagStorage::db_path() {
    static std::string path;
    if (path.empty()) {
        std::string name = Mnt2App::instance().config().get("dbName", "tsdiag.db").asString();
        path = db_dir()/name;
    }
    return path;
}

std::string TsdiagStorage::db_old_path() {
    static std::string path;
    if (path.empty()) {
        std::string name = Mnt2App::instance().config().get("dbOldName", "tsdiag_old.db").asString();
        path = db_dir()/name;
    }
    return path;
}

void TsdiagStorage::init() {
    db_rotate();

    int db_limit_day = Mnt2App::instance().config().get("dbRotateDay", 0).asInt();
    init(db_path(), db_limit_day);
}

void TsdiagStorage::init(const std::string& dbpath, int db_limit_day)
{
    open(dbpath.c_str());
    init_tables();

    loginfo << "opened " << dbpath;

    if (db_limit_day > 0) {
        long limit_sec = utils::timestamp_sec() - db_limit_day*24*3600;

        loginfo << "records older than " << db_limit_day << " days will be removed from database ";
        logdebug << "limit_sec = " << limit_sec;

        Statement stm("DELETE FROM diag WHERE time < datetime(?1, 'unixepoch');", this);
        stm.exec_no_throw([](const char* what){
            logerror << "cannot remove old records into database: " << what;
        }, limit_sec);
    }
}

void TsdiagStorage::init_tables() {
    // -- diagnostic
    // id       id параметра
    // time     время записи
    // map      плейсхолдеры описания
    // type     тип (0 historical event, 1 state (похоже такое вообще не нужно), ?)
    // group_id идентификатор группы записей (нужен ui)
    // value
    // raw_value все 8 байт кан сообщения
    //
    if (!exec_request(R"(
            CREATE TABLE IF NOT EXISTS diag
            (
                id         TEXT        ,
                time        TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
                map         TEXT        ,
                type        INTEGER     ,
                group_id    INTEGER     ,
                value       TEXT

            );
        )")) {
        throw sqlitedb_error("cannot create db table");
    }

    if (!exec_request("PRAGMA journal_mode=WAL;")) {
        logerror << "cannot open db in WAL mode";
    }

    auto version = statement("PRAGMA user_version;").exec_r().column<int64_t>(0);
    loginfo << "db version = " << version;
    if (version == 0) {
        statement("ALTER TABLE diag ADD raw_value INTEGER;").exec();
        statement("PRAGMA user_version = 1;").exec();
        loginfo << "changed db version to 1";
    }
}


void sqlite_err_clb(const char* what) {
    logerror << what;
}

void TsdiagStorage::add_event(std::string_view id, std::string value, std::string_view map, int group_id, uint64_t raw_value)
{
    static Statement stm(
        "INSERT INTO diag (id, map, group_id, value, type, raw_value) VALUES (?, ?, ?, ?, 0, ?);", this);

    stm.exec_no_throw(sqlite_err_clb, id, map, group_id, value, raw_value);
}


long get_time_point_ms() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
}

void TsdiagStorage::begin_transaction_once() {
    static Statement stm("BEGIN TRANSACTION;", this);

    if (begin_transaction_time == 0) {
        stm.exec_no_throw(sqlite_err_clb);
        begin_transaction_time = get_time_point_ms();
    }

}

long TsdiagStorage::commit_if_need() {
    static Statement stm("COMMIT;", this);

    long res = 0;
    if (begin_transaction_time != 0) {
        stm.exec_no_throw(sqlite_err_clb);
        res = get_time_point_ms() - begin_transaction_time;
        begin_transaction_time = 0;
    }
    return res;
}
