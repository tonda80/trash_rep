#ifndef CANITEM_PROCESSER_H
#define CANITEM_PROCESSER_H

#include "request_handler.h"
#include <pb/canparser.pb.h>


class CanItemHandler : public RequestHandler<pb::CanPacket, CanItemHandler>
{
private:
    void process();
};

#endif // CANITEM_PROCESSER_H
