#include <fstream>
#include <future>

#include <json/json.h>

#include <lib/logger/logger.h>
#include <zmqpb.h>

#include "utils.h"
#include "request_handler.h"
#include "mnt2app.h"

#include "canitem_processer.h"
#include "temperature_processer.h"
#include "tsdiag_sqlite_storage.h"
#include "config_dictionary.h"
#include "tsdiag_processer.h"



const std::string version()
{
    return "2.0.6";
    // 0.6      temp module support
    // 0.5      raw value to database
    // 0.4      expression support
    // 0.3      wal mode (+)
    // 0.2      different fixes
    // 0.1      pre-release version
}


class VersionHandler : public RequestNoDataHandler<VersionHandler>
{
    void process() {
        std::string json_version = "{\"version\":\"" + version() +"\"}";
        logdebug  << "send_version " << json_version;
        zmqpb::send_mnt_reply(request->header(), json_version, pb::Success, Mnt2App::instance().router_client());
    }
};

class DbInfoHandler : public RequestNoDataHandler<DbInfoHandler>
{
    void process() {
        std::stringstream ss;
        Json::Value root;

        root["db_path"] = TsdiagStorage::db_path();
        root["db_old_path"] = TsdiagStorage::db_old_path();
        root["dict_path"] = ConfigDictionary::par_dict_path();
        root["db_columns"]["id"] = "id";
        root["db_columns"]["time"] = "time";
        root["db_columns"]["map"] = "map";
        root["db_columns"]["type"] = "type";
        root["db_columns"]["group_id"] = "group_id";
        root["db_columns"]["value"] = "value";

        utils::json_fast_writer()->write(root, &ss);

        logdebug  << "getdbinfo " << ss.str();
        zmqpb::send_mnt_reply(request->header(), ss.str(), pb::Success, Mnt2App::instance().router_client());
    }
};


#define PAIR(Name, HandlerClass) {Name, HandlerClass::creator}
IRequestHandler::T_handler_map IRequestHandler::creators = {
    PAIR("getversion", VersionHandler),
    PAIR("CanMessage", CanItemHandler),
    PAIR("getdbinfo", DbInfoHandler),
    PAIR("AVERAGE", BoardTemperatureHandler),
};


void fmt_error_handler(const std::string& msg) {
    logwarning << "Format error: " << msg;
}
std::function<void(const std::string&)> Fmt::error_handler = fmt_error_handler;

int main(int argc, const char** argv)
{
    const char* module_name = "tsdiag";
    const char* router_client_name = "TSDIAG";

    if (utils::has_option("-v", argc, argv)) {
        std::cout << module_name << " " << version() << std::endl;
        return 0;
    }

    try {
        Mnt2App::instance().init(module_name, router_client_name);
        mnt::log(loginfo, "started {}, version {}", module_name, version());

        TsdiagStorage::instance().init();       // sqlitedb opening there
        ConfigDictionary::instance().init();
        ProcFilter::instance().init();
        BoardTemperatureHandler::init();        // после словаря

        // запишем на всякий случай для разбора полетов рабочий словарь
        TsdiagStorage::instance().add_event("__par_dict_path__", ConfigDictionary::par_dict_path(), "{}", -1, 0);

        loginfo << "starting zmq_poll_loop";
        Mnt2App::instance().zmq_poll_loop();
    }
    catch (std::exception& e) {
        logcritical << "exiting due to an exception: " << e.what();
    }
    catch (...) {
        logcritical << "exiting due to an unknown exception";
    }

    return 0;
}
