#include "temperature_processer.h"

#include "config_dictionary.h"
#include "tsdiag_processer.h"
#include "mnt2app.h"

#include <mnt_log.h>


const int USE_CAN_TEMPER = 1;      // b0 - процессим температуру из can
const int USE_MNT_TEMPER = 2;      // b1 - процессим температуру от модуля temp
static int temperature_mode = 3;

static const char* PAR_ID_EXT = "BoardTemperatureExternal";
static const char* PAR_ID_INT = "BoardTemperatureInternal";
static const ConfigParameter* ext_par_ptr = nullptr;
static const ConfigParameter* int_par_ptr = nullptr;

void BoardTemperatureHandler::init() {
    temperature_mode = Mnt2App::instance().config().get("temperatureMode", 3).asInt();
    mnt::log(loginfo, "Temp sources: can - {}, mnt - {} ({})", use_can_temperature(), use_mnt_temperature(), temperature_mode);

    ext_par_ptr = ConfigDictionary::instance().parameter_ptr(PAR_ID_EXT);
    int_par_ptr = ConfigDictionary::instance().parameter_ptr(PAR_ID_INT);
    if (use_mnt_temperature()) {
        if (!ext_par_ptr) {
            mnt::log(logerror, "config doesn\'t have {} parameter", PAR_ID_EXT);
        }
        if (!int_par_ptr) {
            mnt::log(logerror, "config doesn\'t have {} parameter", PAR_ID_INT);
        }
    }
};

bool BoardTemperatureHandler::use_can_temperature() {return temperature_mode & USE_CAN_TEMPER;}
bool BoardTemperatureHandler::use_mnt_temperature() {return temperature_mode & USE_MNT_TEMPER;}


void process_mnt_temp(const ConfigParameter* par_ptr, int32_t value) {
    if (!par_ptr) {
        return;     // error log в init
    }
    const auto par = *par_ptr;
    extra_data_container extra_data;
    extra_data.emplace("value", ConfigDictionary::get_value(par, value));
    process_tsdiag_item(par, extra_data);
}

void BoardTemperatureHandler::process() {
    if (use_mnt_temperature()) {
        if (data.externaltemperaturevalid()) {
            process_mnt_temp(ext_par_ptr, data.externaltemperature());
        }
        if (data.internaltemperaturevalid()) {
            process_mnt_temp(int_par_ptr, data.internaltemperature());
        }
    }
}
