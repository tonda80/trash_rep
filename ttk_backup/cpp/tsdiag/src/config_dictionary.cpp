#include "config_dictionary.h"

#include <mnt_log.h>

#include <pb/canparser.pb.h>

#include "mnt2app.h"
#include "filesystem_wrapper.h"


ConfigParameter::ConfigParameter(const Json::Value& p) {
    id = p.get("id", "").asString();
    //desc = p.get("desc", "").asString();
    flags = p.get("flags", "").asString();
    group_id = p.get("groupId", -1).asInt();

    dm1_name = p.get("dm1Name", "").asString();
    spn = p.get("spn", 0).asInt();
    base_spn = spn;
    fmi = p.get("fmi", 0).asInt();

    init_placeholders(p);
}

void ConfigParameter::init_placeholders(const Json::Value& par) {
    for (const auto& par_name : {"desc", "err_code", "advice"}) {
        std::string check_str = par.get(par_name, "").asString();
        std::string::size_type start = check_str.find("{");
        while (start != std::string::npos) {
            auto end = check_str.find("}", start+2);
            if (end == std::string::npos  ) {
                throw mnt2app_error("[ConfigParameter.init_placeholders] bad description of "+id);
            }
            else {
                placeholders.emplace(check_str.substr(start+1, end-start-1));
                start = check_str.find("{", end+1);
            }
        }
    }
    //if (!placeholders.empty()) {auto l = logdebug; l << id << " placeholders: "; for (auto& p : placeholders) l << p << " ";}
}


void ConfigDictionary::add_dm1_parameter(ConfigParameter&& par) {
    if (par.spn < 0) {
        logerror << "parameter has dm1Name " << par.dm1_name << ", but has no spn, ignoring it";
        return;
    }
    auto range = dm1_parameters[par.dm1_name].equal_range(static_cast<uint>(par.spn));
    for (auto it = range.first; it != range.second; ++it) {
        if (it->first == static_cast<uint>(par.fmi)) {
            logerror << "spn " << par.spn << " fmi " << par.fmi << " for " << par.dm1_name << " repeated, ignoring it";
            return;
        }
    }
    if (par.has_flag('t')) {
        // костыль, особым образом обрабатываем параметры для шин, добавляем в конфиг 24 параметра вместо 1-го
        // так, чтобы не иметь в конфиге 10*24 одинаковых параметра
        // TODO придумать что-то покрасивее, может просто обобщить
        auto base_id = par.id;
        for(int i = 0; i < 24; ++i) {
            par.id = base_id + "_" + std::to_string(i);
            par.spn = par.base_spn + i;
            dm1_parameters[par.dm1_name].emplace(static_cast<uint>(par.spn), par);
        }
    }
    else {
        dm1_parameters[par.dm1_name].emplace(static_cast<uint>(par.spn), std::move(par));
    }
}

void ConfigDictionary::can_index_filter_init(const Json::Value& json_obj) {
    for (const auto& k : json_obj.getMemberNames()) {
        uint32_t index = static_cast<uint32_t>(std::stoul(k));
        for (const auto& a : json_obj[k]) {
            uint32_t address = static_cast<uint32_t>(std::stoul(a.asString(), nullptr, 16));
            auto it = can_index_filter.find(address);
            if (it == can_index_filter.end()) {
                it = can_index_filter.emplace(std::make_pair(address, decltype(can_index_filter)::mapped_type())).first;
            }
            it->second.insert(index);
        }
    }
    //for (const auto& e : can_index_filter) {auto log = logdebug; log << std::hex << e.first << ": " << std::dec; for (const auto& ee : e.second) log << ee << " ";}
}

void ConfigDictionary::params_init(const Json::Value& json_params) {
    int cnt = 0;
    for (const auto& json_par : json_params) {
        ConfigParameter par(json_par);

        if (par.id.empty()) {
            logerror << "empty parameter id, ignoring it";
            continue;
        }
        if (parameters.count(par.id)) {
            logerror << "parameter id \"" << par.id << "\" repeated, ignoring it";
            continue;
        }

        try_add_expression(par, json_par.get("expr", "").asString());

        if (par.is_dtc()) {
            add_dm1_parameter(std::move(par));
        }
        else {
            parameters.emplace(par.id, std::move(par));
        }

        ++cnt;
    }

    auto q = parameters.size();
    for (const auto& p : dm1_parameters) {
        q += p.second.size();
    }
    loginfo << "readed " << cnt << " parameters; total " << q;
}

void ConfigDictionary::try_add_expression(const ConfigParameter& par, const std::string& str) {
    if (str.empty()) {
        return;
    }
    auto [it, _] = expressions.emplace(par.id, str);

    auto res = it->second.calculate(1);       // тестируем, чтобы упасть тут а не во время работы
    logdebug << "added expression '" << it->second.source_string() << "' for " << par.id << " => " << res;
}


std::string ConfigDictionary::par_dict_path() {
    static std::string path;
    if (path.empty()) {
        auto& main_config = Mnt2App::instance().main_config();
        if (!main_config) {
            throw mnt2app_error("cannot get main configuration");
        }
        int matrix_id = main_config->get("EnableCanMatrixId", -999).asInt();
        if (matrix_id < 0) {
            throw mnt2app_error("cannot get EnableCanMatrixId from main configuration");
        }

        std::string name = Mnt2App::instance().config().get("dictName", "par_dict").asString();
        path = _filesystem::absolute(name + std::to_string(matrix_id) + ".json");
    }
    return path;
}

void ConfigDictionary::init() {
    auto dict_path = par_dict_path();
    mnt::log(loginfo, "ConfigDictionary: {}", dict_path);

    std::string err;
    auto dict_config = utils::read_json(dict_path, err);
    if (dict_config.get() == nullptr) {
        throw mnt2app_error(err);
    }
    if (!dict_config) {
        throw mnt2app_error(err);
    }

    params_init((*dict_config)["params"]);

    try {
        can_index_filter_init((*dict_config)["can_index_filter"]);
    }
    catch (std::exception& e) {
        throw mnt2app_error(std::string("wrong json can_index_filter: ") + e.what());
    }
}

const dm1_par_container* ConfigDictionary::dm1_parameters_ptr(const std::string& name) {
    const auto it = dm1_parameters.find(name);
    if (dm1_parameters.find(name) == dm1_parameters.end()) {
        return nullptr;
    }
    return &it->second;
}

const ConfigParameter* ConfigDictionary::dm1_parameter_ptr(const dm1_par_container& par_map, uint spn, uint fmi) {
    auto range = par_map.equal_range(spn);
    for (auto it = range.first; it != range.second; ++it) {
        if (it->second.fmi == -1 || it->second.fmi == static_cast<int>(fmi)) {
            return &it->second;
        }
    }
    return nullptr;
}

const ConfigParameter* ConfigDictionary::parameter_ptr(const std::string& name) {
    const auto it = parameters.find(name);
    if (parameters.find(name) == parameters.end()) {
        return nullptr;
    }
    return &it->second;
}

bool ConfigDictionary::is_needed_canindex(const pb::CanItem& item) const {
    const auto it = can_index_filter.find(item.address());
    if (it == can_index_filter.end() || it->second.find(item.canindex()) != it->second.end()) {
        return true;
    }
    return false;
}

const Expression* ConfigDictionary::get_expression(const ConfigParameter& par) {
    auto it = expressions.find(par.id);
    if (it == expressions.end()) {
        return nullptr;
    }
    return &(it->second);
}

