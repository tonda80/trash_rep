#ifndef TSDIAG_PROCESSER_H
#define TSDIAG_PROCESSER_H


#include <string>
#include <map>


class ConfigParameter;


// набор функций (и стат. данных для них) для проверки необходимости процессить item
class ProcFilter {
public:
    static ProcFilter& instance() {
        static ProcFilter instance;
        return instance;
    }
    void init();

    bool check_change_and_remember(const std::string& key, const std::string& value);
    bool check_pub_time(const std::string& key);
    void remember_pub_time(const std::string& key);

private:
    ProcFilter() = default;
    std::map<std::string, std::string> last_values;      // key: last_value
    std::map<std::string, long> last_pub_times;          // key: last_pub_time
    int pub_timeout = -1;
}; // class ProcFilter


using extra_data_container = std::map<std::string, std::string>;        // name : value. "value" field is necessary


void process_tsdiag_item(const ConfigParameter& par, const extra_data_container& extra_data,
                         bool begin_transaction = false, const std::string* filter_key = nullptr, uint64_t can_raw_value = 0);


#endif // TSDIAG_PROCESSER_H
