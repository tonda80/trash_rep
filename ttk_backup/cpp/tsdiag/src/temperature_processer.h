#ifndef TEMPERATURE_PROCESSER_H
#define TEMPERATURE_PROCESSER_H


#include "request_handler.h"
#include <pb/boarddevice.pb.h>


class BoardTemperatureHandler : public RequestHandler<pb::BoardTemperature, BoardTemperatureHandler>
{
public:
    static void init();
    static bool use_can_temperature();
    static bool use_mnt_temperature();

private:
    void process();
};

#endif // TEMPERATURE_PROCESSER_H
