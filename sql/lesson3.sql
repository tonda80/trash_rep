USE testdb_0;

-- 1. Пусть в таблице users поля created_at и updated_at оказались незаполненными. Заполните их текущими датой и временем.
DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id SERIAL,
  name VARCHAR(255),
  created_at VARCHAR(255),
  updated_at VARCHAR(255)
);
INSERT INTO users VALUES
  (NULL, 'Vasia', NULL, NULL),
  (NULL, 'Petia', NULL, NULL),
  (NULL, 'Masha', NULL, NULL)
;

SELECT * FROM users;

UPDATE users SET created_at = NOW() WHERE created_at is NULL;
UPDATE users SET updated_at = NOW() WHERE updated_at is NULL;

SELECT * FROM users;

-- 2.Таблица users была неудачно спроектирована. Записи created_at и updated_at были заданы типом VARCHAR и в них долгое время помещались значения в формате "20.10.2017 8:10". Необходимо преобразовать поля к типу DATETIME, сохранив введеные ранее значения.

-- задали нужный по условию формат
UPDATE users SET created_at = '20.10.2017 8:10', updated_at = '2.12.2018 9:10' where id = 1;
UPDATE users SET created_at = '13.10.2017 22:10', updated_at = '2.4.2018 19:10' where id = 2;
UPDATE users SET created_at = '1.1.2017 8:55', updated_at = '2.2.2018 0:10' where id = 3;

SELECT * FROM users;
SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'users' AND (COLUMN_NAME = 'created_at' OR COLUMN_NAME = 'updated_at');

-- приводим к datetime формату https://www.w3schools.com/sql/func_mysql_str_to_date.asp
UPDATE users SET created_at = STR_TO_DATE(created_at, '%d.%m.%Y %H:%i'), updated_at = STR_TO_DATE(updated_at, '%d.%m.%Y %H:%i');
-- и меняем тип столбца
ALTER TABLE users MODIFY updated_at datetime;
ALTER TABLE users MODIFY created_at datetime;

SELECT * FROM users;
SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'users' AND (COLUMN_NAME = 'created_at' OR COLUMN_NAME = 'updated_at');

-- 3. В таблице складских запасов storehouses_products в поле value могут встречаться самые разные цифры: 0, если товар закончился и выше нуля, если на складе имеются запасы. Необходимо отсортировать записи таким образом, чтобы они выводились в порядке увеличения значения value. Нулевые запасы должны выводиться в конце, после всех записей.
DROP TABLE IF EXISTS storehouses_products;
CREATE TABLE storehouses_products (
  id SERIAL,
  name VARCHAR(255),
  value INT
);
INSERT INTO storehouses_products VALUES
  (NULL, 'bread', 220),
  (NULL, 'milk', 15),
  (NULL, 'sugar', 0),
  (NULL, 'bubble water', 12),
  (NULL, 'ice cream', 0),
  (NULL, 'vine', 7),
  (NULL, 'vodka', 100)
;
SELECT * FROM storehouses_products;
-- http://www.sql-tutorial.ru/ru/book_case_order_by_clause.html
SELECT * FROM storehouses_products ORDER BY CASE WHEN value > 0 THEN value ELSE ~0 END;


-- 4. (по желанию) Из таблицы users необходимо извлечь пользователей, родившихся в августе и мае. Месяцы заданы в виде списка английских названий ('may', 'august')
-- переделаем задание, чтобы интересней решалось (все равно не сдавать), др в date

ALTER TABLE users ADD birthday date;
UPDATE users SET birthday = '1977-01-10' where id = 1;
UPDATE users SET birthday = '1980-05-14' where id = 2;
UPDATE users SET birthday = '2010-12-1' where id = 3;
INSERT INTO users VALUES (NULL, 'Gosha', NULL, NULL, '2000-08-1');

SELECT * FROM users;

SELECT * FROM users WHERE DATE_FORMAT(birthday, '%M') IN ('May', 'August');

-- 5. (по желанию) Из таблицы catalogs извлекаются записи при помощи запроса. SELECT * FROM catalogs WHERE id IN (5, 1, 2); Отсортируйте записи в порядке, заданном в списке IN.

DROP TABLE IF EXISTS catalogs;
CREATE TABLE catalogs (
  id SERIAL,
  code int
);
INSERT INTO catalogs VALUES
  (NULL, rand()*100), (NULL, rand()*100), (NULL, rand()*100), (NULL, rand()*100), (NULL, rand()*100), (NULL, rand()*100), (NULL, rand()*100), (NULL, rand()*100), (NULL, rand()*100)
;

SELECT * FROM catalogs;
SELECT * FROM catalogs WHERE id IN (5, 1, 2) ORDER BY CASE WHEN id = 5 THEN 0 WHEN id = 1 THEN 1 ELSE 2 END;