-- замена полей на empty
UPDATE catalogs SET name = 'empty' WHERE name is NULL; UPDATE catalogs SET name = 'empty' WHERE name = '';
-- поле name не может быть уникальным, потому что этот запрос пытатся сделать несколько полей с одинаковым значением

-- таблица с файлами
DROP TABLE IF EXISTS media_files;
CREATE TABLE media_files (
  id SERIAL PRIMARY KEY,
  path_ VARCHAR(4096),
  name VARCHAR(256),
  desription VARCHAR(512),
  keywords JSON,
  user_id int
) COMMENT = 'Медиафайлы';
