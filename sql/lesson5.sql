USE testdb_0;

DROP TABLE IF EXISTS catalogs;
CREATE TABLE catalogs (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255)
);
INSERT INTO catalogs VALUES
	(NULL, 'beer'),
	(NULL, 'vodka'),
	(NULL, 'Cards')
;

DROP TABLE IF EXISTS rubrics;
CREATE TABLE rubrics (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255)
);

INSERT INTO rubrics VALUES
  (NULL, 'Cards'),
  (NULL, 'Memory');

SELECT * from catalogs;
SELECT * from rubrics;

SELECT name FROM catalogs
UNION
SELECT name from rubrics;

-- JOIN

DROP TABLE IF EXISTS tbl1;
CREATE TABLE tbl1 (value VARCHAR(255));
INSERT INTO tbl1 VALUES ('fst1'), ('fst2'), ('fst3');

DROP TABLE IF EXISTS tbl2;
CREATE TABLE tbl2 (value VARCHAR(255));
INSERT INTO tbl2 VALUES ('snd1'), ('snd2'), ('snd3');

SELECT * FROM tbl1, tbl2;
SELECT * FROM tbl1 JOIN tbl2;
