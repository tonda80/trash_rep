-- тестовые таблицы
USE testdb_0;

DROP TABLE IF EXISTS users;
CREATE TABLE users (id SERIAL, name VARCHAR(255));
INSERT INTO users (name) VALUES
  ('Vasia'),  ('Petia'), ('Masha'), ('Gosha'), ('Kolia'), ('Gena'), ('Zina'), ('Goga')
;

DROP TABLE IF EXISTS orders;
CREATE TABLE orders (id SERIAL, user_id INT UNSIGNED);
INSERT INTO orders (user_id) VALUES
  (1), (3), (2), (3), (1), (2), (7)
;

DROP TABLE IF EXISTS catalogs;
CREATE TABLE catalogs (id INT UNSIGNED, name VARCHAR(255));
INSERT INTO catalogs VALUES
  (1, 'CPU'), (2, 'Video cards'), (3, 'HDD')
;

DROP TABLE IF EXISTS products;
CREATE TABLE products (id SERIAL, name VARCHAR(255), description TEXT, price DECIMAL (11,2), catalog_id INT UNSIGNED);
INSERT INTO products (name, description, price, catalog_id) VALUES
  ('Intel Core i3-8100', 'Intel CPU', 7890.00, 1),
  ('Intel Core i5-7400', 'Intel CPU', 12700.00, 1),
  ('AMD FX-8320E', 'AMD CPU', 4780.00, 1),
  ('AMD FX-8320', 'AMD CPU', 7120.00, 1),
  ('MSI GeForce GTX 1650', 'Nvidia card', 1234.56, 2),
  ('GIGABYTE GeForce RTX 2070', 'Nvidia card', 1034.56, 2),
  ('Seagate BarraCuda 4Tb', 'HDD', 2000.56, 3)
;

DROP TABLE IF EXISTS flights;
CREATE TABLE flights (id SERIAL, from_ VARCHAR(255), to_ VARCHAR(255));
INSERT INTO flights (from_, to_) VALUES
  ('Rome', 'Paris'),
  ('London', 'Dublin'),
  ('Samara', 'Oslo'),
  ('Berlin', 'Warsaw')
;

-- chcp 65001 для винды
DROP TABLE IF EXISTS cities;
CREATE TABLE cities (label VARCHAR(255), name VARCHAR(255));
INSERT INTO cities VALUES
  ('Rome', 'Рим'),
  ('London', 'Лондон'),
  ('Samara', 'Самара'),
  ('Berlin', 'Берлин'),
  ('Paris', 'Париж'),
  ('Dublin', 'Дублин'),
  ('Oslo', 'Осло'),
  ('Warsaw', 'Варшава')
;


-- 1. Составьте список пользователей users, которые осуществили хотя бы один заказ (orders) в интернет-магазине.
SELECT name FROM users WHERE id IN (SELECT DISTINCT user_id FROM orders);

-- 2. Выведите список товаров products и разделов catalogs, который соответствует товару.
SELECT p.*, c.* FROM products AS p JOIN catalogs AS c ON p.catalog_id = c.id;

-- 3. (по желанию) Есть таблица рейсов flights (id, from, to) и таблица городов cities (label, name). Поля from, to и label содержат английские названия городов, поле name — русское. Выведите список рейсов (flights) с русскими названиями городов.
SELECT c1.name AS from_, c2.name AS to_ FROM flights JOIN cities as c1 JOIN cities as c2 ON c1.label = flights.from_ AND c2.label = flights.to_;
