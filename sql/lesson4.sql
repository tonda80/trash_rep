USE testdb_0;

DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id SERIAL,
  name VARCHAR(255),
  birthday_at DATE
);
INSERT INTO users VALUES
	(NULL, 'Vasia', '1980-05-14'),
	(NULL, 'Petia', '1997-12-1'),
	(NULL, 'Masha', '1974-11-30'),
	(NULL, 'Gosha', '1995-1-12'),
	(NULL, 'Kolia', '1984-10-1'),
	(NULL, 'Gena', '1999-9-14'),
	(NULL, 'Zina', '1990-3-21'),
	(NULL, 'Goga', '2002-1-12')
;

SELECT * from users;


-- группируем по декадам
SELECT SUBSTRING(birthday_at, 1, 3) AS decade FROM users GROUP BY decade;
-- подсчет элементов группы
SELECT SUBSTRING(birthday_at, 1, 3) AS decade FROM users GROUP BY decade;
-- список пользователей в каждой из групп
SELECT COUNT(*), GROUP_CONCAT(name), SUBSTRING(birthday_at, 1, 3) AS decade FROM users GROUP BY decade;
-- можно так GROUP_CONCAT(name SEPARATOR '|')

-- подсчет неNULLевых
SELECT COUNT(id) FROM users;
-- или всех строк
SELECT COUNT(*) FROM users;

-- сортировка по количеству элементов в группе
SELECT COUNT(*) as cnt, GROUP_CONCAT(name), SUBSTRING(birthday_at, 1, 3) AS decade FROM users GROUP BY decade ORDER BY cnt;

-- есть возможность посчитать мин\макс значения столбцв
SELECT MIN(birthday_at), MAX(birthday_at) FROM users;
