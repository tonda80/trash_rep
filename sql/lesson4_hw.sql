-- тестовая таблица
USE testdb_0;
DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id SERIAL,
  name VARCHAR(255),
  birthday_at DATE
);
INSERT INTO users VALUES
	(NULL, 'Vasia', '1980-05-14'),
	(NULL, 'Petia', '1997-12-1'),
	(NULL, 'Masha', '1974-11-30'),
	(NULL, 'Gosha', '1995-1-12'),
	(NULL, 'Kolia', '1984-10-1'),
	(NULL, 'Gena', '1999-9-14'),
	(NULL, 'Zina', '1990-3-21'),
	(NULL, 'Goga', '2002-1-12')
;

SELECT * from users;

-- 1. Подсчитайте средний возраст пользователей в таблице users.
SELECT AVG(DATEDIFF(NOW(), birthday_at)/365) from users;

-- 2. Подсчитайте количество дней рождения, которые приходятся на каждый из дней недели. Следует учесть, что необходимы дни недели текущего года, а не года рождения.
SELECT DAYOFWEEK(CONCAT('2019', SUBSTRING(birthday_at, 5))) AS day_of_week, COUNT(*) FROM users GROUP BY day_of_week;

-- 3. (по желанию) Подсчитайте произведение чисел в столбце таблицы.
-- пусть будет произведение столбца id
SELECT EXP(SUM(LOG(id))) FROM users;
