#!/bin/sh

cat /etc/resolv.conf | grep -q 8.8.8.8 || echo -e 'nameserver 77.88.8.8\nnameserver 77.88.8.1\nnameserver 8.8.8.8' >> /etc/resolv.conf

if [ -n "$ROOT_PASSWD" ]; then
        echo "root:${ROOT_PASSWD}" | chpasswd
fi

exec "$@"
