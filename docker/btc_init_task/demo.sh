#/bin/bash
set -e

DP0='docker run -it --network=btc_init_task_default --rm ruimarinho/bitcoin-core:0.21 bitcoin-cli -rpcconnect=btc_node0 -regtest -rpcuser=abtc -rpcpassword=aaaa'
DP1='docker run -it --network=btc_init_task_default --rm ruimarinho/bitcoin-core:0.21 bitcoin-cli -rpcconnect=btc_node1 -regtest -rpcuser=abtc -rpcpassword=aaaa'

#docker-compose down ; docker-compose up -d

# создаем кошельки
$DP0 createwallet w0
$DP1 createwallet w1

# шифруем кошелек
PP="big secret"
$DP0 encryptwallet "$PP"

# назначаем адреса
$DP0 getnewaddress a00 legacy
$DP0 getnewaddress a01 p2sh-segwit
ADDR0=`$DP0 getnewaddress a02 bech32 | tr -d '\n\r'`
ADDR1=`$DP1 getnewaddress a10 p2sh-segwit | tr -d '\n\r'`

# коннектим ноду
$DP0 addnode "btc_node1:18444" "onetry"
$DP0 getconnectioncount

# майним на 0
$DP0 generatetoaddress 1000 $ADDR0		1> /dev/null
$DP0 getblockcount
$DP1 getblockcount
$DP0 getbalance


# переводим
$DP0 walletpassphrase "$PP" 60		# unlock
$DP0 send "{\"$ADDR1\": 1}" 5 economical
$DP0 walletlock					# и снова lock
#
$DP1 getbalance
$DP0 generatetoaddress 10 $ADDR0
$DP1 getbalance

# переводим raw
TXID=`$DP0 listunspent 1 10000000 "[\"$ADDR0\"]" false "{\"minimumAmount\": 11, \"maximumCount\": 1}" | python3 get_value.py txid`
TH1=`$DP0 createrawtransaction "[{\"txid\":\"$TXID\",\"vout\":0}]" "[{\"$ADDR1\":10}]" | tr -d '\n\r'`
$DP0 walletpassphrase "$PP" 60		# unlock
TH2=`$DP0 signrawtransactionwithwallet $TH1 | python3 get_value.py hex`
$DP0 walletlock					# и снова lock
#
$DP0 sendrawtransaction $TH2 0
$DP1 getbalance
$DP0 generatetoaddress 10 $ADDR0
$DP1 getbalance


echo "ADDR0=$ADDR0"
echo "ADDR1=$ADDR1"
