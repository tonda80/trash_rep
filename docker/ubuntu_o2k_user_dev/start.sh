#!/bin/bash

source ./env-var.decl

XSOCK=/tmp/.X11-unix
XAUTH=/tmp/.docker.xauth
touch $XAUTH
xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -

echo "IMAGE_NAME:               ${IMAGE_NAME}"
echo "CONTAITER NAME:           ${CONTAINER_NAME}"
echo "HOST SHARED WORKDIR:      ${HOST_SHARED_WORKDIR}"
echo "CONTAINER SHARED WORKDIR: ${CONTAINER_SHARED_WORKDIR}"
echo "USER_NAME:                ${USER_NAME}"
echo "USER_PASSWORD:            ${USER_PASSWORD}"
echo "ROOT_PASSWORD:            ${ROOT_PASSWORD}"
echo "USER_ID:                  ${USER_ID}"
echo "GROUP_ID:                 ${GROUP_ID}"


docker start -ai ${CONTAINER_NAME} 
