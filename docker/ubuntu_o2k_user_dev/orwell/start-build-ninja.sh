#!/bin/bash

BUILD_DIR='build-ninja'

if [ -d $BUILD_DIR ]
then
	rm -rf $BUILD_DIR
fi

mkdir $BUILD_DIR
cd $BUILD_DIR

cmake .. -DO2K_THIRD_PARTY_DIR=/o2k/zork/3rd_party -DO2K_BUILD_WITH_HOST_LIBRARIES=True -G Ninja

