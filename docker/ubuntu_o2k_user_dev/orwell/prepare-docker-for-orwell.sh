#!/bin/bash

# Must be run in o2k directory in docker-volume. Start docker, go to /o2k and run this.
# Script will build '3rd_party_bin_all' branches.
#
#  /
#  |
#  `-- o2k
#      |-- 3rd_party_bin_all
#      |   |
#      |   `-- x86_64-linux-gnu_9
#      |       |-- gstreamer
#      |           `-- 1.11.y
#      |               `-- Release
#      |                   |-- include
#      |                   |-- lib
#      |                   `-- bin
#      |
#      `-- zork
#          |-- 3rd_party
#          |
#          |-- 3rd_party_bin_all        -
#          |   `-- x86_64-linux-gnu_9    | 
#          |       `-- gstreamer         |- TODO: Needs to build without this copy
#          |           `-- 1.11.y        |
#          |               `-- include  -
#          |-- SRS
#          |   `-- Mainline
#          |       `-- SRSInterface
#          |           |-- Common
#          |           |   `-- internal
#          |           `-- LPDraw
#          `-- orwell
#              `-- Mainline

cd /o2k

echo '#!/bin/bash' > time-docker.sh
echo "T1=$(date +%s) # starting ... " >> time-docker.sh

# Building GStreamer takes about 7 munutes.
cd zork/3rd_party/gstreamer/1.11.y
GST_BUILD_PREFIX='/o2k/3rd_party_bin_all/x86_64-linux-gnu_9/gstreamer/1.11.y/Release'
GST_COPY_PREFIX='/o2k/zork/3rd_party_bin_all/x86_64-linux-gnu_9/gstreamer/1.11.y/'
./build.sh $GST_BUILD_PREFIX

cd /o2k
echo "T2=$(date +%s) # building of GStreamer - done " >> time-docker.sh

cd $GST_BUILD_PREFIX
mkdir -p $GST_COPY_PREFIX
cp -r include/ $GST_COPY_PREFIX

cd /o2k
echo "T3=$(date +%s) # copying of GStreamer includes - done " >> time-docker.sh

# Copying SRS building results downloaded as archive.zip from Jenkins.
unzip archive.zip
sudo mkdir -p /opt/elveesneotek.com
sudo cp -r archive/* /opt/elveesneotek.com/
rm -rf archive

cd /o2k
echo "T4=$(date +%s) # extracting and moving the SRS building results - done " >> time-docker.sh

if [ -e 'start-build-make.sh' ]
then
cp start-build-make.sh  zork/orwell/Mainline
fi
if [ -e 'start-build-ninja.sh' ]
then
cp start-build-ninja.sh zork/orwell/Mainline
fi


echo 'min=$(( ($T2 - $T1)/60 ))' >> time-docker.sh
echo 'echo "$min min - building of GStreamer"' >> time-docker.sh

echo 'sec=$(( ($T3 - $T2) ))' >> time-docker.sh
echo 'echo "$sec sec - copying of GStreamer includes"' >> time-docker.sh

echo 'sec=$(( ($T4 - $T3) ))' >> time-docker.sh
echo 'echo "$sec sec - extracting and moving SRS building results"' >> time-docker.sh

chmod u+x time-docker.sh

./time-docker.sh

