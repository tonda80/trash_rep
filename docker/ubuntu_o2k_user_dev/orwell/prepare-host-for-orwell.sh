#!/bin/bash

# Must be run in o2k directory. Go to <host-prefix>/o2k and run this.
# Script will build 'zork' branch except for 3rd_party_bin_all.
#
# <host-prefix>
#  |
#  `-- o2k
#      |-- 3rd_party_bin_all
#      |   `-- x86_64-linux-gnu_9
#      |       |-- gstreamer
#      |           `-- 1.11.y
#      |               `-- Release
#      |
#      `-- zork
#          |-- 3rd_party
#          |
#          |-- 3rd_party_bin_all
#          |   `-- x86_64-linux-gnu_9
#          |       `-- gstreamer
#          |           `-- 1.11.y
#          |               `-- include
#          |-- SRS                      -
#          |   `-- Mainline              |
#          |       `-- SRSInterface      |- TODO: This matches to /opt/elveesneotek.com/include/SRSInterface
#          |           |-- Common        |
#          |           |   `-- internal  |
#          |           `-- LPDraw       -
#          `-- orwell
#              `-- Mainline

function usage
{
        echo "Usage:"
        echo "    $0 <svn-username> <svn-password>"
        echo "  or"
        echo "    $0 < credentials.txt"
        echo "  or"
        echo "    cat creedentials.txt | $0"
        echo ""
        echo "      - where 'credentials.txt' is a text file which contains two lines:"
        echo "          <svn-username>"
        echo "          <svn-password>"
        echo "        Leading and trailing spaces is no matter."
}

function trimstr
{
        local str=$1
        str=`echo $str | sed 's/ *$//'`
        str=`echo $str | sed 's/^ *//'`
        echo "$str"
}

ROOT=`pwd`

# Check pipe
if [ -t 0 ] 
then
        # Reading a command line arguments
        if [ $# -eq 3 ] 
        then
                USERNAME=$1
                PASSWORD=$2
        fi
else
        # Reading a pipe
        index=0
        while read -r line ; do
                MYARRAY[$index]="$line"
                index=$(($index+1))
        done
        if [ $index -eq 2 ] 
        then
                USERNAME=`trimstr "${MYARRAY[0]}"`
                PASSWORD=`trimstr "${MYARRAY[1]}"`
        fi
fi

if [  "x${USERNAME}" = "x" -o "x${PASSWORD}" = "x" ]
then
    usage
    exit 1
fi


echo '#!/bin/bash' > time-host.sh
echo "T1=$(date +%s) # starting ... " >> time-host.sh

mkdir zork
cd zork
# Getting 3rd_party takes about 35 minutes
svn --username=${USERNAME} --password=${PASSWORD} checkout https://zork.elvees.com/svn/3rd_party

cd $ROOT
echo "T2=$(date +%s) # 3rd_party - done " >> time-host.sh

mkdir zork/orwell
cd zork/orwell
svn --username=${USERNAME} --password=${PASSWORD} checkout https://zork.elvees.com/svn/Orwell/Mainline

cd $ROOT
echo "T3=$(date +%s) # orwell - done " >> time-host.sh

cp -r zork/3rd_party/build_helpers/cmake_common_v001/* zork/orwell/Mainline/cmake/

cd $ROOT
echo "T4=$(date +%s) # cmake helpers copying - done " >> time-host.sh

mkdir -p zork/SRS/Mainline
cd zork/SRS/Mainline
svn --username=${USERNAME} --password=${PASSWORD} checkout https://zork.elvees.com/svn/SRS/Mainline/SRSInterface

cd $ROOT
echo "T5=$(date +%s) # SRS Interface - done " >> time-host.sh

wget --no-check-certificate https://jenkins.elvees.com/job/Mainline%20-%20SRS%20-%20docker%20-%20Rebuild%20-%20amd64/lastSuccessfulBuild/artifact/*zip*/archive.zip

cd $ROOT
echo "T6=$(date +%s) # archive.zip copying - done " >> time-host.sh


echo 'min=$(( ($T2 - $T1)/60 ))' >> time-host.sh
echo 'echo "$min min - svn checkout 3rd_party"' >> time-host.sh

echo 'sec=$(( ($T3 - $T2) ))' >> time-host.sh
echo 'echo "$sec sec - svn checkout orwell"' >> time-host.sh

echo 'sec=$(( ($T4 - $T3) ))' >> time-host.sh
echo 'echo "$sec sec - cmake helpers copying"' >> time-host.sh

echo 'sec=$(( ($T5 - $T4) ))' >> time-host.sh
echo 'echo "$sec sec - RS Interface"' >> time-host.sh

echo 'sec=$(( ($T6 - $T5) ))' >> time-host.sh
echo 'echo "$sec sec - wget archive.zip"' >> time-host.sh

chmod u+x time-host.sh

./time-host.sh

