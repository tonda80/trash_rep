#!/bin/bash

source ./env-var.decl

XSOCK=/tmp/.X11-unix
XAUTH=/tmp/.docker.xauth
touch $XAUTH
xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -

# тут вот что то на тему
# https://stackoverflow.com/questions/48235040/run-x-application-in-a-docker-container-reliably-on-a-server-connected-via-ssh-w

echo "IMAGE_NAME:               ${IMAGE_NAME}"
echo "CONTAITER NAME:           ${CONTAINER_NAME}"
echo "HOST SHARED WORKDIR:      ${HOST_SHARED_WORKDIR}"
echo "CONTAINER SHARED WORKDIR: ${CONTAINER_SHARED_WORKDIR}"
echo "USER_NAME:                ${USER_NAME}"
echo "USER_PASSWORD:            ${USER_PASSWORD}"
echo "ROOT_PASSWORD:            ${ROOT_PASSWORD}"
echo "USER_ID:                  ${USER_ID}"
echo "GROUP_ID:                 ${GROUP_ID}"

# removed:
#       --ulimit=99999999999999 \

# check using for --user
# -v /etc/passwd:/etc/passwd


docker run  $* -it --name=${CONTAINER_NAME} \
        --privileged=true \
        --user=${USER_NAME} \
        --volume=$XSOCK:$XSOCK:rw \
        --volume=${HOST_SHARED_WORKDIR}:${CONTAINER_SHARED_WORKDIR} \
        --volume=$XAUTH:$XAUTH:rw \
        --device /dev/dri \
        --env="XAUTHORITY=${XAUTH}" \
        --env="DISPLAY" \
        ${IMAGE_NAME} \
        bash
