#!/bin/bash

source ./env-var.decl

echo "IMAGE NAME:               ${IMAGE_NAME}"
echo "CONTAITER NAME:           ${CONTAINER_NAME}"
echo "HOST SHARED WORKDIR:      ${HOST_SHARED_WORKDIR}"
echo "CONTAINER SHARED WORKDIR: ${CONTAINER_SHARED_WORKDIR}"
echo "USER NAME:                ${USER_NAME}"
echo "USER_PASSWORD:            ${PASSWORD}"
echo "ROOT PASSWORD:            ${ROOT_PASSWORD}"
echo "USER ID:                  ${USER_ID}"
echo "GROUP ID:                 ${GROUP_ID}"

USERADD="RUN groupadd -g ${GROUP_ID} ${USER_NAME} \&\& \\\\\\
    useradd -m -u ${USER_ID} -g ${GROUP_ID} ${USER_NAME} \&\& \\\\\\
    chpasswd << ${USER_NAME}:${USER_PASSWORD} \&\& \\\\\\
    chpasswd << root:${ROOT_PASSWORD} \&\& \\\\\\
    echo '${USER_NAME} ALL=(ALL:ALL) NOPASSWD: ALL' >> \/etc\/sudoers "

sed "s/@@@useradd/${USERADD}/" Dockerfile.tmpl > Dockerfile

docker buildx build -t ${IMAGE_NAME} .
