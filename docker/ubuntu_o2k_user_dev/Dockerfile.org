﻿FROM ubuntu:20.04

LABEL description="Ubuntu container with development environment for O2K"

# Silence nonsensical warnings
ARG DEBIAN_FRONTEND=noninteractive
ARG APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1
ARG APT_INSTALL="apt-get install -y"
ARG APT_REMOVE="apt-get remove -y"

RUN apt-get update && ${APT_INSTALL} apt-utils

# Build tools
RUN ${APT_INSTALL} build-essential cmake ninja-build gettext

# Tools
RUN ${APT_INSTALL} subversion git rsync zip nano htop aptitude aria2 gdb
ARG DOWNLOAD="aria2c --max-connection-per-server=5 --summary-interval=9 --show-console-readout=false \
	--retry-wait=10"

# SSH
RUN ${APT_INSTALL} dumb-init openssh-server && \
	mkdir -p /var/run/sshd && \
	echo 'PasswordAuthentication yes' >> /etc/ssh/sshd_config && \
	echo 'PermitRootLogin yes' >> /etc/ssh/sshd_config && \
	echo 'root:8jKXzYv8FKKg' | chpasswd

EXPOSE 22

# Libraries
RUN cd ~ && \
	${DOWNLOAD} https://github.com/google/googletest/archive/release-1.8.1.tar.gz 2>/dev/null && \
	tar xf googletest-release-1.8.1.tar.gz && \
	cd googletest-release-1.8.1 && \
	cmake -DBUILD_SHARED_LIBS=ON . && \
	make && make install && \
	cd ~ && \
	rm -rf googletest-release-1.8.1.tar.gz googletest-release-1.8.1

RUN cd ~ && \
	${DOWNLOAD} https://github.com/google/benchmark/archive/v1.5.0.tar.gz 2>/dev/null && \
	tar xf benchmark-1.5.0.tar.gz && \
	cd benchmark-1.5.0 && \
	cmake -DBUILD_SHARED_LIBS=ON -DBENCHMARK_ENABLE_GTEST_TESTS=OFF . && \
	make && make install && \
	cd ~ && \
	rm -rf benchmark-1.5.0.tar.gz benchmark-1.5.0

RUN ${APT_INSTALL} libboost-all-dev
RUN ${APT_INSTALL} libavcodec-dev libavformat-dev libswscale-dev libavutil-dev
RUN ${APT_INSTALL} libopencv-dev

#support both amd64 and arm64 images by DPKG_ARCH && REPO_ROOT_URL
RUN DPKG_ARCH=$(dpkg --print-architecture) && REPO_ROOT_URL=$(grep --max-count=1 --only-matching --perl-regexp '(?<=deb )[^ ]+' /etc/apt/sources.list) && cd ~ && \
	echo DPKG_ARCH=${DPKG_ARCH} REPO_ROOT_URL=${REPO_ROOT_URL} && \
	${DOWNLOAD} ${REPO_ROOT_URL}/pool/universe/v/vlfeat/libvlfeat-dev_0.9.20+dfsg0-2_${DPKG_ARCH}.deb && \
	${DOWNLOAD} ${REPO_ROOT_URL}/pool/universe/v/vlfeat/libvlfeat1_0.9.20+dfsg0-2_${DPKG_ARCH}.deb && \
	dpkg -i libvlfeat1_0.9.20+dfsg0-2_${DPKG_ARCH}.deb && \
	dpkg -i libvlfeat-dev_0.9.20+dfsg0-2_${DPKG_ARCH}.deb && \
	rm libvlfeat*.deb

RUN ${APT_INSTALL} libcaffe-cpu-dev liblapack-dev \
	libgoogle-glog-dev libprotobuf-dev libhdf5-dev liblmdb-dev libleveldb-dev
RUN ${APT_INSTALL} libgtest-dev libgmock-dev \
	libsqlite3-dev \
	libxerces-c-dev \
	libssl-dev \
	libminizip-dev \
	libaio-dev libblkid-dev

# for onvifserver

# build unixODBC and libtool with -fPIC option
# https://habr.com/ru/company/badoo/blog/324616/
RUN cd ~ && \
	${DOWNLOAD} ftp://ftp.unixodbc.org/pub/unixODBC/unixODBC-2.3.7.tar.gz && \
	tar xf unixODBC-2.3.7.tar.gz && \
	cd unixODBC-2.3.7 && \
	./configure --prefix=/usr/local/unixODBC --enable-static CFLAGS=-fPIC && \
	make && \
	make install && \
	cd ~ && \
	rm unixODBC-2.3.7.tar.gz && \
	rm -rf unixODBC-2.3.7

# rebuild libtool

RUN ${APT_REMOVE} libtool

RUN cd ~ && \
	${DOWNLOAD} http://ftpmirror.gnu.org/libtool/libtool-2.4.6.tar.gz && \
	tar xf libtool-2.4.6.tar.gz && \
	cd libtool-2.4.6 && \
	./configure --prefix=/usr/local/libtool CFLAGS=-fPIC && \
	make && \
	make install && \
	cd ~ && \
	rm libtool-2.4.6.tar.gz && \
	rm -rf libtool-2.4.6

ENV PATH "$PATH:/usr/local/libtool/bin"
RUN echo 'export PATH="$PATH"' >> /root/.bashrc

RUN ${APT_INSTALL} autoconf autopoint bison flex gtk-doc-tools
RUN ${APT_INSTALL} libglib2.0-dev freeglut3 freeglut3-dev yasm libreadline-dev

RUN ${APT_INSTALL} nasm
RUN cd ~ && \
	git clone https://github.com/cisco/openh264 && \
	cd openh264 && \
	git checkout v1.5.0 -b v1.5.0 && \
	make && make install && \
	cd ~ && \
	rm -rf openh264
RUN ${APT_INSTALL} libxvidcore-dev libcpprest-dev
RUN ${APT_INSTALL} psmisc

RUN cd ~ && \
	${DOWNLOAD} ftp://ftp.freetds.org/pub/freetds/stable/freetds-1.2.5.tar.gz && \
	tar -xvzf freetds-1.2.5.tar.gz && \
	cd freetds-1.2.5 && \
	./configure --prefix=/usr/local/freetds --with-tdsver=7.4 --with-unixodbc=/usr/local/unixODBC && \
	make && \
	make install && \
	cd ~ && \
	rm freetds-1.2.5.tar.gz && \
	rm -rf freetds-1.2.5
RUN echo "[FreeTDS]">> /usr/local/unixODBC/etc/odbcinst.ini && \
	echo "Description=FreeTDS">> /usr/local/unixODBC/etc/odbcinst.ini && \
	echo "Driver=/usr/local/freetds/lib/libtdsodbc.so">> /usr/local/unixODBC/etc/odbcinst.ini

RUN ${APT_INSTALL} iputils-ping traceroute iproute2
