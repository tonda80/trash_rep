import logging
from systemd import journal

SYS_ID = 'journal_log'

log = logging.getLogger('journal_log.py')
log.setLevel(logging.DEBUG)

log.addHandler(journal.JournalHandler(SYSLOG_IDENTIFIER=SYS_ID, KEY0=100))	# KEY0 будет во всех сообщениях. _SYSTEMD_UNIT='tryrewrite' - не переписывается

def some_func():
    log.warning('Call from function', extra={'MESSAGE_ID': 'mid4', 'KEY1': 123})

log.warning('Hello journald! I am %s', 'journal_log.py', extra={'MESSAGE_ID': 'mid', 'KEY1': 123})

some_func()

log.debug('Bye journald!', extra={'MESSAGE_ID': 'mid1', 'KEY1': 456})
