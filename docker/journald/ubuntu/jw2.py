import logging
from logging_journald import JournaldLogHandler, check_journal_stream

# journalctl -f -overbose -tjournal_log
# journalctl -S -1h  -tjournal_log
SYS_ID = 'journal_log'

log = logging.getLogger('journal_log.py')
#log.propagate = False
log.setLevel(logging.DEBUG)

class InjectingFilter(logging.Filter):
    def filter(self, record):
        record.test_filter_field1 = 1
        record.test_filter_field2 = "s2"
        return True

handler = JournaldLogHandler(SYS_ID)
handler.addFilter(InjectingFilter())
log.addHandler(handler)

def some_func():
    log.warning('Call from function', extra={'MESSAGE_ID': 'mid4', 'KEY1': 123})


log.warning('Hello sweet journald! I am %s', 'journal_log.py', extra={'MESSAGE_ID': 'mid', 'KEY2': 456})

some_func()

log.debug('Bye dear journald (run in code + real run in docker volume)!', extra={'MESSAGE_ID': 'mid1', 'KEY3': 789})

log.info("Processing function '%s' for instance %d...", 'func_name', 1)
