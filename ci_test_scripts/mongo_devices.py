# coding=utf8

# mongo devices
# https://app.asana.com/0/354325212918017/928357564208907/f

import os
import sys

sys.path.append('/home/ant/job/rep-s/ci_utils')
from ci.deployment.device import Device
from ci.deployment.device_manager import DeviceManager, XMLDeviceManager

if __name__ == '__main__':
	dev_mgr_xml = XMLDeviceManager('/home/ant/job/rep-s/ci_utils/devices.xml')

	if 0:
		os.environ['MONGO_HOST'] = r'mongodb://cluster0-shard-00-00-adg3l.mongodb.net:27017,cluster0-shard-00-01-adg3l.mongodb.net:27017,cluster0-shard-00-02-adg3l.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true'
		os.environ['MONGO_USER'] = 'mongo_guru'
		os.environ['MONGO_PASSWORD'] = 'uehfghk'
	else:
		os.environ['MONGO_HOST'] = r'mongo.service.atf01.consul'
		os.environ['MONGO_USER'] = 'berezin-a'
		os.environ['MONGO_PASSWORD'] = 'dQCx3azEv2BKszSq8K'
		# export MONGO_HOST='mongo.service.atf01.consul'; export MONGO_USER='berezin-a';export MONGO_PASSWORD='dQCx3azEv2BKszSq8K'
	dev_mgr_mongo = DeviceManager()
	
	actions = int(sys.argv[1] if len(sys.argv)>1 else 0)
	
	if actions & 1:
		for dev in dev_mgr_xml.devices.itervalues():
			print dev
			dev_mgr_mongo.add_new_device(dev)
	
	if actions & 2:
		for dev in dev_mgr_mongo.devices.itervalues():
			print dev
		
	keys1 = set(dev_mgr_xml.devices)
	keys2 = set(dev_mgr_mongo.devices)
	if keys1-keys2 or keys2-keys1:
		print 'keys are different!!\n\n', keys1, '\n', keys2
		sys.exit(1)
	else:
		print 'keys OK'
		
	for k in keys1:
		dev1 = dev_mgr_xml.devices[k]
		dev2 = dev_mgr_mongo.devices[k]
		if str(dev1) != str(dev2):
			print 'devices are different!', dev1, dev2
