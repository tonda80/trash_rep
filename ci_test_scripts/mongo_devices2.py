# coding=utf8

# mongo devices
# https://app.asana.com/0/354325212918017/928357564208907/f

import os
import sys
import argparse

sys.path.append('/home/ant/job/rep-s/ci_utils')
from ci.deployment.device import Device
from ci.deployment.device_manager import DeviceManager, XMLDeviceManager

def parse_args(args=None):
	parser = argparse.ArgumentParser()
	parser.add_argument('--my_db', action='store_true')
	parser.add_argument('--export_xml', action='store_true', help='Export devices.xml')
	parser.add_argument('--read_db', action='store_true')
	parser.add_argument('--compare', action='store_true')
	parser.add_argument('--get_by', help='eval(dict) e.g. {"platform":"ios", "disabled":False}')
	return parser.parse_args(args)

xml_mgr = None
def get_xml_mgr():
	global xml_mgr
	if not xml_mgr:
		xml_mgr = XMLDeviceManager('/home/ant/job/rep-s/ci_utils/devices.xml')
	return xml_mgr

if __name__ == '__main__':
	args = parse_args()

	if args.my_db:
		os.environ['MONGO_HOST'] = r'mongodb://cluster0-shard-00-00-adg3l.mongodb.net:27017,cluster0-shard-00-01-adg3l.mongodb.net:27017,cluster0-shard-00-02-adg3l.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true'
		os.environ['MONGO_USER'] = 'mongo_guru'
		os.environ['MONGO_PASSWORD'] = 'uehfghk'
	else:
		os.environ['MONGO_HOST'] = r'mongo.service.atf01.consul'
		os.environ['MONGO_USER'] = 'berezin-a'
		os.environ['MONGO_PASSWORD'] = 'dQCx3azEv2BKszSq8K'
		# export MONGO_HOST='mongo.service.atf01.consul'; export MONGO_USER='berezin-a';export MONGO_PASSWORD='dQCx3azEv2BKszSq8K'
	dev_mgr_mongo = DeviceManager()

	if args.export_xml:
		for dev in get_xml_mgr().devices.itervalues():
			print dev
			dev_mgr_mongo.add_new_device(dev)

	if args.read_db:
		for dev in dev_mgr_mongo.devices.itervalues():
			print dev

	if args.compare:
		keys1 = set(get_xml_mgr().devices)
		keys2 = set(dev_mgr_mongo.devices)
		if keys1-keys2 or keys2-keys1:
			print 'keys are different!!\n\n', keys1, '\n', keys2
			raise RuntimeError
		for k in keys1:
			dev1 = get_xml_mgr().devices[k]
			dev2 = dev_mgr_mongo.devices[k]
			if str(dev1) != str(dev2):
				print 'devices are different!\n', dev1, '\n', dev2
				raise RuntimeError
		print 'Collections are identical'

	if args.get_by:
		kw = eval(args.get_by)
		cnt = 0
		for d in dev_mgr_mongo.get_devices_by(**kw):
			print d
			cnt += 1
		print 'total', cnt
