# coding=utf8


import argparse
import logging
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))
from common import asana        # noqa: E402
from common import google       # noqa: E402


class AppError(RuntimeError):
    pass


class C:    # константы
    pass


class App:
    def __init__(self, args=None):
        self.args = self.parse_args(args)
        self.set_logger()

    def parse_args(self, args):
        parser = argparse.ArgumentParser()
        parser.add_argument('-v', action='store_true', help='Verbose output')
        #parser.add_argument('--log_only', action='store_true', help=', help='No asana actions'')
        return parser.parse_args(args)

    def set_logger(self):
        logging.basicConfig(format='[%(levelname)s][%(name)s] %(message)s')
        self.log = logging.getLogger('app')
        lvl = logging.DEBUG if self.args.v else logging.INFO
        self.log.setLevel(lvl)
        logging.getLogger().setLevel(lvl)

    def main(self):
        pass

if __name__ == '__main__':
    App().main()
