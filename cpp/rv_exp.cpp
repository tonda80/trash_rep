#include "../../common_rep/cpp/common.h"


#define is_lvalue(x)  std::is_lvalue_reference<decltype(x)>{}.value
#define is_rvalue(x)  std::is_rvalue_reference<decltype(x)>{}.value



void ff(std::vector<int>&) {p("i ve got lvalue");}
void ff(std::vector<int>&&) {p("i ve got rvalue");}


int main(int argc, const char** argv) {
    std::vector<int> v1 = {1,2,3,4};
    std::vector<int>& v1l = v1;
    // ERR  std::vector<int>&& v1r = v1;
    //std::vector<int>&& v1r = static_cast<std::vector<int>&&>(v1);
    std::vector<int>&& v1r = std::move(v1);

    pp(&v1, &v1l, &v1r);
    //p_cont(v1);p_cont(v1l);p_cont(v1r);


    //p(is_lvalue(v1));
    //p(is_rvalue(v1));
    //p('-');
    p(is_lvalue(v1l));
    p(is_rvalue(v1l));
    p('-');
    p(is_lvalue(v1r));
    p(is_rvalue(v1r));
    //p('-');
    //p(is_lvalue(std::move(v1)));
    //p(is_rvalue(std::move(v1)));
    //p('-');
    //p(is_lvalue(std::move(v1l)));
    //p(is_rvalue(std::move(v1l)));
    p("----");

    int i = 7;
    int&& ir = i + 4;
    //p(ir);


    ff(v1);
    ff(v1r);
    ff(std::move(v1));

    p("---------");


    std::vector<int> v2;
    // OK v2 = std::move(v1r);  // std::move(v1)
    //v2 = std::forward<decltype(v1r)>(v1r);
    v2 = v1r;
    //v2 = std::move(v1);

    v1.push_back(10);
    v1.push_back(20);

    p_cont(v1, "v1");
    p_cont(v2, "v2");

    return 0;
}
