#include "../../common_rep/cpp/common.h"

#include <locale>
#include <codecvt>
#include <charconv>
#include <random>
#include <iomanip>
#include <any>
#include <fstream>

#include <sys/socket.h>
#include <sys/un.h>

#include <stdlib.h>
#include <unistd.h>


#include <boost/algorithm/string.hpp>


// fmtlib
// export FMT_PATH=/home/ant/strange_code/fmt  &&  g++ -I$FMT_PATH/include $FMT_PATH/src/format.cc _blank.cpp
// g++  -L$FMT_PATH/build -I$FMT_PATH/include  _blank.cpp -lfmt
//#include <fmt/format.h>
//#include <fmt/core.h>
// extra #include <fmt/format-inl.h>


struct M {
    M(int i_ = 0) : i(i_) {pp("M", i);}
    ~M() {pp("~M", i);}
    M(const M& o) : i(o.i) {pp("M&", i);}
    M(M&& o) : i(o.i) {pp("M&&", i);}
    M& operator=(const M& o) {
        i = o.i;
        pp("M=", i);
        return *this;
    }
    M& operator=(M&& o) {
        i = o.i;
        pp("M=&&", i);
        return *this;
    }
    void do_smth() const {pp("M::do_smth", i);}

    int i;
};

struct C {
    C(int i_ = 0) : i(i_) {pp("C", i);}
    ~C() {pp("~C", i);}
    C(const C& o) : i(o.i) {pp("C&", i);}
    C(C&& o) : i(o.i) {
        o.i = -1;
        pp("C&&", i);
    }
    C& operator=(const C& o) {
        i = o.i;
        pp("C=", i);
        return *this;
    }
    C& operator=(C&& o) {
        i = o.i;
        o.i = -1;
        pp("C=&&", i);
        return *this;
    }

    void do_smth() const {pp("C::do_smth", i);}

    int i;

    //M m;
};



struct B {
	B() {
		p("B()");
		f();
	}
	virtual ~B() {
		p("~B()");
		f();
	}
	virtual void f() {p("B::f");}
};

struct D : public B {
	D() {
		p("D()");
		f();
	}
	virtual ~D() {
		p("~D()");
		f();
	}
	virtual void f() {p("D::f");}
};

int rand(int min, int max) {
	return rand()%(max - min) + min;
}


struct Cc {
  int i;
};


#include <boost/algorithm/string/join.hpp>
#include <boost/range/adaptor/transformed.hpp>



// , uint32_t grid_width, uint32_t grid_height

std::string transform_from_cr_separated_mask(
    std::string cr_separated_string) {
  //if (cr_separated_string.size() != (grid_width + 1) * grid_height) {
  //  throw std::runtime_error("wrong symbols' count in CR-separated string");
  //}
  std::list<int64_t> ones_and_zeros_in_turn;
  if (cr_separated_string[0] == '0') {
    ones_and_zeros_in_turn.push_back(0);
  }
  auto left_it = cr_separated_string.begin();
  auto right_it = cr_separated_string.begin() + 1;
  decltype(ones_and_zeros_in_turn)::difference_type cr_count = 0;
  if (cr_separated_string.size() != 2) {
    while (right_it != cr_separated_string.end()) {
      if (*right_it != *left_it) {
        ones_and_zeros_in_turn.push_back(std::distance(left_it, right_it) - cr_count);
        left_it = right_it;
        cr_count = 0;
      }
      ++right_it;
      if (*right_it == '\n') {
        ++right_it;
        ++cr_count;
      }
    }
  }
  ones_and_zeros_in_turn.push_back(std::distance(left_it, right_it) - cr_count);
  return boost::algorithm::join(
      ones_and_zeros_in_turn | boost::adaptors::transformed([](uint64_t number) {
        return std::to_string(number);
      }),
      ",");
}

std::string mask_to_compact_form(const std::string& mask) {
  if (mask.empty() || mask.find_first_not_of("01") != std::string::npos) {
    throw std::runtime_error("unexpected mask format");
  }
  std::list<int64_t> ones_and_zeros_in_turn;
  if (mask[0] == '0') {
    ones_and_zeros_in_turn.push_back(0);
  }
  auto left_it = mask.begin();
  auto right_it = mask.begin() + 1;
  decltype(ones_and_zeros_in_turn)::difference_type cr_count = 0;
  if (mask.size() != 1) {
    while (right_it != mask.end()) {
      if (*right_it != *left_it) {
        ones_and_zeros_in_turn.push_back(std::distance(left_it, right_it) - cr_count);
        left_it = right_it;
        cr_count = 0;
      }
      ++right_it;
      if (*right_it == '\n') {
        ++right_it;
        ++cr_count;
      }
    }
  }
  ones_and_zeros_in_turn.push_back(std::distance(left_it, right_it) - cr_count);
  return boost::algorithm::join(
      ones_and_zeros_in_turn | boost::adaptors::transformed([](uint64_t number) {
        return std::to_string(number);
      }),
      ",");
}

std::string mask_from_compact_form(const std::string& compact_form) {
  if (compact_form.empty()) {
    throw std::runtime_error("empty mask compact form");
  }
  std::list<std::string> ones_and_zeros_in_turn;
  boost::split(ones_and_zeros_in_turn, compact_form, boost::is_any_of(","));
  bool is_zero = ones_and_zeros_in_turn.front() == std::string("0");
  if (is_zero) {
    ones_and_zeros_in_turn.pop_front();
  }
  for (auto& ones_or_zeros : ones_and_zeros_in_turn) {
	//pp("__deb stoull beg ones_or_zeros =", ones_or_zeros);
    ones_or_zeros = std::string(std::stoul(ones_or_zeros), is_zero ? '0' : '1');
    //pp("__deb stoull ok");
    is_zero = !is_zero;
  }
  return boost::join(ones_and_zeros_in_turn, "");
}

void test_fun() {
    p("hi there");
    return void();
}

const int MODBUS_OPERATION_MAX_TRY = 3;

int main(int argc, const char** argv) {

	int n_try = -1;
    while (++n_try < MODBUS_OPERATION_MAX_TRY) {
        pp("n_try" ,n_try);
    }


    class X {
		int ii = 0;
	public:
		int geti() {return ii;}
	};

	X x;
	p(x.geti());


    test_fun();


    return 0;
}

