#include <iostream>

template <typename T> void p(const T& obj, const char e = '\n') {std::cout << obj << e;}

using namespace std;

struct SimpleStruct
{
	int zero = 0;
	SimpleStruct() {p("SimpleStruct()");}
    std::string first {"first"};
    int second = 2;
    float third = 3.0;    
};

int main(int argc, char** argv)
{
	// тут важен порядок объявления в структуре
	auto [z, f, s, t] = SimpleStruct{};
	std::cout << "first: " << f
			<< ", second: " << s
			<< ", third: " << t << "\n";
	p(z);

	p("--------");
			
	int array[] = {1, 3, 5, 7, 9};
	auto [a1, a2, a3, a4, a5] = array;
	std::cout << "Third element before: "
		<< a3 << "(" << array[2] << ")\n";
	a3 = 4;
	std::cout << "Third element after: "
		<< a3 << "(" << array[2] << ")\n";		
	
    return 0;
}

// g++-7 -std=c++17 struct_bind.cpp
