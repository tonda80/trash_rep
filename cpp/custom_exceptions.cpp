#include "common.h"

struct sqlitedb_error : public std::runtime_error {
	sqlitedb_error(const std::string& msg) : std::runtime_error(msg) {}
};

void rte() {throw std::runtime_error("runtime_error!");}
void cme() {throw sqlitedb_error("sqlitedb_error!");}
void noe() {}

template<typename T>
void run(T f) {
	try {
		f();
		p("OK");
	}
	catch (sqlitedb_error& e) {
		p("caught sqlitedb_error");
	}
	catch (std::runtime_error& e) {
		p("caught runtime_error");
	}
	catch (...) {
		p("caught unexpecetd");
	}
}


int main(int argc, const char** argv)
{

	run([](){});
	run(noe);

	run(rte);
	run(cme);

    return 0;
}
