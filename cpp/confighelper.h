#ifndef CONFIGHELPER_H
#define CONFIGHELPER_H

#include <string>

#include <json/json.h>

#include "mnt_log.h"



namespace mnt {


template <typename T>
void _set_value(T& par, const Json::Value& jvalue) {
    if constexpr(std::is_same<T, std::string>()) {
        par = jvalue.asString();
    }
    else if constexpr(std::is_same<T, bool>()) {
        par = jvalue.asBool();
    }
    else if constexpr(std::is_same<T, int>()) {
        par = jvalue.asInt();
    }
    else if constexpr(std::is_same<T, int64_t>()) {
        par = jvalue.asInt64();
    }
    else if constexpr(std::is_same<T, unsigned int>()) {
        par = jvalue.asUInt();
    }
    else if constexpr(std::is_same<T, uint64_t>()) {
        par = jvalue.asUInt64();
    }
    else if constexpr(std::is_same<T, double>()) {
        par = jvalue.asDouble();
    }
    else {
        T t; t.some_non_existent_method();   // compile time assert
    }
}

const Json::Value& req_json_child(const Json::Value& parent, const std::string& name) {
    const Json::Value& ret = parent[name];
    if (ret.isNull()) {
        throw std::runtime_error(Fmt::format("Config error, no required parameter '{}'", name));
    }
    return ret;
}

// присвоить обязательное поле
template <typename T>
void set_config(T& par, const Json::Value& parent, const std::string& name) {
    const Json::Value& jvalue = req_json_child(parent, name);
    _set_value(par, jvalue);
    mnt::log(loginfo, "config parameter: '{}' = {}", name, par);
}

// присвоить необязательное поле
template <typename T>
void set_config(T& par, const Json::Value& parent, const std::string& name, const T& default_) {
    const Json::Value& jvalue = parent[name];
    if (jvalue.isNull()) {
        par = default_;
        mnt::log(logdebug, "config parameter: '{}' = {} (default)", name, par);
    }
    else {
        _set_value(par, jvalue);
        mnt::log(loginfo, "config parameter: '{}' = {}", name, par);
    }
}

// присвоить обязательное поле и проверить валидность
template <typename T, typename F1, typename F2>
void set_config_valid(T& par, const Json::Value& parent, const std::string& name,
                F1&& validator, F2&& fail_action) {
    set_config(par, parent, name);
    if (!validator(par)) {
        fail_action(par);
    }
}

void set_config_nonempty_string(std::string& par, const Json::Value& parent, const std::string& name) {
    set_config_valid(par, parent, name,
        [](const std::string& s) {
            return !s.empty();
        },
        [&name](const std::string&) {
            throw std::runtime_error(Fmt::format("Config error, parameter '{}' cannot be empty", name));
        }
    );
}

} // namespace mnt {


#endif // CONFIGHELPER_H
