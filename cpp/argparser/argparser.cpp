#include "argparser.h"
#include <iostream>



std::string Argparser::process_word(const std::string& w, bool last)  {
    if (is_key(w)) {
        if (m_args.find(w) == m_args.end()) {
            return "unknown argument: " + w;
        }

        if (cur_arg) {
            auto err = try_set_curr_flag();
            if (!err.empty()) return err;
        }

        cur_arg = &m_args.at(w);

        if (last) {
            return try_set_curr_flag();
        }

        return "";
    }

    // not key
    if (!cur_arg) {
        return "parse error, unexpected: " + w;
    }

    return set_curr_arg(w);
}

std::string Argparser::try_set_curr_flag() {
    if (!cur_arg) {
        throw std::logic_error("Argparser implementation error (1)");
    }

    if (cur_arg->type != ArgType::flag) {
        return "parse error, argument: " + cur_arg->name;
    }
    return set_curr_arg(FLAG_TRUE);
}

std::string Argparser::set_curr_arg(const std::string& v) {
    if (!cur_arg) throw std::logic_error("Argparser implementation error (2)");

    if (cur_arg->set) {
        return "parse error, repeated argument: " + cur_arg->name;
    }

    cur_arg->value = v;
    cur_arg->set = true;
    cur_arg = nullptr;

    return "";
}

bool Argparser::get_flag(const std::string& key) const {
    auto arg = get_arg(key);
    if (arg.type != ArgType::flag) {
        throw std::logic_error("argument is not flag: " + key);
    }

    return arg.value == FLAG_TRUE;
}

std::string Argparser::get_str(const std::string& key) const {
    auto arg = get_arg(key);
    if (arg.type != ArgType::str) {
        throw std::logic_error("argument is not string: " + key);
    }

    return arg.value;
}

int Argparser::get_int(const std::string& key, int def) const {
    auto arg = get_arg(key);

    std::size_t pos = 0;
    int ret = 0;
    try {
        ret = std::stoi(arg.value, &pos, 10);
    }
    catch (...) {}
    if (pos == 0 || pos != arg.value.size()) {
        return def;
    }

    return ret;
}

