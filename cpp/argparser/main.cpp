// копипаста из luxoft's vwac_reps/DiagMaster4Edge/src/swmc_mock
// cp ~/vwac_reps/DiagMaster4Edge/src/swmc_mock/argparser.* ~/trash_rep/cpp/argparser/

#include <iostream>
#include <vector>
#include <algorithm>

#include "argparser.h"


class AppArgParser : public Argparser {
protected:
    virtual std::vector<Arg> app_args() override {
        return {
            Arg{.name = "-z", .comment = "zzzzzzz", .req = true},
            Arg{.name = "--q", .type = ArgType::flag},
            Arg{.name = "--foo", .type = ArgType::flag, .comment = "foo argument"},
            Arg{.name = "--bar", .value = "some default", .comment = "bar argument"},
        };
    }
};



int main(int argc, char* argv[]) {
    AppArgParser args;
    if (!args.parse(argc, argv, std::cerr)) {
        return -1;
    }

    args.deb_state(std::cout);
    std::cout << args.get_flag("--foo") << std::endl;
    std::cout << args.get_flag("--q") << std::endl;
    std::cout << args.get_str("--bar") << std::endl;
    std::cout << args.get_str("-z") << std::endl;
    //std::cout << args.get_str("--q") << std::endl;
    //std::cout << args.get_flag("-z") << std::endl;
    //std::cout << args.get_flag("-wwww") << std::endl;


    return 0;
}
