#ifndef ARGPARSER_H
#define ARGPARSER_H

#include <string>
#include <ostream>
#include <vector>
#include <map>


class Argparser
{
protected:
    enum struct ArgType {
        str,
        flag,
        // num?
    };

    struct Arg {
        std::string name;
        ArgType type = ArgType::str;
        std::string value;
        std::string comment;
        bool req = false;
        bool set = false;
        // validator
    };

    virtual std::vector<Arg> app_args() = 0;

    std::map<std::string, Arg> m_args = {
        {"-h", Arg{.name = "-h", .type = ArgType::flag, .comment = "Show help"}}
    };

    std::string arg0;

private:
    bool is_key(const std::string& s) const {
        return s.size() > 0 && s[0] == '-';
    }

    std::string process_word(const std::string& w, bool last);
    std::string set_curr_arg(const std::string& v);
    std::string try_set_curr_flag();
    Arg* cur_arg = nullptr;

    const Arg& get_arg(const std::string& key) const {
        if (m_args.find(key) == m_args.end()) {
            throw std::logic_error("argument is not defined: " + key);
        }
        return m_args.at(key);
    }

    template<typename T>
    void parse_error(const std::string& err, std::basic_ostream<T>& ss) {
        ss << std::endl << err << std::endl << std::endl;
        usage(ss);
    }

    const std::string FLAG_TRUE = "true";

public:
    template<typename T>
    bool parse(int argc, char* argv[], std::basic_ostream<T>& ss) {
        for (const auto& a : app_args()) {
            if (a.name.empty()) {
                throw std::logic_error("bad implementation of app_args: empty arg name");
            }
            if (!is_key(a.name)) {
                throw std::logic_error("bad implementation of app_args: invalid arg name: " + a.name);
            }
            if (m_args.find(a.name) != m_args.end()) {
                throw std::logic_error("bad implementation of app_args: repeated arg name: " + a.name);
            }

            m_args.emplace(a.name, a);
        }

        arg0 = argv[0];

        for (int i = 1; i < argc; ++i) {
            std::string err = process_word(argv[i], i == argc - 1);
            if (!err.empty()) {
                parse_error(err, ss);
                return false;
            }
        }

        for (const auto& it : m_args) {
            if (it.second.req && !it.second.set) {
                parse_error("Not set requirement argument: " + it.second.name, ss);
                return false;
            }
        }

        return true;
    }

    template<typename T>
    void usage(std::basic_ostream<T>& ss) {
        ss << "Usage:" << std::endl << "./swmc_mock OPTIONS" << std::endl;

        for (const auto& it : m_args) {
            auto& a = it.second;

            std::string type;
            if (a.type == ArgType::str) type = " STR ";

            ss << "  " << a.name << type <<
                  (a.req ? " [required] " : "") <<
                  (!a.comment.empty() ? "\n\t" + a.comment : "") <<
                  std::endl;
        }
    }

    template<typename T>
    void deb_state(std::basic_ostream<T>& ss) {
        for (const auto& it : m_args) {
            auto& a = it.second;

            ss << "  " << a.name << "=" << a.value << std::endl;
        }
    }

    bool get_flag(const std::string& key) const;

    std::string get_str(const std::string& key) const;

    int get_int(const std::string& key, int def) const;
};

#endif // ARGPARSER_H
