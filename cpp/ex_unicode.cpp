#include <iostream>
#include <string>
#include <codecvt>
#include <cassert>
#include <locale>
 
int main()
// https://en.cppreference.com/w/cpp/locale/codecvt_utf8_utf16
/*
{
    std::string u8 = u8"z\u00df\u6c34\U0001f34c";
    std::u16string u16 = u"z\u00df\u6c34\U0001f34c";
 
    // UTF-8 to UTF-16/char16_t
    std::u16string u16_conv = std::wstring_convert<
        std::codecvt_utf8_utf16<char16_t>, char16_t>{}.from_bytes(u8);
    assert(u16 == u16_conv);
    std::cout << "UTF-8 to UTF-16 conversion produced "
              << u16_conv.size() << " code units:\n";
    for (char16_t c : u16_conv)
        std::cout << std::hex << std::showbase << c << ' ';
 
    // UTF-16/char16_t to UTF-8
    std::string u8_conv = std::wstring_convert<
        std::codecvt_utf8_utf16<char16_t>, char16_t>{}.to_bytes(u16);
    assert(u8 == u8_conv);
    std::cout << "\nUTF-16 to UTF-8 conversion produced "
              << std::dec << u8_conv.size() << " bytes:\n" << std::hex;
    for (char c : u8_conv)
        std::cout << +(unsigned char)c << ' ';
}
*/

// "😄"
// '\x3d\xd8\x04\xde'.decode('utf16')
// '\xf0\x9f\x98\x84'.decode('utf8')
{
    std::string str = "😄";
    
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    std::wstring wideStr = converter.from_bytes(str);
    
    std::cout << str << std::endl;
    for (char c : str)
        std::cout << std::hex << +(unsigned char)c << ' ';
    std::cout << std::dec<< std::endl;
    
    char *p = (char*)wideStr.c_str();
    for (int i = 0; i<8; ++i)
	std::cout << std::hex << +(unsigned char)*(p+i) << ' ';
    std::cout << std::dec<< std::endl;
    
    
    std::wstring_convert<std::codecvt_utf8<wchar_t>> cvt;
    //std::u32string
    std::wstring utf32 = cvt.from_bytes(str);
    p = (char*)utf32.c_str();
    for (int i = 0; i<8; ++i)
	std::cout << std::hex << +(unsigned char)*(p+i) << ' ';
    std::cout << std::dec<< std::endl;
    
    
    
    //std::cout << wideStr << std::endl;
    
}
