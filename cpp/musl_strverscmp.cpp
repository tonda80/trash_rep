#include <iostream>

#include <stdexcept>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <tuple>
#include <sstream>
#include <unordered_map>

#include <algorithm>
#include <ctime>
#include <memory>
#include <functional>
#include <thread>
#include <chrono>
#include <atomic>

void print(const char* s) {while (*s){if (*s == '{' && *++s == '}')throw std::runtime_error("print: missing arguments");std::cout << *s++;}}
template<typename T, typename... Args>
void print(const char* s, const T& value, const Args&... args){while (*s){if (*s == '{' && *++s == '}'){std::cout << value;return print(++s, args...);}std::cout << *s++;}throw std::runtime_error("print: extra arguments");}

template <typename T> void p(const T& obj, const char e = '\n') {std::cout << obj << e;}

using namespace std;

int musl_strverscmp(const char *l, const char *r)
	{
		int haszero = 1;
		while (*l == *r) {
			if (!*l) return 0;
			
			if (*l == '0') {
				if (haszero == 1) {
					haszero = 0;
				}
			}
			else if (isdigit(*l)) {
				if (haszero == 1) {
					haszero = 2;
				}
			}
			else {
				haszero = 1;
			}
			l++; r++;
		}
		if (haszero == 1 && (*l == '0' || *r == '0')) {
			haszero = 0;
		}
		if ((isdigit(*l) && isdigit(*r)) && haszero) {
			size_t lenl = 0, lenr = 0;
			while (isdigit(l[lenl])) lenl++;
			while (isdigit(r[lenr])) lenr++;
			if (lenl == lenr) {
				return (*l - *r);
			}
			else if (lenl>lenr) {
				return 1;
			}
			else {
				return -1;
			}
		}
		else {
			return (*l - *r);
		}
	}
	
	std::string FormatVersionString(const std::string& ver)
	{
		if (ver.size() && ver.find('.') == std::string::npos) {

			std::stringstream ss;
			if (ver.size() == 1) {
				ss << "0.0."; //
			}
			else if (ver.size() == 2) {
				ss << "0.";
			}

			for (size_t i = 0; i < ver.size(); i++) {
				if (i != 0 && ver.size() - i < 3) {  // add dots on last to digits (xxxx.y.z)
					ss << '.';
				}
				ss << ver[i];
			}
			return ss.str();
		}
		return ver;
	}

int main(int argc, char** argv)
{
	string s, v1, v2;
	
	p(FormatVersionString("9"));
	p("--------");
	p(musl_strverscmp("0.1.1", "00.1.1"));
	p(musl_strverscmp("0.1.1", "0.01.1"));
	p("--------");
	while (1) {
		p(">", ' ');
		cin >> s;
		size_t n = s.find(';');
		if (n != string::npos) {
			v1 = s.substr(0, n);
			v2 = s.substr(n+1);
			p(v1+" - "+v2+" == "+std::to_string(musl_strverscmp(v1.c_str(), v2.c_str())));	cin.ignore(256, '\n');			
		}
	}
    return 0;
}
