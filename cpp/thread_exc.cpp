#include <iostream>

#include <stdexcept>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <tuple>
#include <sstream>
#include <fstream>
#include <unordered_map>

#include <algorithm>
#include <ctime>
#include <memory>
#include <functional>
#include <thread>
#include <chrono>
#include <atomic>
#include <regex>

void print(const char* s) {while (*s){if (*s == '{' && *++s == '}')throw std::runtime_error("print: missing arguments");std::cout << *s++;}}
template<typename T, typename... Args>
void print(const char* s, const T& value, const Args&... args){while (*s){if (*s == '{' && *++s == '}'){std::cout << value;return print(++s, args...);}std::cout << *s++;}throw std::runtime_error("print: extra arguments");}

template <typename T> void p(const T& obj, const char e = '\n') {std::cout << obj << e;}

using namespace std;

void do_something()
{
	p("i gonna do something");
	throw(std::runtime_error("something goes wrong"));
	p("i've done something");
}

void do_something2() try
{
	p("i gonna do somethig 2");
	throw(std::runtime_error("something goes wrong 2"));
	p("i've done something 2");
}
	catch (const std::exception& e) {
	p(string("\t\t> ")+e.what()+" \nsorry my bad!..");
}

int main(int argc, char** argv)
{
	try {
		std::thread(do_something2).detach();
		//do_something();
	}
	catch (const std::exception& e) {
		p(string("\t\t> ")+e.what()+" \nnevermind..");
	}
	
	std::this_thread::sleep_for(std::chrono::seconds(3));
	
    return 0;
}
