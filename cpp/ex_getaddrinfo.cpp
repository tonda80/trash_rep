#include "../../common_rep/cpp/common.h"


#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>




std::vector<std::string> split(const std::string& src, const std::string& delim) {
    std::vector<std::string> res;

    size_t delim_len = delim.length();
    size_t pos_b = 0; 	// position of begin of search
    while (1) {
        size_t pos_d = src.find(delim, pos_b);
        if (pos_d == std::string::npos) {
            res.push_back(src.substr(pos_b));
            break;
        }
        res.push_back(src.substr(pos_b, pos_d - pos_b));
        pos_b = pos_d + delim_len;
    }
    return res;
}


#define I2S(x) case x: return #x;

const char* family_str(int c) {
	switch (c) {
		I2S(AF_INET)
		I2S(AF_INET6)
		I2S(AF_UNSPEC)
	}
	return "__UNKNOWN__";
}

const char* socktype(int c) {
	switch (c) {
		I2S(SOCK_STREAM)
		I2S(SOCK_DGRAM)
		I2S(SOCK_RAW)
	}
	return "__UNKNOWN__";
}


#define V4A(x) x & 0xff, (x & 0xff00) >> 8, (x & 0xff0000) >> 16, (x & 0xff000000) >> 24
#define V46P(x) x & 0xff, (x & 0xff00) >> 8

void print_sockaddr_info(const sockaddr* s) {
	if (s->sa_family ==  AF_INET) {
		const struct sockaddr_in* s2 = reinterpret_cast<const struct sockaddr_in*>(s);
		pp("v4 port", V46P(s2->sin_port));
		pp("v4 addr", V4A(s2->sin_addr.s_addr));
	}
	else if (s->sa_family ==  AF_INET6) {
		const struct sockaddr_in6* s2 = reinterpret_cast<const struct sockaddr_in6*>(s);
		pp("v6 port", V46P(s2->sin6_port));
		//pp("addr", s2->sin_addr.s_addr);
	}
	else {
		pp("unknown sockaddr family", s->sa_family);
	}
}


void getaddrinfo_demo(const std::string& in_str) {
	auto vv = split(in_str, ";");
	const char* node = vv[0].c_str();
	const char* service = vv.size() > 1 ? vv[1].c_str() : nullptr;
	pp("getaddrinfo input:", node, service ? service : "");

	addrinfo* addr = nullptr;
	int res = getaddrinfo(node, service, nullptr, &addr);
	if (res != 0) {
		pp("getaddrinfo error", res, gai_strerror(res));
		return;
	}

	int cnt = 0;
	addrinfo* curr = addr;
	while (curr) {
		pp("flags", curr->ai_flags);
		pp("ai_family", curr->ai_family, family_str(curr->ai_family));
		pp("ai_socktype", curr->ai_socktype, socktype(curr->ai_socktype));
		pp("protocol", curr->ai_protocol);
		pp("canonname", curr->ai_canonname ? curr->ai_canonname : "");
		print_sockaddr_info(curr->ai_addr);
		p("---");

		curr = curr->ai_next;
		++cnt;
	}

	pp("Total", cnt);

	freeaddrinfo(addr);
}


int main(int argc, const char** argv) {


	while (1) {
		std::string s;
		std::cout << "> ";
		std::cin >> s;

		getaddrinfo_demo(s);
	}

	return 0;
}
