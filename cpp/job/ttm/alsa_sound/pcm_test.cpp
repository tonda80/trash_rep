// g++ pcm_test.cpp -lpthread -lasound -o pcm_test.out
// apt install libasound2-dev


#include <iostream>
#include <thread>
#include <atomic>
#include <chrono>
#include <termios.h>
#include <csignal>
#include <poll.h>
#include <cstring>
#include <cmath>
#include <fstream>

#include <alsa/asoundlib.h>


static const char* device = "default";
static const auto pcm_format = SND_PCM_FORMAT_S16;
static const int pcm_format_size = 2;
static const unsigned int channels = 1;
static const unsigned int rate = 8000;

// play -t raw -r 8k -e signed -b 16 -c 1 rec.pcm
// ffmpeg -f s16le -ar 8000 -ac 1 -i rec.pcm  rec.aac
// ffmpeg -f s16le -ar 8000 -ac 1 -i rec.pcm -acodec libmp3lame rec.mp3
// ffmpeg -f s16le -ar 8000 -ac 1 -i rec_c.pcm -f s16le -ar 8000 -ac 1  -i rec_p.pcm -filter_complex amix=inputs=2:duration=longest rec.aac

static volatile std::sig_atomic_t work_flag;


snd_pcm_t* pcm_capture_handle()
{
    snd_pcm_t* handle;
    int err;

    if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_CAPTURE, 0)) < 0) {
        std::cerr << "[pcm_capture_handler] snd_pcm_open error: " << snd_strerror(err) << " " << err << std::endl;
        return nullptr;
    }
    if ((err = snd_pcm_set_params(handle,
                                  pcm_format,
                                  SND_PCM_ACCESS_RW_INTERLEAVED,	//SND_PCM_ACCESS_RW_INTERLEAVED
                                  channels,
                                  rate,
                                  1,            // allow resampling
                                  500000)) < 0) {
        std::cerr << "[pcm_capture_handler] snd_pcm_set_params error: " << snd_strerror(err) << " " << err << std::endl;
        return nullptr;
    }

    std::cout << "[pcm_capture_handler] OK" << std::endl;
    return handle;
}

snd_pcm_t* pcm_playback_handle(void)
{
    snd_pcm_t* handle;
    int err;

    if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
        std::cerr << "[pcm_playback_handler] snd_pcm_open error: " << snd_strerror(err) << " " << err << std::endl;
        return nullptr;
    }
    if ((err = snd_pcm_set_params(handle,
                                  pcm_format,
                                  SND_PCM_ACCESS_RW_INTERLEAVED,
                                  channels,
                                  rate,
                                  1,        // allow resampling
                                  500000)) < 0) {
        std::cerr << "[pcm_playback_handler] snd_pcm_set_params error: " << snd_strerror(err) << " " << err << std::endl;
        return nullptr;
    }

    std::cout << "[pcm_playback_handler] OK" << std::endl;
    return handle;
}


void signal_handler(int/*signal*/) {
    work_flag = 0;
}

void print_timestamp(const std::string& pref)
{
    auto check_point = std::chrono::steady_clock::now();
    auto tstamp_ms = std::chrono::duration_cast<std::chrono::milliseconds>(check_point.time_since_epoch()).count();
    static auto old_tstamp_ms = tstamp_ms;
    std::cout << pref << " " << tstamp_ms - old_tstamp_ms << std::endl;
    old_tstamp_ms = tstamp_ms;
}


double average_s16(const int16_t* buf, size_t size) {
    double sum = 0;
    for (size_t i=0; i<size; ++i) {
        sum += std::abs(buf[i]);
        //std::cout << std::hex << buf[i] << " ";
    }
    return sum/size;
}


class PcmFile {
	std::ofstream stream;

public:
	PcmFile(const std::string& path) :
		stream(path.c_str(), std::fstream::out | std::fstream::binary)
	{
		if (!stream.good()) {
			throw std::runtime_error("can't open "+ path+std::to_string(stream.is_open()));
		}
		std::cout << "[PcmFile] opened " << path << std::endl;
	}

	void write(const char* s, size_t n) {
		stream.write(s, n);
	}

	~PcmFile() {
		stream.close();
		std::cout << "[PcmFile] closed" << std::endl;
	}

};


void work_thread_function(snd_pcm_t* playback_pcm_handle, snd_pcm_t* capture_pcm_handle)
{
    const int buf_size = 1600;
    char buf[buf_size];

    PcmFile pcm_file("rec.pcm");

    snd_pcm_start(capture_pcm_handle);
    while (work_flag) {
        auto rd = snd_pcm_readi(capture_pcm_handle, buf, buf_size/pcm_format_size);
        if (rd < 0) {
            auto rec_res = snd_pcm_recover(capture_pcm_handle, rd, 1);
            if (rec_res < 0) {
                std::cerr << "[capture_thread] snd_pcm_recover error: " << std::strerror(rec_res) << " " << rec_res << " " << rd << std::endl;
                break;
            }
            snd_pcm_start(capture_pcm_handle);
            continue;
        }
        else if (rd == 0) {
            continue;
        }
        if (rd != buf_size/pcm_format_size) {
            std::cout << "[capture_thread] unexpectedly readed " << rd << std::endl;
        }

        print_timestamp(std::string("[capture_thread] readed "+std::to_string(rd)));
        continue;

        //std::cout << "__deb [capture_thread] readed " << rd << ", average =" << average_s16(reinterpret_cast<int16_t*>(buf), buf_size/pcm_format_size) << std::endl;
        pcm_file.write(buf, buf_size);


		/*
        auto need_wr = rd*pcm_format_size;
        auto wr = write(dev_fd, buf, need_wr);
        fsync(dev_fd);
        //std::cout << "[capture_thread] tried to write " << need_wr << std::endl;

        if (wr != need_wr) {
            auto res = snd_pcm_reset(capture_pcm_handle);
            std::cout << "[capture_thread] unexpectedly written: " << wr << "!=" << need_wr << ", " << res << std::endl;
        }
        */

    }   //while (work_flag)

    std::cout << "[capture_thread] finished" << std::endl;
}

/*
void playback_thread(int dev_fd, snd_pcm_t* handle)
{
    const int buf_size = 640;
    char buf[buf_size];

    int unmute_counter = 0;

    struct pollfd fds;
    fds.fd = dev_fd;
    fds.events = POLLIN;
    while (work_flag) {
        int res = poll(&fds, 1, 250);
        if (res == -1) {
            std::cerr << "[playback_thread] poll error: " << std::strerror(res) << " " << res << std::endl;
            dev_ready.store(false);
            break;
        }
        else if (res == 0) {  // timeout
            dev_ready.store(false);
        }
        else if ( res > 0) {
            if (fds.revents & POLL_IN ) {
                fds.revents = 0;

                dev_ready.store(true);

                auto rd = read(dev_fd, buf, buf_size);
                //std::cout << "[playback_thread] readed bytes " << rd << std::endl;
                if (rd > 0) {
                    double aver = average_s16(reinterpret_cast<int16_t*>(buf), rd/pcm_format_size);
                    bool need_muting = mute_level > 0 && aver > mute_level;
                    //print_timestamp("[playback_thread] after read: " + std::to_string(aver) + " " + std::to_string(need_muting));
                    if (need_muting) {
                        mute_mic.store(true);
                        unmute_counter = unmute_delay;    // 15 => ~300мс
                    }
                    else if (unmute_counter > 0) {
                        --unmute_counter;
                    }
                    else {
                        mute_mic.store(false);
                    }

                    auto wr = snd_pcm_writei(handle, buf, rd/pcm_format_size);
                    if (wr < 0) {
                        auto rec_res = snd_pcm_recover(handle, wr, 1);
                        if (rec_res < 0) {
                            std::cerr << "[playback_thread] snd_pcm_recover error: " << snd_strerror(rec_res) << " " << rec_res << " " << wr << std::endl;
                            break;
                        }
                    }
                }
            }
        }
    }
    std::cout << "[playback_thread] finished" << std::endl;
}
*/


int main(int argc, const char** argv) {

    snd_pcm_t* capture_pcm = pcm_capture_handle();
    if (!capture_pcm) {
        std::cerr << "cannot get capture handler\n";
        return -1;
    }
    snd_pcm_t* playback_pcm = pcm_playback_handle();
    if (!playback_pcm) {
        std::cerr << "cannot get playback handler\n";
        return -1;
    }

    work_flag = 1;
    std::thread wrk_thread = std::thread(work_thread_function, playback_pcm, capture_pcm);

    std::signal(SIGINT, signal_handler);

    wrk_thread.join();

    snd_pcm_close(capture_pcm);
    snd_pcm_close(playback_pcm);
    std::cout << "pcm handles closed\n";

    std::cout << "\nexiting" << std::endl;

    return 0;
}
