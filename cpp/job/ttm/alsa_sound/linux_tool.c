// gcc linux_tool.c -pthread -lasound -o linux_tool

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <time.h>
#include <signal.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>

#include <pthread.h>

#include <alsa/asoundlib.h>



#define READ_OK        (-1)
#define READ_ERROR     (-2)
#define READ_NOTHING   (-3)
#define READ_EXCEPT    (-4)

#define DM_OFFSET     (0)
#define NMEA_OFFSET   (1)
#define AT_OFFSET     (2)
#define MODEM_OFFSET  (3)
#define NDIS_OFFSET   (4)   // Should never be used....

#define SHOW_TIME { \
                    struct timeval now; \
                    int lRes __attribute__((unused)) = gettimeofday(&now, NULL); \
                    printf("%d.%06d-", (int)now.tv_sec, (int)now.tv_usec); \
                  }

char gDMInterface[32];
char gNMEAInterface[32];
char gATInterface[32];
char gModemInterface[32];
char gNDISInterface[32];    // Should never be used....
char * gVoiceInterface = gNMEAInterface;

int gCommandFd  = -1;
int gVoiceFd = -1;

#define REFERENCE_FILE "pesq_ref.wav"
#define RECORD_FILE "record.wav"

char * gAudioFile = (char *)REFERENCE_FILE;
char * gRecordFile = (char *)RECORD_FILE;
FILE * gRecord = (FILE *)NULL;
unsigned long gRecordSize = 0L;

long gRefFileSize;
long gRefFileDuration;
unsigned short gRefFileBitsPerSample;
unsigned long gRefFileSamplesPerSec;

#define MODEM_BUFFER_SIZE 1600
#define MODEM_BUFFER_LISTEN_SIZE 640

char gModemBuffer[MODEM_BUFFER_SIZE];
char gModemListenBuffer[MODEM_BUFFER_LISTEN_SIZE];

#define INIT_CMD_LIST_SIZE 7

/* Resetting the modem: AT+QRST=1,0 */

char * initCmdList[INIT_CMD_LIST_SIZE] = {
  "AT+QURCCFG=\"urcport\",\"usbat\"",       // Select URC port as the AT port
  "AT+CRC=1",                               // Allow incoming call URC
  "AT+QINDCFG=\"ring\",1",                  // Allow RING URC
  "AT+QGPSCFG=\"outport\",\"none\"",	    // config GPSOUTPUT PORT to none
  "AT+QGPSCFG=\"outport\"",		    // query
  "AT+QPCMV=1,0",			    // enable audio over usb with NMEA
  "AT+QPCMV?"				    // query
};

pthread_mutex_t gVoicePlayMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t gVoiceListenMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_t gVoicePlayThread = (pthread_t)NULL;
pthread_t gVoiceListenThread = (pthread_t)NULL;


/*
 * Some RIFF and WAVE files format struct to read data from the
 * format fields.
 */

typedef struct {
  char           chunkID[4];
  long           chunkSize;

  short          wFormatTag;
  unsigned short wChannels;
  unsigned long  dwSamplesPerSec;
  unsigned long  dwAvgBytesPerSec;
  unsigned short wBlockAlign;
  unsigned short wBitsPerSample;
} formatChunk;

typedef struct {
  char           chunkID[4];
  long           chunkSize;

  short          wFormatTag;
  unsigned short wChannels;
  unsigned long  dwSamplesPerSec;
  unsigned long  dwAvgBytesPerSec;
  unsigned short wBlockAlign;
  unsigned short wBitsPerSample;
  unsigned short wShouldBeUnused;
} formatChunk2;

typedef struct {
  char           chunkID[4];
  long           chunkSize;
} chunkStart;

typedef struct {
  char           RIFFId[4];
  long           size;
  char           WAVEId[4];
  formatChunk    fmt;
  chunkStart data;
} wavFormat;

typedef struct {
  char           RIFFId[4];
  long           size;
  char           WAVEId[4];
  formatChunk2   fmt;
  chunkStart data;
} wavFormat2;

wavFormat gWavBuffer;
wavFormat * gWavePtr = &gWavBuffer;

int pSendCommand(int fd, char * command)
{
  char cmdBuffer[255];
  int lCmdLen = strlen(command);
  int lRes;

  strcpy(cmdBuffer, command); strcat(cmdBuffer, "\r");
  lCmdLen ++;

  lRes = write(fd, cmdBuffer, lCmdLen);
  if (lRes != lCmdLen) {
    return(-1);
  } else {
    return(0);
  } /* endif */
}

void pPlayFile(int pFd, char * pFilename)
{
  FILE * lFile;
  wavFormat lWavBuffer;
  wavFormat * lWavePtr = &lWavBuffer;
  long lReadSize;
  long lSize = 0L;
  int lRes;
  char * lDataPtr;

  struct timespec lReq, lRem;

  lFile = fopen(pFilename, "r");
  if (lFile == (FILE *)NULL) {
    return;
  } /* endif */

  fread(&lWavBuffer, sizeof(wavFormat), 1, lFile);

  if (strncmp(lWavePtr->RIFFId, "RIFF", 4) == 0) {
    if (strncmp(lWavePtr->WAVEId, "WAVE", 4) == 0) {
      lReadSize = (lWavePtr->fmt.wChannels * (lWavePtr->fmt.wBitsPerSample/8));
      lDataPtr = (char *)lWavePtr->data.chunkID;
    } else {
    } /* endif */
  } /* endif */

    while(!feof(lFile) && (lSize != -1)){

    lReadSize = fread(gModemBuffer, 1, MODEM_BUFFER_SIZE, lFile);
    printf("lReadSize is%ld\n",lReadSize);
    lSize = write(pFd, gModemBuffer, lReadSize);
    printf("lSize is %ld\n",lSize);
    fsync(pFd);

    if ((lReadSize == lSize) && (lSize == MODEM_BUFFER_SIZE)) {
      printf("/");
    } /* endif */
    if ((lReadSize == lSize) && (lSize != MODEM_BUFFER_SIZE)) {
      printf("\\");
    } /* endif */
    if (lReadSize != lSize) {
      SHOW_TIME; printf("<< plonk >>");
      /* We need some extra delay... */
      lReq.tv_sec = 0; lReq.tv_nsec = 50000000L;

      lRes = nanosleep(&lReq, &lRem);
    } /* endif */
    fflush(stdout);

    lReq.tv_sec = 0; lReq.tv_nsec = 100000000L;
    do {
      lRes = nanosleep(&lReq, &lRem);
      lReq.tv_sec = 0;
      lReq.tv_nsec = lRem.tv_nsec;
    } while (lRes != 0);

  } /* endwhile */

  fclose(lFile);

  return;
}

snd_pcm_t* pcm_capture_handle(void)
{
	int err;
	snd_pcm_t *handle;

	static char *device = "default";

	if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_CAPTURE, 0)) < 0) {
		printf("Capture open error: %s\n", snd_strerror(err));
		return NULL;
	}
	if ((err = snd_pcm_set_params(handle,
	                              SND_PCM_FORMAT_S16,
	                              SND_PCM_ACCESS_RW_INTERLEAVED,		// SND_PCM_ACCESS_RW_INTERLEAVED
	                              1,
	                              8000,
	                              1,
	                              500000)) < 0) {	/* 0.5sec */		//500000
		printf("Capture set_params error: %s\n", snd_strerror(err));
		return NULL;
	}

	printf("pcm_capture_handle OK\n");
	return handle;
}


void _sleep(long ns) {
	int res;
	struct timespec t_req, t_rem;

	t_req.tv_sec = 0; t_req.tv_nsec = ns;
	do {
		res = nanosleep(&t_req, &t_rem);
		t_req.tv_sec = 0;
		t_req.tv_nsec = t_rem.tv_nsec;
	} while (res != 0);
}

void play_alsa_mic(int pFd)
{
	char buf[MODEM_BUFFER_SIZE];	// todo
	int err, lSize, lReadSize;

	snd_pcm_t* handle = pcm_capture_handle();

	snd_pcm_start(handle);
	while (1) {		// TODO
		err = snd_pcm_readi(handle, buf, MODEM_BUFFER_SIZE/2);
		if (err < 0) {
			printf("snd_pcm_readi error: %s (%d)\n", snd_strerror(err), err);
			err = snd_pcm_recover(handle, err, 0);
			if (err < 0) {
			printf("snd_pcm_recover error: %s (%d), exiting\n", snd_strerror(err), err);
			break;
			}
		snd_pcm_start(handle);
		}
		else if (err > 0) {
			//printf("readed %d frames\n", err);
			lReadSize = err*2;

			lSize = write(pFd, buf, err*2);
			fsync(pFd);

			if (lReadSize != lSize) {
				//We need some extra delay
				//printf("<< plonk >>");
				_sleep(50000000L);
			}
		}

		//fflush(stdout);

		//_sleep(100000000L);
	}
}

long pWavGetDuration(char * pFilename, long * pSize,
                     unsigned short * pBitsPerSample,
                     unsigned long * pSamplesPerSec)
{
  FILE * lFile;
  wavFormat lWavBuffer;
  wavFormat * lWavePtr = &lWavBuffer;
  wavFormat2 * lWavePtr2 = (wavFormat2 *)&lWavBuffer;
  long lSize;
  short lBitsPerSample;
  long lSamplesPerSec;
  long lReadSize;
  char * lDataPtr;
  long lDuration;

  if (pSize != NULL) (*pSize) = 0;
  if (pBitsPerSample != NULL) (*pBitsPerSample) = 0;
  if (pSamplesPerSec != NULL) (*pSamplesPerSec) = 0;

  lFile = fopen(pFilename, "r");
  if (lFile == (FILE *)NULL) {
    return(-1);
  } /* endif */

  fread(&lWavBuffer, sizeof(wavFormat), 1, lFile);
  fclose(lFile);

  if (strncmp(lWavePtr->RIFFId, "RIFF", 4) == 0) {
    if (strncmp(lWavePtr->WAVEId, "WAVE", 4) == 0) {
      if (strncmp(lWavePtr->fmt.chunkID, "fmt ", 4) == 0) {
        lBitsPerSample = lWavePtr->fmt.wBitsPerSample;
        lSamplesPerSec = lWavePtr->fmt.dwSamplesPerSec;
      } else {
        return(-4);
      } /* endif */
      lReadSize = (lWavePtr->fmt.wChannels * (lWavePtr->fmt.wBitsPerSample/8));
      /*
       * This is weird. Documentation about RIFF/WAV format that can be found
       * at http://ccrma.stanford.edu/courses/422/projects/WaveFormat/
       * states that there is no extra ExtraParamSize at the end of the
       * WAVE chunk if format is PCM, so data chunk should be at offset 36.
       * BUT, it seems that the WAV file created by Voicetronix is inserting
       * two bytes even for PCM. So, we need to handle the two conditions.
       */
      lDataPtr = (char *)lWavePtr->data.chunkID;
      if (lWavePtr2->fmt.wShouldBeUnused == 0) {
        lDataPtr += 2;
      } /* endif */
      if (strncmp(lDataPtr, "data", 4) == 0) {
        lDataPtr += 4;
        lSize = (long)*(long *)(lDataPtr);
      } else {
        return(-5);
      } /* endif */

    } else {
      return(-3);
    } /* endif */
  } else {
    return(-2);
  } /* endif */

  lDuration = (1000 * lSize) / (lSamplesPerSec * lReadSize);

  if (pSize != NULL) (*pSize) = lSize;
if (pBitsPerSample != NULL) (*pBitsPerSample) = lBitsPerSample;
  if (pSamplesPerSec != NULL) (*pSamplesPerSec) = lSamplesPerSec;

  return(lDuration);
}

int pGetErrorCode(char * pBuffer)
{
  int lErrorCode = -1;

  char * lPtr = strstr(pBuffer, "+CME ERROR");

  if (lPtr != (char *)NULL) {
    lPtr += strlen("+CME ERROR:");
    lErrorCode = atoi(lPtr);
  } /* endif */

  return(lErrorCode);
}

int pReadLine(int fd, char * buffer, int pBufLen)
{
  char * lPtr;
  int lRead;
  int lRes;

  fd_set lReadFds, lExceptFds;
  struct timeval lTimeout;

  FD_ZERO(&lReadFds); FD_ZERO(&lExceptFds);
  FD_SET(fd, &lReadFds); FD_SET(fd, &lExceptFds);
  lTimeout.tv_sec = 1;
  lTimeout.tv_usec = 0;

  lRes = select(1+fd, &lReadFds, NULL, &lExceptFds, &lTimeout);
  if (lRes == 0) {
    return(READ_NOTHING);
  } /* endif */

  if ((lRes < 0) || (FD_ISSET(fd, &lExceptFds))) {
    return(READ_EXCEPT);
  } /* endif */

  /* read characters into our string buffer until we get a CR or NL */
  lPtr = buffer;
  while ((lRead = read(fd, lPtr, buffer + pBufLen - lPtr - 1)) > 0) {
    lPtr += lRead;
    if (lPtr[-1] == '\n' || lPtr[-1] == '\r')
      break;
  } /* endwhile */

  if (lRead < 0) {
    return(READ_EXCEPT);
  } /* endif */

  /* nul terminate the string and see if we got an OK response */
  *lPtr = '\0';

  if (strstr(buffer, "\nOK") != (char *)NULL) {
    return (READ_OK);
  } /* endif */

  if ((strstr(buffer, "\nERROR") != (char *)NULL) ||
      (strstr(buffer, "\n+CME ERROR") != (char *)NULL)) {
    return (READ_ERROR);
  } /* endif */

  lRead = strlen(buffer);
  if (lRead > 0) {
    return(lRead);
  } /* endif */

  return(READ_NOTHING);
}

int pReadReply(int fd, char * buffer, int pBufLen)
{
  int lRes = -1;
  int lCount = 0;
  int lErrorCode;
  int i;

   do {
    lRes = pReadLine(fd, buffer, pBufLen);
    if ((lRes != READ_NOTHING) && (lRes != READ_EXCEPT)) {
      for (i=0; i<strlen(buffer); i++) {
        if ((buffer[i] == '\r') || (buffer[i] == '\n')) {
          buffer[i] = ' ';
        } /* endif */
      } /* endfor */
      SHOW_TIME; printf("< %d - [%d] - '%s'\n", lCount, lRes, buffer);
      if (lRes == READ_ERROR) {
        lErrorCode = pGetErrorCode(buffer);
        SHOW_TIME; printf("<            Error code: %d\n", lErrorCode);
      } /* endif */
    } /* endif */
    lCount ++;
  } while ((lRes != READ_OK) && (lRes != READ_ERROR) && (lRes != READ_EXCEPT));

  return(0);
}

void pClosePort(int fd)
{
  close(fd);
}

int pOpenPort(char * port)
{
  struct termios lOptions;

  int lFd = open(port, O_RDWR | O_NDELAY);
  if (lFd != -1) {
    /* read operations are set to blocking according to the VTIME value */
    fcntl(lFd, F_SETFL, 0);

    tcgetattr(lFd, &lOptions);

    /* Set to 115200 */
    cfsetispeed(&lOptions, B115200);
    cfsetospeed(&lOptions, B115200);

    /* set to 8N1 */
    lOptions.c_cflag &= ~PARENB;
    lOptions.c_cflag &= ~CSTOPB;
    lOptions.c_cflag &= ~CSIZE;
    lOptions.c_cflag |= CS8;
    lOptions.c_iflag &= ~(INPCK | BRKINT |PARMRK | ISTRIP | INLCR | IGNCR | ICRNL);
    lOptions.c_iflag |= IGNBRK;

    lOptions.c_iflag &= ~(IXON|IXOFF|IXANY);
    /* set to RAW mode for input */
    lOptions.c_lflag =0;

    /* set to RAW mode for output */
    lOptions.c_oflag=0;

    lOptions.c_cc[VMIN] = 0;
    lOptions.c_cc[VTIME] = 20;

    tcsetattr(lFd, TCSANOW, &lOptions);

    return(lFd);
  } /* endif */

  return(-1);
}

int pOpenVoicePort(char * port)
{
  struct termios lOptions;

  SHOW_TIME; printf("Opening %s\n", port);
  int lFd = open(port, O_RDWR | O_NDELAY);
  SHOW_TIME; printf("%s opened.\n", port);

  if (lFd != -1) {
    /* read operations are set to blocking according to the VTIME value */
    fcntl(lFd, F_SETFL, FNDELAY);

    tcgetattr(lFd, &lOptions);

    /* Set to 115200 */
    cfsetispeed(&lOptions, B115200);
    cfsetospeed(&lOptions, B115200);

    /* set to 8N1 */
    lOptions.c_cflag &= ~PARENB;
    lOptions.c_cflag &= ~CSTOPB;
    lOptions.c_cflag &= ~CSIZE;
    lOptions.c_cflag |= CS8;
    lOptions.c_iflag &= ~(INPCK | BRKINT |PARMRK | ISTRIP | INLCR | IGNCR | ICRNL);
    lOptions.c_iflag |= IGNBRK;
    lOptions.c_iflag &= ~(IXON|IXOFF|IXANY);
    /* set to RAW mode for input */
    //lOptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

    /* set to RAW mode for output */
    //lOptions.c_oflag &= ~OPOST;
    lOptions.c_oflag=0;
    lOptions.c_lflag=0;
    lOptions.c_cc[VMIN] = 0;
    lOptions.c_cc[VTIME] = 20;

    tcsetattr(lFd, TCSANOW, &lOptions);

    return(lFd);
  } /* endif */

  return(-1);
}

static void pVoicePlayThreadCleanup(void * pArg)
{
  int lRes __attribute__((unused));

  lRes = pthread_mutex_unlock(&gVoicePlayMutex);

  return;
}

void * pVoicePlayThread(void * pInterface)
{
  int lRes;

  /* On accepte les cancel */
  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
  pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

  pthread_cleanup_push(pVoicePlayThreadCleanup, NULL);

  lRes = pthread_mutex_lock(&gVoicePlayMutex);
  SHOW_TIME; printf(">>> Voice thread unlocked.\n");


  play_alsa_mic(gVoiceFd);

  //sleep(2);

  //SHOW_TIME; printf("Playing Audio file...");
  //pPlayFile(gVoiceFd, gAudioFile);
  //// sleep(10);
  //SHOW_TIME; printf(" done.\n");

  //lRes = pthread_mutex_unlock(&gVoicePlayMutex);

  SHOW_TIME; printf(">>> Voice thread exiting.\n"); fflush(stdout);

  pthread_cleanup_pop(0);

  pthread_exit((void *)0);

  return(NULL);
}

static void pVoiceListenThreadCleanup(void * pArg)
{
  int lRes __attribute__((unused));

  if (gRecord != (FILE *)NULL) {
    gWavePtr->size = gRecordSize + sizeof(wavFormat) - 8;
    gWavePtr->data.chunkSize = gRecordSize;
    fseek(gRecord, 0L, SEEK_SET);
    fwrite(gWavePtr, sizeof(wavFormat), 1, gRecord); fflush(gRecord);
    fclose(gRecord);
    gRecord = (FILE *)NULL;
  } /* endif */

  lRes = pthread_mutex_unlock(&gVoiceListenMutex);

  return;
}



snd_pcm_t* pcm_playback_handle(void)
{
	int err;
	snd_pcm_t *handle;

	static char *device = "default";

	if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
		printf("Playback open error: %s\n", snd_strerror(err));
		return NULL;
	}
	if ((err = snd_pcm_set_params(handle,
	                              SND_PCM_FORMAT_S16,
	                              SND_PCM_ACCESS_RW_INTERLEAVED,
	                              1,
	                              8000,
	                              1,
	                              500000)) < 0) {	/* 0.5sec */
		printf("Playback open error: %s\n", snd_strerror(err));
		return NULL;
	}

	printf("setup_pcm_device OK\n");
	return handle;
}

void * pVoiceListenThread(void * pInterface)
{
  int lRes;
  int lRead;
  fd_set lReadFds, lExceptFds;
  struct timeval lTimeout;

  memcpy(gWavePtr->RIFFId, "RIFF", 4);
  gWavePtr->size = 0L;
  memcpy(gWavePtr->WAVEId, "WAVE", 4);
  memcpy(gWavePtr->fmt.chunkID, "fmt ", 4);
  gWavePtr->fmt.chunkSize = sizeof(formatChunk) - sizeof(chunkStart);
  gWavePtr->fmt.wFormatTag = 0x0001;
  gWavePtr->fmt.wChannels = 1;
  gWavePtr->fmt.dwSamplesPerSec = 8000L;
  gWavePtr->fmt.dwAvgBytesPerSec = 16000L;
  gWavePtr->fmt.wBlockAlign = 2;
  gWavePtr->fmt.wBitsPerSample = 16;
  memcpy(gWavePtr->data.chunkID, "data", 4);
  gWavePtr->data.chunkSize = 0L;

  gRecord = fopen(gRecordFile, "w");
  if (gRecord != (FILE *)NULL) {
    fwrite(gWavePtr, sizeof(wavFormat), 1, gRecord); fflush(gRecord);
  } /* endif */

  /* On accepte les cancel */
  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
  pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

  pthread_cleanup_push(pVoiceListenThreadCleanup, NULL);

  lRes = pthread_mutex_lock(&gVoiceListenMutex);
  SHOW_TIME; printf(">>> Voice listen thread unlocked.\n");


  snd_pcm_t* pcm_handle = pcm_playback_handle();

  do {
    FD_ZERO(&lReadFds);
    FD_ZERO(&lExceptFds);
    FD_SET(gVoiceFd, &lReadFds);
    FD_SET(gVoiceFd, &lExceptFds);

    lTimeout.tv_sec = 0;
    lTimeout.tv_usec = 250000;

    lRead = -1;
    lRes = select(1 + gVoiceFd, &lReadFds, NULL, &lExceptFds, &lTimeout);
    if (lRes > 0) {
      if (FD_ISSET(gVoiceFd, &lReadFds)) {
       lRead = read(gVoiceFd, gModemListenBuffer, MODEM_BUFFER_LISTEN_SIZE);
       //printf("receve size: %d", lRead);

		if ((lRead > 0)) {
			snd_pcm_sframes_t frames;
			frames = snd_pcm_writei(pcm_handle, gModemListenBuffer, lRead/2);
			//printf("snd_pcm_writei failed: %s\n", snd_strerror(frames));
			if (frames < 0) {
				frames = snd_pcm_recover(pcm_handle, frames, 0);
				if (frames < 0) {
					printf("snd_pcm_writei failed: %s\n", snd_strerror(frames));
					//frames = snd_pcm_recover(handle, frames, 0);
					//if (frames < 0) {
					//printf("snd_pcm_writei failed: %s\n", snd_strerror(frames));
				}
			}
		}

       if (lRead == MODEM_BUFFER_LISTEN_SIZE) {
          printf("@");
        } else {
          printf("# (%d)", lRead);
        }

        if ((lRead > 0) && (gRecord != (FILE *)NULL)) {
          fwrite(gModemListenBuffer, 1, lRead, gRecord);
          gRecordSize += lRead;
        }

      }
      if (FD_ISSET(gVoiceFd, &lExceptFds)) {
        printf("%%");
      }
      fflush(stdout);
    } else {
      if (lRes < 0) {
      }
    }

  } while ((lRead != 0) && (lRes != EBADF));

  SHOW_TIME; printf(">>> Voice listen thread exiting. [%d - %d]\n", lRead, lRes);
  fflush(stdout);

  if (gRecord != (FILE *)NULL) {
    gWavePtr->size = gRecordSize + sizeof(wavFormat) - 8;
    gWavePtr->data.chunkSize = gRecordSize;
    fseek(gRecord, 0L, SEEK_SET);
    fwrite(gWavePtr, sizeof(wavFormat), 1, gRecord); fflush(gRecord);
    fclose(gRecord);
    gRecord = (FILE *)NULL;
  } /* endif */

  lRes = pthread_mutex_unlock(&gVoiceListenMutex);

  pthread_cleanup_pop(0);

  pthread_exit((void *)0);

  return(NULL);
}

void pSignalHandler(int signal)
{
  int lRes = 0;
  char lBuffer[255];

  if (gVoicePlayThread != (pthread_t)NULL) {
    lRes = pthread_cancel(gVoicePlayThread);
  } /* endif */
  if (gVoiceListenThread != (pthread_t)NULL) {
    lRes |= pthread_cancel(gVoiceListenThread);
  } /* endif */
  SHOW_TIME; printf("Thread cancellation status: %d\n", lRes);

  if (gCommandFd != -1) {
    lRes = pSendCommand(gCommandFd, "AT+QPCMV=0");
    SHOW_TIME; printf("> 0 - [%d] - '%s'\n", lRes, "AT+QPCMV=0");
    if (lRes > 0) {
      lRes = pReadReply(gCommandFd, lBuffer, sizeof(lBuffer));
    } /* endif */

    lRes = pSendCommand(gCommandFd, "ATH");
    SHOW_TIME; printf("> 0 - [%d] - '%s'\n", lRes, "ATH");
    if (lRes > 0) {
      lRes = pReadReply(gCommandFd, lBuffer, sizeof(lBuffer));
    } /* endif */

    pClosePort(gCommandFd);
  } /* endif */

  if (gVoiceFd != -1) {
	exit(1);

    SHOW_TIME; printf("signal - waiting for play thread\n"); fflush(stdout);
    lRes = pthread_mutex_lock(&gVoicePlayMutex);
    SHOW_TIME; printf("signal - waiting for listen thread\n"); fflush(stdout);
    lRes |= pthread_mutex_lock(&gVoiceListenMutex);

    if (gRecord != (FILE *)NULL) {
      gWavePtr->size = gRecordSize + sizeof(wavFormat) - 8;
      gWavePtr->data.chunkSize = gRecordSize;
      fseek(gRecord, 0L, SEEK_SET);
      fwrite(gWavePtr, sizeof(wavFormat), 1, gRecord); fflush(gRecord);
      fclose(gRecord);
      gRecord = (FILE *)NULL;
    } /* endif */

    sleep(5);
    SHOW_TIME; printf("signal - closing voice port\n"); fflush(stdout);
    pClosePort(gVoiceFd);
  } /* endif */

  exit(0);
}

int main(int argc, char * argv[])
{
  int lRes;
  char lBuffer[255];
  int lBasePort = 0;

  int i;

  time_t lStartTime, lEndTime;

  sprintf(gDMInterface, "/dev/ttyUSB%d", DM_OFFSET + lBasePort);
  sprintf(gNMEAInterface, "/dev/ttyUSB%d", NMEA_OFFSET + lBasePort);
  sprintf(gATInterface, "/dev/ttyUSB%d", AT_OFFSET + lBasePort);
  sprintf(gModemInterface, "/dev/ttyUSB%d", MODEM_OFFSET + lBasePort);
  sprintf(gNDISInterface, "/dev/ttyUSB%d", NDIS_OFFSET + lBasePort);

  if (argc > 1) {
    lRes = open(argv[1], O_RDONLY);
    if (lRes >= 0) {
      close(lRes);
      gAudioFile = argv[1];
      printf("Switch audio reference to %s\n\n", gAudioFile);
    } else {
      printf("Can't switch audio reference to %s, keeping %s\n\n", argv[1], gAudioFile);
    } /* endif */
  } /* endif */

  printf("%s\n", gDMInterface);
  printf("%s\n", gNMEAInterface);
  printf("%s\n", gATInterface);
  printf("%s\n", gModemInterface);
  printf("%s\n", gNDISInterface);

  gRefFileDuration = pWavGetDuration(gAudioFile, &gRefFileSize,
                                     &gRefFileBitsPerSample,
                                     &gRefFileSamplesPerSec);

  printf("For the file %s, we have :\n", gAudioFile);
  printf("  lSize = %ld\n", gRefFileSize);
  printf("  lBitsPerSample = %d\n", gRefFileBitsPerSample);
  printf("  lSamplesPerSec = %ld\n", gRefFileSamplesPerSec);
  printf("  lDuration = %ldms\n", gRefFileDuration);

  printf("Will skip %ld bytes at start of file\n", sizeof(wavFormat));

  signal(SIGINT, pSignalHandler);


  //gCommandFd = lRes = pOpenPort(gATInterface);

  //if (lRes == -1) {
    //printf("Error opening the serial port.\n");
    //exit(-1);
  //} /* endif */

  ///* empty buffer at the beginning... */
  //lRes = pReadLine(gCommandFd, lBuffer, sizeof(lBuffer));

  //lRes = pSendCommand(gCommandFd, "ATI");
  //SHOW_TIME; printf("> 0 - [%d] - '%s'\n", lRes, "ATI");

  //lRes = pReadReply(gCommandFd, lBuffer, sizeof(lBuffer));

  //for (i=0; i<INIT_CMD_LIST_SIZE; i++) {
    //lRes = pSendCommand(gCommandFd, initCmdList[i]);
    //SHOW_TIME; printf("> 0 - [%d] - '%s'\n", lRes, initCmdList[i]);

    //lRes = pReadReply(gCommandFd, lBuffer, sizeof(lBuffer));
  //} /* endfor */

  lRes = pthread_mutex_lock(&gVoicePlayMutex);
  lRes = pthread_mutex_lock(&gVoiceListenMutex);

  pthread_create(&gVoicePlayThread, NULL, &pVoicePlayThread, (void *)gVoiceInterface);
  pthread_create(&gVoiceListenThread, NULL, &pVoiceListenThread, (void *)gVoiceInterface);

/*
  do {
    lRes = pReadLine(gCommandFd, lBuffer, sizeof(lBuffer));
    if ( (strstr(lBuffer, "+CRING:") != (char *)NULL) &&
         (strstr(lBuffer, "VOICE") != (char *)NULL) ) {
      SHOW_TIME; printf("- Answering incoming call\n");
      lRes = pSendCommand(gCommandFd, "ATA");
      SHOW_TIME; printf("> 0 - [%d] - '%s'\n", lRes, "ATA");
      lRes = READ_ERROR;
    }
  } while (lRes != READ_ERROR);

  sleep(1);
*/

  SHOW_TIME; printf("Opening voice port %s\n", gVoiceInterface);
  gVoiceFd = pOpenVoicePort(gVoiceInterface);

  lRes = pthread_mutex_unlock(&gVoicePlayMutex);
  lRes |= pthread_mutex_unlock(&gVoiceListenMutex);

  SHOW_TIME; printf("- Unlock mutex: %d [0=OK]\n", lRes);

  SHOW_TIME; printf("Now watching events...\n"); fflush(stdout);

  pthread_join(gVoicePlayThread, NULL);
  pthread_join(gVoiceListenThread, NULL);

  return 0;

  lStartTime = time(NULL);
  do {
    printf("-"); fflush(stdout);
    lRes = pReadLine(gCommandFd, lBuffer, sizeof(lBuffer));
    if ((lRes != READ_NOTHING) && (lRes != READ_EXCEPT)) {
      SHOW_TIME; printf("> 0 - [%d] - '%s'\n", lRes, lBuffer);
      if (strstr(lBuffer, "+QPCMV: 0") != (char *)NULL) {
        SHOW_TIME; printf("Stop audio push !\n");
        //case "+QPCMV" Need to communicate with the thread...
      }
      if (strstr(lBuffer, "+QPCMV: 1") != (char *)NULL) {
        SHOW_TIME; printf("Resume audio push !\n");
        // case "+QPCMV: 1" Need to communicate with the thread,continue to send
      }
    }
    printf("+"); fflush(stdout);

    lEndTime = time(NULL);
  } while ((lRes != READ_EXCEPT) && ((lEndTime - lStartTime) < (time_t)(5+(gRefFileDuration/1000))));

  SHOW_TIME; printf("Stopping everything... \n");
  lRes = pthread_cancel(gVoicePlayThread);
  lRes |= pthread_cancel(gVoiceListenThread);

  SHOW_TIME; printf("Thread cancellation status: %d\n", lRes);
  lRes = pSendCommand(gCommandFd, "ATH");
  SHOW_TIME; printf("> 0 - [%d] - '%s'\n", lRes, "ATH");
  lRes = pReadReply(gCommandFd, lBuffer, sizeof(lBuffer));

  lRes = pSendCommand(gCommandFd, "AT+QPCMV=0");
  SHOW_TIME; printf("> 0 - [%d] - '%s'\n", lRes, "AT+QPCMV=0");
  lRes = pReadReply(gCommandFd, lBuffer, sizeof(lBuffer));

  pClosePort(gCommandFd);

  SHOW_TIME; printf("main - waiting for play thread\n"); fflush(stdout);
  lRes = pthread_mutex_lock(&gVoicePlayMutex);
  SHOW_TIME; printf("main - waiting for listen thread\n"); fflush(stdout);
  lRes |= pthread_mutex_lock(&gVoiceListenMutex);

  sleep(5);
  SHOW_TIME; printf("main - closing voice port\n"); fflush(stdout);
  pClosePort(gVoiceFd);

  exit(0);
}

