#include "wips_session.h"

#include <lib/logger/logger.h>
#include <fmt_wrapper.h>


using namespace boost::asio;
using namespace std::placeholders;


std::string ep2str(const boost::asio::ip::tcp::endpoint& ep) {
    return Fmt::format("{}:{}", ep.address().to_string(), ep.port());
}


template <typename T>
struct BufHolder {
    BufHolder(T&& data_) : data(std::move(data_)) {}
    //~BufHolder() {Fmt::print(logdebug, "BufHolder with {} released", data);}
    T data;
};


WipsSession::WipsSession(io_context& ctx, std::string_view ip, uint16_t port) :
            context(ctx) {
    ep = ip::tcp::endpoint(ip::make_address(ip), port);

    connect();
}

void WipsSession::connect() {
    Fmt::print(logdebug, "try connecting to {}", ep2str(ep));
    tcp_socket = std::make_unique<ip::tcp::socket>(context);
    tcp_socket->async_connect(ep, [this](ErrCode err) {
        if (!on_error("connect", err)) {
            Fmt::print(loginfo, "[WipsSession] connected to {}", ep2str(ep));
            send("hello from asio!\n");
            receive();
        }
    });
}

bool WipsSession::on_error(std::string_view where, ErrCode err) {
    if (!err) {
        return false;
    }
    Fmt::print(logerror, "[WipsSession::on_error] {} {}. {} ({}). Reconnecting in {} seconds",
                        where, ep2str(ep), err.message(), err.value(), reconnect_timeout);
    tcp_socket.reset();
    rd_buf.clear();
    auto t = std::make_shared<deadline_timer>(context, boost::posix_time::seconds(reconnect_timeout));
    t->async_wait([this, t](ErrCode err) {
        if (!err) connect();
    });
    return true;
}

void WipsSession::send(std::string&& data) {
    auto b = std::make_shared<BufHolder<std::string>>(std::move(data));
    async_write(*tcp_socket, buffer(b->data), [this, b](ErrCode err, std::size_t) {
        if (!on_error("send", err)) {
            //Fmt::print(logdebug, "[WipsSession] send OK");
        }
    });
}

void WipsSession::receive() {
    async_read_until(*tcp_socket, dynamic_buffer(rd_buf), "z", [this](ErrCode err, std::size_t q) {
        if (!on_error("receive", err)) {
            std::string packet = rd_buf.substr(0, q);
            rd_buf.erase(0, q);
            Fmt::print(logdebug, "[WipsSession] received '{}'", packet);

            receive();
        }
    });
}
