#ifndef WIPSSESSION_H
#define WIPSSESSION_H

#include <boost/asio.hpp>


class WipsSession
{
public:
    WipsSession(boost::asio::io_context& ctx, std::string_view ip, uint16_t port);

private:
    const int reconnect_timeout = 5;

    using ErrCode = const boost::system::error_code&;

    boost::asio::io_context& context;
    boost::asio::ip::tcp::endpoint ep;
    std::unique_ptr<boost::asio::ip::tcp::socket> tcp_socket;
    std::string rd_buf;

    bool on_error(std::string_view, ErrCode);

    void connect();

    void send(std::string&&);
    void receive();

    void login();


};

#endif // WIPSSESSION_H
