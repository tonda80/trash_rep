#ifndef WIPSCLIENT_H
#define WIPSCLIENT_H

#include <vector>
#include <thread>

#include "wips_session.h"


class WipsClient
{
public:
    WipsClient();
    ~WipsClient();

private:
    boost::asio::io_context io_context;
    boost::asio::executor_work_guard<boost::asio::io_context::executor_type> io_work;
    std::vector<WipsSession> sessions;
    void create_sessions();

    std::vector<std::thread> work_threads;
    void create_threads();
};

#endif // WIPSCLIENT_H
