#include "wips_client.h"

#include <lib/logger/logger.h>
#include <fmt_wrapper.h>


WipsClient::WipsClient() : io_work(io_context.get_executor()) {
    create_sessions();
    create_threads();
}

WipsClient::~WipsClient() {
    Fmt::print(loginfo, "[WipsClient] exiting");
    io_context.stop();
    for (auto& t : work_threads) {
        t.join();
    }
}

void WipsClient::create_sessions() {
    // TODO
    sessions.emplace_back(io_context, "127.0.0.1", 56775);
}

void WipsClient::create_threads() {
    // TODO n threads
    work_threads.emplace_back([this]() {
        io_context.run();
    });
}
