// hello modbus with https://github.com/stephane/libmodbus/tree/v3.1.10

// собрали либу но не установили
// исходный текст программы выдал гпт4)

// export MB=/home/berezin/strange_code/libmodbus
// g++ -I $MB/src -o modbus_hw main.cpp $MB/src/.libs/libmodbus.so			-I$MB для инклуда modbus-private.h
// sudo LD_LIBRARY_PATH=$MB/src/.libs/ ./modbus_hw
// или
// g++ -I $MB/src -I$MB -o modbus_hw.out main.cpp $MB/src/.libs/libmodbus.a  && sudo  ./modbus_hw.out

// самый замороченный вариант сборки для чрута
// ./autogen.sh && export CFLAGS='-std=c99' && ./configure --prefix=/tmp/libmodbus_dir --enable-static=yes && make


#include <modbus.h>
//#include <modbus-private.h>		//  если нужны debug сообщения
#include "../../../common_rep/cpp/common.h"

int main() {
	int res = -1;

    modbus_t *ctx = modbus_new_rtu("/dev/ttyACM1", 115200, 'N', 8, 1);		// ttyUSB0
    if (ctx == NULL) {
        p("Unable to create the libmodbus context");
        return -1;
    }
    //ctx->debug = 1;

    if (modbus_connect(ctx) == -1) {
        pp("modbus_connect failed:", modbus_strerror(errno));
        modbus_free(ctx);
        return -1;
    }

    modbus_set_slave(ctx, 4); // Установка адреса устройства Modbus

	res = modbus_set_response_timeout(ctx, 10, 0);
	//pp("modbus_set_response_timeout", res);

	//res = modbus_rtu_set_serial_mode(ctx, MODBUS_RTU_RS485);
	//pp("modbus_rtu_set_serial_mode", res);
	//if (res == -1) pp("modbus_rtu_set_serial_mode failed:", modbus_strerror(errno));

	pp("modbus_rtu_get_serial_mode ", modbus_rtu_get_serial_mode(ctx));
	pp("modbus_rtu_get_rts ", modbus_rtu_get_rts(ctx));
	pp("modbus_rtu_get_rts_delay ", modbus_rtu_get_rts_delay(ctx));

    uint32_t to_sec = 0;
    uint32_t to_usec = 0;
    res = modbus_get_response_timeout(ctx, &to_sec, &to_usec);
    pp("modbus_get_response_timeout", res, to_sec, to_usec);

    // Теперь можно читать/писать данные используя функции libmodbus, например:
    uint16_t buf[128];
    int addr = 0;//0x400;
    const int nb = 8;
    while (addr < 0x1000) {
		res = modbus_read_registers(ctx, addr, nb, buf);
		//pp("modbus_read_registers result:", res);
		if (res != -1) {
			pp("modbus_read_registers OK:", addr, res);
			p('\t', " ");
			for (int i=0; i<nb; ++i) p(buf[i], " ");
			p("");
			addr += nb;
		} else {
			pp(addr, "modbus_read_registers failed:", modbus_strerror(errno));
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}

    modbus_close(ctx);
    modbus_free(ctx);
    return 0;
}

