#pragma once

#include <boost/spirit/include/qi.hpp>
#include <boost/phoenix/function/function.hpp>

namespace rcpt_parser {

namespace spirit = boost::spirit;
namespace qi = spirit::qi;
namespace standard = qi::standard;
namespace phx = boost::phoenix;

} // namespace rcpt_parser
