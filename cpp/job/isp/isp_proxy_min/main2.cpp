#include <string>
#include <iostream>
#include <regex>

#include <locale>
#include <codecvt>


// https://stackoverflow.com/a/201378
std::regex eaddr_re1( R"((?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\]))");

// простой вариант
std::regex eaddr_re2( R"([^\s@]+@[^\s@]+\.[^\s@]+)");

// тот же простой вариант (изначально), но в широких символах
// оно вроде как то работает и так, но есть ощущение что это неправильно
std::wregex eaddr_re3( LR"([^\s@]+@[^\s@]+\.[^\s@]+)");

// eaddr_re3 + защита от хакеров после @
std::wstring allowed_syms = LR"([^\s@()[\]{}<>$&|'"\\]+)";
std::wregex eaddr_re4(allowed_syms+L"@"+allowed_syms+L"\\."+allowed_syms);



bool new_check_address(const std::string &value) {
	bool ret = std::regex_match(value, eaddr_re2);
	return ret;
}

bool new_check_address(const std::wstring &value) {
	bool ret = std::regex_match(value, eaddr_re4);
	return ret;
}


bool is_ascii(const std::string& s) {
    return !std::any_of(s.begin(), s.end(), [](char c) {
        return static_cast<unsigned char>(c) > 127;
    });
}



std::string EmailFormat(const std::string &value) {
	if (!is_ascii(value)) {
		std::cout << "(NO ASCII) ";

		static std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> conv;
		auto wvalue = conv.from_bytes(value);	// std::range_error  если не конвертит
		if (!new_check_address(wvalue)) {
			return "fail wide";
		}
	}
	else {
		if (!new_check_address(value)) {
			return "fail";
		}
	}

    return std::string{};
}


void check_ok(const std::string& addr) {
	auto res = EmailFormat(addr);

	std::cout << addr << "\t\t\t" << (res.empty() ? "OK" : "ERROR: "+res ) << std::endl;
}

void check_fail(const std::string& addr) {
	auto res = EmailFormat(addr);

	std::cout << addr << "\t\t\t" << (!res.empty() ? "OK FAIL: "+res : "ERROR FAIL" ) << std::endl;
}


int main(int argc, char** argv) {

	check_ok("greed@ispsystem.com");
    check_ok("other@example.cloud");
    check_ok("with.dot@example.com");
    check_ok("with+plus@example.com");
    check_ok("with-dash@example.com");
    check_ok("local@local");
    check_ok(R"("it is ok"@example.com)");
    check_ok(R"(" "@example.com)");
    check_ok(R"(ipv6@[IPv6:2001:db8::1])");

    // Invalid emails
    check_fail("@ispsystem.com");                       // No local part
    check_fail("greed@");                               // No domain part
    check_fail("@");                                    // Only @?
    check_fail("abc.example.com");                      // No @
    check_fail("a@b@c@example.com");                    // Multiple @ without quotes
    check_fail(R"(a"b(c)d,e:f;gi[j\\k]l@example.com)"); // Special characters outside quotes
    check_fail(R"(it"is"wrong@example.com)");           // Bad quotes
    check_fail(R"(it is wrong@example.com)");           // Spaces without quotes
    check_fail(R"(double.dots@example..com)");          // Double dots

    // вообще чиним то что ниже
	std::cout << "------\n";

	// по хорошему бы u8, но целевой тест его не понимает
	// no known conversion from 'const char8_t [27]' to 'const std::string'

    check_ok("вася@gmail.com");

    check_ok("ümax.müstermänn@gmail.com");
    check_ok("max.müstermänn@gmail.com");
    check_ok("вася@gmail.com");
    check_ok("вася@письмо.рф");

    check_fail("йцукty");
    check_fail("вася@gm@ail.com");
    check_fail("вася @ gmail.com");
    // и еще хакеров боимся
    std::cout << "------\n";
    check_fail("вася()[]{}<>@gmail.com");
    check_fail("вася$&|@gmail.com");
    check_fail("вася'\"\\@gmail.com");

    check_fail("вася\\@gmail.com");
    check_fail("вася'@gmail.com");
    check_fail("вася\"@gmail.com");



	return 0;
}
