#include <string>
#include <iostream>

#include <boost/spirit/include/qi.hpp>

#include <locale>
#include <codecvt>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpessimizing-move"
#include "external/YandexMail/recipient_parser/parse.hpp"
#pragma GCC diagnostic pop


namespace qi = boost::spirit::qi;


//qi::parse(value.begin(), value.end(), qi::ascii::char_)

bool is_ascii0 (const std::string& s)
{
    return !std::any_of(s.begin(), s.end(), [](char c) {
        return static_cast<unsigned char>(c) > 127;
    });
}

bool is_ascii (const std::string& s)
{
    return !std::any_of(s.begin(), s.end(), [](char c) {
        return static_cast<unsigned char>(c) > 127;
    });
}


std::string EmailFormat(const std::string &value) {
	if (is_ascii(value)) {
		if (!rcpt_parser::parse_address(value)) {
			return "RFC 5322 fail";
		}
	}
    else {
		std::cout << "NOT ASCII " << value << " skip it here" << std::endl;		// в main2 рабочее решение
		return std::string{};

		//static std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> conv;
		static std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> conv;

		//std::u32string w_value = conv.from_bytes(value);
		std::wstring w_value = conv.from_bytes(value);

		std::cout << "__deb conv to wide; " << value << " " << std::endl;	//<< w_value
		for (wchar_t c : w_value) std::cout << c << " ";		// std::hex << std::showbase <<
		std::cout << std::endl;


		auto it = w_value.begin();
		bool res = rcpt_parser::check_address(it, w_value.end());
		std::cout << "Result " << value << ": " << res << std::endl;

	}

    return std::string{};
}


void check_ok(const std::string& addr) {
	auto res = EmailFormat(addr);

	std::cout << addr << "\t\t\t" << (res.empty() ? "OK" : res) << std::endl;
}

int main(int argc, char** argv) {

	check_ok("greed@ispsystem.com");
    check_ok("other@example.cloud");
    check_ok("with.dot@example.com");
    check_ok("with+plus@example.com");
    check_ok("with-dash@example.com");
    check_ok("local@local");
    check_ok(R"("it is ok"@example.com)");
    check_ok(R"(" "@example.com)");
    check_ok(R"(ipv6@[IPv6:2001:db8::1])");
    check_ok("test+folder@gmail.com");

    // проверяем всякое
    std::cout << "------" << std::endl;

    check_ok("\"other ()[]{}<>{}\"@example.cloud");
    check_ok("ot()her@example.cloud");
    check_ok("ot[]her@example.cloud");
    check_ok("ot<>her@example.cloud");
    check_ok("ot{}her@example.cloud");
    check_ok("ot\"\"her@example.cloud");
    check_ok("ot''her@example.cloud");
    check_ok("ot$her@example.cloud");
    check_ok("ot&her@example.cloud");
    check_ok("ot|her@example.cloud");

    // чиним то что ниже

    std::cout << "------" << std::endl;

    check_ok(u8"йцук");
    check_ok(u8"вася@gmail.com");

    check_ok(u8"ümax.müstermänn@gmail.com");
    check_ok(u8"max.müstermänn@gmail.com");
    check_ok(u8"вася@gmail.com");
    check_ok(u8"вася@письмо.рф");

	return 0;
}


using namespace boost::spirit;

// эксперименты с примером из доки

int main2()
{
	//std::wstring s;
	//std::getline(std::wcin, s);

	std::string s0 = u8"йцук";
	//std::string s0 = "ümäx";
	std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> conv;
	std::wstring s = conv.from_bytes(s0);

	for (wchar_t c : s) std::cout << c << " ";		// std::hex << std::showbase <<
	std::cout << std::endl << s.size() << std::endl;


	auto it = s.begin();
	//bool match = qi::phrase_parse(it, s.end(), ascii::digit, ascii::space,
	bool match = qi::parse(it, s.end(), ascii::digit);
	std::wcout << std::boolalpha << match << '\n' << std::dec;
	if (it != s.end()) {
		std::cout << conv.to_bytes(std::wstring{it, s.end()}) << '\n';
	}

    return 0;
}
