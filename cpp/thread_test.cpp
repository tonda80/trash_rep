#include "../../common_rep/cpp/common.h"

constexpr int N_THREADS = 3;

using int_type = uint64_t;

int_type g_i = 0;
std::atomic<int_type> g_ai = 0;

std::mutex g_prt_mtx;

void print_state(int thread_n, int cnt, int_type i_value, int_type ai_value, int err) {
	std::lock_guard<std::mutex> prt_lock(g_prt_mtx);
	pp("thread", thread_n, "| cnt", cnt, "| g_i", i_value, "| g_ai", ai_value, err);
}


void thread_func(int thread_n) {
	int cnt = 0;

	while (1) {
		if (cnt%1000 == 0) {
		//{
			print_state(thread_n, cnt, g_i, g_ai.load(), false);
		}

		// modify
		++g_i;
		g_ai.fetch_add(1);
		++cnt;

		int cnt2 = 0;
		while (1) {
			// wait all threads
			auto ai_value = g_ai.load();
			if (ai_value/N_THREADS >= cnt) {
				break;
			}

			// continue;		// раскомментируй чтобы увидеть расброд и шатание во всей красе

			// check unprotected var
			auto i_value = g_i;		// чтобы видеть где ломаемся

			bool timeout = cnt2++ > 1000000;
			if (cnt2%100000 == 0) {
				print_state(thread_n, cnt, g_i, g_ai.load(), 4);
			}

			if (i_value < ai_value || i_value > ai_value + N_THREADS || timeout) {		// если начали ждать остальных первыми, то можем попасть на такое что все уже заинкрементили и следующий шаг
				print_state(thread_n, cnt, i_value, ai_value, timeout ? 2 : 1);
				print_state(thread_n, cnt, g_i, g_ai.load(), 9);
				throw std::runtime_error("expected error");
			}
			std::this_thread::yield();
		}
	}
}


int main(int argc, const char** argv) {

	pp("Is g_ai lock free?", g_ai.is_lock_free());

	std::vector<std::thread> tt;
	for (int i = 0; i < N_THREADS; ++i) {
		tt.emplace_back(thread_func, i);
	}

	for (int i = 0; i < N_THREADS; ++i) {
		tt.at(i).join();
	}


    return 0;
}
