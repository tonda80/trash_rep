// https://habr.com/ru/post/248153/

#include "tgaimage.h"

const TGAColor white = TGAColor(255, 255, 255, 255);
const TGAColor red   = TGAColor(255, 0,   0,   255);
const TGAColor green   = TGAColor(0, 255,   0,   255);

int main(int argc, char** argv) {
        TGAImage image(100, 100, TGAImage::RGB);
        image.set(52, 41, white);
	image.set(52, 47, red);
	image.set(52, 35, green);
        image.flip_vertically(); // i want to have the origin at the left bottom corner of the image
        image.write_tga_file("output2.tga");
        return 0;
}
