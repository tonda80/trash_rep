#include "common.h"

#include <experimental/filesystem>
// -lstdc++fs

int main(int argc, const char** argv)
{

	std::experimental::filesystem::rename(argv[1], argv[2]);

    return 0;
}
